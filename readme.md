## Frontend CI/CD configuration

# Local start

For local deployment use ```docker-compose up -d``` and (after successful build) run web browser with url: http://localhost:4080

# CI/CD pipeline

Automatic CI/CD pipeline includes steps:
1. Run tests (eslint / jest)
2. Build JS-bundle with webpack
3. Deliver update to server (development for branch 'dev', staging for branch 'staging', production for branch 'main')
