FROM node:16.13-buster-slim

RUN apt update && \
    apt upgrade -y && \
    apt install -y bash curl python3 make g++ && mkdir -p /opt/app && npm install -g npm

WORKDIR /opt/app

ADD . .

RUN npm rebuild node-sass

RUN npm install

RUN npm run build

RUN ls -la build

FROM nginx

ADD default.conf /etc/nginx/conf.d/

COPY --from=0 /opt/app/build /usr/share/nginx/html

