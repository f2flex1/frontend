import { Collapse } from '@material-ui/core';
import styled, { css } from 'styled-components';

interface IAccordionItem {
  page?: 'additionally';
}

export const Accordion = styled.div``;

const additionally = css`
  max-width: 550px;
`;

export const AccordionItem = styled.div<IAccordionItem>`
  max-width: 850px;
  padding-bottom: 20px;
  border-bottom: 1px solid #5b45a2;

  ${({ page }) => (page === 'additionally' ? additionally : null)};

  &:not(:last-child) {
    margin-bottom: 40px;
  }
`;

export const AccordionBtn = styled.button`
  position: relative;
  width: 100%;
  padding: 0 30px 0 0;
  font-weight: bold;
  font-size: 16px;
  line-height: 24px;
  color: ${({ theme }) => theme.accordion.btn};
  text-align: left;
  border: none;
  background-color: transparent;
  cursor: pointer;

  &:before {
    content: '';
    position: absolute;
    top: 5px;
    right: 0;
    width: 15px;
    height: 10px;
    background: ${({ theme }) => theme.accordion.arrow};
    transition: transform 0.3s ease-in-out;
  }

  &.active {
    &:before {
      transform: rotate(-180deg);
    }
  }
`;

export const AccordionCollapse = styled(Collapse)``;

export const AccordionContent = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 45px 0 25px;
  font-weight: normal;
  font-size: 16px;
  line-height: 29px;
  color: ${({ theme }) => theme.accordion.content};

  a {
    font-weight: 700;
    font-size: 16px;
    line-height: 20px;
    color: ${({ theme }) => theme.accordion.content};
    text-decoration: underline;
  }

  span {
    font-weight: 500;
    font-size: 16px;
    line-height: 20px;
    color: ${({ theme }) => theme.accordion.content};
  }
`;
