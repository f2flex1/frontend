import cloneDeep from 'lodash/cloneDeep';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';

import { Tabs, Container, MainContent, Title } from '@/components/elements';
import { TabsContentItem } from '@/components/elements/Tabs/Tabs.Styles';
import { FAQ_QUESTIONS, FAQ_TABS, FAQ_TABS_LIST } from '@/const/app.constants';
import {
  Accordion,
  AccordionBtn,
  AccordionCollapse,
  AccordionContent,
  AccordionItem,
} from '@/pages/Faq/Faq.Styles';
import { SettingsPage } from '@/pages/Settings/Settings.Styles';
import { AppStateType } from '@/store';
import { ThemeType } from '@/store/app/reducers';
import { UserReducerState } from '@/store/user/reducers';

export interface FaqProps {
  user: UserReducerState;
  themeType?: ThemeType;
  location: any;
}

const Faq: React.FC<FaqProps> = (props: FaqProps) => {
  const { themeType, location } = props;

  const [expanded, setExpanded] = useState<any>([]);
  const { t } = useTranslation();

  const query = new URLSearchParams(location.search);
  const queryTab = query.get('tab') || '';

  const [values, setValues] = useState<{ [key: string]: string }>({
    tab: queryTab || FAQ_TABS.GENERAL,
  });

  const onChange = (field: string, value: string) => {
    setValues(prev => ({
      ...prev,
      [field]: value,
    }));
  };

  const setNewMaterailTabs = (id: number) => {
    let newMaterailTabs: number[] = cloneDeep(expanded);

    if (expanded.includes(id)) {
      newMaterailTabs = newMaterailTabs.filter((exp: number) => exp !== id);
    } else {
      newMaterailTabs.push(id);
    }

    setExpanded(newMaterailTabs);
  };

  return (
    <MainContent className={`content-main content-main_bg-3 -${themeType}`}>
      <Container>
        <SettingsPage className={`settings -${themeType}`}>
          <Title className={`settings__title -${themeType}`}>{t('navBar.faq')}</Title>
          <Tabs
            className={`setting-header -${themeType}`}
            tabHeaderClassName={`setting-header__tab -${themeType}`}
            tabsList={FAQ_TABS_LIST.map((item: string) => {
              return {
                key: item,
                name: t(`faq.tabs.${item}`),
              };
            })}
            field="tab"
            activeTab={values.tab}
            onChange={onChange}
          >
            <TabsContentItem
              className={`setting-tab ${values.tab === FAQ_TABS.GENERAL ? 'active' : ''}`}
            >
              <Accordion>
                {FAQ_QUESTIONS.map((item: any, index: number) => (
                  <AccordionItem>
                    <AccordionBtn
                      className={`${expanded.includes(1) ? 'active' : ''}`}
                      onClick={() => setNewMaterailTabs(1)}
                    >
                      {item.question}
                    </AccordionBtn>
                    <AccordionCollapse in={expanded.includes(1)} timeout="auto" unmountOnExit>
                      <AccordionContent key={`${index + 1}`}>{item.answer}</AccordionContent>
                    </AccordionCollapse>
                  </AccordionItem>
                ))}
              </Accordion>
            </TabsContentItem>
          </Tabs>
        </SettingsPage>
      </Container>
    </MainContent>
  );
};

const mapState = (state: AppStateType) => {
  const { app } = state;
  return {
    themeType: app.theme,
  };
};

export default connect(mapState, {})(Faq);
