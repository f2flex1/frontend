import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';

import { PrivateSignupParams } from '@/api';
import history from '@/common/utils/history';
import Loading from '@/components/Loading';
import { PATHS } from '@/const/paths.constants';
import { AppStateType } from '@/store';
import types from '@/store/actionTypes';
import { selectErrorByKey, selectLoadingByKey } from '@/store/loadingsErrors/selectors';
import { privateSignup } from '@/store/user/actions';
import { UserReducerState } from '@/store/user/reducers';

export interface PrivateEntryProps {
  user: UserReducerState;
  loading: boolean;
  error: string | null;
  location: any;
  privateSignup: (payload: PrivateSignupParams | any) => void;
}

const PrivateEntry: React.FC<PrivateEntryProps> = (props: PrivateEntryProps) => {
  const { user, location, privateSignup } = props;
  const [requestSended, setRequestSended] = useState<boolean>(false);

  const query = new URLSearchParams(location.search);
  const id = query.get('id') || '';
  const token = query.get('token') || '';

  useEffect(() => {
    if (!requestSended) {
      setRequestSended(true);
      privateSignup({ id, token });
    }
  }, [requestSended]);

  useEffect(() => {
    if (user.privateSignup) {
      if (user.loginData.phone) {
        history.push(PATHS.LOGIN_BY_NUMBER);
      }

      if (!user.loginData.phone && user.loginData.email) {
        history.push(PATHS.LOGIN_BY_EMAIL);
      }
    }
  }, [user.privateSignup]);

  useEffect(() => {
    if (user.privateSignupError) {
      history.push(PATHS.REGISTRATION);
    }
  }, [user.privateSignupError]);

  return <Loading />;
};

const mapState = (state: AppStateType) => {
  const { user } = state;

  return {
    user,
    loading: selectLoadingByKey(state, types.PRIVATE_SIGNUP_REQUEST),
    error: selectErrorByKey(state, types.PRIVATE_SIGNUP_REQUEST),
  };
};

export default connect(mapState, { privateSignup })(PrivateEntry);
