import styled from 'styled-components';

import { RegistrationStyles } from '@/pages/Auth/Registration/Registration.Styles';

export const LoginStyles: any = styled(RegistrationStyles)`
  .registration__title {
    margin: 0 0 25px;
  }

  .registration__button {
    margin: 10px 0 40px;
  }

  .registration__link {
    margin: 10px 0 39px;
    font-weight: 700;
    font-size: 14px;
    line-height: 16px;
    text-decoration: underline;
    border: none;
    background-color: transparent;

    &:hover {
      box-shadow: none;
      color: var(--gray);
    }

    &:focus {
      box-shadow: none;
      color: var(--light-gray);
    }
  }
`;
