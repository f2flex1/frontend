import cloneDeep from 'lodash/cloneDeep';
import React, { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { LogInParams } from '@/api';
import LanguageSelector from '@/common/components/LanguageSelector';
import PhoneInput from '@/common/components/PhoneInput';
import history from '@/common/utils/history';
import { validateEmail } from '@/common/utils/validators';
import {
  Button,
  Title,
  Input,
  Tabs,
  Logo,
  SocialNetworks,
  LangSwitch,
  Loader,
} from '@/components/elements';
import { TabsContentItem } from '@/components/elements/Tabs/Tabs.Styles';
import {
  REGISTRATION_TABS,
  REGISTRATION_TABS_LIST,
  VERIFICATION_TYPE,
} from '@/const/app.constants';
import { LoginStyles } from '@/pages/Auth/Login/Login.Styles';
import { AppStateType } from '@/store';
import types from '@/store/actionTypes';
import { ThemeType } from '@/store/app/reducers';
import { selectErrorByKey, selectLoadingByKey } from '@/store/loadingsErrors/selectors';
import { login } from '@/store/user/actions';
import { UserReducerState } from '@/store/user/reducers';

export interface LoginProps {
  user: UserReducerState;
  loading: boolean;
  error: string | null;
  login: (payload: LogInParams | any) => void;
  themeType?: ThemeType;
}

const Login: React.FC<LoginProps> = (props: LoginProps) => {
  const { loading, login, error, user, themeType } = props;
  const { loginErrors, verificationErrors } = user;
  const { t } = useTranslation();

  const [tab, setTab] = useState<string>(REGISTRATION_TABS.EMAIL);

  const [values, setValues] = useState<{ [key: string]: string }>({
    email: '',
    phone: '',
    password: '',
    phonePassword: '',
  });
  const [errors, setErrors] = useState<{ [key: string]: string }>({});

  useEffect(() => {
    if (Object.keys(loginErrors).length) {
      const newErrors: LogInParams = {
        phone: '',
        email: '',
        password: '',
        phonePassword: '',
      };
      Object.keys(loginErrors).forEach((key: string) => {
        newErrors[key] = loginErrors[key];
      });

      setErrors(newErrors);
    }
  }, [loginErrors, setErrors]);

  useEffect(() => {
    if (verificationErrors.code === 'CODE_SENDED') {
      switch (tab) {
        case VERIFICATION_TYPE.EMAIL:
          history.push('/login_by_email');
          break;
        case VERIFICATION_TYPE.MOBILE:
          history.push('/login_by_number');
          break;
        default:
          break;
      }
    }
  }, [verificationErrors]);

  const getFormErrors = (data: { [key: string]: string }) => {
    const { email, password, phone, phonePassword } = data;
    const newErrors: LogInParams = {
      email: '',
      phone: errors.phone,
      password: '',
      phonePassword: '',
    };

    if (tab === REGISTRATION_TABS.EMAIL) {
      if (!email) newErrors.email = 'login.page.form.email.errors.empty';
      if (email && !validateEmail(email)) newErrors.email = 'login.page.form.email.errors.valid';
      if (!password) newErrors.password = 'login.page.form.password.errors.empty';
    }

    if (tab === REGISTRATION_TABS.MOBILE) {
      if (!phone) newErrors.phone = 'login.page.form.phone.errors.empty';
      if (!phonePassword) newErrors.phonePassword = 'login.page.form.password.errors.empty';
    }

    return newErrors;
  };
  //
  // const checkErrors = (data: { [key: string]: string }) => {
  //   for (const error in data) {
  //     if (data[error]) return true;
  //   }
  //   return false;
  // };

  const onChange = (field: string, value: string) => {
    setValues(prev => ({
      ...prev,
      [field]: value,
    }));

    if (!!errors[field]) {
      setErrors({
        ...errors,
        [field]: '',
      });
    }

    if (!value && Object.prototype.hasOwnProperty.call(errors, field)) {
      const newValues = cloneDeep(values);
      newValues[field] = value;
      const newErrors: LogInParams = getFormErrors(newValues);

      setErrors({
        ...errors,
        [field]: newErrors[field],
      });
    }
  };

  const onChangePhone = (field: string, value: string, isValid: boolean) => {
    setValues(prev => ({
      ...prev,
      [field]: value,
    }));

    if (!isValid && value) {
      setErrors({
        ...errors,
        [field]: 'login.page.form.phone.errors.valid',
      });
    }

    if (!value) {
      setErrors({
        ...errors,
        [field]: '',
      });
    }

    if (isValid && value) {
      setErrors({
        ...errors,
        [field]: '',
      });
    }
  };

  const onBlur = (field: string) => {
    if (Object.prototype.hasOwnProperty.call(errors, field)) {
      const newValues = cloneDeep(values);
      const newErrors: LogInParams = getFormErrors(newValues);

      setErrors({
        ...errors,
        [field]: newErrors[field],
      });
    }
  };

  const onSubmit = useCallback(
    (e: React.ChangeEvent<any>) => {
      e.preventDefault();
      const newErrors: LogInParams = getFormErrors(values);
      setErrors(newErrors);

      const data: LogInParams = {
        password: values.password,
      };

      if (tab === REGISTRATION_TABS.EMAIL) {
        data.email = values.email.toLowerCase();

        if (!newErrors.email && !newErrors.password) {
          login(data);
        }
      }

      if (tab === REGISTRATION_TABS.MOBILE) {
        data.phone = `+${values.phone}`;
        data.password = values.phonePassword;

        if (!newErrors.phone && !newErrors.phonePassword) {
          login(data);
        }
      }
    },
    [login, values, tab]
  );

  const onChangeTab = (field: string, value: string) => {
    setTab(value);
    const newErrors: LogInParams = {
      phone: errors.phone,
      email: '',
      password: '',
      phonePassword: '',
    };
    setErrors(newErrors);
  };

  return (
    <LoginStyles className={`registration -${themeType}`}>
      <LangSwitch className="registration__lang">
        <LanguageSelector className={`-${themeType}`} />
      </LangSwitch>
      <div className="registration__container registration__container_auth">
        <Logo className="registration__logo" to="/landing">
          {themeType === 'dark' ? (
            <img src="/img/main/logo.svg" alt="fin2flex" />
          ) : (
            <img src="/img/main/logo-light.svg" alt="fin2flex" />
          )}
        </Logo>
        <div className="registration__box">
          <Title className={`registration__title -${themeType}`}>
            {t('login.page.titles.title')}
          </Title>
          <p className="registration__text">{t('login.page.texts.text')}</p>

          <Tabs
            className={`registration-tabs -${themeType}`}
            tabHeaderClassName={`registration-tabs__item -${themeType}`}
            tabsList={REGISTRATION_TABS_LIST.map((item: string) => {
              return {
                key: item,
                name: t(`login.page.tabs.${item}`),
              };
            })}
            field="tab"
            activeTab={tab}
            onChange={onChangeTab}
          >
            {tab === REGISTRATION_TABS.EMAIL && (
              <TabsContentItem className="active">
                <form onSubmit={onSubmit}>
                  <Input
                    className={`lg bold -${themeType}`}
                    type="email"
                    name="email"
                    value={values.email}
                    placeholder={t('login.page.form.email.placeholder')}
                    error={errors.email}
                    onChange={onChange}
                    onBlur={onBlur}
                  />
                  <Input
                    className={`lg bold -${themeType}`}
                    type="password"
                    name="password"
                    value={values.password}
                    placeholder={t('login.page.form.password.placeholder')}
                    error={errors.password}
                    onChange={onChange}
                    onBlur={onBlur}
                  />
                  {error && <div className="error-text">{t(`${error}`)}</div>}
                  <Button
                    className={`registration__button xl width -${themeType}`}
                    type="submit"
                    disabled={loading}
                  >
                    {t('login.btns.login')}
                    {loading ? <Loader /> : null}
                  </Button>
                </form>
              </TabsContentItem>
            )}

            {tab === REGISTRATION_TABS.MOBILE && (
              <TabsContentItem className="active">
                <form onSubmit={onSubmit}>
                  <PhoneInput
                    className={`lg bold -${themeType}`}
                    placeholder={t('login.page.form.phone.placeholder')}
                    value={values.phone}
                    name="phone"
                    error={errors.phone}
                    onChange={(value: string, isValid: boolean) =>
                      onChangePhone('phone', value, isValid)}
                  />
                  <Input
                    className={`lg bold -${themeType}`}
                    type="password"
                    name="phonePassword"
                    value={values.phonePassword}
                    placeholder={t('login.page.form.password.placeholder')}
                    error={errors.phonePassword}
                    onChange={onChange}
                    onBlur={onBlur}
                  />
                  {error && <div className="error-text">{t(`${error}`)}</div>}
                  <Button
                    className={`registration__button xl width -${themeType}`}
                    type="submit"
                    disabled={loading}
                  >
                    {t('login.btns.login')}
                    {loading ? <Loader /> : null}
                  </Button>
                </form>
              </TabsContentItem>
            )}
          </Tabs>

          <div className="registration__links">
            <Link to="/registration">{t('login.link.registration')}</Link>
            <Link to="/forgot-password">{t('login.link.forgot_password')}</Link>
          </div>
        </div>
        <SocialNetworks className={`registration__social -${themeType}`} />
      </div>
    </LoginStyles>
  );
};

const mapState = (state: AppStateType) => {
  const { user, app } = state;
  return {
    user,
    loading: selectLoadingByKey(state, types.LOGIN_REQUEST),
    error: selectErrorByKey(state, types.LOGIN_REQUEST),
    themeType: app.theme,
  };
};

export default connect(mapState, { login })(Login);
