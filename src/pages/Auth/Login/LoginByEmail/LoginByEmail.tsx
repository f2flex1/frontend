import cloneDeep from 'lodash/cloneDeep';
import React, { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';

import { LogInParams, VerificationParams } from '@/api';
import LanguageSelector from '@/common/components/LanguageSelector';
import history from '@/common/utils/history';
import {
  Button,
  Input,
  LangSwitch,
  Loader,
  Logo,
  SocialNetworks,
  Title,
} from '@/components/elements';
import { LoginStyles } from '@/pages/Auth/Login/Login.Styles';
import { AppStateType } from '@/store';
import types from '@/store/actionTypes';
import { ThemeType } from '@/store/app/reducers';
import { selectErrorByKey, selectLoadingByKey } from '@/store/loadingsErrors/selectors';
import { verification, login } from '@/store/user/actions';
import { UserReducerState } from '@/store/user/reducers';

export interface VerificationProps {
  user: UserReducerState;
  loading: boolean;
  loadingOtp: boolean;
  error: string | null;
  verification: (payload: VerificationParams | any) => void;
  login: (payload: LogInParams | any) => void;
  themeType?: ThemeType;
}

const LoginByEmail: React.FC<VerificationProps> = (props: VerificationProps) => {
  const { user, loading, loadingOtp, verification, login, themeType } = props;
  const { verificationErrors, loginData } = user;
  const { t } = useTranslation();

  const [values, setValues] = useState<{ [key: string]: string }>({ code: '' });
  const [errors, setErrors] = useState<{ [key: string]: string }>({});
  const [timer, setTimer] = useState<number>(120);

  useEffect(() => {
    if (Object.keys(verificationErrors).length) {
      const newErrors: VerificationParams = { code: '' };
      Object.keys(verificationErrors).forEach((key: string) => {
        if (verificationErrors[key] !== 'CODE_SENDED') {
          newErrors[key] = verificationErrors[key];
        }
      });

      setErrors(newErrors);
    }
  }, [verificationErrors, setErrors]);

  useEffect(() => {
    const s = timer - 1;

    if (s < 1) {
      setTimer(0);
      return () => {};
    }

    const intervalId: ReturnType<typeof setTimeout> = setTimeout(() => {
      setTimer(s);
    }, 1000);

    return () => {
      clearTimeout(intervalId);
    };
  }, [timer]);

  const getFormErrors = (data: { [key: string]: string }) => {
    const { code } = data;
    const newErrors: VerificationParams = { code: '' };

    if (!code) newErrors.code = 'verification.page.form.code.errors.empty_email';

    return newErrors;
  };

  const checkErrors = (data: { [key: string]: string }) => {
    for (const error in data) {
      if (data[error]) return true;
    }
    return false;
  };

  const onChange = (field: string, value: string) => {
    setValues(prev => ({
      ...prev,
      [field]: value,
    }));

    if (!!errors[field]) {
      setErrors({
        ...errors,
        [field]: '',
      });
    }

    if (!value && Object.prototype.hasOwnProperty.call(errors, field)) {
      const newValues = cloneDeep(values);
      newValues[field] = value;
      const newErrors: VerificationParams = getFormErrors(newValues);

      setErrors({
        ...errors,
        [field]: newErrors[field],
      });
    }
  };

  const onBlur = (field: string) => {
    if (Object.prototype.hasOwnProperty.call(errors, field)) {
      const newValues = cloneDeep(values);
      const newErrors: VerificationParams = getFormErrors(newValues);

      setErrors({
        ...errors,
        [field]: newErrors[field],
      });
    }
  };

  const onSubmit = useCallback(
    (e: React.ChangeEvent<any>) => {
      e.preventDefault();

      const newErrors: VerificationParams = getFormErrors(values);
      setErrors(newErrors);

      const data: VerificationParams = {
        password: loginData?.password,
        code: values.code,
      };

      if (loginData?.email) {
        data.email = loginData?.email.toLowerCase();
      }

      if (loginData?.phone) {
        data.phone = loginData?.phone;
      }

      if (!checkErrors(newErrors)) {
        verification(data);
      }
    },
    [verification, values]
  );

  const onResendCode = () => {
    const data: any = loginData;
    setTimer(120);

    login(data);
  };

  if (!loginData?.password) {
    history.push('/login');
  }

  return (
    <LoginStyles className={`registration -${themeType}`}>
      <LangSwitch className="registration__lang">
        <LanguageSelector className={`-${themeType}`} />
      </LangSwitch>
      <div className="registration__container registration__container_auth">
        <Logo className="registration__logo" to="/landing">
          {themeType === 'dark' ? (
            <img src="/img/main/logo.svg" alt="fin2flex" />
          ) : (
            <img src="/img/main/logo-light.svg" alt="fin2flex" />
          )}
        </Logo>
        <div className="registration__box">
          <Title className={`registration__title -${themeType}`}>
            {t('verification.page.titles.login_by_email')}
          </Title>
          <p className="registration__text">{t('verification.page.texts.text_by_email')}</p>
          <form onSubmit={onSubmit}>
            <Input
              className={`lg bold -${themeType}`}
              type="text"
              name="code"
              value={values.code}
              placeholder={t('verification.page.form.code.email_placeholder')}
              error={errors.code}
              onChange={onChange}
              onBlur={onBlur}
            />

            {timer > 0 ? (
              <p className="registration__text">
                {t('verification.page.texts.text_by_email2', { timer })}
              </p>
            ) : null}

            {timer === 0 ? (
              <Button
                className="registration__link"
                type="button"
                onClick={onResendCode}
                disabled={loading}
              >
                {t('verification.link.resend_email')}
                {loading ? <Loader /> : null}
              </Button>
            ) : null}

            <Button className={`xl width -${themeType}`} type="submit" disabled={loadingOtp}>
              {t('verification.btns.login')}
              {loadingOtp ? <Loader /> : null}
            </Button>
          </form>
        </div>
        <SocialNetworks className={`registration__social -${themeType}`} />
      </div>
    </LoginStyles>
  );
};

const mapState = (state: AppStateType) => {
  const { user, app } = state;
  return {
    user,
    loading: selectLoadingByKey(state, types.LOGIN_REQUEST),
    loadingOtp: selectLoadingByKey(state, types.VERIFICATION_REQUEST),
    error: selectErrorByKey(state, types.LOGIN_REQUEST),
    themeType: app.theme,
  };
};

export default connect(mapState, { verification, login })(LoginByEmail);
