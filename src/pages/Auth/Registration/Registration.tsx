import { FormControlLabel, Checkbox } from '@material-ui/core';
import cloneDeep from 'lodash/cloneDeep';
import React, { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { LogInParams, RegistrationParams } from '@/api';
import LanguageSelector from '@/common/components/LanguageSelector';
import PhoneInput from '@/common/components/PhoneInput';
import history from '@/common/utils/history';
import {
  Button,
  Tabs,
  Title,
  Input,
  Logo,
  SocialNetworks,
  LangSwitch,
  Loader,
} from '@/components/elements';
import { FormControlInputError } from '@/components/elements/Input/Input.Styles';
import { TabsContentItem } from '@/components/elements/Tabs/Tabs.Styles';
import {
  REGISTRATION_TABS,
  REGISTRATION_TABS_LIST,
  VERIFICATION_TYPE,
} from '@/const/app.constants';
import { STORAGE_KEYS } from '@/const/storage_keys.constants';
import { RegistrationStyles } from '@/pages/Auth/Registration/Registration.Styles';
import { AppStateType } from '@/store';
import types from '@/store/actionTypes';
import { ThemeType } from '@/store/app/reducers';
import { selectErrorByKey, selectLoadingByKey } from '@/store/loadingsErrors/selectors';
import { registration } from '@/store/user/actions';
import { UserReducerState } from '@/store/user/reducers';

export interface RegistrationProps {
  user: UserReducerState;
  loading: boolean;
  error: string | null;
  registration: (payload: RegistrationParams | any) => void;
  location: any;
  themeType?: ThemeType;
}

const Registration: React.FC<RegistrationProps> = (props: RegistrationProps) => {
  const { registration, error, loading, location, user, themeType } = props;
  const { registrationErrors, verificationErrors } = user;
  const { t, i18n } = useTranslation();

  const query = new URLSearchParams(location.search);
  const queryRef = query.get(STORAGE_KEYS.REF_ID) || '';
  const localRef = localStorage.getItem(STORAGE_KEYS.REF_ID);
  const ref = queryRef || localRef;

  const [values, setValues] = useState<{ [key: string]: string }>({ inviter: ref || '' });
  const [errors, setErrors] = useState<{ [key: string]: string }>({});

  const [singleRegistration, setSingleRegistration] = useState<boolean>(false);
  const [termsRegistration, setTermsRegistration] = useState<boolean>(false);

  const [tab, setTab] = useState<string>(REGISTRATION_TABS.EMAIL);

  useEffect(() => {
    if (Object.keys(registrationErrors).length) {
      const newErrors: RegistrationParams = {
        inviter: '',
        phone: '',
        email: '',
        password: '',
        password2: '',
        terms: '',
      };
      Object.keys(registrationErrors).forEach((key: string) => {
        newErrors[key] = registrationErrors[key];
      });

      setErrors(newErrors);
    }
  }, [registrationErrors, setErrors]);

  useEffect(() => {
    if (verificationErrors.code === 'CODE_SENDED') {
      switch (tab) {
        case VERIFICATION_TYPE.EMAIL:
          history.push('/login_by_email');
          break;
        case VERIFICATION_TYPE.MOBILE:
          history.push('/login_by_number');
          break;
        default:
          break;
      }
    }
  }, [verificationErrors]);

  const getFormErrors = (data: { [key: string]: string }) => {
    const { inviter, phone, email, password, password2 } = data;
    const newErrors: RegistrationParams = {
      inviter: '',
      phone: errors.phone,
      email: '',
      password: '',
      password2: '',
      terms: '',
    };

    if (!inviter && !singleRegistration)
      newErrors.inviter = 'registration.page.form.inviter.errors.empty';
    if (!password) newErrors.password = 'registration.page.form.password.errors.empty';
    if (!password2) newErrors.password2 = 'registration.page.form.password2.errors.empty';
    if (password !== password2)
      newErrors.password2 = 'registration.page.form.password2.errors.not_match';

    if (tab === REGISTRATION_TABS.EMAIL) {
      if (!email) newErrors.email = 'registration.page.form.email.errors.empty';
    }

    if (tab === REGISTRATION_TABS.MOBILE) {
      if (!phone) newErrors.phone = 'registration.page.form.phone.errors.empty';
    }

    if (!termsRegistration) newErrors.terms = 'registration.page.form.terms.errors.not_checked';

    return newErrors;
  };

  const checkErrors = (data: { [key: string]: string }) => {
    for (const error in data) {
      if (data[error]) return true;
    }
    return false;
  };

  const onChange = (field: string, value: string) => {
    setValues(prev => ({
      ...prev,
      [field]: value,
    }));

    if (!!errors[field]) {
      setErrors({
        ...errors,
        [field]: '',
      });
    }

    if (!value && Object.prototype.hasOwnProperty.call(errors, field)) {
      const newValues = cloneDeep(values);
      newValues[field] = value;
      const newErrors: RegistrationParams = getFormErrors(newValues);

      setErrors({
        ...errors,
        [field]: newErrors[field],
      });
    }
  };

  const onChangePhone = (field: string, value: string, isValid: boolean) => {
    setValues(prev => ({
      ...prev,
      [field]: value,
    }));

    if (!isValid && value) {
      setErrors({
        ...errors,
        [field]: 'registration.page.form.phone.errors.valid',
      });
    }

    if (!value) {
      setErrors({
        ...errors,
        [field]: '',
      });
    }

    if (isValid && value) {
      setErrors({
        ...errors,
        [field]: '',
      });
    }
  };

  const onBlur = (field: string) => {
    if (Object.prototype.hasOwnProperty.call(errors, field)) {
      const newValues = cloneDeep(values);
      const newErrors: LogInParams = getFormErrors(newValues);

      setErrors({
        ...errors,
        [field]: newErrors[field],
      });
    }
  };

  const onChangeSingleRegistration = (value: boolean) => {
    setSingleRegistration(value);
    if (Object.prototype.hasOwnProperty.call(errors, 'inviter')) {
      const showError = !value && !values.inviter;
      setErrors({
        ...errors,
        inviter: showError ? 'registration.page.form.inviter.errors.empty' : '',
      });
    }
  };

  const onChangeTermsRegistration = (value: boolean) => {
    setTermsRegistration(value);
    if (Object.prototype.hasOwnProperty.call(errors, 'terms')) {
      const showError = !value;
      setErrors({
        ...errors,
        terms: showError ? 'registration.page.form.terms.errors.not_checked' : '',
      });
    }
  };

  const onChangeTab = (field: string, value: string) => {
    setTab(value);
    const newErrors: RegistrationParams = {
      inviter: '',
      phone: errors.phone,
      email: '',
      password: '',
      password2: '',
      terms: '',
    };
    setErrors(newErrors);
  };

  const onSubmit = useCallback(
    (e: React.ChangeEvent<any>) => {
      e.preventDefault();
      const newErrors: RegistrationParams = getFormErrors(values);
      setErrors(newErrors);

      const data: RegistrationParams = {
        password: values.password,
        password2: values.password2,
        language: i18n.language,
      };

      if (values.inviter) {
        data.inviter = values.inviter;
      }

      if (tab === REGISTRATION_TABS.EMAIL) {
        data.email = values.email.toLowerCase();
      }

      if (tab === REGISTRATION_TABS.MOBILE) {
        data.phone = `+${values.phone}`;
      }

      if (!checkErrors(newErrors)) {
        registration(data);
      }
    },
    [registration, values, tab, termsRegistration, getFormErrors]
  );

  return (
    <RegistrationStyles className={`registration -${themeType}`}>
      <LangSwitch className="registration__lang">
        <LanguageSelector className={`-${themeType}`} />
      </LangSwitch>
      <div className="registration__container">
        <Logo className="registration__logo" to="/landing">
          {themeType === 'dark' ? (
            <img src="/img/main/logo.svg" alt="fin2flex" />
          ) : (
            <img src="/img/main/logo-light.svg" alt="fin2flex" />
          )}
        </Logo>
        <div className="registration__box">
          <Title className={`registration__title -${themeType}`}>
            {t('registration.page.title')}
          </Title>
          <p className="registration__text">{t('registration.page.texts.text1')}</p>
          {error && <div className="error-text">{t(`${error}`)}</div>}
          <Tabs
            className={`registration-tabs -${themeType}`}
            tabHeaderClassName={`registration-tabs__item -${themeType}`}
            tabsList={REGISTRATION_TABS_LIST.map((item: string) => {
              return {
                key: item,
                name: t(`registration.tabs.${item}`),
              };
            })}
            field="tab"
            activeTab={tab}
            onChange={onChangeTab}
          >
            {tab === REGISTRATION_TABS.EMAIL && (
              <TabsContentItem className="active">
                <form onSubmit={onSubmit}>
                  <Input
                    className={`lg bold -${themeType}`}
                    type="email"
                    name="email"
                    value={values.email}
                    placeholder={t('registration.page.form.email.placeholder')}
                    error={errors.email}
                    onChange={onChange}
                    onBlur={onBlur}
                  />
                  <Input
                    className={`lg bold -${themeType}`}
                    type="password"
                    name="password"
                    value={values.password}
                    placeholder={t('registration.page.form.password.placeholder')}
                    error={errors.password}
                    onChange={onChange}
                    onBlur={onBlur}
                  />
                  <Input
                    className={`lg bold -${themeType}`}
                    type="password"
                    name="password2"
                    value={values.password2}
                    placeholder={t('registration.page.form.password2.placeholder')}
                    error={errors.password2}
                    onChange={onChange}
                    onBlur={onBlur}
                  />
                  <div className="registration__divider" />
                  <Input
                    className={`lg bold -${themeType}`}
                    type="text"
                    name="inviter"
                    value={values.inviter}
                    placeholder={t('registration.page.form.inviter.placeholder')}
                    error={errors.inviter}
                    onChange={onChange}
                    onBlur={onBlur}
                    disabled={!!ref}
                  />
                  {!!ref ? null : (
                    <p className="registration__label">{t('registration.page.texts.text2')}</p>
                  )}
                  <Button
                    className={`registration__button xl width -${themeType}`}
                    type="submit"
                    disabled={loading}
                  >
                    {t('registration.btns.registration')}
                    {loading ? <Loader /> : null}
                  </Button>
                  {!!ref ? null : (
                    <FormControlLabel
                      className="registration__checkbox"
                      control={
                        <Checkbox
                          checked={singleRegistration}
                          aria-describedby="singleRegistration-text"
                          onChange={(e: React.ChangeEvent<any>) =>
                            onChangeSingleRegistration(e.target.checked)}
                        />
                      }
                      label={
                        <React.Fragment>
                          {t('registration.link.singleRegistration.text1')}
                          {' '}
                          {t('registration.link.singleRegistration.link')}
                          {' '}
                          {t('registration.link.singleRegistration.text2')}
                        </React.Fragment>
                      }
                    />
                  )}
                  <FormControlLabel
                    className={`registration__checkbox ${errors.terms ? '-error' : ''}`}
                    control={
                      <Checkbox
                        checked={termsRegistration}
                        aria-describedby="termsRegistration-text"
                        onChange={(e: React.ChangeEvent<any>) =>
                          onChangeTermsRegistration(e.target.checked)}
                      />
                    }
                    label={
                      <React.Fragment>
                        {t('registration.link.singleRegistration.text1')}
                        {' '}
                        <a
                          href="https://f2f.ac/User_agreement_ENG.pdf"
                          target="_blank"
                          rel="noreferrer"
                        >
                          {t('registration.link.singleRegistration.conditions')}
                        </a>
                      </React.Fragment>
                    }
                  />

                  <FormControlInputError>{t(`${errors.terms}`)}</FormControlInputError>

                  <div className="registration__entrance">
                    <p>{t('registration.link.login.text')}</p>
                    <Link to="/login">{t('registration.link.login.link')}</Link>
                  </div>
                </form>
              </TabsContentItem>
            )}

            {tab === REGISTRATION_TABS.MOBILE && (
              <TabsContentItem className="active">
                <form onSubmit={onSubmit}>
                  <PhoneInput
                    className={`lg bold -${themeType}`}
                    placeholder={t('registration.page.form.phone.placeholder')}
                    value={values.phone}
                    name="phone"
                    error={errors.phone}
                    onChange={(value: string, isValid: boolean) =>
                      onChangePhone('phone', value, isValid)}
                  />
                  <Input
                    className={`lg bold -${themeType}`}
                    type="password"
                    name="password"
                    value={values.password}
                    placeholder={t('registration.page.form.password.placeholder')}
                    error={errors.password}
                    onChange={onChange}
                    onBlur={onBlur}
                  />
                  <Input
                    className={`lg bold -${themeType}`}
                    type="password"
                    name="password2"
                    value={values.password2}
                    placeholder={t('registration.page.form.password2.placeholder')}
                    error={errors.password2}
                    onChange={onChange}
                    onBlur={onBlur}
                  />
                  <div className="registration__divider" />
                  <Input
                    className={`lg bold -${themeType}`}
                    type="text"
                    name="inviter"
                    value={values.inviter}
                    placeholder={t('registration.page.form.inviter.placeholder')}
                    error={errors.inviter}
                    onChange={onChange}
                    onBlur={onBlur}
                    disabled={!!ref}
                  />
                  {!!ref ? null : (
                    <p className="registration__label">{t('registration.page.texts.text2')}</p>
                  )}
                  <Button
                    className={`registration__button xl width -${themeType}`}
                    type="submit"
                    disabled={loading}
                  >
                    {t('registration.btns.registration')}
                    {loading ? <Loader /> : null}
                  </Button>

                  {!!ref ? null : (
                    <FormControlLabel
                      className="registration__checkbox"
                      control={
                        <Checkbox
                          checked={singleRegistration}
                          aria-describedby="singleRegistration-text"
                          onChange={(e: React.ChangeEvent<any>) =>
                            onChangeSingleRegistration(e.target.checked)}
                        />
                      }
                      label={
                        <React.Fragment>
                          {t('registration.link.singleRegistration.text1')}
                          {' '}
                          {t('registration.link.singleRegistration.link')}
                          {' '}
                          {t('registration.link.singleRegistration.text2')}
                        </React.Fragment>
                      }
                    />
                  )}

                  <FormControlLabel
                    className={`registration__checkbox ${errors.terms ? '-error' : ''}`}
                    control={
                      <Checkbox
                        checked={termsRegistration}
                        aria-describedby="termsRegistration-text"
                        onChange={(e: React.ChangeEvent<any>) =>
                          onChangeTermsRegistration(e.target.checked)}
                      />
                    }
                    label={
                      <React.Fragment>
                        {t('registration.link.singleRegistration.text1')}
                        {' '}
                        <a
                          href="https://f2f.ac/User_agreement_ENG.pdf"
                          target="_blank"
                          rel="noreferrer"
                        >
                          {t('registration.link.singleRegistration.conditions')}
                        </a>
                      </React.Fragment>
                    }
                  />

                  <FormControlInputError>{t(`${errors.terms}`)}</FormControlInputError>

                  <div className="registration__entrance">
                    <p>{t('registration.link.login.text')}</p>
                    <Link to="/login">{t('registration.link.login.link')}</Link>
                  </div>
                </form>
              </TabsContentItem>
            )}
          </Tabs>
        </div>
        <SocialNetworks className={`registration__social -${themeType}`} />
      </div>
    </RegistrationStyles>
  );
};

const mapState = (state: AppStateType) => {
  const { user, app } = state;
  return {
    user,
    loading: selectLoadingByKey(state, types.REGISTRATION_REQUEST),
    error: selectErrorByKey(state, types.REGISTRATION_REQUEST),
    themeType: app.theme,
  };
};

export default connect(mapState, { registration })(Registration);
