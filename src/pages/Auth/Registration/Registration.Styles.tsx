import styled from 'styled-components';

export const RegistrationStyles: any = styled.div`
  position: relative;
  min-height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0 16px;

  .registration__container {
    margin: 20px 0;
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 100%;
    max-width: 484px;
  }

  .registration__title {
    margin: 0 0 25px;
  }

  .registration__text {
    margin: 0 0 30px;
    font-weight: 500;
    font-size: 14px;
    line-height: 23px;
    color: var(--white);
  }

  .registration__logo {
    margin-bottom: 83px;

    img {
      width: 245px;
      height: 55px;
    }
  }

  .registration__box {
    width: 100%;
    padding: 47px 46px 58px;
    background: #373d47;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.12);
    border-radius: 18px;
    text-align: center;
  }

  .registration__divider {
    position: relative;

    &::after {
      content: '';
      width: 210px;
      height: 1px;
      display: block;
      margin: 0 auto 23px;
      background: linear-gradient(
        90deg,
        rgba(255, 255, 255, 0) 0%,
        rgba(255, 255, 255, 0.21) 53.91%,
        rgba(255, 255, 255, 0) 99.52%
      );
    }
  }

  .registration__label {
    margin: -12px 0 57px;
    text-align: left;
    font-weight: 500;
    font-size: 14px;
    line-height: 17px;
    color: #bcbec2;
  }

  .registration__entrance {
    margin-left: 33px;
    display: flex;
    align-items: center;
    justify-content: flex-start;
    font-weight: 500;
    font-size: 14px;
    line-height: 17px;
    text-align: left;

    p {
      margin: 0;
    }

    a {
      text-decoration: underline;
      margin-left: 16px;
    }
  }

  .registration__checkbox {
    margin: 0 0 13px;
    align-items: center;
    text-align: left;
    display: flex;

    a {
      display: inline;
      text-decoration: underline;
    }

    .MuiFormControlLabel-label {
      font-weight: 500;
      font-size: 14px;
      line-height: 17px;
    }

    .MuiCheckbox-root {
      padding: 0;
      margin-right: 9px;
      color: var(--white);

      &.Mui-checked {
        color: var(--violet);
      }
    }

    + p {
      display: none;
      animation: fadeIn 0.8s;
    }

    &.-error {
      .MuiSvgIcon-root {
        fill: var(--red);
      }

      + p {
        display: block;
      }
    }
  }

  .registration__button {
    margin: 0 0 31px;
  }

  .registration__links {
    display: flex;
    align-items: center;
    justify-content: center;

    a {
      position: relative;
      margin: 0;
      padding-right: 40px;
      font-weight: 500;
      font-size: 14px;
      line-height: 17px;
      text-decoration: none;

      &::after {
        content: '';
        position: absolute;
        right: 0;
        top: 50%;
        transform: translateY(-50%);
        width: 2px;
        height: 35px;
        background: linear-gradient(
          rgba(255, 255, 255, 0) 0%,
          rgba(255, 255, 255, 0.06) 71.01%,
          rgba(255, 255, 255, 0) 99.52%
        );
      }

      &:last-child {
        padding-right: 0;
        padding-left: 40px;

        &::after {
          display: none;
        }
      }
    }
  }

  .error-text {
    margin: 5px 0;
    color: var(--red);
    font-size: 12px;
    line-height: 14px;
    text-align: left;
  }

  &.-light {
    .registration__box {
      background: #fafafa;
      box-shadow: 0 0 11px rgba(0, 0, 0, 0.04);
    }

    .registration__text {
      color: var(--brown);
    }

    .registration__label {
      color: var(--brown);
    }

    .registration__checkbox {
      .MuiCheckbox-root {
        color: var(--brown);

        &.Mui-checked {
          color: #c5d5fb;
        }
      }

      .MuiFormControlLabel-label {
        color: var(--brown);
      }
    }

    .registration__entrance {
      color: var(--brown);
    }

    .registration__link {
      color: var(--brown);
    }
  }

  @media (max-width: 1440px) {
    //.registration__container {
    //  margin: 30px 0 30px;
    //}

    .registration__logo {
      margin-bottom: 30px;

      img {
        max-width: 200px;
      }
    }

    .registration__box {
      padding: 45px;
    }

    .registration__title {
      font-size: 20px;
    }

    .registration__links {
      a {
        padding-right: 20px;

        &:last-child {
          padding-left: 20px;
        }
      }
    }
  }

  @media (max-width: 992px) {
    .registration__container {
      margin: 80px 0 30px;
    }

    .registration__logo {
      margin-bottom: 80px;
    }

    .registration__title {
      font-size: 20px;
    }

    .registration__box {
      padding: 45px 20px;
    }

    .registration__links {
      a {
        padding-right: 20px;

        &:last-child {
          padding-left: 20px;
        }
      }
    }
  }

  @media (max-width: 576px) {
    .registration-tabs__item {
      padding: 9px 6px;
      font-size: 10px;
      width: 50%;

      &:not(:last-child) {
        margin-right: 5px;
      }
    }

    .registration__checkbox {
      .MuiFormControlLabel-label {
        font-size: 13px;
      }
    }
  }

  @media (max-width: 374px) {
    .registration__checkbox {
      .MuiFormControlLabel-label {
        font-size: 12px;
      }
    }

    .registration__entrance {
      margin-left: 0;
      font-size: 12px;
    }

    .registration__links {
      a {
        font-size: 12px;
      }
    }
  }
`;
