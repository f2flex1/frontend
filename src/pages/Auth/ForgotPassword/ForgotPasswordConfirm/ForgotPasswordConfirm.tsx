import cloneDeep from 'lodash/cloneDeep';
import React, { useCallback, useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { ForgotPasswordParams } from '@/api';
import LanguageSelector from '@/common/components/LanguageSelector';
import history from '@/common/utils/history';
import {
  Button,
  Input,
  LangSwitch,
  Loader,
  Logo,
  SocialNetworks,
  Title,
} from '@/components/elements';
import { LoginProps } from '@/pages/Auth/ForgotPassword/ForgotPassword';
import { ForgotPasswordStyles } from '@/pages/Auth/ForgotPassword/ForgotPassword.Styles';
import { AppStateType } from '@/store';
import types from '@/store/actionTypes';
import { ThemeType } from '@/store/app/reducers';
import { selectErrorByKey, selectLoadingByKey } from '@/store/loadingsErrors/selectors';
import { changePassword, forgotPassword } from '@/store/user/actions';
import { UserReducerState } from '@/store/user/reducers';

export interface Props {
  user: UserReducerState;
  loading: boolean;
  loadingOtp: boolean;
  error: string | null;
  forgotPassword: (payload: any) => void;
  changePassword: (payload: any) => void;
  themeType?: ThemeType;
}

const ForgotPasswordConfirm: React.FC<LoginProps> = (props: Props) => {
  const { user, loading, loadingOtp, forgotPassword, changePassword, error, themeType } = props;
  const { forgotPasswordErrors, loginData } = user;

  const { t } = useTranslation();
  const methods = useForm();

  const [values, setValues] = useState<{ [key: string]: string }>({});
  const [errors, setErrors] = useState<{ [key: string]: string }>({});
  const [timer, setTimer] = useState<number>(120);

  useEffect(() => {
    if (Object.keys(forgotPasswordErrors).length) {
      const newErrors: ForgotPasswordParams = { code: '' };
      Object.keys(forgotPasswordErrors).forEach((key: string) => {
        if (forgotPasswordErrors[key] !== 'CODE_SENDED') {
          newErrors[key] = forgotPasswordErrors[key];
        }

        if (forgotPasswordErrors.code === 'CODE_SUCCESS') {
          history.push('/forgot-password_reset');
        }
      });

      setErrors(newErrors);
    }
  }, [forgotPasswordErrors, setErrors]);

  useEffect(() => {
    const s = timer - 1;

    if (s < 1) {
      setTimer(0);
      return () => {};
    }

    const intervalId: ReturnType<typeof setTimeout> = setTimeout(() => {
      setTimer(s);
    }, 1000);

    return () => {
      clearTimeout(intervalId);
    };
  }, [timer]);

  const getFormErrors = (data: { [key: string]: string }) => {
    const { code } = data;
    const newErrors: ForgotPasswordParams = {
      code: '',
    };

    if (!code) {
      if (loginData?.email) {
        newErrors.code = 'forgotPassword.page.form.code.errors.empty_email';
      } else {
        newErrors.code = 'forgotPassword.page.form.code.errors.empty';
      }
    }

    return newErrors;
  };

  const checkErrors = (data: { [key: string]: string }) => {
    for (const error in data) {
      if (data[error]) return true;
    }
    return false;
  };

  const onChange = (field: string, value: string) => {
    setValues(prev => ({
      ...prev,
      [field]: value,
    }));

    if (!!errors[field]) {
      setErrors({
        ...errors,
        [field]: '',
      });
    }

    if (!value && Object.prototype.hasOwnProperty.call(errors, field)) {
      const newValues = cloneDeep(values);
      newValues[field] = value;
      const newErrors: ForgotPasswordParams = getFormErrors(newValues);

      setErrors({
        ...errors,
        [field]: newErrors[field],
      });
    }
  };

  const onBlur = (field: string) => {
    if (Object.prototype.hasOwnProperty.call(errors, field)) {
      const newValues = cloneDeep(values);
      const newErrors: ForgotPasswordParams = getFormErrors(newValues);

      setErrors({
        ...errors,
        [field]: newErrors[field],
      });
    }
  };

  const onSubmit = useCallback(
    (e: React.ChangeEvent<any>) => {
      e.preventDefault();
      const newErrors: ForgotPasswordParams = getFormErrors(values);
      setErrors(newErrors);

      const data: ForgotPasswordParams = {
        code: values.code,
      };

      if (loginData?.email) {
        data.email = loginData?.email.toLowerCase();
      }

      if (loginData?.phone) {
        data.phone = loginData?.phone;
      }

      if (!checkErrors(newErrors)) {
        changePassword(data);
      }
    },
    [changePassword, values]
  );

  const onResendCode = () => {
    const data: any = loginData;
    setTimer(120);

    forgotPassword(data);
  };

  if (!loginData?.email && !loginData?.phone) {
    history.push('/forgot-password');
  }

  return (
    <ForgotPasswordStyles className={`registration -${themeType}`}>
      <LangSwitch className="registration__lang">
        <LanguageSelector className={`-${themeType}`} />
      </LangSwitch>
      <div className="registration__container registration__container_auth">
        <Logo className="registration__logo" to="/landing">
          {themeType === 'dark' ? (
            <img src="/img/main/logo.svg" alt="fin2flex" />
          ) : (
            <img src="/img/main/logo-light.svg" alt="fin2flex" />
          )}
        </Logo>
        <div className="registration__box">
          <Title className={`registration__title -${themeType}`}>
            {t('forgotPassword.page.title')}
          </Title>
          <p className="registration__text">
            {t(
              `forgotPassword.page.texts.${loginData?.email ? 'text_by_email' : 'text_by_number'}`
            )}
            {' '}
            {`${loginData?.email ? loginData?.email : loginData?.phone}.`}
          </p>
          {error && <div className="error-text">{t(`${error}`)}</div>}
          <FormProvider {...methods}>
            <form onSubmit={onSubmit}>
              <Input
                className={`lg -${themeType}`}
                type="text"
                name="code"
                value={values.code}
                placeholder={t(
                  `forgotPassword.page.form.code.${
                    loginData?.email ? 'email_placeholder' : 'placeholder'
                  }`
                )}
                error={errors.code}
                onChange={onChange}
                onBlur={onBlur}
              />

              {timer > 0 ? (
                <p className="registration__text">
                  {t(
                    `forgotPassword.page.texts.${
                      loginData?.email ? 'text_by_email2' : 'text_by_number2'
                    }`,
                    { timer }
                  )}
                </p>
              ) : null}

              {timer === 0 ? (
                <Button
                  className="registration__link"
                  type="button"
                  onClick={onResendCode}
                  disabled={loadingOtp}
                >
                  {t('forgotPassword.links.resend_email')}
                  {loadingOtp ? <Loader /> : null}
                </Button>
              ) : null}

              <Button
                className={`registration__button xl width -${themeType}`}
                type="submit"
                disabled={loading}
              >
                {t('forgotPassword.btns.confirm')}
                {loading ? <Loader /> : null}
              </Button>
            </form>
          </FormProvider>
          <div className="registration__links">
            <Link to="/registration">{t('forgotPassword.links.registration')}</Link>
            <Link to="/login">{t('forgotPassword.links.login')}</Link>
          </div>
        </div>
        <SocialNetworks className={`registration__social -${themeType}`} />
      </div>
    </ForgotPasswordStyles>
  );
};

const mapStateToProps = (state: AppStateType) => {
  const { app, user } = state;
  return {
    user,
    loading: selectLoadingByKey(state, types.CHANGE_PASSWORD_REQUEST),
    loadingOtp: selectLoadingByKey(state, types.FORGOT_PASSWORD_REQUEST),
    error: selectErrorByKey(state, types.CHANGE_PASSWORD_REQUEST),
    themeType: app.theme,
  };
};

export default connect(mapStateToProps, { forgotPassword, changePassword })(ForgotPasswordConfirm);
