import cloneDeep from 'lodash/cloneDeep';
import React, { useCallback, useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';

import { ForgotPasswordParams } from '@/api';
import LanguageSelector from '@/common/components/LanguageSelector';
import history from '@/common/utils/history';
import {
  Button,
  Input,
  LangSwitch,
  Loader,
  Logo,
  SocialNetworks,
  Title,
} from '@/components/elements';
import { ForgotPasswordStyles } from '@/pages/Auth/ForgotPassword/ForgotPassword.Styles';
import { AppStateType } from '@/store';
import types from '@/store/actionTypes';
import { ThemeType } from '@/store/app/reducers';
import { selectErrorByKey, selectLoadingByKey } from '@/store/loadingsErrors/selectors';
import { changePassword } from '@/store/user/actions';
import { UserReducerState } from '@/store/user/reducers';

export interface VerificationProps {
  user: UserReducerState;
  loading: boolean;
  error: string | null;
  changePassword: (payload: any) => void;
  themeType?: ThemeType;
}

const ForgotPasswordReset: React.FC<VerificationProps> = (props: VerificationProps) => {
  const { user, loading, changePassword, error, themeType } = props;
  const { forgotPasswordErrors, loginData } = user;
  const { t } = useTranslation();
  const methods = useForm();

  const [values, setValues] = useState<{ [key: string]: string }>({});
  const [errors, setErrors] = useState<{ [key: string]: string }>({});

  useEffect(() => {
    if (Object.keys(forgotPasswordErrors).length) {
      const newErrors: ForgotPasswordParams = { code: '' };
      Object.keys(forgotPasswordErrors).forEach((key: string) => {
        if (forgotPasswordErrors[key] !== 'CODE_SENDED') {
          newErrors[key] = forgotPasswordErrors[key];
        }

        if (forgotPasswordErrors.code === 'SUCCESS') {
          history.push('/forgot-password_success');
        }
      });

      setErrors(newErrors);
    }
  }, [forgotPasswordErrors, setErrors]);

  const getFormErrors = (data: { [key: string]: string }) => {
    const { password, password2 } = data;
    const newErrors: ForgotPasswordParams = {
      password: '',
      password2: '',
    };

    if (!password) newErrors.password = 'forgotPassword.page.form.password.errors.empty';
    if (!password2) newErrors.password2 = 'forgotPassword.page.form.password2.errors.empty';
    if (password !== password2)
      newErrors.password2 = 'forgotPassword.page.form.password2.errors.not_match';

    return newErrors;
  };

  const checkErrors = (data: { [key: string]: string }) => {
    for (const error in data) {
      if (data[error]) return true;
    }
    return false;
  };

  const onChange = (field: string, value: string) => {
    setValues(prev => ({
      ...prev,
      [field]: value,
    }));

    if (!!errors[field]) {
      setErrors({
        ...errors,
        [field]: '',
      });
    }

    if (!value && Object.prototype.hasOwnProperty.call(errors, field)) {
      const newValues = cloneDeep(values);
      newValues[field] = value;
      const newErrors: ForgotPasswordParams = getFormErrors(newValues);

      setErrors({
        ...errors,
        [field]: newErrors[field],
      });
    }
  };

  const onBlur = (field: string) => {
    if (Object.prototype.hasOwnProperty.call(errors, field)) {
      const newValues = cloneDeep(values);
      const newErrors: ForgotPasswordParams = getFormErrors(newValues);

      setErrors({
        ...errors,
        [field]: newErrors[field],
      });
    }
  };

  const onSubmit = useCallback(
    (e: React.ChangeEvent<any>) => {
      e.preventDefault();
      const newErrors: ForgotPasswordParams = getFormErrors(values);
      setErrors(newErrors);

      const data: ForgotPasswordParams = {
        password: values.password,
      };

      if (loginData?.email) {
        data.email = loginData?.email.toLowerCase();
      }

      if (loginData?.phone) {
        data.phone = loginData?.phone;
      }

      if (loginData?.code) {
        data.code = loginData?.code;
      }

      if (!checkErrors(newErrors)) {
        changePassword(data);
      }
    },
    [changePassword, values]
  );

  if (!loginData?.email && !loginData?.phone) {
    history.push('/forgot-password');
  }

  return (
    <ForgotPasswordStyles className={`registration -${themeType}`}>
      <LangSwitch className="registration__lang">
        <LanguageSelector className={`-${themeType}`} />
      </LangSwitch>
      <div className="registration__container registration__container_auth">
        <Logo className="registration__logo" to="/landing">
          {themeType === 'dark' ? (
            <img src="/img/main/logo.svg" alt="fin2flex" />
          ) : (
            <img src="/img/main/logo-light.svg" alt="fin2flex" />
          )}
        </Logo>
        <div className="registration__box">
          <Title className={`registration__title -${themeType}`}>
            {t('forgotPassword.page.title')}
          </Title>
          <p className="registration__text">{t('forgotPassword.page.text3')}</p>
          {error && <div className="error-text">{t(`${error}`)}</div>}
          <FormProvider {...methods}>
            <form onSubmit={onSubmit}>
              <Input
                className={`lg bold -${themeType}`}
                type="password"
                name="password"
                value={values.password}
                placeholder={t('forgotPassword.page.form.password.placeholder')}
                error={errors.password}
                onChange={onChange}
                onBlur={onBlur}
              />
              <Input
                className={`lg bold -${themeType}`}
                type="password"
                name="password2"
                value={values.password2}
                placeholder={t('forgotPassword.page.form.password2.placeholder')}
                error={errors.password2}
                onChange={onChange}
                onBlur={onBlur}
              />

              <Button
                className={`registration__button xl width -${themeType}`}
                type="submit"
                disabled={loading}
              >
                {t('forgotPassword.btns.change')}
                {loading ? <Loader /> : null}
              </Button>
            </form>
          </FormProvider>
        </div>
        <SocialNetworks className={`registration__social -${themeType}`} />
      </div>
    </ForgotPasswordStyles>
  );
};

const mapState = (state: AppStateType) => {
  const { user, app } = state;
  return {
    user,
    loading: selectLoadingByKey(state, types.CHANGE_PASSWORD_REQUEST),
    error: selectErrorByKey(state, types.CHANGE_PASSWORD_REQUEST),
    themeType: app.theme,
  };
};

export default connect(mapState, { changePassword })(ForgotPasswordReset);
