import React from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import LanguageSelector from '@/common/components/LanguageSelector';
import { LangSwitch, Logo, SecondaryButton, SocialNetworks, Title } from '@/components/elements';
import { LoginProps } from '@/pages/Auth/ForgotPassword/ForgotPassword';
import { ForgotPasswordStyles } from '@/pages/Auth/ForgotPassword/ForgotPassword.Styles';
import { AppStateType } from '@/store';

const ForgotPasswordSuccess: React.FC<LoginProps> = ({ loading, themeType }) => {
  const { t } = useTranslation();

  return (
    <ForgotPasswordStyles className={`registration -${themeType}`}>
      <LangSwitch className="registration__lang">
        <LanguageSelector className={`-${themeType}`} />
      </LangSwitch>
      <div className="registration__container registration__container_auth">
        <Logo className="registration__logo" to="/landing">
          {themeType === 'dark' ? (
            <img src="/img/main/logo.svg" alt="fin2flex" />
          ) : (
            <img src="/img/main/logo-light.svg" alt="fin2flex" />
          )}
        </Logo>
        <div className="registration__box">
          <Title className={`registration__title -${themeType}`}>
            {t('forgotPassword.page.title2')}
          </Title>
          <p className="registration__text">{t('forgotPassword.page.text4')}</p>
          <SecondaryButton as={Link} to="/login" className={`md -${themeType}`}>
            {loading}
            {t('forgotPassword.btns.enter')}
          </SecondaryButton>
        </div>
        <SocialNetworks className={`registration__social -${themeType}`} />
      </div>
    </ForgotPasswordStyles>
  );
};

const mapStateToProps = (state: AppStateType) => {
  const { app } = state;
  return {
    themeType: app.theme,
  };
};

export default connect(mapStateToProps)(ForgotPasswordSuccess);
