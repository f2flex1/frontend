import cloneDeep from 'lodash/cloneDeep';
import React, { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { ForgotPasswordParams } from '@/api';
import LanguageSelector from '@/common/components/LanguageSelector';
import PhoneInput from '@/common/components/PhoneInput';
import history from '@/common/utils/history';
import { validateEmail } from '@/common/utils/validators';
import {
  Button,
  Title,
  Input,
  Logo,
  Tabs,
  SocialNetworks,
  LangSwitch,
  Loader,
} from '@/components/elements';
import { TabsContentItem } from '@/components/elements/Tabs/Tabs.Styles';
import { REGISTRATION_TABS, REGISTRATION_TABS_LIST } from '@/const/app.constants';
import { ForgotPasswordStyles } from '@/pages/Auth/ForgotPassword/ForgotPassword.Styles';
import { AppStateType } from '@/store';
import types from '@/store/actionTypes';
import { ThemeType } from '@/store/app/reducers';
import { selectErrorByKey, selectLoadingByKey } from '@/store/loadingsErrors/selectors';
import { forgotPassword } from '@/store/user/actions';
import { UserReducerState } from '@/store/user/reducers';

export interface LoginProps {
  user: UserReducerState;
  loading: boolean;
  error: string | null;
  forgotPassword: (payload: ForgotPasswordParams | any) => void;
  themeType?: ThemeType;
}

const ForgotPassword: React.FC<LoginProps> = (props: LoginProps) => {
  const { user, loading, forgotPassword, error, themeType } = props;
  const { forgotPasswordErrors } = user;
  const { t } = useTranslation();

  const [tab, setTab] = useState<string>(REGISTRATION_TABS.EMAIL);

  const [values, setValues] = useState<{ [key: string]: string }>({});
  const [errors, setErrors] = useState<{ [key: string]: string }>({});

  useEffect(() => {
    if (forgotPasswordErrors.code === 'CODE_SENDED') {
      history.push('/forgot-password_confirm');
    }
  }, [forgotPasswordErrors]);

  const getFormErrors = (data: { [key: string]: string }) => {
    const { email, phone } = data;
    const newErrors: ForgotPasswordParams = {
      email: '',
      phone: errors.phone,
    };

    if (tab === REGISTRATION_TABS.EMAIL) {
      if (!email) newErrors.email = 'forgotPassword.page.form.email.errors.empty';
      if (email && !validateEmail(email))
        newErrors.email = 'forgotPassword.page.form.email.errors.valid';
    }

    if (tab === REGISTRATION_TABS.MOBILE) {
      if (!phone) newErrors.phone = 'forgotPassword.page.form.phone.errors.empty';
    }

    return newErrors;
  };

  const checkErrors = (data: { [key: string]: string }) => {
    for (const error in data) {
      if (data[error]) return true;
    }
    return false;
  };

  const onChange = (field: string, value: string) => {
    setValues(prev => ({
      ...prev,
      [field]: value,
    }));

    if (!!errors[field]) {
      setErrors({
        ...errors,
        [field]: '',
      });
    }

    if (!value && Object.prototype.hasOwnProperty.call(errors, field)) {
      const newValues = cloneDeep(values);
      newValues[field] = value;
      const newErrors: ForgotPasswordParams = getFormErrors(newValues);

      setErrors({
        ...errors,
        [field]: newErrors[field],
      });
    }
  };

  const onChangePhone = (field: string, value: string, isValid: boolean) => {
    setValues(prev => ({
      ...prev,
      [field]: value,
    }));

    if (!isValid && value) {
      setErrors({
        ...errors,
        [field]: 'forgotPassword.page.form.phone.errors.valid',
      });
    }

    if (!value) {
      setErrors({
        ...errors,
        [field]: '',
      });
    }

    if (isValid && value) {
      setErrors({
        ...errors,
        [field]: '',
      });
    }
  };

  const onSubmit = useCallback(
    (e: React.ChangeEvent<any>) => {
      e.preventDefault();
      const newErrors: ForgotPasswordParams = getFormErrors(values);
      setErrors(newErrors);

      const requestData: any = {};

      if (tab === REGISTRATION_TABS.EMAIL) {
        requestData.email = values.email.toLowerCase();
      }

      if (tab === REGISTRATION_TABS.MOBILE) {
        requestData.phone = `+${values.phone}`;
      }

      if (!checkErrors(newErrors)) {
        forgotPassword(requestData);
      }
    },
    [forgotPassword, values, tab]
  );

  const onChangeTab = (field: string, value: string) => {
    setTab(value);
    const newErrors: ForgotPasswordParams = {
      email: '',
      phone: errors.phone,
    };
    setErrors(newErrors);
  };

  return (
    <ForgotPasswordStyles className={`registration -${themeType}`}>
      <LangSwitch className="registration__lang">
        <LanguageSelector className={`-${themeType}`} />
      </LangSwitch>
      <div className="registration__container registration__container_auth">
        <Logo className="registration__logo" to="/landing">
          {themeType === 'dark' ? (
            <img src="/img/main/logo.svg" alt="fin2flex" />
          ) : (
            <img src="/img/main/logo-light.svg" alt="fin2flex" />
          )}
        </Logo>
        <div className="registration__box">
          <Title className={`registration__title -${themeType}`}>
            {t('forgotPassword.page.title')}
          </Title>
          <p className="registration__text">{t('forgotPassword.page.text5')}</p>
          {error && <div className="error-text">{t(`${error}`)}</div>}

          <Tabs
            className={`registration-tabs -${themeType}`}
            tabHeaderClassName={`registration-tabs__item -${themeType}`}
            tabsList={REGISTRATION_TABS_LIST.map((item: string) => {
              return {
                key: item,
                name: t(`forgotPassword.page.tabs.${item}`),
              };
            })}
            field="tab"
            activeTab={tab}
            onChange={onChangeTab}
          >
            {tab === REGISTRATION_TABS.EMAIL && (
              <TabsContentItem className="active">
                <form onSubmit={onSubmit}>
                  <Input
                    className={`lg bold -${themeType}`}
                    type="email"
                    name="email"
                    value={values.email}
                    placeholder={t('forgotPassword.page.form.email.placeholder')}
                    error={errors.email}
                    onChange={onChange}
                  />
                  <Button
                    className={`registration__button xl width -${themeType}`}
                    type="submit"
                    disabled={loading}
                  >
                    {t('forgotPassword.btns.send')}
                    {loading ? <Loader /> : null}
                  </Button>
                </form>
              </TabsContentItem>
            )}

            {tab === REGISTRATION_TABS.MOBILE && (
              <TabsContentItem className="active">
                <form onSubmit={onSubmit}>
                  <PhoneInput
                    className={`lg bold -${themeType}`}
                    placeholder={t('forgotPassword.page.form.phone.placeholder')}
                    value={values.phone}
                    name="phone"
                    error={errors.phone}
                    onChange={(value: string, isValid: boolean) =>
                      onChangePhone('phone', value, isValid)}
                  />
                  <Button
                    className={`registration__button xl width -${themeType}`}
                    type="submit"
                    disabled={loading}
                  >
                    {t('forgotPassword.btns.send')}
                    {loading ? <Loader /> : null}
                  </Button>
                </form>
              </TabsContentItem>
            )}
          </Tabs>
          <div className="registration__links">
            <Link to="/registration">{t('forgotPassword.links.registration')}</Link>
            <Link to="/login">{t('forgotPassword.links.login')}</Link>
          </div>
        </div>
        <SocialNetworks className={`registration__social -${themeType}`} />
      </div>
    </ForgotPasswordStyles>
  );
};

const mapState = (state: AppStateType) => {
  const { user, app } = state;
  return {
    user,
    loading: selectLoadingByKey(state, types.FORGOT_PASSWORD_REQUEST),
    error: selectErrorByKey(state, types.FORGOT_PASSWORD_REQUEST),
    themeType: app.theme,
  };
};

export default connect(mapState, { forgotPassword })(ForgotPassword);
