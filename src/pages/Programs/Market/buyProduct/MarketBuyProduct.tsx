import React from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import BreadCrumb from '@/components/BreadCrumb';
import {
  Button,
  Container,
  Icon,
  InfoItem,
  MainContent,
  PageModal,
  SecondaryButton,
} from '@/components/elements';
import {
  ModalDescription,
  ModalIcon,
  ModalPrice,
  ModalTitle,
} from '@/components/elements/PageModal/PageModal.Styles';
import { BuyContractStyles } from '@/pages/Investments/buyContract/buyContract.Styles';
import { AppStateType } from '@/store';
import { ThemeType } from '@/store/app/reducers';

interface Props {
  themeType: ThemeType;
}

const MarketBuyProduct: React.FC<Props> = ({ themeType }) => {
  const { t } = useTranslation();

  return (
    <MainContent className={`content-main content-main_bg-2 -${themeType}`}>
      <Container>
        <BreadCrumb />
        <BuyContractStyles className={`-${themeType}`}>
          <SecondaryButton
            as={Link}
            className={`sm back -${themeType}`}
            to="/investments/create_contract"
          >
            {t('common.back')}
          </SecondaryButton>
          <PageModal className={`-${themeType}`}>
            <ModalTitle className="modal__title">{t('market.buy_product.titles.buy')}</ModalTitle>
            <ModalIcon>
              <Icon name="cart" size="66" />
            </ModalIcon>
            <ModalDescription className="modal__description">Apple MacBook Air M1</ModalDescription>
            <div className="info-list">
              <InfoItem
                infoText={t('market.buy_product.texts.retail')}
                infoValue="132 900 FLEX"
                className="disable-divider old"
              />
              <InfoItem
                infoText={t('market.buy_product.texts.for_you')}
                infoValue="59 000 FLEX"
                className="disable-divider text-pink"
              />
              <InfoItem infoText="USDT" infoValue="400" />
              <InfoItem infoText="BTC" infoValue="0.01232553" />
            </div>
            <ModalTitle className="modal__subtitle">Сумма к оплате</ModalTitle>
            <ModalPrice className="modal__price">
              <span>62 900</span>
              {' FLEX'}
            </ModalPrice>
            <Button className="md">
              {t('market.buy_product.btns.buy_btn')}
              {/* {loading ? <Loader /> : null}*/}
            </Button>
          </PageModal>
        </BuyContractStyles>
      </Container>
    </MainContent>
  );
};

const mapState = (state: AppStateType) => {
  const { app } = state;
  return {
    themeType: app.theme,
  };
};

export default connect(mapState)(MarketBuyProduct);
