import cloneDeep from 'lodash/cloneDeep';
import find from 'lodash/find';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';

import { Product, Program } from '@/api';
import history from '@/common/utils/history';
import BreadCrumb from '@/components/BreadCrumb';
import {
  Container,
  DropDown,
  InfoItem,
  Input,
  MainContent,
  ScrollBar,
  Placeholder,
  SecondaryButton,
  Title,
} from '@/components/elements';
import { PATHS } from '@/const/paths.constants';
import {
  Create,
  CreateInfo,
  CreateOptions,
  CreateOptionsText,
  Total,
  TotalBtn,
  TotalContainer,
  TotalInfo,
  TotalPrice,
  TotalText,
} from '@/pages/Investments/createContract/createContract.Styles';
import {
  Table,
  TableBody,
  TableBodyRow,
  TableBodyWrapper,
  TableCol,
  TableContainer,
  TableHeader,
  TableHeaderRow,
} from '@/pages/Programs/Market/Market.Styles';
import { AppStateType } from '@/store';
import { ThemeType } from '@/store/app/reducers';
import { getProducts, setProduct, getPrograms, setProgram } from '@/store/programs/actions';
import { ProgramsReducerState } from '@/store/programs/reducers';

export interface Props {
  programs: ProgramsReducerState;
  getProducts: () => void;
  getPrograms: () => void;
  setProduct: any;
  setProgram: any;
  themeType: ThemeType;
}

const MarketOrder: React.FC<Props> = (props: Props) => {
  const { programs, getProducts, getPrograms, setProgram, themeType } = props;
  const { products, activeProduct, activeProgram } = programs;
  const { t } = useTranslation();

  const [productsList, setProductsList] = useState<string[]>([]);
  const [selectedProduct, setSelectedProduct] = useState<Product | null>(activeProduct || null);
  const [values, setValues] = useState<{ [key: string]: string }>({
    name: '',
    amount_usd: '',
  });
  const [errors, setErrors] = useState<{ [key: string]: string }>({});

  useEffect(() => {
    if (!programs.products.loaded) {
      getProducts();
    } else {
      const newProductsList: any[] = programs.products.list.map((product: Product) => product.name);
      setProductsList(newProductsList);
    }

    if (!programs.programs.loaded) {
      getPrograms();
    }

    if (!selectedProduct && !!programs.products.list.length) {
      setSelectedProduct(programs.products.list[0]);
    }
  }, [programs, selectedProduct, getProducts, getPrograms, setSelectedProduct]);

  const getFormErrors = (data: any) => {
    const { name, amount_usd } = data;
    const newErrors: any = {
      name: '',
      amount_usd: '',
    };

    if (!name) newErrors.name = 'market.order.forms.product.errors.empty';
    if (!amount_usd) newErrors.amount_usd = 'market.order.forms.price.errors.empty';

    return newErrors;
  };

  const checkErrors = (data: { [key: string]: string }) => {
    for (const error in data) {
      if (data[error]) return true;
    }
    return false;
  };

  const onChange = (field: string, value: any) => {
    setValues(prev => ({
      ...prev,
      [field]: value,
    }));

    if (!!errors[field]) {
      setErrors({
        ...errors,
        [field]: '',
      });
    }

    if (!value && Object.prototype.hasOwnProperty.call(errors, field)) {
      const newValues = cloneDeep(values);
      newValues[field] = value;
      const newErrors: Product = getFormErrors(newValues);

      setErrors({
        ...errors,
        [field]: newErrors[field],
      });
    }
  };

  const onChangeProduct = (name: string, value: string) => {
    const product: Product | null = find(products.list, { name: value }) || null;
    setSelectedProduct(product);
  };

  const onCreateProgram = () => {
    const newErrors: any = getFormErrors(values);
    setErrors(newErrors);

    if (!checkErrors(newErrors)) {
      setProgram({
        name: values.name,
        amount_usd: values.amount_usd,
      });
    }
  };

  const onCreate = () => {
    history.push(PATHS.PROGRAMS_MARKET_BUY_PRODUCT);
  };

  return (
    <MainContent>
      <Container>
        <BreadCrumb />
        <Create>
          <CreateOptions>
            <CreateOptionsText>{t('market.order.texts.info')}</CreateOptionsText>
            <Input
              className={`lg bold -${themeType} ${errors.name ? '-error' : ''}`}
              type="text"
              name="name"
              error={errors.name}
              placeholder={t('market.order.forms.product.placeholder')}
              value={values.name}
              onChange={(field: string, value: string) => onChange(field, value)}
            />
            <Input
              className={`lg bold -${themeType} ${errors.amount_usd ? '-error' : ''}`}
              type="number"
              name="amount_usd"
              placeholder={t('market.order.forms.price.placeholder')}
              error={errors.amount_usd}
              value={values?.amount_usd}
              onChange={(field: string, value: string) => onChange(field, value)}
            />
            <DropDown
              className={`create__options-dropdown lg -${themeType}`}
              name="product"
              value={`${selectedProduct?.name}`}
              placeholder={t('market.order.forms.select.placeholder')}
              list={productsList}
              disableUnderline
              fullWidth
              onChange={onChangeProduct}
            />
            <div className="info-list">
              <InfoItem
                infoText={t('market.order.texts.contribution')}
                infoValue="40%"
                className="pink"
              />
              <InfoItem infoText={t('market.order.texts.days_waiting')} infoValue="140 дней" />
              <InfoItem infoText={t('market.order.texts.date_receiving')} infoValue="25.09.2021" />
              <InfoItem
                infoText={t('market.order.texts.price')}
                infoValue="1244 FLEX"
                className="deposit"
              />
            </div>
            <SecondaryButton
              className={`create__button xl width -${themeType}`}
              onClick={onCreateProgram}
            >
              {t('market.order.btns.btn_generate')}
            </SecondaryButton>
          </CreateOptions>

          <CreateInfo>
            <Title>{t('market.order.titles.order')}</Title>

            {!activeProgram.length ? (
              <Placeholder>
                <p>{t('market.order.table.placeholder.title')}</p>
                <span>{t('market.order.table.placeholder.text')}</span>
              </Placeholder>
            ) : (
              <Table>
                <TableContainer>
                  <TableHeader>
                    <TableHeaderRow>
                      <TableCol>{t('market.order.table.header.col_01')}</TableCol>
                      <TableCol>{t('market.order.table.header.col_02')}</TableCol>
                      <TableCol>{t('market.order.table.header.col_03')}</TableCol>
                      <TableCol>{t('market.order.table.header.col_04')}</TableCol>
                    </TableHeaderRow>
                  </TableHeader>
                  <TableBodyWrapper>
                    <ScrollBar className={`-${themeType}`} style={{ maxHeight: '225px' }}>
                      <TableBody>
                        {activeProgram.map((program: Program, index: number) => (
                          <TableBodyRow key={`market-order-${index + 1}`}>
                            <TableCol className="table__col">{program.name}</TableCol>
                            <TableCol className="table__col">{`${program.amount_usd} FLEX`}</TableCol>
                            <TableCol className="table__col">40 000 FLEX</TableCol>
                            <TableCol className="table__col">29.06.2021</TableCol>
                          </TableBodyRow>
                        ))}
                      </TableBody>
                    </ScrollBar>
                  </TableBodyWrapper>
                </TableContainer>
              </Table>
            )}

            <Total>
              <TotalContainer>
                <TotalInfo>
                  <TotalText>{t('market.order.texts.total')}</TotalText>
                  <TotalPrice>40 000 FLEX</TotalPrice>
                </TotalInfo>
                <TotalBtn
                  className="total__btn lg width"
                  onClick={onCreate}
                  disabled={!activeProgram.length}
                >
                  {t('market.order.btns.btn_checkout')}
                </TotalBtn>
              </TotalContainer>
            </Total>
          </CreateInfo>
        </Create>
      </Container>
    </MainContent>
  );
};

const mapState = (state: AppStateType) => {
  const { app, programs } = state;
  return {
    programs,
    themeType: app.theme,
  };
};

export default connect(mapState, { getProducts, getPrograms, setProduct, setProgram })(MarketOrder);
