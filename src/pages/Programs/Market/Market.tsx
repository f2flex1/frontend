import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';

import { Product } from '@/api';
import history from '@/common/utils/history';
import { MainContent, ScrollBar } from '@/components/elements';
import { PATHS } from '@/const/paths.constants';
import {
  Card,
  CardBtn,
  CardContainer,
  CardFooter,
  CardList,
  CardListItem,
  CardListText,
  CardListValue,
  CardTitle,
  Market,
  MarketContent,
  MarketImages,
  MarketImagesItem,
  MarketLeft,
  MarketLimit,
  MarketLimitTitle,
  MarketRight,
  MarketText,
  MarketTitle,
} from '@/pages/Programs/Market/Market.Styles';
import { AppStateType } from '@/store';
import { ThemeType } from '@/store/app/reducers';
import { getProducts, setProduct } from '@/store/programs/actions';
import { ProgramsReducerState } from '@/store/programs/reducers';

export interface Props {
  programs: ProgramsReducerState;
  getProducts: () => void;
  setProduct: any;
  themeType: ThemeType;
}

const MarketProgram: React.FC<Props> = (props: Props) => {
  const { t } = useTranslation();
  const { programs, getProducts, setProduct, themeType } = props;
  const { products } = programs;

  useEffect(() => {
    if (!products.loaded) {
      getProducts();
    }
  }, [products, getProducts]);

  const onProgramClick = (product: Product) => {
    setProduct(product).then(() => history.push(PATHS.PROGRAMS_MARKET_ORDER));
  };

  return (
    <MainContent>
      <Market>
        <MarketLeft>
          <div>
            <MarketTitle>FLEX Market</MarketTitle>
            <MarketContent>
              <p>{t('market.home.texts.main')}</p>
              <p>{t('market.home.texts.offer')}</p>
              <p>{t('market.home.texts.info')}</p>
            </MarketContent>
          </div>
          <MarketLimit>
            <MarketLimitTitle as="h2">
              {t('market.home.titles.limit')}
              {' '}
              20 000 FLEX
            </MarketLimitTitle>
            <ScrollBar style={{ maxWidth: '720px' }}>
              <CardContainer>
                {products.list.length
                  ? products.list.map((product: Product, index: number) => (
                    <Card key={`investments-packet-${index + 1}`}>
                      <CardTitle>bronze</CardTitle>
                      <CardList>
                        <CardListItem>
                          <CardListText>{t('market.home.cards.percent')}</CardListText>
                          <CardListValue>30%</CardListValue>
                        </CardListItem>
                        <CardListItem>
                          <CardListText>{t('market.home.cards.waiting')}</CardListText>
                          <CardListValue>30%</CardListValue>
                        </CardListItem>
                        <CardListItem>
                          <CardListText>{t('market.home.cards.reward')}</CardListText>
                          <CardListValue>30%</CardListValue>
                        </CardListItem>
                      </CardList>
                      <CardFooter>
                        <CardBtn
                          className={`card__btn md -${themeType}`}
                          onClick={() => onProgramClick(product)}
                        >
                          {t('market.home.btns.btn_more')}
                        </CardBtn>
                      </CardFooter>
                    </Card>
                  ))
                  : null}
              </CardContainer>
            </ScrollBar>
          </MarketLimit>
        </MarketLeft>
        <MarketRight>
          <MarketImages>
            <MarketImagesItem src="/img/market/macbook.png" alt="macbook" device="macbook" />
            <MarketImagesItem src="/img/market/iphone.png" alt="iphone" device="iphone" />
            <MarketImagesItem src="/img/market/watch.png" alt="watch" device="watch" />
          </MarketImages>
          <MarketText>
            <span>FLEX</span>
            {' '}
            MARKET
          </MarketText>
        </MarketRight>
      </Market>
    </MainContent>
  );
};

const mapState = (state: AppStateType) => {
  const { app, programs } = state;
  return {
    programs,
    themeType: app.theme,
  };
};

export default connect(mapState, { getProducts, setProduct })(MarketProgram);
