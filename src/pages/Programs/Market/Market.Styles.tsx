import styled, { css } from 'styled-components';

import { Button, Title } from '@/components/elements';
import { SecondaryBtn } from '@/components/elements/SecondaryButton/SecondaryButton.Styles';

interface IMarketImagesItem {
  device?: 'macbook' | 'iphone' | 'watch';
  program?: 'cashback';
}

export const Market = styled.div`
  height: 100%;
  min-height: calc(100vh - 104px);
  padding-top: 40px;
  display: flex;
  justify-content: space-between;
  overflow: hidden;

  @media (max-width: 1300px) {
    flex-direction: column;
  }

  @media (max-width: 992px) {
    padding: 0 16px;
  }
`;

export const MarketTitle = styled(Title)`
  margin-bottom: 28px;
`;

const cashbackText = css`
  position: static;
`;

export const MarketText = styled.div<IMarketImagesItem>`
  position: absolute;
  bottom: 50px;
  right: 65px;
  margin: 0;
  font-size: 40px;
  line-height: 33px;
  color: ${({ theme }) => theme.market.flexLabel};

  ${({ program }) => {
    switch (program) {
      case 'cashback':
        return cashbackText;
      default:
        return null;
    }
  }};

  span {
    font-weight: 700;
  }

  @media (max-width: 768px) {
    right: 80px;
    font-size: 30px;
    line-height: 24px;
  }

  @media (max-width: 400px) {
    right: 50px;
  }
`;

export const MarketImages = styled.div`
  position: relative;
  height: 100%;
  max-width: 725px;
  min-height: 800px;

  @media (max-width: 1300px) {
    max-width: 100%;
  }

  @media (max-width: 576px) {
    min-height: 650px;
  }

  @media (max-width: 400px) {
    min-height: 450px;
  }
`;

const macbook = css`
  top: 0;
  left: 0;

  @media (max-width: 768px) {
    width: 400px;
  }

  @media (max-width: 576px) {
    width: 300px;
  }

  @media (max-width: 400px) {
    width: 200px;
  }
`;

const iphone = css`
  top: 0;
  right: 0;

  @media (max-width: 1500px) {
    right: -100px;
  }

  @media (max-width: 1300px) {
    right: 0;
  }

  @media (max-width: 768px) {
    height: 400px;
  }

  @media (max-width: 576px) {
    height: 300px;
  }

  @media (max-width: 400px) {
    height: 200px;
  }
`;

const watch = css`
  bottom: 0;
  left: 50%;
  transform: translateX(-50%);

  @media (max-width: 768px) {
    width: 400px;
    bottom: 100px;
  }

  @media (max-width: 576px) {
    width: 300px;
    bottom: 100px;
  }

  @media (max-width: 400px) {
    width: 200px;
  }
`;

export const MarketImagesItem = styled.img<IMarketImagesItem>`
  position: absolute;
  width: auto;

  ${({ device }) => {
    switch (device) {
      case 'macbook':
        return macbook;
      case 'iphone':
        return iphone;
      case 'watch':
        return watch;
      default:
        return null;
    }
  }};

  @media (max-width: 768px) {
    object-fit: contain;
  }
`;

export const MarketImg = styled.img`
  position: absolute;
  bottom: 0;
  right: 0;
  max-height: 700px;

  @media (max-width: 1500px) {
    right: -50%;
    transform: rotate(-30deg);
  }
`;

export const MarketLeft = styled.div`
  width: 50%;

  @media (max-width: 1300px) {
    width: 100%;
    margin-bottom: 50px;
  }
`;

export const MarketRight = styled.div`
  position: relative;
  width: 50%;

  @media (max-width: 1300px) {
    width: 100%;
  }
`;

export const MarketContent = styled.div`
  margin-bottom: 30px;
  width: 100%;
  max-width: 480px;

  p {
    margin: 0 0 20px;
    font-size: 14px;
    line-height: 20px;
    color: ${({ theme }) => theme.market.content.text};

    &:first-child {
      margin: 0 0 50px;
      font-size: 25px;
      line-height: 30px;
    }
  }

  @media (max-width: 768px) {
    p {
      &:first-child {
        font-weight: 500;
        font-size: 20px;
        line-height: 24px;
      }
    }
  }
`;

export const MarketLimit = styled.div`
  margin-top: 105px;

  @media (max-width: 992px) {
    margin-right: -16px;
  }
`;

export const MarketLimitTitle = styled(Title)`
  margin: 0 0 35px;
  font-weight: 600;
  text-transform: uppercase;

  @media (max-width: 768px) {
    font-size: 18px;
    line-height: 22px;
  }
`;

export const MarketBtn = styled(Button)`
  margin-top: 120px;
`;

export const CardContainer = styled.div`
  display: flex;
  margin: 0 0 15px;
`;

export const Card = styled.div`
  min-width: 220px;
  min-height: 290px;
  padding: 25px 15px;
  margin: 0 0 15px;
  display: flex;
  flex-direction: column;
  background-color: ${({ theme }) => theme.card.background};
  border-radius: var(--border-radius);

  &:not(:last-child) {
    margin-right: 30px;
  }
`;

export const CardTitle = styled.h3`
  margin: 0 0 15px;
  font-weight: bold;
  font-size: 20px;
  line-height: 24px;
  text-transform: uppercase;
  background-image: ${({ theme }) => theme.card.title.background};
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
`;

export const CardList = styled.ul``;

export const CardListItem = styled.li`
  display: flex;
  justify-content: space-between;
  align-items: center;

  &:not(:last-child) {
    margin-bottom: 20px;
  }
`;

export const CardListText = styled.span`
  font-size: 12px;
  line-height: 15px;
  color: ${({ theme }) => theme.card.list.text};
`;

export const CardListValue = styled.span`
  font-weight: bold;
  font-size: 18px;
  line-height: 22px;
  color: ${({ theme }) => theme.card.list.value};
`;

export const CardFooter = styled.div`
  margin-top: auto;
`;

export const CardBtn = styled(SecondaryBtn)`
  display: inline-flex;
`;

export const Table = styled.div`
  border-radius: var(--border-radius);
  overflow: auto;
  scrollbar-width: none;

  ::-webkit-scrollbar {
    display: none;
  }

  @media (max-width: 1350px) {
    margin-right: -40px;
    border-radius: var(--border-radius) 0 0 var(--border-radius);
  }

  @media (max-width: 992px) {
    margin-right: -16px;
  }
`;

export const TableContainer = styled.div`
  background: ${({ theme }) => theme.table.container.background};
  box-shadow: ${({ theme }) => theme.table.container.boxShadow};

  @media (max-width: 1350px) {
    width: 1200px;
  }
`;

export const TableHeader = styled.div`
  padding: 15px 30px 0;
  font-weight: 700;
  font-size: 15px;
  line-height: 18px;
  color: ${({ theme }) => theme.table.header.color};
`;

export const TableHeaderRow = styled.div`
  display: flex;
  align-items: center;
  flex-shrink: 0;
  padding: 18px 0;
`;

export const TableCol = styled.div`
  flex: 1 0 0;
  padding: 5px;
`;

export const TableBodyWrapper = styled.div`
  padding: 0 13px 15px 30px;
`;

export const TableBody = styled.div`
  padding-right: 20px;
  font-weight: 400;
  font-size: 15px;
  line-height: 18px;
  color: ${({ theme }) => theme.table.body.color};
`;

export const TableBodyRow = styled.div`
  display: flex;
  align-items: center;
  flex-shrink: 0;
  position: relative;
  padding: 30px 0;

  &::after {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 2px;
    background-image: ${({ theme }) => theme.table.bodyRow.after};
    opacity: 0.19;
  }
`;
