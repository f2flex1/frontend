import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';

import { Contract } from '@/api';
import history from '@/common/utils/history';
import { papulate } from '@/common/utils/papulate';
import { MainContent, SubTitle, Button } from '@/components/elements';
import { AnimatedImage } from '@/components/elements/AnimatedImage/AnimatedImage';
import { Contracts, ContractsItem } from '@/pages/Investments/Investments.Style';
import { AppStateType } from '@/store';
import { ThemeType } from '@/store/app/reducers';
import { getContracts, setContract } from '@/store/packets/actions';
import { PacketsReducerState } from '@/store/packets/reducers';

export interface Props {
  packets: PacketsReducerState;
  getContracts: () => void;
  setContract: any;
  themeType: ThemeType;
}

const AutoProgram: React.FC<Props> = (props: Props) => {
  const { t } = useTranslation();
  const { packets, getContracts, setContract, themeType } = props;
  const { contracts } = packets;

  useEffect(() => {
    if (!contracts.loaded) {
      getContracts();
    }
  }, [contracts, getContracts]);
  const onContractClick = (contract: Contract) => {
    setContract(contract).then(() => history.push('/investments/create_contract'));
  };

  return (
    <MainContent className={`content-main content-main_bg-1 investments-page -${themeType}`}>
      <div className="market" style={{ display: 'flex' }}>
        <div className="market__left">
          <div>
            <SubTitle>FLEX Auto</SubTitle>
            <div className="market__content">
              <p>Приобретайте авто мечты с выгодой до 50% от стоимости</p>
              <p>
                Платформа FIN2FLEX предлагает приобрести любой автомобиль с выгодой до 50% от
                рыночной стоимости
              </p>
              <p>
                Вам остается только вписать название марку, модель и рыночную цену авто, внести
                оплату до половины от стоимости и получить средства на покупку через фиксированный
                промежуток времени.
              </p>
            </div>
          </div>

          <div className="limit">
            <SubTitle>ЛИМИТ ПРОГРАММЫ 20 000 FLEX</SubTitle>
            <Contracts className="contracts">
              <div className="contracts__box">
                <div className="contracts__container">
                  {contracts.list.length
                    ? contracts.list.map((contract: Contract, index: number) => (
                      <ContractsItem
                        key={`investments-packet-${index + 1}`}
                        className="contracts__item contracts-item"
                      >
                        <h3 className="contracts-item__title">TEST</h3>
                        <ul className="contracts-item__list">
                          <li className="contracts-item__list-item">
                            <span className="contracts-item__left">Депозит</span>
                            <span className="contracts-item__right">{`${contract.amount_usd} FLEX`}</span>
                          </li>
                          <li className="contracts-item__list-item">
                            <span className="contracts-item__left">Чистая прибыль</span>
                            <span className="contracts-item__right">
                              {`${(
                                contract.amount_usd *
                                  ((contract.daily_percents / 100) * contract.days)
                              ).toFixed(2)} FLEX`}
                            </span>
                          </li>
                          <li className="contracts-item__list-item">
                            <span className="contracts-item__left">Срок вложения</span>
                            <span className="contracts-item__right">
                              {`${contract.days} ${t(`common.days.${papulate(contract.days)}`)}`}
                            </span>
                          </li>
                          <li className="contracts-item__list-item">
                            <span className="contracts-item__left">Ежедневный доход</span>
                            <span className="contracts-item__right">{`${contract.daily_percents} %`}</span>
                          </li>
                        </ul>
                        <span className="contracts-item__total">{`${contract.amount_usd} FLEX`}</span>
                        <Button className="sm" onClick={() => onContractClick(contract)}>
                          Инвестировать
                        </Button>
                      </ContractsItem>
                    ))
                    : null}
                </div>
              </div>
            </Contracts>
          </div>
        </div>
        <div className="market__right">
          <AnimatedImage
            src="/img/market/market_items.png"
            alt="market"
            height={200}
            width={100}
            direction="bottom"
          />
          <p className="market__text">
            <span>FLEX</span>
            {' '}
            MARKET
          </p>
        </div>
      </div>
    </MainContent>
  );
};

const mapState = (state: AppStateType) => {
  const { app, packets } = state;
  return {
    packets,
    themeType: app.theme,
  };
};

export default connect(mapState, { getContracts, setContract })(AutoProgram);
