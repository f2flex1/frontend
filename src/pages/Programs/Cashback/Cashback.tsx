import React from 'react';
import { connect } from 'react-redux';

import { MainContent } from '@/components/elements';
import {
  Market,
  MarketBtn,
  MarketContent,
  MarketImages,
  MarketImg,
  MarketLeft,
  MarketRight,
  MarketText,
  MarketTitle,
} from '@/pages/Programs/Market/Market.Styles';
import { AppStateType } from '@/store';
import { ThemeType } from '@/store/app/reducers';

export interface Props {
  themeType: ThemeType;
}

const CashbackProgram: React.FC<Props> = () => {
  return (
    <MainContent>
      <Market>
        <MarketLeft>
          <div>
            <MarketTitle>FLEX Cashback</MarketTitle>
            <MarketContent>
              <p>Начните получать до 25% кэшбэка на все ваши покупки в магазинах</p>
              <p>
                Внесите депозит и каждый месяц cможете возвращать до 25% от всех ваших покупок
                обратно на счёт.
              </p>
              <p>
                Рассчитайте ту сумму, которую вы тратите в месяц на все ваши бытовые расходы,
                внесите ее на платформу FIN2FLEX и мы вам вернем до 25% от этой суммы кэшбэком на
                ваш баланс.
              </p>
            </MarketContent>
            <MarketBtn className="btn lg market__btn">Внести депозит</MarketBtn>
          </div>
        </MarketLeft>
        <MarketRight>
          <MarketText program="cashback">
            <span>FLEX</span>
            {' '}
            Cashback
          </MarketText>
          <MarketImages>
            <MarketImg src="/img/cashback/cashback.png" alt="cashback" />
          </MarketImages>
        </MarketRight>
      </Market>
    </MainContent>
  );
};

const mapState = (state: AppStateType) => {
  const { app } = state;
  return {
    themeType: app.theme,
  };
};

export default connect(mapState)(CashbackProgram);
