import React from 'react';
import { connect } from 'react-redux';

import BreadCrumb from '@/components/BreadCrumb';
import {
  Button,
  InfoItem,
  Input,
  Label,
  MainContent,
  ProfitComponent,
  Title,
} from '@/components/elements';
import { Profitability } from '@/pages/Home/Home';
import { Profit, ProfitContainer } from '@/pages/Home/Home.Styles';
import { AppStateType } from '@/store';
import { ThemeType } from '@/store/app/reducers';

export interface Props {
  themeType: ThemeType;
}

const CashbackProgramOrder: React.FC<Props> = ({ themeType }) => {
  const profitabilities: Profitability[] = [
    {
      title: 'Баланс кэшбека',
      tooltip: '',
      value: 1562,
      currency: 'FLEX',
    },
    {
      title: 'Получено кэшбека за месяц',
      tooltip: '',
      value: 392,
      currency: 'FLEX',
    },
  ];

  return (
    <MainContent className={`content-main home-page -${themeType}`}>
      <BreadCrumb />
      <div className="order-styles">
        <div className="order">
          <div className="order__left">
            <Title className="order__title">FLEX Cashback</Title>
            <Label className="" htmlFor="deposit_sum">
              Введите сумму депозита
            </Label>
            <Input
              id="deposit_sum"
              className={`order__input   -${themeType}`}
              type="text"
              name="deposit_sum"
              placeholder="Сумма депозита FLEX"
              // value={''}
              // error={''}
              // onChange={''}
              // onBlur={}
            />
            <ul className="info-list order__info-list">
              <InfoItem className="white" infoText="USDT" infoValue="123" />
              <InfoItem className="white" infoText="BTC" infoValue="123" />
              <InfoItem
                className="balance disable-divider"
                infoText="Сумма кэшбека"
                infoValue="123"
              />
            </ul>
            <Button className="order__btn lg">Инвестировать</Button>
          </div>
          <div className="order__right">
            <Profit className={`profit profit_order -${themeType}`}>
              <ProfitContainer className="profit__container">
                {profitabilities.map((item: Profitability, index: number) => {
                  return <ProfitComponent key={`profitability-${index + 1}`} item={item} />;
                })}
              </ProfitContainer>
            </Profit>
          </div>
        </div>
      </div>
    </MainContent>
  );
};

const mapState = (state: AppStateType) => {
  const { app } = state;
  return {
    themeType: app.theme,
  };
};

export default connect(mapState)(CashbackProgramOrder);
