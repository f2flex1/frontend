import cloneDeep from 'lodash/cloneDeep';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';

import { Tabs, Container, MainContent, Title } from '@/components/elements';
import { TabsContentItem } from '@/components/elements/Tabs/Tabs.Styles';
import {
  ADDITIONALLY_MATERIALS,
  ADDITIONALLY_MATERIALS_LINKS,
  ADDITIONALLY_TABS,
  ADDITIONALLY_TABS_LIST,
} from '@/const/app.constants';
import {
  Accordion,
  AccordionBtn,
  AccordionContent,
  AccordionCollapse,
  AccordionItem,
} from '@/pages/Faq/Faq.Styles';
import { SettingsPage, SettingsFormBox, SettingsItem } from '@/pages/Settings/Settings.Styles';
import { AppStateType } from '@/store';
import { ThemeType } from '@/store/app/reducers';
import { UserReducerState } from '@/store/user/reducers';

export interface AdditionallyProps {
  user: UserReducerState;
  themeType?: ThemeType;
  location: any;
}

const Additionally: React.FC<AdditionallyProps> = (props: AdditionallyProps) => {
  const { user, themeType, location } = props;

  const [expanded, setExpanded] = useState<any>([]);
  const { t } = useTranslation();

  const query = new URLSearchParams(location.search);
  const queryTab = query.get('tab') || '';

  const [values, setValues] = useState<{ [key: string]: string }>({
    tab: queryTab || ADDITIONALLY_TABS.MATERIALS,
  });

  const onChange = (field: string, value: string) => {
    setValues(prev => ({
      ...prev,
      [field]: value,
    }));
  };

  const setNewMaterailTabs = (id: number) => {
    let newMaterailTabs: number[] = cloneDeep(expanded);

    if (expanded.includes(id)) {
      newMaterailTabs = newMaterailTabs.filter((exp: number) => exp !== id);
    } else {
      newMaterailTabs.push(id);
    }

    setExpanded(newMaterailTabs);
  };

  return (
    <MainContent className={`content-main content-main_bg-3 -${themeType}`}>
      <Container>
        <SettingsPage className={`settings -${themeType}`}>
          <Title className={`settings__title -${themeType}`}>{t('navBar.additionally')}</Title>
          <Tabs
            className={`setting-header -${themeType}`}
            tabHeaderClassName={`setting-header__tab -${themeType}`}
            tabsList={ADDITIONALLY_TABS_LIST.map((item: string) => {
              return {
                key: item,
                name: t(`settings.tabs.${item}`),
              };
            })}
            field="tab"
            activeTab={values.tab}
            onChange={onChange}
          >
            <TabsContentItem
              className={`setting-tab ${
                values.tab === ADDITIONALLY_TABS.MATERIALS ? 'active' : ''
              }`}
            >
              <Accordion>
                <AccordionItem page="additionally">
                  <AccordionBtn
                    className={`${expanded.includes(1) ? 'active' : ''}`}
                    onClick={() => setNewMaterailTabs(1)}
                  >
                    {t('settings.collapses.titles.title1')}
                  </AccordionBtn>
                  <AccordionCollapse in={expanded.includes(1)} timeout="auto" unmountOnExit>
                    {ADDITIONALLY_MATERIALS_LINKS.presentations.map((link: any, index: number) => (
                      <AccordionContent key={`presentations${index + 1}`}>
                        <a
                          target="_blank"
                          rel="noreferrer"
                          href={ADDITIONALLY_MATERIALS[link.lang].presentation}
                        >
                          {t(link.title)}
                          &nbsp;
                          {link.prefix}
                        </a>
                        <span>PDF</span>
                      </AccordionContent>
                    ))}
                  </AccordionCollapse>
                </AccordionItem>
                <AccordionItem page="additionally">
                  <AccordionBtn
                    className={`settings-materials__btn ${expanded.includes(2) ? 'active' : ''}`}
                    onClick={() => setNewMaterailTabs(2)}
                  >
                    {t('settings.collapses.titles.title2')}
                  </AccordionBtn>
                  <AccordionCollapse in={expanded.includes(2)} timeout="auto" unmountOnExit>
                    {ADDITIONALLY_MATERIALS_LINKS.marketings.map((link: any, index: number) => (
                      <AccordionContent key={`marketings${index + 1}`}>
                        <a
                          target="_blank"
                          rel="noreferrer"
                          href={ADDITIONALLY_MATERIALS[link.lang].marketing}
                        >
                          {t(link.title)}
                          &nbsp;
                          {link.prefix}
                        </a>
                        <span>PDF</span>
                      </AccordionContent>
                    ))}
                  </AccordionCollapse>
                </AccordionItem>
                <AccordionItem page="additionally">
                  <AccordionBtn
                    className={`${expanded.includes(3) ? 'active' : ''}`}
                    onClick={() => setNewMaterailTabs(3)}
                  >
                    {t('settings.collapses.titles.title3')}
                  </AccordionBtn>
                  <AccordionCollapse in={expanded.includes(3)} timeout="auto" unmountOnExit>
                    {ADDITIONALLY_MATERIALS_LINKS.brandbooks.map((link: any, index: number) => (
                      <AccordionContent key={`brandbooks${index + 1}`}>
                        <a
                          target="_blank"
                          rel="noreferrer"
                          href={ADDITIONALLY_MATERIALS[link.lang].brandbook}
                        >
                          {t(link.title)}
                          &nbsp;
                          {link.prefix}
                        </a>
                        <span>PDF</span>
                      </AccordionContent>
                    ))}
                  </AccordionCollapse>
                </AccordionItem>
                <AccordionItem page="additionally">
                  <AccordionBtn
                    className={`${expanded.includes(4) ? 'active' : ''}`}
                    onClick={() => setNewMaterailTabs(4)}
                  >
                    {t('settings.collapses.titles.title4')}
                  </AccordionBtn>
                  <AccordionCollapse in={expanded.includes(4)} timeout="auto" unmountOnExit>
                    {ADDITIONALLY_MATERIALS_LINKS.completeBrandbooks.map(
                      (link: any, index: number) => (
                        <AccordionContent key={`completeBrandbooks${index + 1}`}>
                          <a
                            target="_blank"
                            rel="noreferrer"
                            href={ADDITIONALLY_MATERIALS[link.lang].completeBrandbook}
                          >
                            {t(link.title)}
                            &nbsp;
                            {link.prefix}
                          </a>
                          <span>PDF</span>
                        </AccordionContent>
                      )
                    )}
                  </AccordionCollapse>
                </AccordionItem>
              </Accordion>
            </TabsContentItem>
            <TabsContentItem
              className={`setting-tab ${values.tab === ADDITIONALLY_TABS.KYC ? 'active' : ''}`}
            >
              <SettingsFormBox className="setting-tab__box __password">
                <SettingsItem className="setting-tab__item">
                  <div className="setting__turnover-info">
                    <p>{t('settings.texts.text3')}</p>
                    <span>
                      {user.userData?.turnover}
                      /35000 FLEX
                    </span>
                    <p>{t('settings.texts.text4')}</p>
                  </div>
                </SettingsItem>
              </SettingsFormBox>
            </TabsContentItem>
          </Tabs>
        </SettingsPage>
      </Container>
    </MainContent>
  );
};

const mapState = (state: AppStateType) => {
  const { user, app } = state;
  return {
    user,
    themeType: app.theme,
  };
};

export default connect(mapState, {})(Additionally);
