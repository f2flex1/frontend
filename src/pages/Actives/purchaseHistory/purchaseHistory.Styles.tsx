import styled from 'styled-components';

export const PurchaseHistoryStyles: any = styled.div`
  .product-history__box {
    //overflow-y: auto;
    max-height: 580px;
    display: flex;
    flex-wrap: wrap;
    margin: 0 -20px;
  }

  .active-products__item {
    width: calc(100% / 3 - 40px);
    margin: 0 20px 40px;
  }

  &.-light {
    .contract-history__tabs-item {
      color: var(--brown);

      &:focus {
        color: #929292;
      }
    }
  }

  @media (max-width: 1500px) {
    .active-products__item {
      width: calc(100% / 2 - 40px);
    }
  }

  @media (max-width: 992px) {
    .product-history__box {
      margin: 0;
      scrollbar-width: none;

      ::-webkit-scrollbar {
        display: none;
      }
    }

    .active-products__item {
      width: calc(100%);
      margin: 0 0 40px;
    }
  }
`;
