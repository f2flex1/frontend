import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import BreadCrumb from '@/components/BreadCrumb';
import { Container, InfoItem, MainContent, SubTitle, Tabs } from '@/components/elements';
import { TabsContentItem } from '@/components/elements/Tabs/Tabs.Styles';
import { CONTRACT_HISTORY_TABS, CONTRACT_HISTORY_TABS_LIST } from '@/const/app.constants';
import { ActivesHeader, ActivesProductsItem } from '@/pages/Actives/Actives.Styles';
import { PurchaseHistoryStyles } from '@/pages/Actives/purchaseHistory/purchaseHistory.Styles';
import { AppStateType } from '@/store';
import { ThemeType } from '@/store/app/reducers';

export interface Props {
  themeType: ThemeType;
}

const PurchaseHistory: React.FC<Props> = ({ themeType }) => {
  const { t } = useTranslation();
  const [values, setValues] = useState<{ [key: string]: string }>({
    currency: CONTRACT_HISTORY_TABS.ALL_PERIOD,
  });

  const onChange = (field: string, value: string) => {
    setValues(prev => ({
      ...prev,
      [field]: value,
    }));
  };

  return (
    <MainContent className={`content-main -${themeType}`}>
      <Container>
        <BreadCrumb />
        <PurchaseHistoryStyles className={`purchase-history -${themeType}`}>
          <ActivesHeader className={`actives__header -${themeType}`}>
            <SubTitle as="h1" className={`actives__title -${themeType}`}>
              {t('actives.titles.products')}
            </SubTitle>
            <Link to="/actives" className="actives__link">
              {t('actives.texts.products')}
            </Link>
          </ActivesHeader>
          <Tabs
            className="contract-history__tabs"
            tabHeaderClassName="contract-history__tabs-item"
            tabsList={CONTRACT_HISTORY_TABS_LIST.map((item: string) => {
              return {
                key: item,
                name: t(`actives.tabs.${item}`),
              };
            })}
            field="currency"
            activeTab={values.currency || CONTRACT_HISTORY_TABS.ALL_PERIOD}
            onChange={onChange}
          >
            <TabsContentItem
              className={`${CONTRACT_HISTORY_TABS.ALL_PERIOD} ${
                values.currency === CONTRACT_HISTORY_TABS.ALL_PERIOD ? 'active' : ''
              }`}
            >
              <div className="product-history__box">
                <ActivesProductsItem className={`active-products__item -${themeType}`}>
                  <h4 className="active-products__title">Iphone 12 pro max</h4>
                  <div className="active-products__info-items">
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.price')}
                      infoValue="40 550 FLEX"
                    />
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.date')}
                      infoValue="25.09.2021"
                    />
                  </div>
                </ActivesProductsItem>
                <ActivesProductsItem className={`active-products__item -${themeType}`}>
                  <h4 className="active-products__title">Iphone 12 pro max</h4>
                  <div className="active-products__info-items">
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.price')}
                      infoValue="40 550 FLEX"
                    />
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.date')}
                      infoValue="25.09.2021"
                    />
                  </div>
                </ActivesProductsItem>
                <ActivesProductsItem className={`active-products__item -${themeType}`}>
                  <h4 className="active-products__title">Iphone 12 pro max</h4>
                  <div className="active-products__info-items">
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.price')}
                      infoValue="40 550 FLEX"
                    />
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.date')}
                      infoValue="25.09.2021"
                    />
                  </div>
                </ActivesProductsItem>
                <ActivesProductsItem className={`active-products__item -${themeType}`}>
                  <h4 className="active-products__title">Iphone 12 pro max</h4>
                  <div className="active-products__info-items">
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.price')}
                      infoValue="40 550 FLEX"
                    />
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.date')}
                      infoValue="25.09.2021"
                    />
                  </div>
                </ActivesProductsItem>
                <ActivesProductsItem className={`active-products__item -${themeType}`}>
                  <h4 className="active-products__title">Iphone 12 pro max</h4>
                  <div className="active-products__info-items">
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.price')}
                      infoValue="40 550 FLEX"
                    />
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.date')}
                      infoValue="25.09.2021"
                    />
                  </div>
                </ActivesProductsItem>
                <ActivesProductsItem className={`active-products__item -${themeType}`}>
                  <h4 className="active-products__title">Iphone 12 pro max</h4>
                  <div className="active-products__info-items">
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.price')}
                      infoValue="40 550 FLEX"
                    />
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.date')}
                      infoValue="25.09.2021"
                    />
                  </div>
                </ActivesProductsItem>
                <ActivesProductsItem className={`active-products__item -${themeType}`}>
                  <h4 className="active-products__title">Iphone 12 pro max</h4>
                  <div className="active-products__info-items">
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.price')}
                      infoValue="40 550 FLEX"
                    />
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.date')}
                      infoValue="25.09.2021"
                    />
                  </div>
                </ActivesProductsItem>
                <ActivesProductsItem className={`active-products__item -${themeType}`}>
                  <h4 className="active-products__title">Iphone 12 pro max</h4>
                  <div className="active-products__info-items">
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.price')}
                      infoValue="40 550 FLEX"
                    />
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.date')}
                      infoValue="25.09.2021"
                    />
                  </div>
                </ActivesProductsItem>
              </div>
            </TabsContentItem>
            <TabsContentItem
              className={`${CONTRACT_HISTORY_TABS.AUGUST} ${
                values.currency === CONTRACT_HISTORY_TABS.AUGUST ? 'active' : ''
              }`}
            >
              <div className="product-history__box">
                <ActivesProductsItem className={`active-products__item -${themeType}`}>
                  <h4 className="active-products__title">Iphone 12 pro max</h4>
                  <div className="active-products__info-items">
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.price')}
                      infoValue="40 550 FLEX"
                    />
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.date')}
                      infoValue="25.09.2021"
                    />
                  </div>
                </ActivesProductsItem>
              </div>
            </TabsContentItem>
            <TabsContentItem
              className={`${CONTRACT_HISTORY_TABS.JULY} ${
                values.currency === CONTRACT_HISTORY_TABS.JULY ? 'active' : ''
              }`}
            >
              <div className="product-history__box">
                <ActivesProductsItem className={`active-products__item -${themeType}`}>
                  <h4 className="active-products__title">Iphone 12 pro max</h4>
                  <div className="active-products__info-items">
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.price')}
                      infoValue="40 550 FLEX"
                    />
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.date')}
                      infoValue="25.09.2021"
                    />
                  </div>
                </ActivesProductsItem>
              </div>
            </TabsContentItem>
            <TabsContentItem
              className={`${CONTRACT_HISTORY_TABS.JUNE} ${
                values.currency === CONTRACT_HISTORY_TABS.JUNE ? 'active' : ''
              }`}
            >
              <div className="product-history__box">
                <ActivesProductsItem className={`active-products__item -${themeType}`}>
                  <h4 className="active-products__title">Iphone 12 pro max</h4>
                  <div className="active-products__info-items">
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.price')}
                      infoValue="40 550 FLEX"
                    />
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.date')}
                      infoValue="25.09.2021"
                    />
                  </div>
                </ActivesProductsItem>
              </div>
            </TabsContentItem>
            <TabsContentItem
              className={`${CONTRACT_HISTORY_TABS.MAY} ${
                values.currency === CONTRACT_HISTORY_TABS.MAY ? 'active' : ''
              }`}
            >
              <div className="product-history__box">
                <ActivesProductsItem className={`active-products__item -${themeType}`}>
                  <h4 className="active-products__title">Iphone 12 pro max</h4>
                  <div className="active-products__info-items">
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.price')}
                      infoValue="40 550 FLEX"
                    />
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.date')}
                      infoValue="25.09.2021"
                    />
                  </div>
                </ActivesProductsItem>
              </div>
            </TabsContentItem>
            <TabsContentItem
              className={`${CONTRACT_HISTORY_TABS.APRIL} ${
                values.currency === CONTRACT_HISTORY_TABS.APRIL ? 'active' : ''
              }`}
            >
              <div className="product-history__box">
                <ActivesProductsItem className={`active-products__item -${themeType}`}>
                  <h4 className="active-products__title">Iphone 12 pro max</h4>
                  <div className="active-products__info-items">
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.price')}
                      infoValue="40 550 FLEX"
                    />
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.date')}
                      infoValue="25.09.2021"
                    />
                  </div>
                </ActivesProductsItem>
              </div>
            </TabsContentItem>
            <TabsContentItem
              className={`${CONTRACT_HISTORY_TABS.MARCH} ${
                values.currency === CONTRACT_HISTORY_TABS.MARCH ? 'active' : ''
              }`}
            >
              <div className="product-history__box">
                <ActivesProductsItem className={`active-products__item -${themeType}`}>
                  <h4 className="active-products__title">Iphone 12 pro max</h4>
                  <div className="active-products__info-items">
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.price')}
                      infoValue="40 550 FLEX"
                    />
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.date')}
                      infoValue="25.09.2021"
                    />
                  </div>
                </ActivesProductsItem>
              </div>
            </TabsContentItem>
            <TabsContentItem
              className={`${CONTRACT_HISTORY_TABS.FEBRUARY} ${
                values.currency === CONTRACT_HISTORY_TABS.FEBRUARY ? 'active' : ''
              }`}
            >
              <div className="product-history__box">
                <ActivesProductsItem className={`active-products__item -${themeType}`}>
                  <h4 className="active-products__title">Iphone 12 pro max</h4>
                  <div className="active-products__info-items">
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.price')}
                      infoValue="40 550 FLEX"
                    />
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.date')}
                      infoValue="25.09.2021"
                    />
                  </div>
                </ActivesProductsItem>
              </div>
            </TabsContentItem>
            <TabsContentItem
              className={`${CONTRACT_HISTORY_TABS.JANUARY} ${
                values.currency === CONTRACT_HISTORY_TABS.JANUARY ? 'active' : ''
              }`}
            >
              <div className="product-history__box">
                <ActivesProductsItem className={`active-products__item -${themeType}`}>
                  <h4 className="active-products__title">Iphone 12 pro max</h4>
                  <div className="active-products__info-items">
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.price')}
                      infoValue="40 550 FLEX"
                    />
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.date')}
                      infoValue="25.09.2021"
                    />
                  </div>
                </ActivesProductsItem>
              </div>
            </TabsContentItem>
            <TabsContentItem
              className={`${CONTRACT_HISTORY_TABS.TWO_THOUSAND_TWENTY} ${
                values.currency === CONTRACT_HISTORY_TABS.TWO_THOUSAND_TWENTY ? 'active' : ''
              }`}
            >
              <div className="product-history__box">
                <ActivesProductsItem className={`active-products__item -${themeType}`}>
                  <h4 className="active-products__title">Iphone 12 pro max</h4>
                  <div className="active-products__info-items">
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.price')}
                      infoValue="40 550 FLEX"
                    />
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.date')}
                      infoValue="25.09.2021"
                    />
                  </div>
                </ActivesProductsItem>
              </div>
            </TabsContentItem>
            <TabsContentItem
              className={`${CONTRACT_HISTORY_TABS.TWO_THOUSAND_NINETEEN} ${
                values.currency === CONTRACT_HISTORY_TABS.TWO_THOUSAND_NINETEEN ? 'active' : ''
              }`}
            >
              <div className="product-history__box">
                <ActivesProductsItem className={`active-products__item -${themeType}`}>
                  <h4 className="active-products__title">Iphone 12 pro max</h4>
                  <div className="active-products__info-items">
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.price')}
                      infoValue="40 550 FLEX"
                    />
                    <InfoItem
                      className=""
                      infoText={t('actives.products_info.date')}
                      infoValue="25.09.2021"
                    />
                  </div>
                </ActivesProductsItem>
              </div>
            </TabsContentItem>
          </Tabs>
        </PurchaseHistoryStyles>
      </Container>
    </MainContent>
  );
};

const mapState = (state: AppStateType) => {
  const { app } = state;
  return {
    themeType: app.theme,
  };
};

export default connect(mapState)(PurchaseHistory);
