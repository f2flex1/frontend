import orderBy from 'lodash/orderBy';
import uniq from 'lodash/uniq';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { Packet } from '@/api';
import { selfMoment } from '@/common/utils/i18n';
import { papulate } from '@/common/utils/papulate';
import BreadCrumb from '@/components/BreadCrumb';
import { Container, InfoItem, MainContent, SubTitle, Tabs } from '@/components/elements';
import { TabsContentItem } from '@/components/elements/Tabs/Tabs.Styles';
import { CONTRACT_HISTORY_TABS } from '@/const/app.constants';
import { ActivesContractsItem, ActivesHeader } from '@/pages/Actives/Actives.Styles';
import { ContractHistoryStyles } from '@/pages/Actives/contractHistory/ContractHistory.Styles';
import { AppStateType } from '@/store';
import { ThemeType } from '@/store/app/reducers';
import { getPackets } from '@/store/packets/actions';
import { PacketsReducerState } from '@/store/packets/reducers';

export interface Props {
  packets: PacketsReducerState;
  getPackets: () => void;
  themeType: ThemeType;
}

const ContractHistory: React.FC<Props> = (props: Props) => {
  const { packets, getPackets, themeType } = props;
  const { t } = useTranslation();
  const [values, setValues] = useState<{ [key: string]: string }>({
    period: CONTRACT_HISTORY_TABS.ALL,
  });
  const [periodsList, setPeriodsList] = useState<any[]>([]);

  useEffect(() => {
    if (!packets.packets.loaded) {
      getPackets();
    } else {
      const currentYear: string = selfMoment(new Date().toISOString()).format('YYYY');
      const periodsList: string[] = orderBy(packets.packets.list, 'created', 'desc').map(
        (packet: Packet) => {
          return selfMoment(packet.created).format('MM-YYYY');
        }
      );
      const newPeriodsList: string[] = uniq(periodsList).map((period: string) => {
        const year: string = period.split('-')[1];
        return year < currentYear ? year : period;
      });

      const periods: any[] = [
        {
          key: CONTRACT_HISTORY_TABS.ALL,
          name: CONTRACT_HISTORY_TABS.ALL,
        },
        ...uniq(newPeriodsList).map((item: string) => {
          return {
            key: item,
            name: CONTRACT_HISTORY_TABS[item.split('-')[0]] || item,
          };
        }),
      ];

      setPeriodsList(periods);
    }
  }, [packets, getPackets, setPeriodsList]);

  const onChange = (field: string, value: string) => {
    setValues(prev => ({
      ...prev,
      [field]: value,
    }));
  };

  const renderActivesContracts = (period: string) => {
    const packetsList: Packet[] = packets.packets.list.length ? packets.packets.list : [];

    const activesContracts: Packet[] =
      period === CONTRACT_HISTORY_TABS.ALL
        ? orderBy(packetsList, 'created', 'asc')
        : orderBy(packetsList, 'created', 'asc').filter((packet: Packet) => {
          const date: string = selfMoment(packet.created).format('MM-YYYY');
          const year: string = date.split('-')[1];

          return period.includes('-') ? date === period : year === period;
        });

    return activesContracts.map((packet: Packet, index: number) => (
      <ActivesContractsItem
        key={`actives-packet-history-${index + 1}`}
        className={`active-contracts__item -${themeType}`}
      >
        <div className="active-contracts__header">
          <p className="active-contracts__percent light">
            {`${packet.amount_usd} FLEX`}
            {' '}
            <span>{`+${(Number(packet.percent) * Number(packet.days_open)).toFixed(2)}%`}</span>
          </p>
          <span className="active-contracts__easy light">{packet.name}</span>
        </div>
        <div className="active-contracts__info-items">
          <InfoItem
            className="active-contracts__info"
            infoText={t('actives.contracts_info.invested')}
            infoValue={`${packet.amount_usd} FLEX`}
          />
          <InfoItem
            className="active-contracts__info"
            infoText={t('actives.contracts_info.days')}
            infoValue={`${packet.days_left} ${t(`common.days.${papulate(packet.days_left)}`)}`}
          />
          <InfoItem
            className="active-contracts__info"
            infoText={t('actives.contracts_info.date')}
            infoValue={selfMoment(packet.created).add(packet.days, 'days').format('DD.MM.YYYY')}
          />
          <InfoItem
            className="total"
            infoText={t('actives.contracts_info.total')}
            infoValue={`${(packet.amount_usd * ((packet.percent / 100) * packet.days_open)).toFixed(
              2
            )} FLEX`}
          />
        </div>
      </ActivesContractsItem>
    ));
  };

  return (
    <MainContent className={`content-main -${themeType}`}>
      <Container>
        <BreadCrumb />
        <ContractHistoryStyles className={`contract-history -${themeType}`}>
          <ActivesHeader className={`actives__header -${themeType}`}>
            <SubTitle as="h1" className={`actives__title -${themeType}`}>
              {t('actives.titles.contracts')}
            </SubTitle>
            <Link to="/actives" className="actives__link">
              {t('actives.texts.contracts')}
            </Link>
          </ActivesHeader>
          <Tabs
            className="contract-history__tabs"
            tabHeaderClassName="contract-history__tabs-item"
            tabsList={periodsList.map((item: any) => {
              return {
                key: item.key,
                name: Number(item.name) ? item.name : t(`actives.tabs.${item.name}`),
              };
            })}
            field="period"
            activeTab={values.period || CONTRACT_HISTORY_TABS.ALL}
            onChange={onChange}
          >
            <TabsContentItem className="active">
              <div className="contract-history__box">{renderActivesContracts(values.period)}</div>
            </TabsContentItem>
          </Tabs>
        </ContractHistoryStyles>
      </Container>
    </MainContent>
  );
};

const mapState = (state: AppStateType) => {
  const { packets, app } = state;
  return {
    packets,
    themeType: app.theme,
  };
};

export default connect(mapState, { getPackets })(ContractHistory);
