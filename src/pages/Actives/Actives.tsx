import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { Packet } from '@/api';
import { selfMoment } from '@/common/utils/i18n';
import { papulate } from '@/common/utils/papulate';
import {
  Container,
  InfoItem,
  MainContent,
  Placeholder,
  SubTitle,
  Title,
} from '@/components/elements';
import {
  ActivesContracts,
  ActivesContractsItem,
  ActivesHeader,
  ActivesStyles,
} from '@/pages/Actives/Actives.Styles';
import { AppStateType } from '@/store';
import { ThemeType } from '@/store/app/reducers';
import { getPackets } from '@/store/packets/actions';
import { PacketsReducerState } from '@/store/packets/reducers';

export interface ActivesProps {
  packets: PacketsReducerState;
  getPackets: () => void;
  themeType: ThemeType;
}

const Actives: React.FC<ActivesProps> = (props: ActivesProps) => {
  const { packets, getPackets, themeType } = props;
  const { t } = useTranslation();

  useEffect(() => {
    if (!packets.packets.loaded) {
      getPackets();
    }
  }, [packets, getPackets]);

  return (
    <MainContent className={`content-main actives-page -${themeType}`}>
      <Container>
        <Title className={`-${themeType}`}>{t('actives.titles.my_actives')}</Title>
        <ActivesStyles className={`actives -${themeType}`}>
          <ActivesHeader className={`actives__header -${themeType}`}>
            <SubTitle className={`actives__title -${themeType}`}>
              {t('actives.titles.active_contracts')}
            </SubTitle>
            {packets.packets.list.length ? (
              <Link to="/actives/contract_history" className="actives__link">
                {t('actives.texts.history_contracts')}
              </Link>
            ) : null}
          </ActivesHeader>
          <ActivesContracts className="active-contracts">
            {packets.packets.list.length ? (
              packets.packets.list.map((packet: Packet, index: number) => (
                <ActivesContractsItem
                  key={`actives-packet-${index + 1}`}
                  className={`active-contracts__item -${themeType}`}
                >
                  <div className="active-contracts__header">
                    <p className="active-contracts__percent">
                      {`${packet.amount_usd} FLEX`}
                      {' '}
                      <span>
                        {`+${(Number(packet.percent) * Number(packet.days_open)).toFixed(2)}%`}
                      </span>
                    </p>
                    <span className="active-contracts__easy">{packet.name}</span>
                  </div>
                  <div className="active-contracts__info-items">
                    <InfoItem
                      className=""
                      infoText={t('actives.contracts_info.invested')}
                      infoValue={`${packet.amount_usd} FLEX`}
                    />
                    <InfoItem
                      className=""
                      infoText={t('actives.contracts_info.days')}
                      infoValue={`${packet.days_left} ${t(
                        `common.days.${papulate(packet.days_left)}`
                      )}`}
                    />
                    <InfoItem
                      className=""
                      infoText={t('actives.contracts_info.date')}
                      infoValue={selfMoment(packet.created)
                        .add(packet.days, 'days')
                        .format('DD.MM.YYYY')}
                    />
                    <InfoItem
                      className="total"
                      infoText={t('actives.contracts_info.total')}
                      infoValue={`${(
                        packet.amount_usd *
                        ((packet.percent / 100) * packet.days_open)
                      ).toFixed(2)} FLEX`}
                    />
                  </div>
                </ActivesContractsItem>
              ))
            ) : (
              <Placeholder className={`actives-placeholder -${themeType}`}>
                <p>{t('actives.table.placeholder.title')}</p>
                <span>{t('actives.table.placeholder.text')}</span>
              </Placeholder>
            )}
          </ActivesContracts>
          {/* <ActivesHeader className={`actives__header -${themeType}`}>*/}
          {/*  <SubTitle className={`actives__title -${themeType}`}>{t('actives.titles.active_products')}</SubTitle>*/}
          {/*  <Link to='/actives/purchase_history' className='actives__link'>*/}
          {/*    {t('actives.texts.history_products')}*/}
          {/*  </Link>*/}
          {/* </ActivesHeader>*/}
          {/* <ActivesProducts className={`active-products -${themeType}`}>*/}
          {/*  <ActivesProductsItem className={`active-products__item -${themeType}`}>*/}
          {/*    <h4 className='active-products__title'>Iphone 12 pro max</h4>*/}
          {/*    <div className='active-products__info-items'>*/}
          {/*      <InfoItem*/}
          {/*        className=''*/}
          {/*        infoText={t('actives.products_info.price')}*/}
          {/*        infoValue='40 550 FLEX'*/}
          {/*      />*/}
          {/*      <InfoItem*/}
          {/*        className=''*/}
          {/*        infoText={t('actives.products_info.date')}*/}
          {/*        infoValue='25.09.2021'*/}
          {/*      />*/}
          {/*    </div>*/}
          {/*    <Button className={`active-products__btn md -${themeType}`}>{t('actives.btns.receive_btn')}</Button>*/}
          {/*  </ActivesProductsItem>*/}
          {/*  <ActivesProductsItem className={`active-products__item -${themeType}`}>*/}
          {/*    <h4 className='active-products__title'>macbook air m1 2021</h4>*/}
          {/*    <div className='active-products__info-items'>*/}
          {/*      <InfoItem*/}
          {/*        className=''*/}
          {/*        infoText={t('actives.products_info.price')}*/}
          {/*        infoValue='40 550 FLEX'*/}
          {/*      />*/}
          {/*      <InfoItem*/}
          {/*        className=''*/}
          {/*        infoText={t('actives.products_info.date')}*/}
          {/*        infoValue='25.09.2021'*/}
          {/*      />*/}
          {/*    </div>*/}
          {/*    <span className='active-products__available'>{t('actives.texts.available')}</span>*/}
          {/*  </ActivesProductsItem>*/}
          {/*  <ActivesProductsItem className={`active-products__item -${themeType}`}>*/}
          {/*    <h4 className='active-products__title'>Tesla model x</h4>*/}
          {/*    <div className='active-products__info-items'>*/}
          {/*      <InfoItem*/}
          {/*        className=''*/}
          {/*        infoText={t('actives.products_info.price')}*/}
          {/*        infoValue='40 550 FLEX'*/}
          {/*      />*/}
          {/*      <InfoItem*/}
          {/*        className=''*/}
          {/*        infoText={t('actives.products_info.date')}*/}
          {/*        infoValue='25.09.2021'*/}
          {/*      />*/}
          {/*    </div>*/}
          {/*    <span className='active-products__available'>{t('actives.texts.available')}</span>*/}
          {/*  </ActivesProductsItem>*/}
          {/*  <ActivesProductsItem className={`active-products__item -${themeType}`}>*/}
          {/*    <h4 className='active-products__title'>Iphone 12 pro max</h4>*/}
          {/*    <div className='active-products__info-items'>*/}
          {/*      <InfoItem*/}
          {/*        className=''*/}
          {/*        infoText={t('actives.products_info.price')}*/}
          {/*        infoValue='40 550 FLEX'*/}
          {/*      />*/}
          {/*      <InfoItem*/}
          {/*        className=''*/}
          {/*        infoText={t('actives.products_info.date')}*/}
          {/*        infoValue='25.09.2021'*/}
          {/*      />*/}
          {/*    </div>*/}
          {/*    <Button className={`active-products__btn md -${themeType}`}>{t('actives.btns.receive_btn')}</Button>*/}
          {/*  </ActivesProductsItem>*/}
          {/*  <ActivesProductsItem className={`active-products__item -${themeType}`}>*/}
          {/*    <h4 className='active-products__title'>macbook air m1 2021</h4>*/}
          {/*    <div className='active-products__info-items'>*/}
          {/*      <InfoItem*/}
          {/*        className=''*/}
          {/*        infoText={t('actives.products_info.price')}*/}
          {/*        infoValue='40 550 FLEX'*/}
          {/*      />*/}
          {/*      <InfoItem*/}
          {/*        className=''*/}
          {/*        infoText={t('actives.products_info.date')}*/}
          {/*        infoValue='25.09.2021'*/}
          {/*      />*/}
          {/*    </div>*/}
          {/*    <span className='active-products__available'>{t('actives.texts.available')}</span>*/}
          {/*  </ActivesProductsItem>*/}
          {/*  <ActivesProductsItem className={`active-products__item -${themeType}`}>*/}
          {/*    <h4 className='active-products__title'>Tesla model x</h4>*/}
          {/*    <div className='active-products__info-items'>*/}
          {/*      <InfoItem*/}
          {/*        className=''*/}
          {/*        infoText={t('actives.products_info.price')}*/}
          {/*        infoValue='40 550 FLEX'*/}
          {/*      />*/}
          {/*      <InfoItem*/}
          {/*        className=''*/}
          {/*        infoText={t('actives.products_info.date')}*/}
          {/*        infoValue='25.09.2021'*/}
          {/*      />*/}
          {/*    </div>*/}
          {/*    <span className='active-products__available'>{t('actives.texts.available')}</span>*/}
          {/*  </ActivesProductsItem>*/}
          {/* </ActivesProducts>*/}
        </ActivesStyles>
      </Container>
    </MainContent>
  );
};

const mapState = (state: AppStateType) => {
  const { packets, app } = state;
  return {
    packets,
    themeType: app.theme,
  };
};

export default connect(mapState, { getPackets })(Actives);
