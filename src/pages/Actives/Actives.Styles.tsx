import styled from 'styled-components';

export const ActivesStyles: any = styled.div`
  padding-right: 35px;

  &.-light {
    .actives__link {
      color: var(--brown);
    }
  }

  @media (max-width: 992px) {
    padding-right: 0;
  }
`;

export const ActivesHeader: any = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 45px;

  .actives__title {
    margin: 0;
  }

  .actives__link {
    font-weight: bold;
    font-size: 18px;
    line-height: 22px;
    text-decoration: underline;
  }

  &.-light {
    .actives__link {
      color: var(--brown);
    }
  }

  @media (max-width: 768px) {
    margin-bottom: 25px;

    .actives__link {
      font-size: 14px;
      line-height: 17px;
    }
  }

  @media (max-width: 374px) {
    .actives__title,
    .actives__link {
      font-size: 12px;
      line-height: 14px;
    }
  }
`;

export const ActivesContracts: any = styled.section`
  margin-bottom: 25px;
  display: flex;
  flex-wrap: wrap;
  margin: 0 -20px;

  @media (max-width: 1200px) {
    margin: 0 -8px;
  }

  @media (max-width: 992px) {
    margin: 0 -16px 45px 0;
    overflow-x: auto;
    flex-wrap: nowrap;
    scrollbar-width: none;

    ::-webkit-scrollbar {
      display: none;
    }
  }
`;

export const ActivesContractsItem: any = styled.article`
  width: calc(100% / 3 - 40px);
  min-width: 300px;
  margin: 0 20px 40px;
  padding: 40px 25px;
  background-color: var(--dark);
  border-radius: var(--border-radius);

  &:last-child {
    margin-right: 0;
  }

  .active-contracts__header {
    display: flex;
    justify-content: space-between;
    margin-bottom: 25px;
  }

  .active-contracts__percent {
    margin: 0;
    font-size: 25px;
    line-height: 30px;
    color: #e9b9e9;
    font-weight: bold;

    &.light {
      color: #f1f1f1;
    }

    span {
      font-weight: 500;
      font-size: 25px;
      line-height: 30px;
      background-image: linear-gradient(180deg, #5cf9dd 0%, #10b698 100%);
      -webkit-background-clip: text;
      -webkit-text-fill-color: transparent;
    }
  }

  .active-contracts__easy {
    font-weight: bold;
    font-size: 14px;
    line-height: 17px;
    text-transform: uppercase;
    background-image: linear-gradient(180deg, rgba(226, 174, 255, 0.66) 0%, #fc54ff 100%);
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;

    &.light {
      background-image: linear-gradient(180deg, rgba(255, 255, 255, 0.66) 0%, #9a9a9a 100%);
      -webkit-background-clip: text;
      -webkit-text-fill-color: transparent;
    }
  }

  .info-item {
    margin-bottom: 20px;

    &:last-child {
      margin-bottom: 0;
    }

    &:first-child {
      .info-item__divider {
        background-image: linear-gradient(
          -90deg,
          #dd59f8 0%,
          rgba(255, 255, 255, 0.06) 71.01%,
          rgba(255, 255, 255, 0) 99.52%
        );
      }
    }

    &__text {
      font-weight: 300;
      font-size: 14px;
      line-height: 17px;
      color: rgba(255, 255, 255, 0.53);
    }

    &__value {
      font-weight: 700;
      font-size: 14px;
      line-height: 17px;
      color: rgba(255, 255, 255, 0.53);
    }
  }

  .info-item.total {
    margin-top: 30px;

    .info-item__text {
      color: var(--white);
    }

    .info-item__divider {
      display: none;
    }

    .info-item__value {
      font-weight: 700;
      font-size: 18px;
      line-height: 22px;
      color: #ffedff;
    }
  }

  &.-light {
    background: #fafafa;
    box-shadow: 0 0 11px rgba(0, 0, 0, 0.04);

    .active-contracts__percent {
      color: #8569f9;

      span {
        background-image: linear-gradient(180deg, #00d6af 0%, #10b698 100%);
      }
    }

    .active-contracts__easy {
      background-image: linear-gradient(180deg, #8569f9 0%, #b7a7fc 100%);
    }

    .info-item {
      &__text {
        color: var(--light-brown);
      }

      &__value {
        color: var(--brown);
      }
    }

    .info-item.total {
      .info-item__text {
        font-weight: 700;
        color: var(--brown);
      }

      .info-item__value {
        color: #774bff;
      }
    }
  }

  @media (max-width: 1600px) {
    width: calc(100% / 2 - 40px);
  }

  @media (max-width: 1200px) {
    width: calc(100% - 16px);
    margin: 0 8px 30px;
  }

  @media (max-width: 992px) {
    width: 100%;
    margin: 0 16px 0 0;
    padding: 28px 20px;

    .active-contracts__header {
      flex-direction: column;
    }

    .active-contracts__percent {
      font-size: 20px;
      line-height: 24px;
      margin-bottom: 7px;
    }

    .active-contracts__easy {
      display: block;
      color: var(--white);
      background-image: linear-gradient(0deg, #ffffff, #ffffff);
    }

    .info-item {
      &__text {
        font-size: 12px;
        line-height: 15px;
      }

      &__value {
        font-size: 12px;
        line-height: 15px;
      }
    }

    .info-item.total {
      margin-top: 20px;

      .info-item__text {
        font-size: 14px;
        line-height: 17px;
      }

      .info-item__value {
        font-size: 15px;
        line-height: 18px;
      }
    }
  }
`;

export const ActivesProducts: any = styled.section`
  display: flex;
  flex-wrap: wrap;
  margin: 0 -20px;

  @media (max-width: 1200px) {
    margin: 0 -8px;
  }
`;

export const ActivesProductsItem: any = styled.article`
  width: calc(100% / 3 - 40px);
  margin: 0 20px 40px;
  padding: 30px 35px;
  background-color: var(--dark);
  border-radius: var(--border-radius);

  .active-products__title {
    margin: 0 0 21px;
    font-weight: 400;
    font-size: 25px;
    line-height: 30px;
    text-transform: uppercase;
    color: #eaeaea;
  }

  .info-item {
    margin-bottom: 20px;

    &:last-child {
      margin-bottom: 0;
    }

    &__text {
      font-weight: 300;
      font-size: 14px;
      line-height: 17px;
      color: rgba(255, 255, 255, 0.53);
    }

    &__divider {
      background-image: linear-gradient(
        -90deg,
        #dd59f8 0%,
        rgba(255, 255, 255, 0.06) 71.01%,
        rgba(255, 255, 255, 0) 99.52%
      );
    }

    &__value {
      font-weight: 700;
      font-size: 14px;
      line-height: 17px;
      color: rgba(255, 255, 255, 0.53);
    }
  }

  .active-products__btn {
    display: block;
    padding: 10px 35px;
    font-weight: 700;
    margin-top: 26px;
  }

  .active-products__available {
    display: block;
    margin-top: 34px;
    font-weight: 700;
    font-size: 14px;
    line-height: 17px;
    color: rgba(255, 255, 255, 0.85);
  }

  &.-light {
    background-color: #f3f7ff;
    box-shadow: 0 0 11px rgba(0, 0, 0, 0.04);

    .active-products__title {
      color: var(--brown);
    }

    .info-item {
      &__text {
        color: var(--dark-blue);
      }

      &__value {
        color: var(--brown);
      }
    }

    .active-products__available {
      color: var(--brown);
    }
  }

  @media (max-width: 1600px) {
    width: calc(100% / 2 - 40px);
  }

  @media (max-width: 1200px) {
    width: calc(100% - 16px);
    margin: 0 8px 30px;
  }

  @media (max-width: 992px) {
    .info-item {
      &__text {
        font-size: 12px;
        line-height: 15px;
      }

      &__value {
        font-size: 12px;
        line-height: 15px;
      }
    }
  }

  @media (max-width: 768px) {
    padding: 28px 22px;

    .active-products__title {
      font-weight: 300;
      font-size: 18px;
      line-height: 24px;
    }

    .active-products__info {
      margin-bottom: 18px;
    }

    .active-products__btn {
      margin-top: 19px;
      padding: 5px 40px;
      line-height: 16px;
    }

    .active-products__available {
      margin-top: 29px;
      font-size: 11px;
      line-height: 13px;
    }
  }
`;
