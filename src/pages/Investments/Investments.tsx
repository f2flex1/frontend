import moment from 'moment-timezone';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';

import { Contract } from '@/api';
import history from '@/common/utils/history';
import { papulate } from '@/common/utils/papulate';
import {
  Container,
  MainContent,
  ScrollBar,
  SecondaryButton,
  SubTitle,
  Title,
} from '@/components/elements';
import { ProductComponent } from '@/components/elements/ProductComponent/ProductComponent';
import { PROGRAMS_ACTIVE_DATE } from '@/const/app.constants';
import { PATHS } from '@/const/paths.constants';
import { ProfitabilityCommercial } from '@/pages/Home/Home';
import { Products, ProductsContainer } from '@/pages/Home/Home.Styles';
import { Contracts, ContractsItem } from '@/pages/Investments/Investments.Style';
import { AppStateType } from '@/store';
import { ThemeType } from '@/store/app/reducers';
import { getContracts, setContract } from '@/store/packets/actions';
import { PacketsReducerState } from '@/store/packets/reducers';

const profitabilityCommercials: ProfitabilityCommercial[] = [
  {
    image: '/img/home/phone.png',
    imageClassName: 'phone',
    imageParams: { width: 255, height: 330 },
    title: 'MARKET',
    subTitle: 'home.products.title',
    valueText: 'home.products.text',
    value: '20 000',
    currency: 'FLEX',
    redirectUrl: PATHS.PROGRAMS_MARKET,
    description: 'home.products.text2',
    date: moment(PROGRAMS_ACTIVE_DATE.MARKET).format('DD.MM.YYYY'),
  },
  {
    image: '/img/home/auto.png',
    imageClassName: 'auto',
    imageParams: { width: 255, height: 330 },
    title: 'AUTO',
    subTitle: 'home.products.title',
    valueText: 'home.products.text',
    value: '400 000',
    currency: 'FLEX',
    redirectUrl: PATHS.PROGRAMS_AUTO,
    description: 'home.products.text2',
    date: moment(PROGRAMS_ACTIVE_DATE.AUTO).format('DD.MM.YYYY'),
  },
  {
    image: '/img/home/cashback.png',
    imageClassName: 'chk',
    imageParams: { width: 255, height: 330 },
    title: 'CASHBACK',
    subTitle: 'home.products.title',
    valueText: 'home.products.text',
    value: '1 500 000',
    currency: 'FLEX',
    redirectUrl: PATHS.PROGRAMS_CASHBACK,
    description: 'home.products.text2',
    date: moment(PROGRAMS_ACTIVE_DATE.CASHBACK).format('DD.MM.YYYY'),
  },
  {
    image: '/img/home/home.png',
    imageClassName: 'home',
    imageParams: { width: 255, height: 330 },
    title: 'HOME',
    subTitle: 'home.products.title',
    valueText: 'home.products.text',
    value: '1 890 000',
    currency: 'FLEX',
    redirectUrl: PATHS.PROGRAMS_HOME,
    description: 'home.products.text2',
    date: moment(PROGRAMS_ACTIVE_DATE.HOME).format('DD.MM.YYYY'),
  },
];

export interface InvestmentsProps {
  packets: PacketsReducerState;
  getContracts: () => void;
  setContract: any;
  themeType: ThemeType;
}

const Investments: React.FC<InvestmentsProps> = (props: InvestmentsProps) => {
  const { packets, getContracts, setContract, themeType } = props;
  const { contracts } = packets;
  const { t } = useTranslation();

  useEffect(() => {
    if (!contracts.loaded) {
      getContracts();
    }
  }, [contracts, getContracts]);

  const onContractClick = (contract: Contract) => {
    setContract(contract).then(() => history.push('/investments/create_contract'));
  };

  return (
    <MainContent className={`content-main content-main_bg-1 investments-page -${themeType}`}>
      <Container>
        <Title className={`-${themeType}`}>{t('investments.titles.investments')}</Title>
        <Contracts className={`contracts -${themeType}`}>
          <SubTitle className={`-${themeType}`}>{t('investments.titles.contracts')}</SubTitle>
          <div className="contracts__wrapper">
            <ScrollBar className={`-${themeType}`}>
              <div className="contracts__container">
                {contracts.list.length
                  ? contracts.list.map((contract: Contract, index: number) => (
                    <ContractsItem
                      key={`investments-packet-${index + 1}`}
                      className={`contracts__item contracts-item -${themeType}`}
                    >
                      <h3 className="contracts-item__title">{contract.name}</h3>
                      <ul className="contracts-item__list">
                        <li className="contracts-item__list-item">
                          <span className="contracts-item__left">
                            {t('investments.texts.deposit')}
                          </span>
                          <span className="contracts-item__right">{`${contract.amount_usd} FLEX`}</span>
                        </li>
                        <li className="contracts-item__list-item">
                          <span className="contracts-item__left">
                            {t('investments.texts.clean_profit')}
                          </span>
                          <span className="contracts-item__right">
                            {`${(
                              contract.amount_usd *
                                ((contract.daily_percents / 100) * contract.days)
                            ).toFixed(2)} FLEX`}
                          </span>
                        </li>
                        <li className="contracts-item__list-item">
                          <span className="contracts-item__left">
                            {t('investments.texts.investment_term')}
                          </span>
                          <span className="contracts-item__right">
                            {`${contract.days} ${t(`common.days.${papulate(contract.days)}`)}`}
                          </span>
                        </li>
                        <li className="contracts-item__list-item">
                          <span className="contracts-item__left">
                            {t('investments.texts.daily_revenue')}
                          </span>
                          <span className="contracts-item__right">{`${contract.daily_percents} %`}</span>
                        </li>
                      </ul>
                      <span className="contracts-item__total">{`${contract.amount_usd} FLEX`}</span>
                      <SecondaryButton
                        className={`sm -${themeType}`}
                        onClick={() => onContractClick(contract)}
                      >
                        {t('investments.btns.invest_btn')}
                      </SecondaryButton>
                    </ContractsItem>
                  ))
                  : null}
              </div>
            </ScrollBar>
          </div>
        </Contracts>

        <Products className={`products investments-products -${themeType}`}>
          <SubTitle className={`-${themeType}`}>{t('investments.titles.products')}</SubTitle>
          <ScrollBar className={`-${themeType}`}>
            <ProductsContainer className="products__container">
              {profitabilityCommercials.map((item: ProfitabilityCommercial, index: number) => (
                <ProductComponent
                  themeType={themeType}
                  item={item}
                  key={`investment-profitability-commercial-${index + 1}`}
                />
              ))}
            </ProductsContainer>
          </ScrollBar>
        </Products>
      </Container>
    </MainContent>
  );
};

const mapState = (state: AppStateType) => {
  const { packets, app } = state;
  return {
    packets,
    themeType: app.theme,
  };
};

export default connect(mapState, { getContracts, setContract })(Investments);
