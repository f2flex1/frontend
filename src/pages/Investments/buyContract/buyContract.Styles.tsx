import styled from 'styled-components';

export const BuyContractStyles: any = styled.div`
  position: relative;
  display: flex;
  align-items: flex-start;

  .back {
    position: absolute;
    top: 0;
    left: 0;
  }

  .modal {
    width: 100%;
  }

  .info-list {
    width: 100%;
  }

  .info-item {
    margin-bottom: 16px;

    &:last-child {
      margin-bottom: 0;
    }

    &__text {
      font-family: var(--poppins);
      font-weight: 700;
      font-size: 12px;
      line-height: 18px;
    }

    &__value {
      font-weight: 700;
      font-size: 14px;
      line-height: 17px;
      color: var(--white);
    }
  }

  .info-item.price {
    margin-bottom: 20px;

    .info-item__text {
      font-family: var(--montserrat);
      font-weight: 700;
      font-size: 12px;
      line-height: 15px;
      color: #989898;
    }

    .info-item__divider {
      display: none;
    }

    .info-item__value {
      font-weight: 700;
      font-size: 14px;
      line-height: 17px;
      color: #ff89cb;
    }
  }

  .modal__subtitle {
    margin: 38px 0 13px;
  }

  &.-light {
    .info-item {
      &__text {
        color: var(--brown);
      }

      &__value {
        color: var(--brown);
      }
    }

    .info-item.price {
      .info-item__text {
        color: var(--brown);
      }

      .info-item__value {
        background-image: linear-gradient(180deg, #8569f9 0%, #b7a7fc 100%);
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
        color: #774bff;
      }
    }

    .info-item.old {
      .info-item__value {
        color: var(--brown);
      }
    }
  }

  @media (max-width: 1200px) {
    .back {
      display: none;
    }
  }
`;
