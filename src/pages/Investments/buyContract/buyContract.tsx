import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import history from '@/common/utils/history';
import { selfMoment } from '@/common/utils/i18n';
import BreadCrumb from '@/components/BreadCrumb';
import {
  Button,
  Container,
  InfoItem,
  Loader,
  MainContent,
  PageModal,
  SecondaryButton,
} from '@/components/elements';
import { ModalPrice, ModalTitle } from '@/components/elements/PageModal/PageModal.Styles';
import { CURRENCIES, INVESTMENTS_RULES } from '@/const/app.constants';
import { BuyContractStyles } from '@/pages/Investments/buyContract/buyContract.Styles';
import { AppStateType } from '@/store';
import types from '@/store/actionTypes';
import { ThemeType } from '@/store/app/reducers';
import { setUnderstand } from '@/store/loadingsErrors/actions';
import { selectLoadingByKey } from '@/store/loadingsErrors/selectors';
import { getMessages } from '@/store/messages/actions';
import { getOperations } from '@/store/operations/actions';
import { createPacket, getPackets } from '@/store/packets/actions';
import { PacketsReducerState } from '@/store/packets/reducers';
import { fetchUserData } from '@/store/user/actions';
import { getWalletStat } from '@/store/wallet/actions';
import { WalletReducerState } from '@/store/wallet/reducers';

export interface InvestmentsProps {
  packets: PacketsReducerState;
  wallet: WalletReducerState;
  createPacket: any;
  themeType: ThemeType;
  getMessages: () => void;
  fetchUserData: () => void;
  getPackets: () => void;
  getOperations: () => void;
  getWalletStat: () => void;
  setUnderstand: (message: string | null) => void;
  loading: boolean;
}

const BuyContract: React.FC<InvestmentsProps> = (props: InvestmentsProps) => {
  const {
    packets,
    wallet,
    createPacket,
    themeType,
    fetchUserData,
    getMessages,
    getPackets,
    getOperations,
    getWalletStat,
    setUnderstand,
    loading,
  } = props;
  const { activeContract, buyContractError } = packets;
  const { t } = useTranslation();

  useEffect(() => {
    if (!wallet.loaded) {
      getWalletStat();
    }
  }, [wallet, getWalletStat]);

  useEffect(() => {
    if (!activeContract) history.push('/investments/create_contract');
  }, [activeContract]);

  useEffect(() => {
    if (buyContractError === 'SUCCESS') {
      fetchUserData();
      getMessages();
      getPackets();
      getOperations();
      getWalletStat();
      history.push('/investments/create_contract/buy_contract_success');
    }
  }, [buyContractError]);

  const getRatesSumm = (sum: string, currency: string, returnedCurrency: string) => {
    switch (`${currency}${returnedCurrency}`) {
      case `${CURRENCIES.USDT}${CURRENCIES.BTC}`:
        return Number(sum)
          ? (Number(sum) / Number(wallet.exchangeRates.BTCUSDT || 1)).toFixed(4)
          : '0';
      case `${CURRENCIES.BTC}${CURRENCIES.USDT}`:
        return Number(sum)
          ? (Number(sum) * Number(wallet.exchangeRates.BTCUSDT || 1)).toFixed(2)
          : '0';
      default:
        return `${sum || 0}`;
    }
  };

  const summTest = (amount_usd?: number) => {
    let testSuccess = true;
    INVESTMENTS_RULES.forEach((rule: any) => {
      if (
        selfMoment().unix() > selfMoment(rule.from_date).unix() &&
        selfMoment().unix() < selfMoment(rule.to_date).unix()
      ) {
        const summ: number = Number(wallet.stat?.invested || 0) + Number(amount_usd || 0);
        if (summ > rule.max_value) {
          testSuccess = false;
        }
      }
    });
    return testSuccess;
  };

  const onSubmit = () => {
    if (summTest(activeContract?.amount_usd)) {
      createPacket({ amount_usd: activeContract?.amount_usd });
    } else {
      setUnderstand(t('investments.modal.summ.message'));
    }
  };

  return (
    <MainContent className={`content-main content-main_bg-2 -${themeType}`}>
      <Container>
        <BreadCrumb />
        <BuyContractStyles className={`-${themeType}`}>
          <SecondaryButton
            as={Link}
            className={`sm back -${themeType}`}
            to="/investments/create_contract"
          >
            {t('common.back')}
          </SecondaryButton>
          <PageModal className={`-${themeType}`}>
            <ModalTitle className="modal__title">{t('investments.titles.buy_contract')}</ModalTitle>
            <ul className="info-list">
              <InfoItem
                className="price"
                infoText={t('investments.texts.contract_price')}
                infoValue={`${activeContract?.amount_usd} FLEX`}
              />
              <InfoItem
                className=""
                infoText="USDT"
                infoValue={getRatesSumm(
                  `${activeContract?.amount_usd || 0}`,
                  CURRENCIES.USDT,
                  CURRENCIES.USDT
                )}
              />
              <InfoItem
                className=""
                infoText="BTC"
                infoValue={getRatesSumm(
                  `${activeContract?.amount_usd || 0}`,
                  CURRENCIES.USDT,
                  CURRENCIES.BTC
                )}
              />
            </ul>
            <ModalTitle className="modal__subtitle">{t('investments.texts.sum')}</ModalTitle>
            <ModalPrice className="modal__price">
              <span>{activeContract?.amount_usd}</span>
              {' FLEX'}
            </ModalPrice>
            <Button className="md" onClick={onSubmit} disabled={loading}>
              {t('investments.btns.pay_btn')}
              {loading ? <Loader /> : null}
            </Button>
          </PageModal>
        </BuyContractStyles>
      </Container>
    </MainContent>
  );
};

const mapState = (state: AppStateType) => {
  const { packets, wallet, app } = state;
  return {
    packets,
    wallet,
    themeType: app.theme,
    loading: selectLoadingByKey(state, types.CREATE_PACKET_REQUEST),
  };
};

export default connect(mapState, {
  createPacket,
  fetchUserData,
  getMessages,
  getPackets,
  getOperations,
  getWalletStat,
  setUnderstand,
})(BuyContract);
