import styled from 'styled-components';

export const Contracts: any = styled.section`
  margin-bottom: 25px;

  .contracts__wrapper {
    margin-right: -40px;
  }

  .contracts__container {
    display: flex;
    padding-top: 10px;
    width: 100%;
  }

  @media (max-width: 992px) {
    margin-bottom: 30px;

    .contracts__wrapper {
      margin-right: -16px;
    }
  }
`;

export const ContractsItem: any = styled.article`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: calc(100% / 6 - 30px);
  min-width: 220px;
  margin: 0 30px 30px 0;
  padding: 25px 13px;
  background: var(--dark);
  border-radius: var(--border-radius);
  transition: transform 0.3s ease-in-out;
  cursor: pointer;

  &:hover {
    transform: translateY(-7px);
  }

  &:last-child {
    margin-right: 0;
  }

  .contracts-item__title {
    margin: 0 0 15px;
    font-weight: 700;
    font-size: 20px;
    line-height: 24px;
    text-transform: uppercase;
  }

  .contracts-item__list {
    display: flex;
    flex-direction: column;
  }

  .contracts-item__list-item {
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 18px;
    font-size: 12px;
    line-height: 15px;

    &:last-child {
      margin-bottom: 21px;
    }
  }

  .contracts-item__left {
    color: #d1d1d1;
  }

  .contracts-item__right {
    font-weight: 700;
    text-align: right;
  }

  .contracts-item__total {
    margin: 0 0 5px;
    font-weight: 700;
    font-size: 20px;
    line-height: 24px;
    text-align: center;
    text-transform: uppercase;
    background: linear-gradient(180deg, rgba(226, 174, 255, 0.66) 0%, #fc54ff 100%);
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
  }

  &.-light {
    background: #fafafa;
    box-shadow: 0 0 11px rgba(0, 0, 0, 0.04);

    .contracts-item__title {
      color: #8569f9;
    }

    .contracts-item__left {
      color: var(--light-brown);
    }

    .contracts-item__right {
      color: var(--brown);
    }

    .contracts-item__total {
      background-image: linear-gradient(45.04deg, #ffd6ef -35.82%, #df45a3 100.3%);
      -webkit-background-clip: text;
      -webkit-text-fill-color: transparent;
      color: #774bff;
    }
  }

  @media (max-width: 992px) {
    width: calc(100% - 10px);
    margin: 0 10px 0 0;
  }
`;
