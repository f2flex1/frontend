import find from 'lodash/find';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { Contract, Packet } from '@/api';
import history from '@/common/utils/history';
import { selfMoment } from '@/common/utils/i18n';
import { papulate } from '@/common/utils/papulate';
import BreadCrumb from '@/components/BreadCrumb';
import {
  Button,
  Container,
  DropDown,
  InfoItem,
  Input,
  MainContent,
  Placeholder,
  ScrollBar,
  Title,
} from '@/components/elements';
import { Create } from '@/pages/Investments/createContract/createContract.Styles';
import { AppStateType } from '@/store';
import { ThemeType } from '@/store/app/reducers';
import { getContracts, getPackets, setContract } from '@/store/packets/actions';
import { PacketsReducerState } from '@/store/packets/reducers';
import { UserReducerState } from '@/store/user/reducers';

export interface InvestmentsProps {
  packets: PacketsReducerState;
  getContracts: () => void;
  getPackets: () => void;
  setContract: any;
  themeType: ThemeType;
  user: UserReducerState;
}

const CreateContract: React.FC<InvestmentsProps> = (props: InvestmentsProps) => {
  const { packets, getContracts, getPackets, setContract, themeType, user } = props;
  const { contracts, activeContract } = packets;
  const { userData } = user;
  const { t } = useTranslation();
  const [contractsList, setContractsList] = useState<string[]>([]);
  const [selectedContract, setSelectedContract] = useState<Contract | null>(activeContract || null);

  useEffect(() => {
    if (!packets.contracts.loaded) {
      getContracts();
    } else {
      const newContractsList: any[] = contracts.list.map((contract: Contract) => contract.name);
      setContractsList(newContractsList);
    }

    if (!packets.packets.loaded) {
      getPackets();
    }

    if (!selectedContract && !!contracts.list.length) {
      setSelectedContract(contracts.list[0]);
    }
  }, [packets, selectedContract, getContracts, getPackets, setSelectedContract]);

  const onChange = (name: string, value: string) => {
    const contract: Contract | null = find(contracts.list, { name: value }) || null;
    setSelectedContract(contract);
  };

  const onCreate = () => {
    setContract(selectedContract).then(() =>
      history.push('/investments/create_contract/buy_contract')
    );
  };

  return (
    <MainContent className={`content-main -${themeType}`}>
      <Container>
        <BreadCrumb />
        <Create className={`create -${themeType}`}>
          <div className="create__options">
            <p className="create__options-text">{t('investments.texts.select_type')}</p>
            <DropDown
              className={`create__options-dropdown lg -${themeType}`}
              name="contract"
              value={`${selectedContract?.name}`}
              placeholder={t('investments.dropdown.placeholder')}
              list={contractsList}
              disableUnderline
              fullWidth
              onChange={onChange}
            />
            <Input
              className={`create__options-input lg bold -${themeType}`}
              type="text"
              name="text"
              disabled
              value={`${selectedContract?.amount_usd || 0} FLEX`}
            />
            <div className="info-list">
              <InfoItem
                infoText={t('investments.texts.investment_period')}
                infoValue={`${selectedContract?.days || 0} ${t(
                  `common.days.${papulate(selectedContract?.days || 0)}`
                )}`}
              />
              <InfoItem
                infoText={t('investments.texts.profitability')}
                infoValue={`${selectedContract?.daily_percents || 0}%`}
              />
              <InfoItem
                infoText={t('investments.texts.profit_accrual')}
                infoValue={t('investments.texts.daily')}
              />
              <InfoItem
                className="deposit"
                infoText={t('investments.texts.your_deposit')}
                infoValue={`${selectedContract?.amount_usd || 0} FLEX`}
              />
            </div>
            <Button
              className={`xl width -${themeType}`}
              onClick={onCreate}
              disabled={Number(userData?.balance_usd) < Number(selectedContract?.amount_usd)}
            >
              {t('investments.btns.create_contract_btn')}
            </Button>

            {Number(userData?.balance_usd) < Number(selectedContract?.amount_usd) ? (
              <div className="create__warning">
                <p>{t('finances.create.error')}</p>
                <Link to="/finances">{t('finances.create.go_to_finance')}</Link>
              </div>
            ) : null}
          </div>

          <div className="create__info">
            <Title className={`-${themeType}`}>{t('investments.titles.my_contracts')}</Title>

            {!packets.packets.list.length ? (
              <Placeholder className={`-${themeType}`}>
                <p>{t('investments.table.placeholder.title')}</p>
                <span>{t('investments.table.placeholder.text')}</span>
              </Placeholder>
            ) : (
              <div className="table">
                <div className="table__container">
                  <div className="table__header">
                    <div className="table__row">
                      <div className="table__col">{t('investments.table.header.col1')}</div>
                      <div className="table__col">{t('investments.table.header.col2')}</div>
                      <div className="table__col">{t('investments.table.header.col3')}</div>
                      <div className="table__col">{t('investments.table.header.col4')}</div>
                      <div className="table__col">{t('investments.table.header.col5')}</div>
                    </div>
                  </div>
                  <div className="table__body-wrapper">
                    <ScrollBar className={`-${themeType}`} style={{ maxHeight: '225px' }}>
                      <div className="table__body">
                        {packets.packets.list.length
                          ? packets.packets.list.map((packet: Packet, index: number) => (
                            <div className="table__row" key={`packets-${index + 1}`}>
                              <div className="table__col">{packet.name}</div>
                              <div className="table__col">{`${packet?.amount_usd} FLEX`}</div>
                              <div className="table__col">
                                {`${(
                                  packet.amount_usd *
                                    ((packet.percent / 100) * packet.days_open)
                                ).toFixed(2)} FLEX`}
                              </div>
                              <div className="table__col">
                                {`${packet?.percent}% ${t('investments.texts.in_day')}`}
                              </div>
                              <div className="table__col">
                                {selfMoment(packet.created).format('DD.MM.YYYY')}
                              </div>
                            </div>
                          ))
                          : null}
                      </div>
                    </ScrollBar>
                  </div>
                </div>
              </div>
            )}
          </div>
        </Create>
      </Container>
    </MainContent>
  );
};

const mapState = (state: AppStateType) => {
  const { user, packets, app } = state;
  return {
    packets,
    user,
    themeType: app.theme,
  };
};

export default connect(mapState, { getContracts, getPackets, setContract })(CreateContract);
