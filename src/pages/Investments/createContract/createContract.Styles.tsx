import styled from 'styled-components';

import { Button } from '@/components/elements';

export const Create = styled.section`
  display: flex;

  .create__options {
    width: 40%;
    min-width: 350px;
    margin-right: 70px;
  }

  .create__options-text {
    margin: 0 0 56px;
    font-size: 14px;
    line-height: 20px;
    color: var(--white);
  }

  .create__button {
    padding: 10px 14px;
  }

  .create__options-input {
    margin: 0 0 45px;
  }

  .create__info {
    width: 100%;
  }

  .create__warning {
    margin-top: 20px;
    text-align: center;
    font-size: 14px;
    line-height: 20px;

    p {
      margin: 0 0 5px;
    }

    a {
      font-weight: 700;
      text-decoration: underline;
    }
  }

  .info-list {
    margin: 0 0 30px;
  }

  .info-item {
    &__text {
      font-weight: 300;
      font-size: 14px;
      line-height: 17px;
      color: rgba(255, 255, 255, 0.53);
    }

    &__value {
      font-weight: 700;
      font-size: 14px;
      line-height: 17px;
      color: rgba(255, 255, 255, 0.53);
    }
  }

  .info-item.deposit {
    margin-top: 30px;

    .info-item__text {
      color: var(--white);
    }

    .info-item__divider {
      display: none;
    }

    .info-item__value {
      font-size: 18px;
      line-height: 22px;
      color: #ffedff;
    }
  }

  .table {
    border-radius: var(--border-radius);
    overflow: auto;
    scrollbar-width: none;

    ::-webkit-scrollbar {
      display: none;
    }
  }

  .table__container {
    background: #414753;
  }

  .table__header {
    padding: 15px 30px 0;
    font-weight: 700;
    font-size: 15px;
    line-height: 18px;
    color: #868686;
  }

  .table__body-wrapper {
    padding: 0 13px 15px 30px;
  }

  .table__body {
    padding-right: 20px;
    font-weight: 400;
    font-size: 15px;
    line-height: 18px;
    color: var(--white);
  }

  .table__row {
    display: flex;
    align-items: center;
    flex-shrink: 0;
  }

  .table__col {
    flex: 1 0 0;
    padding: 5px;
  }

  .table__header .table__row {
    padding: 18px 0;
  }

  .table__body .table__row {
    position: relative;
    padding: 30px 0;

    &::after {
      content: '';
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 2px;
      background-image: linear-gradient(3.98deg, #373a43 191.71%, rgba(0, 0, 0, 0.83) 282.29%);
      opacity: 0.19;
    }
  }

  .total {
    display: flex;
    justify-content: flex-end;
    margin: 45px 0;

    &__container {
      font-family: var(--poppins);
    }

    &__info {
      margin-bottom: 30px;
    }

    &__text {
      display: inline-block;
      margin-right: 50px;
      font-weight: 300;
      font-size: 20px;
      line-height: 30px;
      text-transform: uppercase;
      color: rgba(255, 255, 255, 0.53);
    }

    &__price {
      font-weight: bold;
      font-size: 18px;
      line-height: 27px;
      text-transform: capitalize;
      color: #ff89cb;
    }

    &__btn {
      font-family: var(--poppins);
    }
  }

  &.-light {
    .create__options-text {
      color: var(--brown);
    }

    .create__options-dropdown {
      .MuiSelect-selectMenu {
        color: var(--dark-blue);
      }

      .MuiSelect-icon {
        color: var(--dark-blue);
      }
    }

    .info-item {
      &__text {
        color: var(--light-brown);
      }

      &__value {
        color: #999999;
      }
    }

    .info-item.deposit {
      .info-item__text {
        color: var(--light-brown);
      }

      .info-item__value {
        color: #774bff;
      }
    }

    .table__container {
      background-color: var(--white);
      box-shadow: 0 0 11px rgba(0, 0, 0, 0.04);
    }

    .table__header {
      color: var(--light-brown);
    }

    .table__body {
      color: #999999;
    }

    .table__body .table__row {
      &::after {
        height: 1px;
        background-image: linear-gradient(3.98deg, #212121 191.71%, rgba(0, 0, 0, 0.83) 282.29%);
      }
    }
  }

  @media (max-width: 1700px) {
    .create__options {
      margin-right: 20px;
    }
  }

  @media (max-width: 1350px) {
    flex-direction: column;

    .table {
      margin-right: -40px;
      border-radius: var(--border-radius) 0 0 var(--border-radius);
      scrollbar-width: none;

      ::-webkit-scrollbar {
        display: none;
      }
    }

    .table__container {
      width: 1200px;
    }

    .create__options {
      width: 70%;
      margin: 0 0 50px;
    }
  }

  @media (max-width: 992px) {
    .table {
      margin-right: -16px;
    }

    .create__options-dropdown {
      .MuiSelect-icon {
        width: 24px;
        height: 24px;
      }
    }
  }

  @media (max-width: 768px) {
    .create__options {
      width: 100%;
      min-width: auto;
    }

    .create__button {
      padding: 12px;
    }

    .total {
      &__container {
        width: 100%;
      }

      &__info {
        display: flex;
        justify-content: space-between;
      }
    }
  }
`;

export const CreateOptions = styled.div`
  width: 40%;
  min-width: 350px;
  margin-right: 70px;

  @media (max-width: 1700px) {
    margin-right: 20px;
  }

  @media (max-width: 1350px) {
    width: 70%;
    margin: 0 0 50px;
  }

  @media (max-width: 768px) {
    width: 100%;
    min-width: auto;
  }
`;

export const CreateOptionsText = styled.p`
  margin: 0 0 56px;
  font-size: 14px;
  line-height: 20px;
  color: ${({ theme }) => theme.create.optionsText.color};
`;

export const CreateInfo = styled.div`
  width: 100%;
`;

export const Total = styled.div`
  display: flex;
  justify-content: flex-end;
  margin: 45px 0;
`;

export const TotalContainer = styled.div`
  font-family: var(--poppins);
`;

export const TotalInfo = styled.div`
  margin-bottom: 30px;
`;

export const TotalText = styled.span`
  display: inline-block;
  margin-right: 50px;
  font-weight: 300;
  font-size: 20px;
  line-height: 30px;
  text-transform: uppercase;
  color: ${({ theme }) => theme.total.text};
`;

export const TotalPrice = styled.span`
  font-weight: bold;
  font-size: 18px;
  line-height: 27px;
  background-image: ${({ theme }) => theme.total.price};
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
`;

export const TotalBtn = styled(Button)`
  font-family: var(--poppins);
  color: ${({ theme }) => theme.total.btn.color};
  background-color: ${({ theme }) => theme.total.btn.background};
  border-color: ${({ theme }) => theme.total.btn.border};

  &:focus {
    color: ${({ theme }) => theme.total.btn.color};
  }
`;
