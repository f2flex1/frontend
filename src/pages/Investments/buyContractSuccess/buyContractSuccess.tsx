import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import BreadCrumb from '@/components/BreadCrumb';
import { MainContent, PageModal, SecondaryButton, Container } from '@/components/elements';
import {
  ModalDescription,
  ModalIcon,
  ModalTitle,
} from '@/components/elements/PageModal/PageModal.Styles';
import { BuyContractStyles } from '@/pages/Investments/buyContract/buyContract.Styles';
import { AppStateType } from '@/store';
import { ThemeType } from '@/store/app/reducers';
import { setContract } from '@/store/packets/actions';
import { PacketsReducerState } from '@/store/packets/reducers';

export interface Props {
  packets: PacketsReducerState;
  setContract: any;
  themeType: ThemeType;
}

const BuyContractSuccess: React.FC<Props> = (props: Props) => {
  const { packets, themeType, setContract } = props;
  const { buyContractError } = packets;
  const { t } = useTranslation();

  useEffect(() => {
    if (buyContractError === 'SUCCESS') {
      setContract(null);
    }
  }, [buyContractError]);

  return (
    <MainContent className={`content-main content-main_bg-2 -${themeType}`}>
      <Container>
        <BreadCrumb />
        <BuyContractStyles className={`-${themeType}`}>
          <PageModal className={`-${themeType}`}>
            <ModalIcon className="modal__icon">
              <svg
                width="78"
                height="78"
                viewBox="0 0 78 78"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <circle
                  fill="none"
                  stroke="#2ABD7F"
                  strokeWidth="8"
                  cx="39"
                  cy="39"
                  r="35"
                  strokeDasharray="360"
                  strokeLinecap="round"
                  transform="rotate(-90) translate(-78 0)"
                >
                  <animate
                    attributeName="stroke-dashoffset"
                    values="360;0"
                    dur="3s"
                    repeatCount="1"
                    begin="0.1"
                  />
                </circle>
                <path
                  d="M38.9999 5C20.2223 5 5 20.2223 5 38.9999C5 57.7776 20.2223 72.9999 38.9999 72.9999C57.7776 72.9999 72.9999 57.7776 72.9999 38.9999C72.9799 20.2307 57.7693 5.02006 38.9999 5ZM57.7169 28.5741L33.4312 52.8598C32.4828 53.808 30.9455 53.808 29.9972 52.8598L20.2829 43.1455C19.3181 42.2137 19.2914 40.6763 20.2231 39.7116C21.1549 38.7468 22.6923 38.72 23.6571 39.6518C23.6773 39.6714 23.6972 39.6912 23.7169 39.7116L31.7142 47.7089L54.283 25.1401C55.2478 24.2084 56.7852 24.2351 57.7169 25.1998C58.6259 26.1409 58.6259 27.6329 57.7169 28.5741Z"
                  fill={themeType === 'light' ? '#6278AF' : '#D8E1F7'}
                />
              </svg>
            </ModalIcon>
            <ModalTitle className="modal__title">
              {t('investments.titles.payment_success')}
            </ModalTitle>
            <ModalDescription className="modal__description">
              {t('investments.texts.position_added')}
            </ModalDescription>
            <SecondaryButton as={Link} className={`md -${themeType}`} to="/actives">
              {t('investments.btns.my_actives')}
            </SecondaryButton>
          </PageModal>
        </BuyContractStyles>
      </Container>
    </MainContent>
  );
};

const mapState = (state: AppStateType) => {
  const { app, packets } = state;
  return {
    packets,
    themeType: app.theme,
  };
};

export default connect(mapState, { setContract })(BuyContractSuccess);
