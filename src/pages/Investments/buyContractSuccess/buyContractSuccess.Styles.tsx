import styled from 'styled-components';

export const BuyContractSuccessStyles: any = styled.div`
  display: flex;
  align-items: flex-start;

  .modal {
    width: 100%;
  }

  .info-list {
    width: 100%;
  }

  .modal__subtitle {
    margin: 38px 0 13px;
  }
`;
