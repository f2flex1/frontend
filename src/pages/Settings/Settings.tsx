import { validate, Network } from 'bitcoin-address-validation';
import cloneDeep from 'lodash/cloneDeep';
import React, { useEffect, useState, useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';

import { LogInParams, RegistrationParams, UserData, UserNewPassword } from '@/api';
import PhoneInput from '@/common/components/PhoneInput';
import { validateEmail } from '@/common/utils/validators';
import {
  Button,
  Input,
  Tabs,
  ReferralInput,
  Label,
  Container,
  MainContent,
  Title,
  Loader,
  Modal,
  SecondaryButton,
} from '@/components/elements';
import { TabsContentItem } from '@/components/elements/Tabs/Tabs.Styles';
import { UserInfoBlock, UserInfoImg } from '@/components/elements/UserInfo/UserInfo.Styles';
import {
  ALERT_TYPES,
  LEVEL_STYLES_TYPES,
  SETTINGS_TABS,
  SETTINGS_TABS_LIST,
} from '@/const/app.constants';
import { SITE_URL, STATIC_URL } from '@/const/general.constants';
import {
  SettingsPage,
  SettingsUser,
  SettingsFormBox,
  SettingsItem,
} from '@/pages/Settings/Settings.Styles';
import { AppStateType } from '@/store';
import types from '@/store/actionTypes';
import { ThemeType } from '@/store/app/reducers';
import { setAlert, setUnderstand } from '@/store/loadingsErrors/actions';
import { selectErrorByKey, selectLoadingByKey } from '@/store/loadingsErrors/selectors';
import { sendUserOtp, updateUserData, updateUserPassword } from '@/store/user/actions';
import { UserReducerState } from '@/store/user/reducers';

export interface SettingsProps {
  user: UserReducerState;
  error: string | null;
  loading: boolean;
  loadingOtp: boolean;
  loadingPassword: boolean;
  themeType?: ThemeType;
  updateUserData: (payload: any) => void;
  updateUserPassword: (payload: UserNewPassword) => void;
  sendUserOtp: () => void;
  setAlert: (message: string | null, messageType: string | null) => void;
  location: any;
  setUnderstand: (message: string | null) => void;
}

const Settings: React.FC<SettingsProps> = (props: SettingsProps) => {
  const {
    user,
    updateUserData,
    updateUserPassword,
    error,
    loading,
    loadingOtp,
    loadingPassword,
    themeType,
    sendUserOtp,
    setAlert,
    location,
    setUnderstand,
  } = props;
  const { loginErrors, userErrors } = user;

  const [modalChangeWalletOpened, setModalChangeWalletOpened] = useState<boolean>(false);
  const [timer, setTimer] = useState<number>(0);
  const [modalNewWalletOpened, setModalNewWalletOpened] = useState<boolean>(false);

  const hiddenFileInput = useRef<HTMLInputElement | null>(null);
  const { t } = useTranslation();

  const query = new URLSearchParams(location.search);
  const queryTab = query.get('tab') || '';

  const [values, setValues] = useState<{ [key: string]: string }>({
    tab: queryTab || SETTINGS_TABS.PROFILE,
    first_name: user.userData?.first_name || '',
    last_name: user.userData?.last_name || '',
    email: user.userData?.email || '',
    phone: user.userData?.phone || '',
    debit_address: user.userData?.debit_address || '',
    nickname: user.userData?.nickname || '',
  });
  const [errors, setErrors] = useState<{ [key: string]: string }>({});

  useEffect(() => {
    if (Object.keys(loginErrors).length) {
      const newErrors: UserNewPassword = {
        old_password: '',
        new_password: '',
      };
      Object.keys(loginErrors).forEach((key: string) => {
        newErrors[key] = loginErrors[key];
      });

      setErrors(newErrors);
    }

    if (Object.keys(userErrors).length) {
      const newErrors: { [key: string]: string } = {
        email: '',
        phone: '',
      };
      Object.keys(userErrors).forEach((key: string) => {
        newErrors[key] = userErrors[key];
      });

      setErrors(newErrors);
    }
  }, [loginErrors, userErrors, setErrors]);

  useEffect(() => {
    if (error) {
      setValues(prev => ({
        ...prev,
        old_password: '',
        new_password: '',
        re_new_password: '',
      }));
    }
  }, [error, setValues]);

  useEffect(() => {
    if (!values.email && !values.phone) {
      setValues(prev => ({
        ...prev,
        first_name: user.userData?.first_name || '',
        last_name: user.userData?.last_name || '',
        email: user.userData?.email || '',
        phone: user.userData?.phone || '',
        debit_address: user.userData?.debit_address || '',
        nickname: user.userData?.nickname || '',
      }));
    }

    if (
      user.userData?.debit_address !== values.debit_address &&
      user.userData?.debit_address === values.modal_debit_address
    ) {
      setValues(prev => ({
        ...prev,
        debit_address: user.userData?.debit_address || '',
      }));
    }
  }, [user]);

  useEffect(() => {
    const s = timer - 1;

    if (s < 1) {
      setTimer(0);
      return () => {};
    }

    const intervalId: ReturnType<typeof setTimeout> = setTimeout(() => {
      setTimer(s);
    }, 1000);

    return () => {
      clearTimeout(intervalId);
    };
  }, [timer]);

  const getFormErrors = (data: any) => {
    const { email, modal_debit_address, tab, old_password, new_password, re_new_password, code } =
      data;

    const newErrors: any = {
      first_name: '',
      last_name: '',
      email: '',
      phone: errors.phone,
      modal_debit_address: '',
      old_password: '',
      new_password: '',
      re_new_password: '',
      code: '',
    };

    // if (tab === SETTINGS_TABS.PROFILE) {
    //   if (!first_name) newErrors.first_name = 'Введите имя пользователя';
    //   if (!last_name) newErrors.last_name = 'Введите фамилию пользователя';
    // }
    //
    if (tab === SETTINGS_TABS.SECURITY) {
      if (email && !validateEmail(email)) newErrors.email = 'settings.form.email.errors.valid';
      if (modal_debit_address && !validate(modal_debit_address, Network.mainnet)) {
        newErrors.modal_debit_address = 'settings.form.debit_address.errors.valid';
      }
    }

    if (tab === SETTINGS_TABS.PASSWORD) {
      if (!old_password) newErrors.old_password = 'settings.form.old_password.errors.empty';
      if (!new_password) newErrors.new_password = 'settings.form.new_password.errors.empty';
      if (!re_new_password) newErrors.re_new_password = 'settings.form.new_password2.errors.empty';
      if (old_password && new_password && re_new_password && new_password !== re_new_password)
        newErrors.new_password = 'settings.form.new_password2.errors.not_match';
      if (old_password && new_password && re_new_password && old_password === new_password)
        newErrors.new_password = 'settings.form.new_password2.errors.match';
    }

    if (modalChangeWalletOpened) {
      if (!code) newErrors.code = 'settings.form.new_debit_address.code';
    }

    return newErrors;
  };

  const checkErrors = (data: { [key: string]: string }) => {
    for (const error in data) {
      if (data[error]) return true;
    }
    return false;
  };

  const onChange = (field: string, value: string) => {
    setValues(prev => ({
      ...prev,
      [field]: value,
    }));

    if (!!errors[field]) {
      setErrors({
        ...errors,
        [field]: '',
      });
    }

    if (!value && Object.prototype.hasOwnProperty.call(errors, field)) {
      const newValues = cloneDeep(values);
      newValues[field] = value;
      const newErrors: RegistrationParams = getFormErrors(newValues);

      setErrors({
        ...errors,
        [field]: newErrors[field],
      });
    }
  };

  const onChangePhone = (field: string, value: string, isValid: boolean) => {
    setValues(prev => ({
      ...prev,
      [field]: value,
    }));

    if (!isValid && value) {
      setErrors({
        ...errors,
        [field]: 'settings.form.phone.errors.valid',
      });
    }

    if (!value) {
      setErrors({
        ...errors,
        [field]: '',
      });
    }

    if (isValid && value) {
      setErrors({
        ...errors,
        [field]: '',
      });
    }
  };

  const onBlur = (field: string) => {
    if (Object.prototype.hasOwnProperty.call(errors, field)) {
      const newValues = cloneDeep(values);
      const newErrors: LogInParams = getFormErrors(newValues);

      setErrors({
        ...errors,
        [field]: newErrors[field],
      });
    }
  };

  const onChangeAvatar = () => {
    if (hiddenFileInput.current?.click) {
      hiddenFileInput.current.click();
    }
  };

  const handleUploadAvatar = (event: React.ChangeEvent<any>) => {
    const fileUploaded = event.target.files[0];
    const formData = [
      {
        name: 'photo',
        value: fileUploaded,
      },
    ];

    const updateData = {
      data: formData,
      type: 'form',
    };

    updateUserData(updateData);
  };

  const onPasswordSubmit = () => {
    const newErrors: any = getFormErrors(values);
    setErrors(newErrors);

    if (!checkErrors(newErrors)) {
      const requestPassword: UserNewPassword = {};
      if (values.tab === SETTINGS_TABS.PASSWORD) {
        if (values.new_password && values.old_password) {
          requestPassword.new_password = values.new_password;
          requestPassword.old_password = values.old_password;
        }
      }

      if (Object.keys(requestPassword).length) {
        updateUserPassword(requestPassword);
      }
    }
  };

  const onSendCode = () => {
    const newErrors: any = getFormErrors(values);
    setErrors(newErrors);

    if (!checkErrors(newErrors)) {
      let hasChanges = false;

      if (values.email && values.email !== user.userData?.email) {
        hasChanges = true;
      }

      if (values.phone && values.phone !== user.userData?.phone) {
        hasChanges = true;
      }

      if (
        values.modal_debit_address &&
        values.modal_debit_address !== user.userData?.debit_address
      ) {
        hasChanges = true;
      }

      if (!hasChanges) {
        setAlert('error.no_changes', ALERT_TYPES.DANGER);
      } else {
        sendUserOtp();
        setTimer(120);
        setModalChangeWalletOpened(true);
      }
    }
  };

  const onReSendCode = () => {
    sendUserOtp();
    setTimer(120);
  };

  const toogleConfirmModal = (opened: boolean) => {
    if (opened) {
      onSendCode();
    } else {
      setModalChangeWalletOpened(opened);
    }
  };

  const toogleNewDebitAdressModal = (opened: boolean) => {
    if (opened) {
      onChange('modal_debit_address', '');
      setModalNewWalletOpened(true);
    } else {
      setModalNewWalletOpened(false);
    }
  };

  const onCheckNewDebitAdress = () => {
    const newErrors: any = getFormErrors(values);
    setErrors(newErrors);

    if (!checkErrors(newErrors)) {
      // onChange('debit_address', values.modal_debit_address);
      toogleNewDebitAdressModal(false);
      toogleConfirmModal(true);
    }
  };

  const onSubmit = () => {
    const newErrors: any = getFormErrors(values);
    setErrors(newErrors);
    if (!checkErrors(newErrors)) {
      const requestData: UserData = {};
      if (values.tab === SETTINGS_TABS.PROFILE) {
        if (values.first_name !== user.userData?.first_name) {
          requestData.first_name = values.first_name;
        }
        if (values.last_name !== user.userData?.last_name) {
          requestData.last_name = values.last_name;
        }

        if (values.nickname !== user.userData?.nickname) {
          requestData.nickname = values.nickname;
        }
      }
      if (values.tab === SETTINGS_TABS.SECURITY) {
        if (
          values.modal_debit_address &&
          values.modal_debit_address !== user.userData?.debit_address
        ) {
          requestData.new_debit_address = values.modal_debit_address;
        } else {
          if (values.email && values.email !== user.userData?.email) {
            requestData.email = values.email;
          }

          if (values.phone && values.phone !== user.userData?.phone) {
            requestData.phone = `+${values.phone}`;
          }
        }

        requestData.code = values.code;
      }

      const updateData = {
        data: requestData,
        type: 'json',
      };

      if (Object.keys(requestData).length) {
        toogleConfirmModal(false);
        setValues(prev => ({
          ...prev,
          code: '',
        }));
        updateUserData(updateData);
      }
    }
  };

  return (
    <MainContent className={`content-main content-main_bg-3 -${themeType}`}>
      <Container>
        <SettingsPage className={`settings -${themeType}`}>
          <Title className={`settings__title -${themeType}`}>{t('settings.title')}</Title>
          <Tabs
            className={`setting-header -${themeType}`}
            tabHeaderClassName={`setting-header__tab -${themeType}`}
            tabsList={SETTINGS_TABS_LIST.map((item: string) => {
              return {
                key: item,
                name: t(`settings.tabs.${item}`),
              };
            })}
            field="tab"
            activeTab={values.tab}
            onChange={onChange}
          >
            <TabsContentItem className={`setting-tab ${values.tab === 'PROFILE' ? 'active' : ''}`}>
              <SettingsFormBox className="setting-tab__box">
                <SettingsItem className="setting-tab__item">
                  <SettingsUser>
                    <UserInfoBlock
                      className={`${
                        LEVEL_STYLES_TYPES[user.userData?.level_info?.level || 'BEGINNER']
                      } -${themeType}`}
                    >
                      <UserInfoImg className="user-info__img">
                        <img
                          src={
                            user.userData?.photo
                              ? `${STATIC_URL}${user.userData?.photo}`
                              : `${
                                themeType === 'dark'
                                  ? 'img/user-placeholder.svg'
                                  : 'img/user-placeholder-light.svg'
                              }`
                          }
                          alt="user"
                        />
                      </UserInfoImg>
                    </UserInfoBlock>
                    <button type="button" className="setting-user__link" onClick={onChangeAvatar}>
                      {t('settings.btns.change_photo')}
                    </button>
                    <input
                      type="file"
                      accept="image/png, image/gif, image/jpeg"
                      ref={(instance: HTMLInputElement) => (hiddenFileInput.current = instance)}
                      onChange={handleUploadAvatar}
                      className="setting-user__input"
                    />
                  </SettingsUser>

                  <Label htmlFor="name" className={`-${themeType}`}>
                    {t('settings.form.labels.name')}
                  </Label>
                  <Input
                    className={`setting__input sm -${themeType}`}
                    id="first_name"
                    type="text"
                    name="first_name"
                    value={values.first_name}
                    placeholder={t('settings.form.first_name.placeholder')}
                    onChange={onChange}
                  />
                  <Label htmlFor="lastname" className={`-${themeType}`}>
                    {t('settings.form.labels.last_name')}
                  </Label>
                  <Input
                    className={`setting__input sm -${themeType}`}
                    id="last_name"
                    type="text"
                    name="last_name"
                    value={values.last_name}
                    placeholder={t('settings.form.last_name.placeholder')}
                    onChange={onChange}
                  />
                  <Label htmlFor="nickname" className={`-${themeType}`}>
                    {t('settings.form.labels.nick_name')}
                  </Label>
                  <Input
                    className={`setting__input sm -${themeType}`}
                    id="nickname"
                    type="text"
                    name="nickname"
                    value={values.nickname}
                    placeholder={t('settings.form.nick_name.placeholder')}
                    onChange={(field: string, value: string) =>
                      onChange(field, value.replace(/[А-Яа-я]/i, ''))}
                  />
                  <Button
                    type="button"
                    className={`setting__btn lg loading-btn -${themeType}`}
                    onClick={onSubmit}
                    disabled={loading}
                  >
                    {t('settings.btns.save')}
                    {loading ? <Loader /> : null}
                  </Button>
                </SettingsItem>

                <SettingsItem className="ref-item">
                  <Label htmlFor="ref" className={`-${themeType}`}>
                    {t('settings.form.labels.ref_id')}
                  </Label>
                  <ReferralInput
                    className={`setting__ref-input -${themeType}`}
                    id="ref"
                    name="ref"
                    type="text"
                    value={user.userData?.ref_id}
                    ref_id={user.userData?.ref_id}
                    setUnderstand={setUnderstand}
                  />
                  <p className="setting-input__text">{t('settings.texts.text1')}</p>
                  <ReferralInput
                    className={`setting__ref-input -${themeType}`}
                    id="ref_link"
                    name="ref_link"
                    type="text"
                    value={`${SITE_URL}/registration?ref=${user.userData?.ref_id}`}
                    ref_id={user.userData?.ref_id}
                    setUnderstand={setUnderstand}
                  />
                </SettingsItem>
              </SettingsFormBox>
            </TabsContentItem>

            <TabsContentItem className={`setting-tab ${values.tab === 'SECURITY' ? 'active' : ''}`}>
              <SettingsFormBox className="setting-tab__box __password">
                <SettingsItem className="setting-tab__item">
                  <Label htmlFor="email" className={`-${themeType}`}>
                    {t('settings.form.labels.email')}
                  </Label>
                  <Input
                    className={`setting__input sm -${themeType}`}
                    id="email"
                    type="text"
                    name="email"
                    value={values.email}
                    error={errors.email}
                    placeholder={t('settings.form.email.placeholder')}
                    onChange={onChange}
                    onBlur={onBlur}
                  />

                  <Label htmlFor="phone" className={`-${themeType}`}>
                    {t('settings.form.labels.phone')}
                  </Label>
                  <PhoneInput
                    className={`setting__input sm -${themeType}`}
                    placeholder={t('settings.form.phone.placeholder')}
                    value={values.phone}
                    name="phone"
                    error={errors.phone}
                    onChange={(value: string, isValid: boolean) =>
                      onChangePhone('phone', value, isValid)}
                  />

                  {/* <Label htmlFor="debit_address" className={`-${themeType}`}>*/}
                  {/*  {t('settings.form.labels.btc_wallet')}*/}
                  {/* </Label>*/}
                  {/* <Input*/}
                  {/*  className={`setting__input sm -${themeType}`}*/}
                  {/*  id="debit_address"*/}
                  {/*  type="text"*/}
                  {/*  name="debit_address"*/}
                  {/*  value={values.debit_address}*/}
                  {/*  error={errors.debit_address}*/}
                  {/*  placeholder={t('settings.form.debit_address.placeholder')}*/}
                  {/*  onChange={onChange}*/}
                  {/*  onBlur={onBlur}*/}
                  {/* />*/}

                  <span className="settings__wallet-label">
                    {t('settings.form.labels.btc_wallet')}
                  </span>
                  <p className="settings__wallet-text">{values.debit_address}</p>

                  <SecondaryButton
                    className={`setting__btn_secondary md -${themeType}`}
                    type="button"
                    onClick={() => toogleNewDebitAdressModal(true)}
                  >
                    {t(
                      values.debit_address ? 'settings.btns.change' : 'settings.btns.create_wallet'
                    )}
                  </SecondaryButton>

                  {values.debit_address && values.debit_address !== user.userData?.debit_address ? (
                    <p className="settings__update-text">{t('settings.texts.text2')}</p>
                  ) : null}

                  <Button
                    type="button"
                    className={`setting__btn lg loading-btn -${themeType}`}
                    onClick={() => toogleConfirmModal(true)}
                    disabled={loadingOtp}
                  >
                    {t('settings.btns.save')}
                    {loadingOtp ? <Loader /> : null}
                  </Button>
                  {error && <p>{t(`${error}`)}</p>}
                </SettingsItem>
              </SettingsFormBox>
            </TabsContentItem>

            <TabsContentItem className={`setting-tab ${values.tab === 'PASSWORD' ? 'active' : ''}`}>
              <SettingsFormBox className="setting-tab__box __password">
                <SettingsItem className="setting-tab__item">
                  <Label htmlFor="old_password" className={`-${themeType}`}>
                    {t('settings.form.labels.old_password')}
                  </Label>
                  <Input
                    id="old_password"
                    className={`setting__input setting__input_mb sm -${themeType}`}
                    type="password"
                    name="old_password"
                    value={values.old_password}
                    error={errors.old_password}
                    placeholder={t('settings.form.old_password.placeholder')}
                    onChange={onChange}
                    onBlur={onBlur}
                  />
                  <Label htmlFor="new_password" className={`-${themeType}`}>
                    {t('settings.form.labels.new_password')}
                  </Label>
                  <Input
                    id="new_password"
                    className={`setting__input sm -${themeType}`}
                    type="password"
                    name="new_password"
                    value={values.new_password}
                    error={errors.new_password}
                    placeholder={t('settings.form.new_password.placeholder')}
                    onChange={onChange}
                    onBlur={onBlur}
                  />
                  <Label htmlFor="re_new_password" className={`-${themeType}`}>
                    {t('settings.form.labels.re_new_password')}
                  </Label>
                  <Input
                    id="re_new_password"
                    className={`setting__input sm -${themeType}`}
                    type="password"
                    name="re_new_password"
                    value={values.re_new_password}
                    error={errors.re_new_password}
                    placeholder={t('settings.form.new_password2.placeholder')}
                    onChange={onChange}
                    onBlur={onBlur}
                  />
                  <Button
                    type="button"
                    className={`setting__btn lg loading-btn -${themeType}`}
                    onClick={onPasswordSubmit}
                    disabled={loadingPassword}
                  >
                    {t('settings.btns.save')}
                    {loadingPassword ? <Loader /> : null}
                  </Button>
                  {error && <p>{t(`${error}`)}</p>}
                </SettingsItem>
              </SettingsFormBox>
            </TabsContentItem>
          </Tabs>
        </SettingsPage>
        <Modal
          opened={modalChangeWalletOpened}
          closeModal={() => toogleConfirmModal(false)}
          className={`settings-modal -${themeType}`}
        >
          <div className="modal__content">
            <h4 className="modal__title">{t('modals.titles.change_wallet_confirm')}</h4>
            <p className="modal__text">
              {t(
                `forgotPassword.page.texts.${
                  user.userData?.phone ? 'text_by_number' : 'text_by_email'
                }`
              )}
              {' '}
              {`${user.userData?.phone ? user.userData?.phone : user.userData?.email}.`}
            </p>
            <Input
              className={`lg modal__input -${themeType}`}
              type="text"
              name="code"
              value={values.code}
              placeholder={t(
                `forgotPassword.page.form.code.${
                  user.userData?.phone ? 'placeholder' : 'email_placeholder'
                }`
              )}
              error={errors.code}
              onChange={onChange}
              onBlur={onBlur}
            />
            {timer > 0 ? (
              <p className="modal__text">
                {t(
                  `forgotPassword.page.texts.${
                    user.userData?.phone ? 'text_by_number2' : 'text_by_email2'
                  }`,
                  { timer }
                )}
              </p>
            ) : null}

            {timer === 0 ? (
              <Button
                className="modal__resend"
                type="button"
                onClick={onReSendCode}
                disabled={loadingOtp}
              >
                {t('forgotPassword.links.resend_email')}
                {loadingOtp ? <Loader /> : null}
              </Button>
            ) : null}
            <Button
              className={`xl width -${themeType}`}
              type="button"
              onClick={onSubmit}
              disabled={loading}
            >
              {t('forgotPassword.btns.confirm')}
              {loading ? <Loader /> : null}
            </Button>
          </div>
        </Modal>

        <Modal
          opened={modalNewWalletOpened}
          closeModal={() => toogleNewDebitAdressModal(false)}
          className={`settings-modal -${themeType}`}
        >
          <div className="modal__content">
            <h4 className="modal__title">{t('modals.titles.change_wallet')}</h4>
            <p className="modal__text">{t('modals.texts.btc_wallet')}</p>
            <Input
              className={`lg modal__input -${themeType}`}
              id="modal_debit_address"
              type="text"
              name="modal_debit_address"
              value={values.modal_debit_address}
              error={errors.modal_debit_address}
              placeholder={t('settings.form.debit_address.placeholder')}
              onChange={onChange}
              onBlur={onBlur}
              maxLength={1000}
            />
            <Button
              className={`xl width -${themeType}`}
              type="button"
              onClick={onCheckNewDebitAdress}
              disabled={!values.modal_debit_address}
            >
              {t('settings.btns.save')}
            </Button>
          </div>
        </Modal>
      </Container>
    </MainContent>
  );
};

const mapState = (state: AppStateType) => {
  const { user, app } = state;
  return {
    user,
    loading: selectLoadingByKey(state, types.UPDATE_USER_DATA_REQUEST),
    loadingOtp: selectLoadingByKey(state, types.SEND_USER_OTP_REQUEST),
    loadingPassword: selectLoadingByKey(state, types.UPDATE_USER_PASSWORD_REQUEST),
    error: selectErrorByKey(state, types.UPDATE_USER_PASSWORD_REQUEST),
    themeType: app.theme,
  };
};

export default connect(mapState, {
  updateUserData,
  updateUserPassword,
  sendUserOtp,
  setAlert,
  setUnderstand,
})(Settings);
