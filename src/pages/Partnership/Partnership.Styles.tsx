import { Collapse, List, ListItem } from '@material-ui/core';
import styled from 'styled-components';

export const Partners: any = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: space-between;
  flex-wrap: wrap;

  .partners__title {
    margin: 0 0 30px;
    font-weight: 400;
  }

  .partners__subtitle {
    margin: 0 0 20px;
    font-weight: 700;
  }

  .partners-referral__text {
    max-width: 365px;
    margin: 0 0 40px;
    font-size: 14px;
    line-height: 17px;
  }

  .partners-referral__link {
    font-size: 12px;
    line-height: 15px;
    text-decoration: underline;
    cursor: pointer;
  }

  .partners__item_ref {
    width: 35%;
  }

  .partners__item_info {
    margin-top: 60px;
    width: 50%;

    @media (max-width: 1400px) {
      margin-top: 0;
    }
  }

  .partners-box {
    display: flex;
    align-items: flex-start;
    justify-content: space-between;
    padding: 30px;
    background: var(--dark);
    border-radius: var(--border-radius);
  }

  .partners-box__left {
    margin-right: 20px;
    min-width: 40%;

    .finance-user {
      margin-bottom: 35px;
    }
  }

  .partners-box__right {
    width: 100%;
  }

  .partners-box__friend {
    display: block;
    font-size: 14px;
    line-height: 17px;
    color: rgba(255, 255, 255, 0.66);

    span {
      display: block;
      margin: 0 0 15px;
      font-weight: bold;
      font-size: 14px;
      line-height: 17px;
      color: rgba(255, 255, 255, 0.66);
    }

    .user-item {
      cursor: default;

      &:after {
        display: none;
      }

      &__name {
        font-size: 14px;
        line-height: 17px;
        color: var(--white);
      }
    }
  }

  .partners-box__info {
    display: flex;
    align-items: center;
  }

  .partners-box__name {
    margin: 0;
    font-size: 14px;
    line-height: 17px;
    color: var(--white);
  }

  .info-item {
    &:first-child,
    &:last-child {
      .info-item__divider {
        background-image: linear-gradient(
          -90deg,
          #dd59f8 0%,
          rgba(255, 255, 255, 0.06) 71.01%,
          rgba(255, 255, 255, 0) 99.52%
        );
      }
    }

    &__text {
      font-family: var(--poppins);
      font-weight: 300;
      font-size: 14px;
      line-height: 21px;
      color: rgba(255, 255, 255, 0.53);
    }

    &__value {
      font-family: var(--poppins);
      font-weight: 700;
      font-size: 14px;
      line-height: 21px;
      color: rgba(255, 255, 255, 0.53);
    }
  }

  &.-light {
    .partners-box {
      background: #fafafa;
      box-shadow: 0 0 11px rgba(0, 0, 0, 0.04);
    }

    .partners__subtitle {
      color: var(--dark-blue);
    }

    .info-item {
      &__text {
        color: var(--light-brown);
      }

      &__value {
        color: #999999;
      }
    }

    .partners-box__friend {
      span {
        color: var(--brown);
      }
    }

    .user-item__name {
      color: var(--brown);
    }
  }

  @media (max-width: 1600px) {
    .partners-box {
      flex-direction: column;
    }

    .partners-box__left {
      width: 100%;
      margin: 0 0 45px;
    }

    .partners-box__right {
      width: 100%;
    }

    .partners__item_ref {
      width: 45%;
    }
  }

  @media (max-width: 1400px) {
    .partners__item {
      width: 80%;
    }

    .partners__item_ref {
      margin-bottom: 50px;
    }
  }

  @media (max-width: 992px) {
    .partners__item {
      width: 100%;
    }

    .partners__title {
      margin: 0 0 38px;
      font-size: 14px;
      line-height: 17px;
    }

    .partners__subtitle {
      font-size: 20px;
      line-height: 24px;
    }

    .partners-referral__text {
      max-width: 100%;
      margin: 0 0 36px;
    }

    .partners-box__friend {
      margin: 0;
    }

    .partners-box {
      padding: 28px 15px;
    }

    .info-item {
      &__text {
        font-size: 12px;
        line-height: 16px;
      }

      &__value {
        font-size: 12px;
        line-height: 16px;
      }
    }
  }
`;

export const UserItem: any = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  cursor: pointer;

  &::after {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background: linear-gradient(90deg, rgba(232, 232, 232, 0.12) 0%, rgba(196, 196, 196, 0) 78.12%);
    border-radius: 21px;
    opacity: 0;
    visibility: hidden;
    transition: opacity 0.5s ease-in-out, visibility 0.5s ease-in-out;
  }

  &:hover {
    &::after {
      opacity: 1;
      visibility: visible;
    }
  }

  &.beginner {
    .user-item__img {
      border-color: var(--light-gray);
      box-shadow: 0 0 4px var(--light-gray);
    }
  }

  &.ametis-junior,
  &.ametis-middle,
  &.ametis-senior,
  &.ametis-lead {
    .user-item__img {
      border-color: var(--ametis);
      box-shadow: 0 0 4px var(--ametis);
    }
  }

  &.lazurit-junior,
  &.lazurit-middle,
  &.lazurit-senior,
  &.lazurit-lead {
    .user-item__img {
      border-color: var(--lazurit);
      box-shadow: 0 0 4px var(--lazurit);
    }
  }

  &.nefrit-junior,
  &.nefrit-middle,
  &.nefrit-senior,
  &.nefrit-lead {
    .user-item__img {
      border-color: var(--nefrit);
      box-shadow: 0 0 4px var(--nefrit);
    }
  }

  &.emerald-junior,
  &.emerald-middle,
  &.emerald-senior,
  &.emerald-lead {
    .user-item__img {
      border-color: var(--emerald);
      box-shadow: 0 0 4px var(--emerald);
    }
  }

  &.ruby-junior,
  &.ruby-middle,
  &.ruby-senior,
  &.ruby-lead {
    .user-item__img {
      border-color: var(--ruby);
      box-shadow: 0 0 4px var(--ruby);
    }
  }

  &.saphire-junior,
  &.saphire-middle,
  &.saphire-senior,
  &.saphire-lead {
    .user-item__img {
      border-color: var(--saphire);
      box-shadow: 0 0 4px var(--saphire);
    }
  }

  &.diamond-junior,
  &.diamond-middle,
  &.diamond-senior,
  &.diamond-lead {
    .user-item__img {
      border-color: var(--diamond);
      box-shadow: 0 0 4px var(--diamond);
    }
  }

  .user-item__img {
    flex: 0 0 auto;
    display: flex;
    align-items: center;
    justify-content: center;
    width: 41px;
    height: 41px;
    padding: 4px;
    margin-right: 20px;
    border: 1px solid;
    border-radius: 50%;
    overflow: hidden;

    img {
      width: 100%;
      height: 100%;
      border-radius: 50%;
      object-fit: cover;
    }
  }

  .user-item__text {
    display: flex;
    align-items: center;
  }

  .user-item__name {
    margin: 0 20px 0 0;
    font-size: 15px;
    line-height: 18px;
    color: var(--white);
  }

  .user-item__partners {
    margin: 0;
  }
`;

export const UserItemField: any = styled.div`
  font-weight: 400;
  font-size: 15px;
  line-height: 18px;
  color: var(--white);

  &.gradient {
    background: linear-gradient(180deg, #e9b9e9 0%, #ffffff 100%);
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
  }
`;

export const Structure: any = styled.div`
  width: 100%;
  margin-top: 70px;

  .partners__subtitle {
    font-weight: 700;
  }

  .table {
    overflow: auto;
    scrollbar-width: none;

    ::-webkit-scrollbar {
      display: none;
    }

    &.-show-all {
      @media (min-width: 992px) {
        overflow: visible;
      }
    }
  }

  .table__container {
    margin-bottom: 14px;
  }

  .table__header {
    padding: 0 27px 10px;
    font-weight: 400;
    font-size: 12px;
    line-height: 15px;
    color: rgba(255, 255, 255, 0.66);
  }

  .table__body-wrapper {
    border-radius: 18px;
    overflow-x: hidden;

    &.-show-all {
      overflow: visible;
      .simplebar-wrapper {
        overflow: visible;
      }

      .simplebar-mask {
        overflow: visible;
      }

      .simplebar-content-wrapper {
        overflow: visible !important;
      }
    }
  }

  .table__body {
    padding: 20px 0;
    border-radius: 10px;
    background-color: var(--dark);
  }

  .table__row {
    position: relative;
    display: flex;
    align-items: center;
    flex-shrink: 0;
  }

  .table__col {
    &:nth-child(1) {
      width: 30%;
    }

    &:nth-child(2) {
      width: 15%;
    }

    &:nth-child(3) {
      width: 15%;
    }

    &:nth-child(4) {
      width: 15%;
    }

    &:nth-child(5) {
      width: 25%;
    }
  }

  .table__header .table__col {
    &:first-child {
      padding-left: 65px;
    }
  }

  .table__body .table__row {
    padding: 5px 27px 8px;

    &:before {
      content: '';
      position: absolute;
      top: 5px;
      left: 27px;
      width: 100%;
      max-width: 50%;
      height: 41px;
      background-image: linear-gradient(
        90deg,
        rgba(232, 232, 232, 0.12) 0%,
        rgba(196, 196, 196, 0) 78.12%
      );
      border-radius: 21px;
      opacity: 0;
      visibility: hidden;
      transition: opacity 0.3s, visibility 0.3s;
    }

    &:after {
      content: '';
      position: absolute;
      bottom: 0;
      left: 0;
      display: block;
      width: 100%;
      height: 3px;
      background-image: linear-gradient(3.98deg, #373a43 191.71%, rgba(0, 0, 0, 0.83) 282.29%);
    }

    &:last-child {
      &:after {
        display: none;
      }
    }

    &:last-child,
    &:nth-last-child(2) {
      .structure__info {
        bottom: 50px;
        top: auto;
      }
    }

    &:hover {
      cursor: pointer;

      &:before {
        opacity: 1;
        visibility: visible;
      }
    }

    &.show {
      &:before {
        left: 66px;
      }

      .structure__content:first-child {
        position: relative;
        padding-left: 40px;

        &:before {
          content: '';
          position: absolute;
          top: 50%;
          left: 20px;
          width: 20px;
          height: 1px;
          background-color: var(--ametis);
        }

        &:after {
          content: '';
          position: absolute;
          bottom: 50%;
          left: 20px;
          height: 60px;
          width: 1px;
          background-color: var(--ametis);
        }
      }
    }
  }

  .table__body .table__row:first-child + .table__row.show {
    &:before {
      left: 27px;
    }

    .structure__content:first-child {
      padding-left: 0;

      &:before {
        display: none;
      }
    }

    .user-item:before {
      display: none;
    }
  }

  .table__body .table__row_line {
    font-size: 10px;
    line-height: 12px;
    text-decoration: underline;

    span {
      position: relative;
      z-index: 1;
      background-color: var(--dark);
    }

    &::after {
      display: none;
    }

    &::before {
      content: '';
      position: absolute;
      top: -10px;
      left: 47px !important;
      height: 60px;
      width: 1px;
      opacity: 1;
      visibility: visible;
      background-color: var(--ametis);
    }

    &:last-child {
      &::before {
        display: none;
      }
    }
  }

  .structure__content {
    position: relative;
    display: flex;
    align-items: center;
    font-weight: 400;
    font-size: 15px;
    line-height: 18px;

    .user-item__img {
      overflow: hidden;
      background-color: var(--dark);
    }
  }

  .structure__img {
    position: relative;
    z-index: 1;

    &:hover {
      ~ .structure__info {
        opacity: 1;
        visibility: visible;
        transform: translateY(0);
      }
    }
  }

  .structure__button {
    margin-left: auto;
    min-width: 190px;
  }

  .structure__info {
    position: absolute;
    top: 50px;
    min-width: 200px;
    padding: 10px 20px;
    background: #2f343d;
    border-radius: var(--border-radius);
    border: 1px solid #7357bd;
    z-index: 3;
    opacity: 0;
    visibility: hidden;
    transform: translateY(-10px);
    transition: opacity 0.3s, visibility 0.3s, transform 0.3s;
  }

  .structure__status {
    display: flex;
    align-items: center;
    margin-bottom: 15px;
  }

  .structure__status-img {
    margin-right: 15px;
    width: 40px;
    height: 40px;
    border-radius: 50%;
    overflow: hidden;

    img {
      width: 100%;
      height: 100%;
      object-fit: cover;
      border-radius: 50%;
    }
  }

  .structure__status-text {
    max-width: 85px;
    font-weight: 700;
    font-size: 11px;
    line-height: 13px;
  }

  .structure__list {
    display: flex;
    flex-direction: column;

    li:not(:last-child) {
      margin-bottom: 6px;
    }

    li {
      font-size: 11px;
      line-height: 13px;
    }

    span {
      display: inline-block;
      margin-right: 6px;
      font-weight: 700;
    }
  }

  &.-light {
    .table {
      &__header {
        color: var(--brown);
      }

      &__body {
        background-color: #fafafa;
        box-shadow: 0 0 11px rgba(0, 0, 0, 0.04);

        .table__row {
          &:after {
            height: 1px;
            background-color: #e5e5e5;
            background-image: none;
          }
        }

        .table__row_line span {
          background-color: #fafafa;
        }
      }

      &__col {
        .user-item__img {
          background: #fafafa;
        }

        .user-item__name {
          color: var(--brown);
        }

        .user-item__partners {
          color: var(--brown);
        }

        .gradient {
          background-image: linear-gradient(180deg, #e9b9e9 0%, #7a007a 100%);
        }

        .structure__info {
          background: var(--white);
          box-shadow: 0 0 11px rgba(0, 0, 0, 0.04);
        }
      }

      &__row {
        &:before {
          background-image: linear-gradient(
            90deg,
            rgba(0, 71, 255, 0.12) 0%,
            rgba(196, 196, 196, 0) 78.12%
          );
        }
      }
    }
  }

  @media (max-width: 1200px) {
    .table {
      margin-right: -40px;
    }

    .table__container {
      width: 1200px;
    }
  }

  @media (max-width: 992px) {
    .table {
      margin-right: -16px;
    }

    .structure__content {
      font-size: 13px;
      line-height: 16px;
    }
  }
`;

export const ModalStructure: any = styled.div`
  display: block;
  position: relative;
  width: 100%;
  margin: 0 auto;

  .user-item__status,
  .user-item__partners {
    opacity: 0;
    visibility: hidden;
    transition: opacity 0.5s ease-in-out, visibility 0.5s ease-in-out;
  }

  .user-item:hover {
    .user-item__status,
    .user-item__partners {
      opacity: 1;
      visibility: visible;
    }
  }
`;

export const ModalStructureList: any = styled(List)`
  background-color: transparent;
  padding: 5px 0;
`;

export const ModalStructureListItem: any = styled(ListItem)`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  padding: 0;

  .user-item {
    position: relative;
    padding: 0 0 0 20px;

    &::after {
      left: 20px;
    }

    &:before {
      content: '';
      position: absolute;
      top: 15px;
      left: 0;
      width: 10px;
      height: 10px;
      background: url('../img/svg/icon_arrow_accordeon.svg') center / contain no-repeat;
      transform: rotate(-90deg);
    }

    &.open {
      &:before {
        transform: rotate(0deg);
      }
    }
  }

  @media (max-width: 992px) {
    .user-item {
      &:before {
        top: 10px;
      }

      &__img {
        width: 28px;
        height: 28px;
        margin-right: 10px;
      }

      &__text {
      }

      &__name {
        font-size: 12px;
        line-height: 15px;
        margin-right: 0;
      }

      &__status {
        display: none;
      }

      &__partners {
        display: none;
      }
    }
  }
`;

export const ModalStructureChilds: any = styled(Collapse)`
  padding-left: 30px;

  @media (max-width: 992px) {
    padding-left: 15px;
  }
`;
