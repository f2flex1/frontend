import cloneDeep from 'lodash/cloneDeep';
import filter from 'lodash/filter';
import get from 'lodash/get';
import React, { useEffect, useState, Fragment } from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';

import { UserData } from '@/api';
import { papulate } from '@/common/utils/papulate';
import {
  Container,
  InfoItem,
  Label,
  MainContent,
  ReferralInput,
  SecondaryButton,
  SubTitle,
  Title,
  UserInfo,
  Modal,
  Placeholder,
  ScrollBar,
} from '@/components/elements';
import { LEVEL_IMAGES_TYPES, LEVEL_STYLES_TYPES } from '@/const/app.constants';
import { SITE_URL, STATIC_URL } from '@/const/general.constants';
import {
  Partners,
  Structure,
  ModalStructure,
  ModalStructureList,
  ModalStructureListItem,
  ModalStructureChilds,
  UserItem,
  UserItemField,
} from '@/pages/Partnership/Partnership.Styles';
import { AppStateType } from '@/store';
import { ThemeType } from '@/store/app/reducers';
import { setUnderstand } from '@/store/loadingsErrors/actions';
import { getPartners } from '@/store/partners/actions';
import { PartnersReducerState } from '@/store/partners/reducers';
import { UserReducerState } from '@/store/user/reducers';
import { getWalletStat } from '@/store/wallet/actions';
import { WalletReducerState } from '@/store/wallet/reducers';

export interface PartnershipProps {
  user: UserReducerState;
  wallet: WalletReducerState;
  partners: PartnersReducerState;
  getPartners: (payload: { user: UserData | null }) => void;
  getWalletStat: () => void;
  themeType?: ThemeType;
  setUnderstand: (message: string | null) => void;
}

const Partnership: React.FC<PartnershipProps> = (props: PartnershipProps) => {
  const { user, wallet, partners, getPartners, getWalletStat, themeType, setUnderstand } = props;
  const { t } = useTranslation();

  const [childrens, setChildrens] = useState<any | null>(null);
  const [childrensCount, setChildrensCount] = useState<number>(0);
  const [showChildrens, setShowChildrens] = useState<boolean>(false);
  const [parentPartner, setParentPartner] = useState<any | null>(null);
  const [modalOpened, setModalOpened] = useState<boolean>(false);
  const [structure, setStructure] = useState<any | null>(null);
  const [treeParents, setTreeParents] = useState<any | []>([]);

  useEffect(() => {
    if (user.userData && !partners.partners.loaded) {
      getPartners({ user: user.userData });
    } else {
      setChildrens(partners.partners.levels);
      setChildrensCount(partners.partners?.levels?.length);
      setStructure(partners.partners.levels);
    }
  }, [partners, user, getPartners]);

  useEffect(() => {
    if (!wallet.loaded) {
      getWalletStat();
    }
  }, [wallet, getWalletStat]);

  const updatePartnersTree = (level: any) => {
    let current = [...treeParents];
    const index = current.findIndex(el => el.uid === level.uid);
    if (index !== -1) {
      current = current.slice(0, index);
    } else {
      current.push(level);
    }
    setTreeParents(current);
  };

  const showPartner = (index: number, level: any, e: React.ChangeEvent<any>) => {
    e.stopPropagation();
    updatePartnersTree(level);
    if (parentPartner && parentPartner.level === level.level && showChildrens) {
      setChildrens(partners.partners.levels);
      setChildrensCount(partners.partners?.levels?.length);
      setParentPartner(null);
      setShowChildrens(false);
    } else {
      setChildrens(level);
      setChildrensCount(level.partners?.length);
      setShowChildrens(true);
    }

    if (level.level === 1) {
      setParentPartner(level);
    }

    if (parentPartner && parentPartner.level === level.level && showChildrens) {
      setChildrens(partners.partners.levels);
      setChildrensCount(partners.partners?.levels?.length);
      setParentPartner(null);
      setShowChildrens(false);
    }
  };

  // const getStructureTurnover = () => {
  //   let turnover = 0;
  //   partners.partners.list.forEach((partner: Partner) => {
  //     turnover += partner?.turnover;
  //   });
  //   return turnover;
  // };

  const getPartnersFirstLine = () => {
    const partnersFirstLine = filter(partners.partners.list, { parent: user.userData?.ref_id });
    return partnersFirstLine.length || 0;
  };

  const getNewStructureLevel = (level: any, beforeIndexes: number[], open: boolean) => {
    const newLevel = cloneDeep(level);
    const newBeforeIndexes = cloneDeep(beforeIndexes);
    if (newLevel.index === get(newBeforeIndexes, '0', null)) {
      if (newBeforeIndexes.length === 1 && newBeforeIndexes[0] === newLevel.index) {
        newLevel.opened = open;
      } else {
        newLevel.opened = true;
      }

      newBeforeIndexes.shift();
    } else {
      newLevel.opened = false;
    }

    if (newLevel.partners.length) {
      newLevel.partners = newLevel.partners.map((str: any) =>
        getNewStructureLevel(str, newBeforeIndexes, open)
      );
    }

    return newLevel;
  };

  const openStructureItem = (level: any, open: boolean) => {
    const newStructure = structure.map((str: any) =>
      getNewStructureLevel(str, level.beforeIndexes, open)
    );
    setStructure(newStructure);
  };

  const toogleStructureItem = (e: any, level: any, open: boolean) => {
    e.stopPropagation();
    openStructureItem(level, open);
  };

  const renderPartnersModal = (level: any) => {
    openStructureItem(level, true);
    setModalOpened(true);
  };

  const closeModal = () => {
    setModalOpened(false);
  };

  const renderPartners = (level: any, parent: boolean, isHideButton: boolean) => (
    <div
      key={`partner-${level.uid}`}
      className={`table__row ${parent ? '' : 'show'}`}
      onClick={() => renderPartnersModal(level)}
    >
      <div className="table__col structure__content">
        <UserItem
          className={`user-item structure__img ${
            LEVEL_STYLES_TYPES[level.user.level || 'BEGINNER']
          }`}
        >
          <div className="user-item__img">
            <img
              src={
                level.user.photo
                  ? `${STATIC_URL}/${level.user.photo}`
                  : `${
                    themeType === 'dark'
                      ? 'img/user-placeholder.svg'
                      : 'img/user-placeholder-light.svg'
                  }`
              }
              alt="user"
            />
          </div>
          <div className="user-item__text">
            <p className="user-item__name">{`${level.user?.nickname || 'unknown'}`}</p>
          </div>
        </UserItem>
        <div className="structure__info">
          <div className="structure__status">
            <div className="structure__status-img">
              <img src={LEVEL_IMAGES_TYPES[level.user.level || 'BEGINNER']} alt="" />
            </div>
            <span className="structure__status-text">{t(`levels.${level.user.level}`)}</span>
          </div>
          <ul className="structure__list">
            <li>
              <span>{level.user?.partners}</span>
              {t(`common.partners.${papulate(level.user?.partners || 0)}`)}
            </li>
            <li>
              <span>{`${level.user.turnover} FLEX`}</span>
              {t('partnership.titles.turnover')}
            </li>
          </ul>
        </div>
      </div>
      <div className="table__col structure__content">
        <UserItemField className="gradient">
          <span>{`${level.user.ref_id}`}</span>
        </UserItemField>
      </div>
      <div className="table__col structure__content">
        <UserItemField className="gradient">
          <span>{t(`levels.${level.user.level}`)}</span>
        </UserItemField>
      </div>
      <div className="table__col structure__content">
        <UserItemField className="gradient">
          <span>{`${level.user.investments} FLEX`}</span>
        </UserItemField>
      </div>
      <div className="table__col structure__content">
        <span>{level.user?.partners}</span>
        {level.partners.length ? (
          <SecondaryButton
            onClick={(e: React.ChangeEvent<any>) => showPartner(level.index, level, e)}
            className={`target-button structure__button -${themeType} ${
              isHideButton ||
              (parentPartner && parentPartner.level === level.level && showChildrens)
                ? 'show'
                : ''
            }`}
          >
            {isHideButton || (parentPartner && parentPartner.level === level.level && showChildrens)
              ? `${t('partnership.btns.hide_btn')}`
              : `${t('partnership.btns.show_btn')}`}
          </SecondaryButton>
        ) : null}
      </div>
    </div>
  );

  const renderStructureChilds = (child: any) => (
    <ModalStructureList component="ul" key={child.index}>
      <ModalStructureListItem>
        <UserItem
          className={`user-item ${LEVEL_STYLES_TYPES[child.user.level || 'BEGINNER']} ${
            child.opened ? 'open' : ''
          }`}
          onClick={(e: React.ChangeEvent<any>) => toogleStructureItem(e, child, !child.opened)}
        >
          <div className="user-item__img">
            <img
              src={
                child.user.photo
                  ? `${STATIC_URL}/${child.user.photo}`
                  : `${
                    themeType === 'dark'
                      ? 'img/user-placeholder.svg'
                      : 'img/user-placeholder-light.svg'
                  }`
              }
              alt="user"
            />
          </div>
          <div className="user-item__text">
            <UserItemField className="user-item__name">
              {`${child.user?.nickname || 'unknown'}`}
            </UserItemField>
            <UserItemField className="user-item__status gradient">
              <span>{t(`levels.${child.user.level}`)}</span>
            </UserItemField>
            <UserItemField className="user-item__partners">
              {`${child.user?.partners} ${t(
                `common.partners.${papulate(child.user?.partners || 0)}`
              )}`}
            </UserItemField>
          </div>
        </UserItem>
        {child.partners.length ? (
          <ModalStructureChilds in={child.opened} component="div" timeout="auto" unmountOnExit>
            {child.partners.map((childChild: any) => renderStructureChilds(childChild))}
          </ModalStructureChilds>
        ) : null}
      </ModalStructureListItem>
    </ModalStructureList>
  );

  return (
    <MainContent className={`content-main -${themeType}`}>
      <Container>
        <Partners className={`partners -${themeType}`}>
          <div className="partners__item partners__item_ref">
            <Title className={`title partners__title -${themeType}`}>
              {t('partnership.titles.partnership_program')}
            </Title>
            <SubTitle className={`subtitle partners__subtitle -${themeType}`}>
              {t('partnership.texts.invite_friends')}
            </SubTitle>

            <div className="partners-referral">
              <p className="partners-referral__text">{t('partnership.texts.get_income')}</p>
              <Label htmlFor="ref" className={`-${themeType}`}>
                {t('partnership.labels.ref_link')}
              </Label>
              <ReferralInput
                className={`-${themeType}`}
                id="ref"
                name="ref"
                type="text"
                value={`${SITE_URL}/registration?ref=${user.userData?.ref_id}`}
                ref_id={user.userData?.ref_id}
                setUnderstand={setUnderstand}
              />
            </div>
          </div>

          <div className="partners__item partners__item_info">
            <div className="partners-box">
              <div className="partners-box__left">
                <UserInfo
                  className={`partners-box__user ${
                    LEVEL_STYLES_TYPES[user.userData?.level_info?.level || 'BEGINNER']
                  } -${themeType}`}
                  src={
                    user.userData?.photo
                      ? `${STATIC_URL}${user.userData?.photo}`
                      : `${
                        themeType === 'dark'
                          ? 'img/user-placeholder.svg'
                          : 'img/user-placeholder-light.svg'
                      }`
                  }
                  alt="user"
                  userName={
                    user.userData?.first_name || user.userData?.phone || user.userData?.email
                  }
                  userLastname={user.userData?.last_name}
                  userId={`ID ${user.userData?.ref_id}`}
                />
                {user.userData?.parent?.id && (
                  <div className="partners-box__friend">
                    <span>{t('partnership.texts.invited_you')}</span>
                    <div className="partners-box__info">
                      <UserItem
                        className={`user-item ${
                          LEVEL_STYLES_TYPES[user.userData?.parent?.level || 'BEGINNER']
                        }`}
                      >
                        <div className="user-item__img">
                          <img
                            src={
                              user.userData?.parent?.photo
                                ? `${STATIC_URL}${user.userData?.parent?.photo}`
                                : `${
                                  themeType === 'dark'
                                    ? 'img/user-placeholder.svg'
                                    : 'img/user-placeholder-light.svg'
                                }`
                            }
                            alt="user"
                          />
                        </div>
                        <div className="user-item__text">
                          <p className="user-item__name">
                            {user.userData?.parent?.nickname || 'unknown'}
                          </p>
                        </div>
                      </UserItem>
                    </div>
                  </div>
                )}
              </div>

              <div className="partners-box__right">
                <div className="partners-box__info-list">
                  <InfoItem
                    className={`info-list__item -${themeType}`}
                    infoText={t('partnership.titles.personal_investments')}
                    infoValue={`${wallet.stat?.invested || 0} FLEX`}
                  />
                  <InfoItem
                    className={`info-list__item -${themeType}`}
                    infoText={t('partnership.titles.structure_turnover')}
                    // infoValue={`${getStructureTurnover()} FLEX`}
                    infoValue={`${user.userData?.turnover || 0} FLEX`}
                  />
                  <InfoItem
                    className={`info-list__item -${themeType}`}
                    infoText={t('partnership.titles.invited')}
                    infoValue={`${getPartnersFirstLine()} ${t('partnership.texts.people')}.`}
                  />
                  <InfoItem
                    className={`info-list__item -${themeType}`}
                    infoText={t('partnership.titles.general_structure')}
                    infoValue={`${partners.partners.list.length} ${t('partnership.texts.people')}.`}
                  />
                </div>
              </div>
            </div>
          </div>
        </Partners>

        {!showChildrens && !childrens?.length ? (
          <Placeholder className={`partnership-placeholder -${themeType}`}>
            <p>{t('partnership.table.placeholder.title')}</p>
            <span>{t('partnership.table.placeholder.text')}</span>
          </Placeholder>
        ) : (
          <Structure className={`structure -${themeType}`}>
            <SubTitle className={`subtitle partners__subtitle -${themeType}`}>
              {t('partnership.titles.my_structure')}
            </SubTitle>
            <div className={`table structure-table ${childrensCount < 5 ? '-show-all' : ''}`}>
              <div className="table__container">
                <div className="table__header">
                  <div className="table__row">
                    <div className="table__col">{t('partnership.table.headers.name')}</div>
                    <div className="table__col">{t('partnership.table.headers.id')}</div>
                    <div className="table__col">{t('partnership.table.headers.level')}</div>
                    <div className="table__col">{t('partnership.table.headers.investments')}</div>
                    <div className="table__col">{t('partnership.table.headers.partners')}</div>
                  </div>
                </div>
                <div className={`table__body-wrapper ${childrensCount < 5 ? '-show-all' : ''}`}>
                  <ScrollBar
                    className={`structure-scrollbar -${themeType}`}
                    style={{ maxHeight: '560px' }}
                  >
                    <div className="table__body">
                      {!treeParents.length &&
                        !showChildrens &&
                        childrens &&
                        childrens.map((partner: any) => renderPartners(partner, true, false))}

                      {parentPartner && renderPartners(parentPartner, true, true)}

                      {!!treeParents.length &&
                        treeParents.map((partner: any, index: number, arr: any) => (
                          <Fragment>
                            <div className="table__row table__row_line show">
                              <span>{`${partner.level}-${t('partnership.texts.line')}`}</span>
                            </div>
                            {index !== arr.length - 1
                              ? renderPartners(arr[index + 1], true, true)
                              : partner?.partners.map((partner: any) =>
                                renderPartners(partner, false, false)
                              )}
                          </Fragment>
                        ))}
                    </div>
                  </ScrollBar>
                </div>
              </div>
            </div>
          </Structure>
        )}
      </Container>
      <Modal
        opened={modalOpened}
        closeModal={closeModal}
        className={`partners-modal -${themeType}`}
      >
        <Title as="h3" className={`modal__title -${themeType}`}>
          {t('partnership.titles.my_structure')}
        </Title>
        <ModalStructure className="modal__structure">
          <ScrollBar className={`-${themeType}`} style={{ maxHeight: '400px' }}>
            <ModalStructureList component="ul">
              {structure
                ? structure.map((level: any) => (
                  <ModalStructureListItem key={level.index}>
                    <UserItem
                      className={`user-item ${
                        LEVEL_STYLES_TYPES[level.user.level || 'BEGINNER']
                      } ${level.user.partners > 0 && level.opened ? 'open' : ''}`}
                      onClick={(e: React.ChangeEvent<any>) =>
                        toogleStructureItem(e, level, !level.opened)}
                    >
                      <div className="user-item__img">
                        <img
                          src={
                            level.user.photo
                              ? `${STATIC_URL}/${level.user.photo}`
                              : `${
                                themeType === 'dark'
                                  ? 'img/user-placeholder.svg'
                                  : 'img/user-placeholder-light.svg'
                              }`
                          }
                          alt="user"
                        />
                      </div>
                      <div className="user-item__text">
                        <UserItemField className="user-item__name">
                          {`${level.user?.nickname || 'unknown'}`}
                        </UserItemField>
                        <UserItemField className="user-item__status gradient">
                          <span>{t(`levels.${level.user.level}`)}</span>
                        </UserItemField>
                        <UserItemField className="user-item__partners">
                          {`${level.user?.partners} ${t(
                            `common.partners.${papulate(level.user?.partners || 0)}`
                          )}`}
                        </UserItemField>
                      </div>
                    </UserItem>
                    {level.partners.length ? (
                      <ModalStructureChilds
                        in={level.opened}
                        component="div"
                        timeout="auto"
                        unmountOnExit
                      >
                        {level.partners.map((child: any) => renderStructureChilds(child))}
                      </ModalStructureChilds>
                    ) : null}
                  </ModalStructureListItem>
                ))
                : null}
            </ModalStructureList>
          </ScrollBar>
        </ModalStructure>
      </Modal>
    </MainContent>
  );
};

const mapState = (state: AppStateType) => {
  const { user, wallet, partners, app } = state;
  return {
    user,
    wallet,
    partners,
    themeType: app.theme,
  };
};

export default connect(mapState, { getPartners, getWalletStat, setUnderstand })(Partnership);
