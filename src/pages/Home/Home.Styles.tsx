import { Card } from '@material-ui/core';
import styled from 'styled-components';

export const ContentMainGains: any = styled.div`
  display: flex;
  justify-content: space-between;
  //align-items: flex-start;

  @media (max-width: 992px) {
    display: flex;
    flex-direction: column;
  }
`;

export const Gains: any = styled(Card)`
  width: 325px;
  display: flex;
  flex-direction: column;
  align-items: center;
  //min-height: 765px;
  padding: 30px 18px;
  background-color: var(--dark);
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.12);
  border-radius: var(--border-radius);

  .gains-progress {
    margin-top: 18px;
    display: none;
    font-family: var(--poppins);
    font-size: 10px;
    line-height: 15px;
  }

  &.-light {
    background: #fafafa;
    box-shadow: 0 0 11px rgba(0, 0, 0, 0.04);
  }

  @media (max-width: 992px) {
    min-height: fit-content;
    width: 100%;
    margin-bottom: 20px;

    .gains-progress {
      display: block;
    }
  }
`;

export const GainsRank: any = styled.div`
  position: relative;
  width: 100%;
`;

export const GainsRankHeader: any = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  @media (max-width: 992px) {
    flex-direction: row;
    margin-bottom: 24px;
  }
`;

export const GainsRankImg: any = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex: 0 0 auto;
  width: 82px;
  height: 82px;
  border-radius: 50%;
  margin-bottom: 20px;
  overflow: hidden;

  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }

  @media (max-width: 992px) {
    margin: 0 13px 0 0;
    width: 47px;
    height: 47px;
  }
`;

export const GainsRankDescr: any = styled.span`
  display: none;
  margin-bottom: 3px;
  font-weight: 300;
  font-size: 10px;
  line-height: 12px;
  color: rgba(255, 255, 255, 0.66);

  &.-light {
    color: var(--brown);
  }

  @media (max-width: 992px) {
    display: block;
  }
`;

export const GainsRankTitle: any = styled.h2`
  text-align: center;
  margin: 0 0 30px;
  font-size: 16px;
  line-height: 20px;
  font-weight: 700;
  text-transform: uppercase;

  &.-light {
    color: var(--brown);
  }

  @media (max-width: 992px) {
    margin: 0;
    text-align: left;
  }
`;

export const GainsRankFooter: any = styled.div`
  .level__text {
    p {
      font-size: 12px;
      line-height: 15px;
      text-transform: uppercase;
    }
  }

  @media (max-width: 992px) {
    margin-bottom: 0;
  }
`;

export const GainsRankText: any = styled.p`
  margin: 0 0 13px;
  font-weight: 300;
  font-size: 10px;
  line-height: 12px;
  color: rgba(255, 255, 255, 0.66);

  &.-light {
    color: var(--brown);
  }
`;

export const GainsMore: any = styled.div`
  width: 100%;

  &::before {
    content: '';
    display: block;
    margin: 30px 0;
    width: 100%;
    height: 1px;
    background-image: var(--horizontal-border);
  }

  .level:last-child {
    &[data-title] {
      &:before {
        bottom: 100%;
        top: auto;
      }
    }
  }

  @media (max-width: 992px) {
    display: none;

    &.active {
      display: block;
    }

    &::before {
      margin: 20px 0;
    }
  }

  @keyframes height {
    from {
      visibility: hidden;
      height: 0;
    }

    to {
      visibility: visible;
      height: 100%;
    }
  }
`;

export const ContentMainProfit: any = styled.div`
  width: calc(100% - 325px);
  padding-left: 40px;

  @media (max-width: 992px) {
    width: 100%;
    padding-left: 0;
  }
`;

export const Profit: any = styled.div`
  overflow-x: auto;
  margin-right: -40px;
  scrollbar-width: none;

  ::-webkit-scrollbar {
    display: none;
  }

  &.profit_order {
    overflow: initial;
    margin-right: 0;

    .profit__container {
      flex-wrap: nowrap;
    }
  }

  &.-light {
    .profit-card {
      background: #fafafa;
      box-shadow: 0 0 11px rgba(0, 0, 0, 0.04);

      &[data-title] {
        &:before {
          color: var(--brown);
          background: var(--light-blue);
          box-shadow: 0 0 11px rgba(0, 0, 0, 0.04);
        }
      }
    }

    .profit-card__title {
      color: #1b1b1b;
    }

    .profit-card__text {
      background-image: linear-gradient(180deg, #cdcdcd 0%, #272727 100%);
      -webkit-background-clip: text;
      -webkit-text-fill-color: transparent;
    }

    .profit-card.active .profit-card__text {
      background-image: linear-gradient(45.04deg, #ffd6ef -35.82%, #df45a3 100.3%);
      -webkit-background-clip: text;
      -webkit-text-fill-color: transparent;
    }

    .profit-card__value {
      font-weight: 700;
    }

    .profit-card__currency {
      font-weight: 700;
    }
  }

  @media (max-width: 992px) {
    margin-right: -16px;
  }
`;

export const ProfitContainer: any = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;

  @media (max-width: 1700px) {
    width: 1100px;
  }

  @media (max-width: 576px) {
    width: 660px;
  }
`;

export const Products: any = styled.div`
  &.investments-products {
    max-width: 1450px;
  }

  &.-light {
    .products-item {
      background: linear-gradient(0deg, #f3f7ff, #f3f7ff);
      box-shadow: 0 0 11px rgba(0, 0, 0, 0.04);

      &__title {
        color: #1b1b1b;
      }

      &__subtitle {
        color: #1b1b1b;
      }

      &__content {
        span {
          color: #3f3f3f;
        }
      }
    }
  }

  &.products {
    margin-right: -40px;
  }

  @media (max-width: 992px) {
    &.products {
      margin-right: -16px;
    }
  }

  @media (max-width: 576px) {
    &.products {
      margin-right: 0;
    }
  }
`;

export const ProductsContainer: any = styled.div`
  display: flex;
  justify-content: space-between;
  padding-top: 7px;
  margin-bottom: 20px;
  width: 1945px;

  @media (max-width: 768px) {
    width: 1000px;
  }

  @media (max-width: 576px) {
    flex-direction: column;
    width: 100%;
    margin-bottom: 0;
  }
`;

export const ProductsCardSubtitle: any = styled.p`
  margin: 0 0 30px;
  font-family: var(--poppins);
  font-weight: 300;
  font-size: 14px;
  line-height: 21px;
  color: var(--white);

  @media (max-width: 768px) {
    margin: 0 0 12px;
    font-size: 12px;
    line-height: 18px;
  }
`;

export const ProductsCardContent: any = styled.div`
  margin-bottom: 16px;

  p {
    margin: 0 0 4px;
    font-family: var(--poppins);
    font-weight: 600;
    font-size: 14px;
    line-height: 21px;
    color: var(--light-gray);
  }

  span {
    font-family: var(--poppins);
    font-weight: 300;
    font-size: 18px;
    line-height: 27px;
    text-transform: uppercase;
    color: #d1d1d1;
  }

  @media (max-width: 768px) {
    margin-bottom: 5px;
  }
`;
