import { Checkbox, FormControlLabel } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';
import moment from 'moment-timezone';
import React, { ChangeEvent, useEffect, useState } from 'react';
import { useCookies } from 'react-cookie';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';

import {
  Title,
  Modal,
  Button,
  ScrollBar,
  MainContent,
  SecondaryButton,
  ProfitComponent,
  ProductComponent,
} from '@/components/elements';
import { Container } from '@/components/elements/Container/Container';
import { GainsLevel } from '@/components/elements/GainsLevel/GainsLevel';
import { Image } from '@/components/elements/Image/Image';
import { ModalDescription } from '@/components/elements/PageModal/PageModal.Styles';
import { LEVEL_IMAGES_TYPES, PROGRAMS_ACTIVE_DATE } from '@/const/app.constants';
import { Modals } from '@/const/modals.constants';
import { PATHS } from '@/const/paths.constants';
import {
  Gains,
  Profit,
  Products,
  GainsMore,
  GainsRank,
  GainsRankImg,
  GainsRankText,
  GainsRankTitle,
  GainsRankDescr,
  GainsRankFooter,
  GainsRankHeader,
  ProfitContainer,
  ContentMainGains,
  ProductsContainer,
  ContentMainProfit,
} from '@/pages/Home/Home.Styles';
import { AppStateType } from '@/store';
import { ThemeType } from '@/store/app/reducers';
import { setUnderstand } from '@/store/loadingsErrors/actions';
import { PayloadModalsWindow } from '@/store/modals/actionTypes';
import { closeModalWindow } from '@/store/modals/actions';
import { UserReducerState } from '@/store/user/reducers';
import { getWalletStat } from '@/store/wallet/actions';
import { WalletReducerState } from '@/store/wallet/reducers';

export type Profitability = {
  title: string;
  tooltip?: string;
  value: number | string;
  currency: string;
};

export type ProfitabilityCommercial = Profitability & {
  image: string;
  imageParams: { width: number; height: number };
  currency: string;
  redirectUrl: string;
  imageClassName?: string;
  subTitle: string;
  valueText: string;
  description: string;
  date: string;
};

const profitabilityCommercials: ProfitabilityCommercial[] = [
  {
    image: '/img/home/phone.png',
    imageClassName: 'phone',
    imageParams: { width: 255, height: 330 },
    title: 'MARKET',
    subTitle: 'home.products.title',
    valueText: 'home.products.text',
    value: '20 000',
    currency: 'FLEX',
    redirectUrl: PATHS.PROGRAMS_MARKET,
    description: 'home.products.text2',
    date: moment(PROGRAMS_ACTIVE_DATE.MARKET).format('DD.MM.YYYY'),
  },
  {
    image: '/img/home/auto.png',
    imageClassName: 'auto',
    imageParams: { width: 255, height: 330 },
    title: 'AUTO',
    subTitle: 'home.products.title',
    valueText: 'home.products.text',
    value: '400 000',
    currency: 'FLEX',
    redirectUrl: PATHS.PROGRAMS_AUTO,
    description: 'home.products.text2',
    date: moment(PROGRAMS_ACTIVE_DATE.AUTO).format('DD.MM.YYYY'),
  },
  {
    image: '/img/home/cashback.png',
    imageClassName: 'chk',
    imageParams: { width: 255, height: 330 },
    title: 'CASHBACK',
    subTitle: 'home.products.title',
    valueText: 'home.products.text',
    value: '1 500 000',
    currency: 'FLEX',
    redirectUrl: PATHS.PROGRAMS_CASHBACK,
    description: 'home.products.text2',
    date: moment(PROGRAMS_ACTIVE_DATE.CASHBACK).format('DD.MM.YYYY'),
  },
  {
    image: '/img/home/home.png',
    imageClassName: 'home',
    imageParams: { width: 255, height: 330 },
    title: 'HOME',
    subTitle: 'home.products.title',
    valueText: 'home.products.text',
    value: '1 890 000',
    currency: 'FLEX',
    redirectUrl: PATHS.PROGRAMS_HOME,
    description: 'home.products.text2',
    date: moment(PROGRAMS_ACTIVE_DATE.HOME).format('DD.MM.YYYY'),
  },
];

interface HomeProps {
  user: UserReducerState;
  wallet: WalletReducerState;
  themeType: ThemeType;
  getWalletStat: () => void;
  setUnderstand: (message: string | null) => void;
  closeModalWindow: (payload: PayloadModalsWindow) => void;
  isShowIUnderstand: boolean;
}

const Home: React.FC<HomeProps> = (props: HomeProps) => {
  const {
    user,
    wallet,
    themeType,
    getWalletStat,
    setUnderstand,
    closeModalWindow,
    isShowIUnderstand,
  } = props;

  const { t } = useTranslation();
  const [selectedCard, setSelectedCard] = useState(0);
  const [open, setOpen] = useState(false);
  const [profitabilities, setProfitabilities] = useState<Array<Profitability>>([]);
  const [isTerms, setIsTerms] = useState<boolean>(false);
  const [cookies, setCookie] = useCookies(['i-understand']);
  const [understandShown, setUnderstandShown] = useState<boolean>(
    isShowIUnderstand && !cookies['i-understand']
  );

  useEffect(() => {
    if (user.privateSignup) {
      setUnderstand(
        t('PrivateEntry.modal.understand.message', {
          phone: !!user.userData?.phone ? t('common.phone') : t('common.email'),
        })
      );
    }
  });

  useEffect(() => {
    if (!isShowIUnderstand) {
      setUnderstandShown(false);
    }
  }, [isShowIUnderstand]);

  useEffect(() => {
    if (!wallet.loaded) {
      getWalletStat();
    }

    if (wallet.stat) {
      const profitabilities: Profitability[] = [
        {
          title: 'home.profit.titles.title1',
          tooltip: 'home.profit.tooltips.tooltip1',
          value:
            Number(wallet.stat?.balance)
              .toFixed(2)
              .replace(/\.?0+$/, '') || 0,
          currency: 'FLEX',
        },
        {
          title: 'home.profit.titles.title2',
          tooltip: 'home.profit.tooltips.tooltip2',
          value:
            Number(wallet.stat?.waiting)
              .toFixed(2)
              .replace(/\.?0+$/, '') || 0,
          currency: 'FLEX',
        },
        {
          title: 'home.profit.titles.title3',
          tooltip: 'home.profit.tooltips.tooltip3',
          value:
            Number(user.userData?.turnover)
              .toFixed(2)
              .replace(/\.?0+$/, '') || 0,
          currency: 'FLEX',
        },
        {
          title: 'home.profit.titles.title4',
          tooltip: 'home.profit.tooltips.tooltip4',
          value:
            Number(wallet.stat?.profit)
              .toFixed(2)
              .replace(/\.?0+$/, '') || 0,
          currency: 'FLEX',
        },
        {
          title: 'home.profit.titles.title5',
          tooltip: 'home.profit.tooltips.tooltip5',
          value:
            Number(wallet.stat?.income)
              .toFixed(2)
              .replace(/\.?0+$/, '') || 0,
          currency: 'FLEX',
        },
        {
          title: 'home.profit.titles.title6',
          tooltip: 'home.profit.tooltips.tooltip6',
          value:
            Number(wallet.stat?.invested)
              .toFixed(2)
              .replace(/\.?0+$/, '') || 0,
          currency: 'FLEX',
        },
      ];
      setProfitabilities(profitabilities);
    }
  }, [wallet, user, getWalletStat, setProfitabilities]);

  const onClick = () => {
    setOpen(!open);
  };

  const handleCloseModal = () => {
    closeModalWindow({
      modalType: Modals.I_UNDERSTAND,
    });
  };

  const handleClick = () => {
    closeModalWindow({
      modalType: Modals.I_UNDERSTAND,
    });

    if (isTerms) {
      setCookie('i-understand', 'true');
    }
  };

  const handleChangeTerms = (event: ChangeEvent<HTMLInputElement>) => {
    setIsTerms(event.target.checked);
  };

  return (
    <MainContent className={`content-main home-page -${themeType}`}>
      <Container>
        <Title className={`home-title -${themeType}`}>
          {`${t('home.page.titles.greetings')}, ${
            user.userData?.first_name || user.userData?.phone || user.userData?.email || ''
          } ${user.userData?.last_name || ''}`}
        </Title>

        <ContentMainGains className="content-main__gains">
          <Gains
            className={`-${themeType}`}
            classes={{ root: 'gains' }}
            hidden={!user.userData?.level_info}
          >
            <GainsRank className="gains-rank">
              <GainsRankHeader className="gains-rank__header">
                <GainsRankImg className="gains-rank__img">
                  <Image
                    src={LEVEL_IMAGES_TYPES[user.userData?.level_info?.level || 'BEGINNER']}
                    alt="rank"
                    variant="circle"
                    animation="wave"
                    width={66}
                    height={66}
                  />
                </GainsRankImg>
                <div>
                  <GainsRankDescr className={`-${themeType}`}>
                    {t('home.rank.titles.your_level')}
                  </GainsRankDescr>
                  <GainsRankTitle className={`-${themeType}`}>
                    {!user.userData?.level_info ? (
                      <Skeleton variant="text" animation="wave" width={110} />
                    ) : (
                      t(`levels.${user.userData?.level_info?.level}`)
                    )}
                  </GainsRankTitle>
                </div>
              </GainsRankHeader>

              <GainsRankFooter className="gains-rank__footer">
                <GainsRankText className={`-${themeType}`}>
                  {t('home.rank.titles.next_level')}
                </GainsRankText>

                <GainsLevel
                  className={`-${themeType}`}
                  title={t(`levels.${user.userData?.level_info?.next_level}`)}
                  text={t('home.rank.texts.text1')}
                  img={LEVEL_IMAGES_TYPES[user.userData?.level_info?.next_level || 'BEGINNER']}
                  currency="%"
                  value={
                    ((Number(user.userData?.level_info?.investments) /
                      Number(user.userData?.level_info?.required_investments) >
                    1
                      ? 100
                      : (Number(user.userData?.level_info?.investments) /
                          Number(user.userData?.level_info?.required_investments)) *
                        100) +
                      (Number(user.userData?.level_info?.partner_turnover) /
                        Number(user.userData?.level_info?.required_partner_turnover) >
                      1
                        ? 100
                        : (Number(user.userData?.level_info?.partner_turnover) /
                            Number(user.userData?.level_info?.required_partner_turnover)) *
                          100)) /
                    2
                  }
                  maxValue={100}
                  isMain
                />
              </GainsRankFooter>
            </GainsRank>

            <GainsMore className={`gains-more ${open ? 'active' : ''}`}>
              <GainsLevel
                className={`-${themeType}`}
                title="home.rank.texts.text2"
                tooltip="home.rank.tooltips.text2"
                tooltipParams={{ amount: Number(user.userData?.level_info?.required_investments) }}
                text={`${Number(user.userData?.level_info?.required_investments)
                  .toFixed(2)
                  .replace(/\.?0+$/, '')} FLEX ${t('home.rank.texts.text3')}`}
                img={themeType === 'dark' ? 'img/rank/level-01.svg' : 'img/rank/level-01-light.svg'}
                value={Number(user.userData?.level_info?.investments)}
                currency="FLEX"
                maxValue={Number(user.userData?.level_info?.required_investments)}
              />
              <GainsLevel
                className={`-${themeType}`}
                title="home.rank.texts.text4"
                tooltip="home.rank.tooltips.text4"
                tooltipParams={{
                  amount: Number(user.userData?.level_info?.required_partner_turnover),
                  lines: `${
                    user.userData?.level_info?.required_partner_lines[0] ===
                    user.userData?.level_info?.required_partner_lines[
                      user.userData?.level_info?.required_partner_lines.length - 1
                    ]
                      ? `${user.userData?.level_info?.required_partner_lines[0]}
                      ${t('common.lines.many')}`
                      : `${t('common.from')}
                      ${user.userData?.level_info?.required_partner_lines[0]}
                      ${t('common.to')}
                      ${
    user.userData?.level_info?.required_partner_lines[
      user.userData?.level_info?.required_partner_lines.length - 1
    ]
    }
                      ${t('common.lines.other')}`
                  }`,
                }}
                text={`${t('home.rank.texts.text5')} >= ${Number(
                  user.userData?.level_info?.required_partner_turnover
                )
                  .toFixed(2)
                  .replace(/\.?0+$/, '')} FLEX`}
                img={themeType === 'dark' ? 'img/rank/level-02.svg' : 'img/rank/level-02-light.svg'}
                value={Number(user.userData?.level_info?.partner_turnover)}
                currency="FLEX"
                maxValue={Number(user.userData?.level_info?.required_partner_turnover)}
              />
              <GainsLevel
                className={`-${themeType}`}
                title="home.rank.texts.text6"
                tooltip="home.rank.tooltips.text6"
                tooltipParams={{
                  level: t(`levels.${user.userData?.level_info?.next_level}`),
                  amount: Number(user.userData?.level_info?.next_bonus_balance)
                    .toFixed(2)
                    .replace(/\.?0+$/, ''),
                }}
                text={`${Number(user.userData?.level_info?.next_bonus_balance)
                  .toFixed(2)
                  .replace(/\.?0+$/, '')} FLEX ${t('home.rank.texts.text7')}`}
                img={themeType === 'dark' ? 'img/rank/level-03.svg' : 'img/rank/level-03-light.svg'}
                value={1}
                currency="FLEX"
                maxValue={100}
                isBolean
              />
              <GainsLevel
                className={`-${themeType}`}
                title="home.rank.texts.text8"
                tooltip="home.rank.tooltips.text8"
                tooltipParams={{
                  level: t(`levels.${user.userData?.level_info?.next_level}`),
                  percent: user.userData?.level_info?.next_percent,
                }}
                text={`${user.userData?.level_info?.next_percent}% ${t(
                  'home.rank.texts.text9'
                )} ${t(`levels.${user.userData?.level_info?.next_level}`)}`}
                img={themeType === 'dark' ? 'img/rank/level-04.svg' : 'img/rank/level-04-light.svg'}
                value={1}
                currency="%"
                maxValue={100}
                isBolean
              />
            </GainsMore>

            <SecondaryButton onClick={onClick} className={`gains-progress sm -${themeType}`}>
              {!open ? t('home.rank.texts.text10') : t('home.btns.btn_collapse')}
            </SecondaryButton>
          </Gains>

          <ContentMainProfit className="profitability">
            <Profit className={`profit -${themeType}`}>
              <ScrollBar className={`profit-scrollbar -${themeType}`}>
                <ProfitContainer className="profit__container">
                  {profitabilities.map((item: Profitability, index: number) => {
                    return (
                      <ProfitComponent
                        selected={selectedCard === index && !!item.value}
                        key={`profitability-${index + 1}`}
                        onClick={() => setSelectedCard(index)}
                        item={item}
                      />
                    );
                  })}
                </ProfitContainer>
              </ScrollBar>
            </Profit>

            <Products className={`products -${themeType}`}>
              <ScrollBar className={`products-scrollbar -${themeType}`}>
                <ProductsContainer className="products__container">
                  {profitabilityCommercials.map((item: ProfitabilityCommercial, index: number) => (
                    <ProductComponent
                      themeType={themeType}
                      item={item}
                      key={`profitability-commercial-${index + 1}`}
                    />
                  ))}
                </ProductsContainer>
              </ScrollBar>
            </Products>
          </ContentMainProfit>
        </ContentMainGains>

        <Modal
          opened={understandShown}
          className={`understanding-modal auth-modal -${themeType}`}
          closeModal={handleCloseModal}
        >
          <ModalDescription
            className="modal__description"
            dangerouslySetInnerHTML={{ __html: t('investments.modal.summ.message') }}
          />
          <FormControlLabel
            className="modal__checkbox"
            control={
              <Checkbox
                checked={isTerms}
                onChange={handleChangeTerms}
                aria-describedby="termsRegistration-text"
              />
            }
            label={<React.Fragment>{t('modals.btns.do_not_show_again')}</React.Fragment>}
          />
          <Button className={`xl width -${themeType}`} onClick={handleClick}>
            {t('modals.btns.understand_btn')}
          </Button>
        </Modal>
      </Container>
    </MainContent>
  );
};

const mapStateToProps = (state: AppStateType) => {
  const { user, wallet, app, modals } = state;
  return {
    user,
    wallet,
    themeType: app.theme,
    isShowIUnderstand: modals[Modals.I_UNDERSTAND].isShow,
  };
};

export default connect(mapStateToProps, { getWalletStat, setUnderstand, closeModalWindow })(Home);
