import styled from 'styled-components';

export const FinanceWrapper: any = styled.div`
  //margin-bottom: 50px;

  input[type='number']::-webkit-outer-spin-button,
  input[type='number']::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }

  input[type='number'] {
    -moz-appearance: textfield;
  }


  .finance__range {
    margin-bottom: 25px;

    span {
      font-size: 12px;
      line-height: 15px;
      color: rgba(209, 209, 209, 0.7);

      &:first-child {
        margin-right: 30px;
      }
    }
  }

  .finance-input {
    position:relative;

    &:after {
      content:'FLEX';
      position: absolute;
      top: 15px;
      right: 15px;
      font-weight: 700;
      font-size: 16px;
      line-height: 20px;
    }
  }

  &.-light {

    .finance__info-items {

      .info-item {

        &__text {
          color: var(--light-brown);
        }

        &__value {
          color: var(--brown);
        }
      }

      .info-item.balance {

        .info-item__value {
          color: #774BFF;
        }
      }
    }

    .finance__currency-items {

      .info-item {

        &__text {
          color: var(--light-brown);
        }

        &__value {
          color: #774BFF;
        }
      }
    }

    .finance__range {

      span {
        color: var(--brown);
      }
    }
`;

export const FinanceInner: any = styled.div`
  position: relative;
  margin-top: 69px;
  display: flex;
  align-items: flex-start;

  .finance-qrcode {
    width: 175px;
    height: 175px;
    display: block;
    margin: 25px auto 0;
    padding: 10px;
    border-radius: var(--border-radius);
    background-color: var(--white);
  }

  @media (max-width: 992px) {
    margin-top: 61px;
    display: block;
  }
`;

export const FinanceContent: any = styled.div`
  display: flex;
  margin-bottom: 45px;
  min-height: 313px;

  @media (max-width: 768px) {
    flex-direction: column;
  }
`;

export const FinanceItem: any = styled.div`
  width: 100%;
  max-width: 388px;

  &:first-child {
    margin-right: 100px;
  }

  .finance__info-items {
    .info-item {
      margin-bottom: 20px;

      &:last-child {
        margin-bottom: 0;
      }

      &:first-child,
      &:nth-child(4) {
        .info-item__divider {
          background-image: linear-gradient(
            -90deg,
            #dd59f8 0%,
            rgba(255, 255, 255, 0.06) 71.01%,
            rgba(255, 255, 255, 0) 99.52%
          );
        }
      }

      &__text {
        font-weight: 400;
        font-size: 14px;
        line-height: 17px;
        color: rgba(255, 255, 255, 0.53);
      }

      &__value {
        font-family: var(--poppins);
        font-weight: 700;
        font-size: 14px;
        line-height: 21px;
        color: rgba(255, 255, 255, 0.53);
      }
    }

    .info-item.balance {
      margin-top: 45px;

      .info-item__divider {
        display: none;
      }

      .info-item__value {
        color: #ff89cb;
      }
    }
  }

  .finance__currency-items {
    margin-bottom: 35px;

    .info-item {
      margin-bottom: 10px;

      &:last-child {
        margin-bottom: 0;
      }

      &__text {
        font-family: var(--poppins);
        font-weight: 300;
        font-size: 12px;
        line-height: 18px;
        color: #989898;
      }

      &__value {
        font-family: var(--poppins);
        font-weight: 700;
        font-size: 14px;
        line-height: 21px;
        color: var(--white);
      }
    }
  }

  @media (max-width: 768px) {
    max-width: 100%;

    &:first-child {
      margin-right: 0;
      margin-bottom: 34px;
    }

    .finance-button {
      height: 46px;
    }
  }

  @media (max-width: 576px) {
    max-width: 100%;

    &:first-child {
      margin-right: 0;
    }

    &:last-child {
      max-width: 100%;
    }

    .finance__btn {
      width: 100%;
    }
  }
`;

export const FinanceItemText: any = styled.p`
  margin: 0 0 13px;
  font-size: 12px;
  line-height: 15px;
  color: #d1d1d1;

  &.-light {
    color: var(--brown);
  }
`;

export const FinanceItemFormGroup: any = styled.div`
  display: flex;
  align-items: flex-start;
  margin-bottom: 25px;

  .finance-dropdown {
    min-width: 115px;
    margin: 0 20px 0 0;

    .MuiSelect-selectMenu {
      padding: 14px 35px 14px 20px;
      font-weight: 700;
      font-size: 16px;
      line-height: 21px;
    }
  }

  .finance-input {
    margin-bottom: 0;
  }

  @media (max-width: 768px) {
    .finance-dropdown {
      margin-right: 10px;
    }
  }
`;

export const FinanceUser: any = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 32px;
`;

export const FinanceUserImg: any = styled.div`
  margin-right: 28px;
  width: 61px;
  height: 61px;
  box-sizing: border-box;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 50%;
  overflow: hidden;

  img {
    width: 100%;
    height: 100%;
    border-radius: 50%;
    overflow: hidden;
    object-fit: cover;
  }
`;

export const FinanceUserText: any = styled.div`
  p {
    max-width: 150px;
    margin: 0 0 7px;
    font-family: var(--poppins);
    font-weight: 700;
    font-size: 14px;
    line-height: 21px;
  }

  span {
    font-size: 11px;
    line-height: 16px;
    color: #8a8c98;
  }
`;

export const FinanceBalance: any = styled.div``;

export const FinanceItemGroup: any = styled.div`
  &.finance__item-output {
    margin-bottom: 30px;
  }

  @media (max-width: 768px) {
    .finance-input {
      width: 100%;
      max-width: 100%;
    }
  }
`;

export const FinanceTable: any = styled.div`
  overflow: auto;
  scrollbar-width: none;

  ::-webkit-scrollbar {
    display: none;
  }

  &.-light {
    .finance-table__header-col {
      color: var(--light-brown);
    }

    .finance-table__body-wrapper {
      background: var(--white);
      box-shadow: 0 0 11px rgba(0, 0, 0, 0.04);
    }

    .finance-table__col {
      color: #999999;
    }

    .finance-table__row {
      &:after {
        height: 1px;
        background-color: #d5d5d5;
      }
    }
  }

  @media (max-width: 1400px) {
    margin-right: -40px;
  }

  @media (max-width: 992px) {
    margin-right: -16px;
  }
`;

export const FinanceTableWrapper: any = styled.div`
  @media (max-width: 1200px) {
    width: 1000px;
    margin-bottom: 10px;
  }

  @media (max-width: 992px) {
    margin-bottom: 0;
  }
`;

export const FinanceTableHeader: any = styled.div`
  padding: 0 40px 0 3px;
`;

export const FinanceTableBodyWrapper: any = styled.div`
  padding: 10px 10px 10px 0;
  background-color: #353b44;
  border-radius: var(--border-radius);
  overflow: hidden;
`;

export const FinanceTableBody: any = styled.div`
  max-height: 443px;
  padding: 0 20px 0 3px;

  .finance-table__btn {
    text-align: center;
    padding: 20px 0;
  }
`;

export const FinanceTableRow: any = styled.div`
  padding: 20px;
  position: relative;
  display: flex;
  align-items: center;
  flex-shrink: 0;

  &::after {
    content: '';
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 3px;
    background-color: #2d333a;
  }

  &:last-child::after {
    display: none;
  }
`;

export const FinanceTableHeaderCol: any = styled.div`
  font-weight: 400;
  font-size: 12px;
  line-height: 15px;
  color: rgba(255, 255, 255, 0.66);

  &:nth-child(1) {
    width: 20%;
  }

  &:nth-child(2) {
    width: 20%;
  }

  &:nth-child(3) {
    width: 20%;
  }

  &:nth-child(4) {
    width: 40%;
  }
`;

export const FinanceTableCol: any = styled.div`
  font-weight: 400;
  font-size: 15px;
  line-height: 18px;
  color: var(--white);

  &:nth-child(1) {
    width: 20%;
  }

  &:nth-child(2) {
    width: 20%;
  }

  &:nth-child(3) {
    width: 20%;
    padding-right: 20px;
  }

  &:nth-child(4) {
    width: 40%;
  }

  &.reject {
    color: #868686;
  }
`;
