import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import QRCode from 'react-qr-code';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import history from '@/common/utils/history';
import BreadCrumb from '@/components/BreadCrumb';
import {
  Container,
  MainContent,
  PageModal,
  Tabs,
  SecondaryButton,
  ReferralInput,
  InfoItem,
} from '@/components/elements';
import {
  ModalTitle,
  ModalDescription,
  ModalText,
  ModalPrice,
  ModalInfo,
  // ModalTime,
} from '@/components/elements/PageModal/PageModal.Styles';
import { TabsContentItem } from '@/components/elements/Tabs/Tabs.Styles';
import { CURRENCIES, CURRENCIES_LIST } from '@/const/app.constants';
import { FinanceInner } from '@/pages/Finances/Finances.Styles';
import { AppStateType } from '@/store';
import { ThemeType } from '@/store/app/reducers';
import { OperationsReducerState } from '@/store/operations/reducers';
import { UserReducerState } from '@/store/user/reducers';
import { WalletReducerState } from '@/store/wallet/reducers';

export interface FinancesFillProps {
  user: UserReducerState;
  operations: OperationsReducerState;
  wallet: WalletReducerState;
  themeType: ThemeType;
  getQrCode: (payload: { link: string }) => void;
}

const FinancesFill: React.FC<any> = (props: FinancesFillProps) => {
  const { user, operations, wallet, themeType } = props;

  const { t } = useTranslation();
  const [timer, setTimer] = useState<number>(12000);
  const [values, setValues] = useState<{ [key: string]: string }>({
    currency: '',
  });
  const [qrCode, setQrCode] = useState<string>('');

  const getRatesSumm = (sum: string, currency: string, returnedCurrency: string) => {
    switch (`${currency}${returnedCurrency}`) {
      case `${CURRENCIES.USDT}${CURRENCIES.BTC}`:
        return Number(sum)
          ? (Number(sum) / Number(wallet.exchangeRates.BTCUSDT || 1)).toFixed(4)
          : 0;
      case `${CURRENCIES.BTC}${CURRENCIES.USDT}`:
        return Number(sum)
          ? (Number(sum) * Number(wallet.exchangeRates.BTCUSDT || 1)).toFixed(2)
          : 0;
      default:
        return sum || 0;
    }
  };

  const onChange = (field: string, value: string) => {
    setValues(prev => ({
      ...prev,
      [field]: value,
    }));

    if (field === 'currency') {
      let link = '';
      switch (value) {
        case `${CURRENCIES.BTC}`:
          link = `${user.userData?.credit_btc_address}`;
          break;
        case `${CURRENCIES.USDT}`:
          link = `${user.userData?.credit_usdt_address}`;
          break;
        default:
          link = '';
      }

      if (link) {
        setQrCode(link);
      }
    }
  };

  useEffect(() => {
    if (!operations.operationCreated) history.push('/finances');
    // onChange('currency', operations.operationCreated?.currency || CURRENCIES.BTC);
  }, [operations]);

  useEffect(() => {
    const s = timer - 1;

    if (s < 1) {
      setTimer(0);
      return () => {};
    }

    const intervalId: ReturnType<typeof setTimeout> = setTimeout(() => {
      setTimer(s);
    }, 1000);

    return () => {
      clearTimeout(intervalId);
    };
  }, [timer]);

  return (
    <MainContent className={`content-main content-main_bg-2 -${themeType}`}>
      <Container>
        <BreadCrumb />
        <FinanceInner>
          <SecondaryButton as={Link} className={`back sm -${themeType}`} to="/finances">
            {t('common.back')}
          </SecondaryButton>
          <PageModal className={`finance-modal -${themeType}`}>
            <ModalTitle className={`modal__title -${themeType}`}>
              {t('finances.fill.title')}
            </ModalTitle>
            <ModalText className="modal__text">{t('finances.fill.texts.text1')}</ModalText>
            <ModalPrice className="modal__price">
              <span>
                {getRatesSumm(
                  operations.operationCreated?.sum || '0',
                  operations.operationCreated?.currency || CURRENCIES.BTC,
                  CURRENCIES.USDT
                )}
              </span>
              {' FLEX'}
            </ModalPrice>
            <ModalInfo className={`modal__info -${themeType}`}>
              <InfoItem
                infoText="USDT"
                infoValue={getRatesSumm(
                  operations.operationCreated?.sum || '0',
                  operations.operationCreated?.currency || CURRENCIES.BTC,
                  CURRENCIES.USDT
                )}
              />
              <InfoItem
                infoText="BTC"
                infoValue={getRatesSumm(
                  operations.operationCreated?.sum || '0',
                  operations.operationCreated?.currency || CURRENCIES.BTC,
                  CURRENCIES.BTC
                )}
              />
            </ModalInfo>
            {!timer ? <p>{t('finances.timer.text')}</p> : null}
            {!timer ? (
              <SecondaryButton as={Link} className={`sm -${themeType}`} to="/finances">
                {t('finances.btns.button_repeat')}
              </SecondaryButton>
            ) : null}
            {timer ? (
              <ModalDescription className="modal__description">
                {t('finances.fill.texts.text2')}
              </ModalDescription>
            ) : null}
            {timer ? (
              <Tabs
                className="primary-tab finance-tab"
                tabHeaderClassName="primary-tab__item"
                tabsList={CURRENCIES_LIST.map((item: string) => {
                  return {
                    key: item,
                    name: item === CURRENCIES.USDT ? `${CURRENCIES.USDT} (ERC20)` : item,
                  };
                })}
                field="currency"
                activeTab={values.currency}
                onChange={onChange}
              >
                <TabsContentItem
                  className={`${CURRENCIES.BTC} ${
                    values.currency === CURRENCIES.BTC ? 'active' : ''
                  }`}
                >
                  <div className="finance-qrcode">
                    <QRCode value={qrCode} size={155} />
                  </div>
                  <ReferralInput
                    className={`finance-ref -${themeType}`}
                    name="referral"
                    type="text"
                    value={`${user.userData?.credit_btc_address}`}
                  />
                </TabsContentItem>
                <TabsContentItem
                  className={`${CURRENCIES.USDT} ${
                    values.currency === CURRENCIES.USDT ? 'active' : ''
                  }`}
                >
                  <div className="finance-qrcode">
                    <QRCode value={qrCode} size={155} />
                  </div>
                  <ReferralInput
                    className={`finance-ref -${themeType}`}
                    name="referral"
                    type="text"
                    value={`${user.userData?.credit_usdt_address}`}
                  />
                </TabsContentItem>
              </Tabs>
            ) : null}
          </PageModal>
        </FinanceInner>
      </Container>
    </MainContent>
  );
};

const mapState = (state: AppStateType) => {
  const { user, operations, wallet, app } = state;
  return {
    user,
    operations,
    wallet,
    themeType: app.theme,
  };
};

export default connect(mapState, {})(FinancesFill);
