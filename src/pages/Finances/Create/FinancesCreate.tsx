import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import history from '@/common/utils/history';
import BreadCrumb from '@/components/BreadCrumb';
import { Container, MainContent, PageModal, SecondaryButton, Icon } from '@/components/elements';
import {
  ModalIcon,
  ModalTitle,
  ModalDescription,
} from '@/components/elements/PageModal/PageModal.Styles';
import { AppStateType } from '@/store';
import { ThemeType } from '@/store/app/reducers';
import { OperationsReducerState } from '@/store/operations/reducers';

export interface FinancesCreateProps {
  themeType: ThemeType;
  operations: OperationsReducerState;
}

const FinancesCreate: React.FC<FinancesCreateProps> = (props: FinancesCreateProps) => {
  const { themeType, operations } = props;
  const { t } = useTranslation();

  useEffect(() => {
    if (!operations.operationCreated) history.push('/finances');
  }, [operations]);

  return (
    <MainContent className={`content-main content-main_bg-2 -${themeType}`}>
      <Container>
        <BreadCrumb />
        <PageModal className={`-${themeType}`}>
          <ModalIcon className="modal__icon">
            <Icon name="ok" size="68" />
          </ModalIcon>
          <ModalTitle className="modal__title">{t('finances.create.title')}</ModalTitle>
          <ModalDescription className="modal__text">{t('finances.create.text')}</ModalDescription>
          <SecondaryButton className={`md -${themeType}`} as={Link} to="/finances">
            {t('finances.btns.button_create')}
          </SecondaryButton>
        </PageModal>
      </Container>
    </MainContent>
  );
};

const mapStateToProps = (state: AppStateType) => {
  const { app, operations } = state;
  return {
    themeType: app.theme,
    operations,
  };
};

export default connect(mapStateToProps)(FinancesCreate);
