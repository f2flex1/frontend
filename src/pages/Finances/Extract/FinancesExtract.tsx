import { validate } from 'bitcoin-address-validation';
import cloneDeep from 'lodash/cloneDeep';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';

import { LogInParams, RegistrationParams } from '@/api';
import history from '@/common/utils/history';
import BreadCrumb from '@/components/BreadCrumb';
import {
  Container,
  MainContent,
  PageModal,
  Label,
  Input,
  Button,
  Loader,
  InfoItem,
} from '@/components/elements';
import {
  ModalTitle,
  ModalText,
  ModalLink,
  ModalPrice,
  ModalInfo,
} from '@/components/elements/PageModal/PageModal.Styles';
import { CURRENCIES, SETTINGS_TABS } from '@/const/app.constants';
import { AppStateType } from '@/store';
import types from '@/store/actionTypes';
import { ThemeType } from '@/store/app/reducers';
import { selectErrorByKey, selectLoadingByKey } from '@/store/loadingsErrors/selectors';
import { getMessages } from '@/store/messages/actions';
import { createOperation, getOperations } from '@/store/operations/actions';
import { OperationCreatedReducerState, OperationsReducerState } from '@/store/operations/reducers';
import { getPackets } from '@/store/packets/actions';
import { fetchUserData } from '@/store/user/actions';
import { UserReducerState } from '@/store/user/reducers';
import { getWalletStat } from '@/store/wallet/actions';
import { WalletReducerState } from '@/store/wallet/reducers';

export interface FinancesExtractProps {
  themeType: ThemeType;
  user: UserReducerState;
  operations: OperationsReducerState;
  wallet: WalletReducerState;
  createOperation: (payload: OperationCreatedReducerState) => void;
  error: string | null;
  loading: boolean;
  getMessages: () => void;
  fetchUserData: () => void;
  getPackets: () => void;
  getOperations: () => void;
  getWalletStat: () => void;
}

const FinancesExtract: React.FC<FinancesExtractProps> = (props: FinancesExtractProps) => {
  const {
    themeType,
    user,
    operations,
    createOperation,
    error,
    loading,
    wallet,
    fetchUserData,
    getMessages,
    getPackets,
    getOperations,
    getWalletStat,
  } = props;
  const { t } = useTranslation();

  const [values, setValues] = useState<{ [key: string]: string }>({
    debit_address: user.userData?.debit_address || '',
  });
  const [errors, setErrors] = useState<{ [key: string]: string }>({ debit_address: '' });

  useEffect(() => {
    if (!operations.operationCreated) history.push('/finances');
    if (operations.operationCreated?.id) {
      fetchUserData();
      getMessages();
      getPackets();
      getOperations();
      getWalletStat();
      history.push('/finances/create');
    }
  }, [operations]);

  useEffect(() => {
    setValues(prev => ({
      ...prev,
      debit_address: user.userData?.debit_address || '',
    }));
  }, [user]);

  const getFormErrors = (data: any) => {
    const { debit_address } = data;
    const newErrors: any = {
      debit_address: '',
    };

    if (!debit_address) newErrors.debit_address = 'finances.page.form.btc-wallet.errors.empty';
    if (debit_address && !validate(debit_address))
      newErrors.debit_address = 'finances.page.form.btc-wallet.errors.valid';

    return newErrors;
  };

  const checkErrors = (data: { [key: string]: string }) => {
    for (const error in data) {
      if (data[error]) return true;
    }
    return false;
  };

  const onChange = (field: string, value: string) => {
    setValues(prev => ({
      ...prev,
      [field]: value,
    }));

    if (!!errors[field]) {
      setErrors({
        ...errors,
        [field]: '',
      });
    }

    if (!value && Object.prototype.hasOwnProperty.call(errors, field)) {
      const newValues = cloneDeep(values);
      newValues[field] = value;
      const newErrors: RegistrationParams = getFormErrors(newValues);

      setErrors({
        ...errors,
        [field]: newErrors[field],
      });
    }
  };

  const onBlur = (field: string) => {
    if (Object.prototype.hasOwnProperty.call(errors, field)) {
      const newValues = cloneDeep(values);
      const newErrors: LogInParams = getFormErrors(newValues);

      setErrors({
        ...errors,
        [field]: newErrors[field],
      });
    }
  };

  const onSubmit = () => {
    const newErrors: LogInParams = getFormErrors(values);
    setErrors(newErrors);

    const requestData: OperationCreatedReducerState = {
      type: 'EXTRACT_SUBMIT',
      sum: operations.operationCreated?.sum || '',
      currency: CURRENCIES.USDT,
    };
    if (!checkErrors(newErrors)) {
      createOperation(requestData);
    }
  };

  const getRatesSumm = (sum: string, currency: string, returnedCurrency: string) => {
    switch (`${currency}${returnedCurrency}`) {
      case `${CURRENCIES.USDT}${CURRENCIES.BTC}`:
        return Number(sum)
          ? (Number(sum) / Number(wallet.exchangeRates.BTCUSDT || 1)).toFixed(4)
          : 0;
      case `${CURRENCIES.BTC}${CURRENCIES.USDT}`:
        return Number(sum)
          ? (Number(sum) * Number(wallet.exchangeRates.BTCUSDT || 1)).toFixed(2)
          : 0;
      default:
        return sum || 0;
    }
  };

  return (
    <MainContent className={`content-main content-main_bg-2 -${themeType}`}>
      <Container>
        <BreadCrumb />
        <PageModal className={`finance-modal -${themeType}`}>
          <ModalTitle className="modal__title">{t('finances.extract.title')}</ModalTitle>
          <ModalText className="modal__text">{t('finances.extract.texts.text1')}</ModalText>
          <ModalPrice className="modal__price mb-none">
            <span>
              {Number((Number(operations.operationCreated?.sum) / 100) * 97)
                .toFixed(2)
                .replace(/\.?0+$/, '')}
            </span>
            {' FLEX'}
          </ModalPrice>
          <span className="modal__price-descr">{t('finances.extract.texts.text5')}</span>
          <ModalInfo className={`modal__info -${themeType}`}>
            <InfoItem
              infoText="USDT"
              infoValue={getRatesSumm(
                `${Number((Number(operations.operationCreated?.sum) / 100) * 97 || 0)
                  .toFixed(2)
                  .replace(/\.?0+$/, '')}`,
                CURRENCIES.USDT,
                CURRENCIES.USDT
              )}
            />
            <InfoItem
              infoText="BTC"
              infoValue={getRatesSumm(
                `${(Number(operations.operationCreated?.sum) / 100) * 97 || 0}`,
                CURRENCIES.USDT,
                CURRENCIES.BTC
              )}
            />
          </ModalInfo>
          {values.debit_address ? (
            <Label className="modal__label left" htmlFor="btc-wallet">
              {t('finances.extract.texts.text2')}
            </Label>
          ) : null}
          {values.debit_address ? (
            <Input
              id="btc-wallet"
              className={`modal__input -${themeType}`}
              type="text"
              name="debit_address"
              placeholder={t('finances.page.form.btc-wallet.placeholder')}
              disabled
              value={values.debit_address}
              error={errors.debit_address}
              onChange={onChange}
              onBlur={onBlur}
            />
          ) : null}
          {error ? <p>{t(`${error}`)}</p> : null}
          <ModalLink className="modal__link" to={`/settings?tab=${SETTINGS_TABS.SECURITY}`}>
            {t(
              `${
                values.debit_address
                  ? 'finances.extract.texts.text3'
                  : 'finances.extract.texts.text4'
              }`
            )}
          </ModalLink>
          <Button
            className={`lg modal__button -${themeType}`}
            onClick={onSubmit}
            disabled={loading || !values.debit_address}
          >
            {t('finances.btns.button_extract')}
            {loading ? <Loader /> : null}
          </Button>
        </PageModal>
      </Container>
    </MainContent>
  );
};

const mapStateToProps = (state: AppStateType) => {
  const { app, user, operations, wallet } = state;
  return {
    themeType: app.theme,
    user,
    operations,
    wallet,
    error: selectErrorByKey(state, types.CREATE_OPERATION_REQUEST),
    loading: selectLoadingByKey(state, types.CREATE_OPERATION_REQUEST),
  };
};

export default connect(mapStateToProps, {
  createOperation,
  fetchUserData,
  getMessages,
  getPackets,
  getOperations,
  getWalletStat,
})(FinancesExtract);
