import cloneDeep from 'lodash/cloneDeep';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';

import { Operation, RegistrationParams } from '@/api';
import { selfMoment } from '@/common/utils/i18n';
import {
  Button,
  Container,
  // DropDown,
  InfoItem,
  Input,
  Loader,
  MainContent,
  Placeholder,
  ScrollBar,
  SecondaryButton,
  Tabs,
  Title,
} from '@/components/elements';
import { Image } from '@/components/elements/Image/Image';
import { TabsContentItem } from '@/components/elements/Tabs/Tabs.Styles';
import {
  CURRENCIES,
  // CURRENCIES_LIST,
  FINANCES_TABS,
  FINANCES_TABS_LIST,
  LEVEL_IMAGES_TYPES,
} from '@/const/app.constants';
import {
  FinanceContent,
  FinanceItem,
  FinanceTable,
  FinanceTableBody,
  FinanceTableCol,
  FinanceTableHeader,
  FinanceTableHeaderCol,
  FinanceTableRow,
  FinanceTableWrapper,
  FinanceUser,
  FinanceUserImg,
  FinanceUserText,
  FinanceWrapper,
  FinanceItemGroup,
  FinanceItemText,
  // FinanceItemFormGroup,
  FinanceTableBodyWrapper,
} from '@/pages/Finances/Finances.Styles';
import { AppStateType } from '@/store';
import types from '@/store/actionTypes';
import { ThemeType } from '@/store/app/reducers';
import { selectLoadingByKey } from '@/store/loadingsErrors/selectors';
import {
  createOperation,
  getOperations,
  getOperationsWithdrawals,
} from '@/store/operations/actions';
import { OperationCreatedReducerState, OperationsReducerState } from '@/store/operations/reducers';
import { UserReducerState } from '@/store/user/reducers';
import { getWalletStat } from '@/store/wallet/actions';
import { WalletReducerState } from '@/store/wallet/reducers';

export interface FinancesProps {
  user: UserReducerState;
  operations: OperationsReducerState;
  wallet: WalletReducerState;
  getOperations: (page?: number) => void;
  getOperationsWithdrawals: () => void;
  getWalletStat: () => void;
  createOperation: (payload: OperationCreatedReducerState) => void;
  themeType: ThemeType;
  loading: boolean;
  loadingOperations: boolean;
}

const Finances: React.FC<FinancesProps> = (props: FinancesProps) => {
  const {
    user,
    operations,
    wallet,
    getOperations,
    getOperationsWithdrawals,
    getWalletStat,
    createOperation,
    themeType,
    loading,
    loadingOperations,
  } = props;
  const { t } = useTranslation();
  const { userData } = user;
  const { operationsList } = operations;

  const [values, setValues] = useState<{ [key: string]: any }>({
    currency: CURRENCIES.USDT,
    operation: FINANCES_TABS.FILL,
    fill_sum: '',
    extract_sum: '',
  });
  const [errors, setErrors] = useState<{ [key: string]: string }>({});

  useEffect(() => {
    if (!operations.operationsWithdrawalsLoaded) {
      getOperationsWithdrawals();
    }
    if (!operations.loaded) {
      getOperations();
    }
    if (!wallet.loaded) {
      getWalletStat();
    }
  }, [operations, wallet, getOperations, getOperationsWithdrawals, getWalletStat]);

  const getFormErrors = (data: any) => {
    const { fill_sum, extract_sum, operation } = data;
    const newErrors: any = {
      fill_sum: '',
      extract_sum: '',
    };

    if (operation === FINANCES_TABS.FILL) {
      if (!fill_sum) newErrors.fill_sum = 'finances.page.form.number.errors.fill';
      if (!!fill_sum && fill_sum < 50)
        newErrors.fill_sum = 'finances.page.form.number.errors.fill_min';
    }

    if (operation === FINANCES_TABS.EXTRACT) {
      if (!extract_sum) newErrors.fill_sum = 'finances.page.form.number.errors.extract';
      if (!!extract_sum && extract_sum < 10)
        newErrors.extract_sum = 'finances.page.form.number.errors.extract_min';
    }

    return newErrors;
  };

  const checkErrors = (data: { [key: string]: string }) => {
    for (const error in data) {
      if (data[error]) return true;
    }
    return false;
  };

  const onChange = (field: string, value: any) => {
    setValues(prev => ({
      ...prev,
      [field]: value,
    }));

    if (!!errors[field]) {
      setErrors({
        ...errors,
        [field]: '',
      });
    }

    if (!value && Object.prototype.hasOwnProperty.call(errors, field)) {
      const newValues = cloneDeep(values);
      newValues[field] = value;
      const newErrors: RegistrationParams = getFormErrors(newValues);

      setErrors({
        ...errors,
        [field]: newErrors[field],
      });
    }
  };

  const onSubmit = () => {
    const newErrors: any = getFormErrors(values);
    setErrors(newErrors);

    if (!checkErrors(newErrors)) {
      const requestData: OperationCreatedReducerState = {
        type: values.operation,
        sum: values.operation === FINANCES_TABS.FILL ? values.fill_sum : values.extract_sum,
        currency: values.currency,
      };
      createOperation(requestData);
    }
  };

  const getRatesSumm = (sum: string, currency: string, returnedCurrency?: string) => {
    switch (`${returnedCurrency || values.currency}${currency}`) {
      case `${CURRENCIES.USDT}${CURRENCIES.BTC}`:
        return Number(sum)
          ? (Number(sum) / Number(wallet.exchangeRates.BTCUSDT || 1)).toFixed(4)
          : 0;
      case `${CURRENCIES.BTC}${CURRENCIES.USDT}`:
        return Number(sum)
          ? (Number(sum) * Number(wallet.exchangeRates.BTCUSDT || 1)).toFixed(2)
          : 0;
      default:
        return (
          Number(sum)
            .toFixed(returnedCurrency || values.currency === CURRENCIES.BTC ? 4 : 2)
            .replace(/\.?0+$/, '') || 0
        );
    }
  };

  const getRatesBtc = (sum: number) => {
    return values.currency === CURRENCIES.BTC
      ? (sum / Number(wallet.exchangeRates.BTCUSDT || 1)).toFixed(4)
      : sum;
  };

  const getMaxExternal = (value: string) => {
    return Number(value) > getRatesBtc(500000)
      ? getRatesBtc(500000)
      : Number(user.userData?.balance_usd);
  };

  const getOperationsDesc = (operation: Operation) => {
    switch (operation.type_name) {
      case 'PACKET_CREDIT':
        return t(`operations.${operation.type_name}`, operation);
      case 'PACKET_DAILY_DIVIDEND':
        return t(`operations.${operation.type_name}`, operation);
      case 'PACKET_PARTNER_PERCENT_BY_PERCENT':
        return t(`operations.${operation.type_name}`, operation);
      case 'PACKET_PARTNER_CREDIT_REWARD':
        return t(`operations.${operation.type_name}`, operation);
      case 'PROGRAM_AUTO_CREDIT':
        return t(`operations.${operation.type_name}`, operation);
      case 'PROGRAM_AUTO_DEBIT':
        return t(`operations.${operation.type_name}`, operation);
      case 'PROGRAM_AUTO_PARTNER_REWARD':
        return t(`operations.${operation.type_name}`, operation);
      case 'PROGRAM_MARKET_CREDIT':
        return t(`operations.${operation.type_name}`, operation);
      case 'PROGRAM_MARKET_DEBIT':
        return t(`operations.${operation.type_name}`, operation);
      case 'PROGRAM_MARKET_PARTNER_REWARD':
        return t(`operations.${operation.type_name}`, operation);
      case 'PROGRAM_REALTY_CREDIT':
        return t(`operations.${operation.type_name}`, operation);
      case 'PROGRAM_REALTY_DEBIT':
        return t(`operations.${operation.type_name}`, operation);
      case 'PROGRAM_REALTY_PARTNER_REWARD':
        return t(`operations.${operation.type_name}`, operation);
      case 'BALANCE_CREDIT':
        return t(`operations.${operation.type_name}`, operation);
      case 'BALANCE_DEBIT':
        return t(`operations.${operation.type_name}`, operation);
      case 'BONUS_LEVEL_BALANCE_CREDIT':
        return t(`operations.${operation.type_name}`, {
          level: t(`levels.${operation.level}`),
          amount_usd: operation.amount_usd,
        });
      case 'BONUS_LEVEL_PACKET_CREDIT':
        return t(`operations.${operation.type_name}`, {
          level: t(`levels.${operation.level}`),
          packet_name: operation.packet_name,
        });
      case 'CORRECTION_BALANCE':
        return t(`operations.${operation.type_name}`, operation);
      case 'WITHDRAW_REQUEST':
        return t('operations.WITHDRAW_REQUEST', {
          amount_usd: operation.amount_usd,
          wallet_currency: 'BTC',
          wallet_address: user.userData?.debit_address,
          id: operation.id,
        });
      default:
        return '';
    }
  };

  const loadMore = () => {
    getOperations(operations.page + 1);
  };

  return (
    <MainContent className={`content-main -${themeType}`}>
      <Container>
        <Title className={`-${themeType}`}>{t('finances.page.title')}</Title>
        <FinanceWrapper className={`finance -${themeType}`}>
          <FinanceContent className="finance-content">
            <FinanceItem className="finance__item">
              <FinanceUser className="finance-user">
                <FinanceUserImg className="finance-user__img">
                  <Image
                    src={LEVEL_IMAGES_TYPES[userData?.level_info?.level || 'BEGINNER']}
                    alt="rank"
                    variant="circle"
                    animation="wave"
                    width={49}
                    height={49}
                  />
                </FinanceUserImg>
                <FinanceUserText className={`finance-user__text -${themeType}`}>
                  <p>
                    {`${userData?.first_name || userData?.phone || userData?.email} ${
                      userData?.last_name ?? ''
                    }`}
                  </p>
                  <span>{`id ${userData?.ref_id ?? ''}`}</span>
                </FinanceUserText>
              </FinanceUser>
              <div className="finance__info-items">
                <InfoItem
                  infoText={t('finances.page.texts.text1')}
                  infoValue={`${
                    wallet.stat?.invested ? Number(wallet.stat?.invested).toFixed(2) : 0
                  } FLEX`}
                />
                <InfoItem
                  infoText={t('finances.page.texts.text2')}
                  infoValue={`${
                    wallet.stat?.waiting ? Number(wallet.stat?.waiting).toFixed(2) : 0
                  } FLEX`}
                />
                <InfoItem
                  infoText={t('finances.page.texts.text3')}
                  infoValue={`${
                    wallet.stat?.income ? Number(wallet.stat?.income).toFixed(2) : 0
                  } FLEX`}
                />
                <InfoItem
                  infoText={t('finances.page.texts.text4')}
                  infoValue={`${
                    wallet.stat?.profit ? Number(wallet.stat?.profit).toFixed(2) : 0
                  } FLEX`}
                />
                <InfoItem
                  className="balance"
                  infoText={t('finances.page.texts.text5')}
                  infoValue={`${
                    wallet.stat?.balance ? Number(wallet.stat?.balance).toFixed(2) : 0
                  } FLEX`}
                />
              </div>
            </FinanceItem>

            <FinanceItem className="finance__item">
              <div className="finance-box">
                <Tabs
                  className={`finance-header -${themeType}`}
                  tabHeaderClassName={`finance-header_tab -${themeType}`}
                  tabsList={FINANCES_TABS_LIST.map((item: string) => {
                    return {
                      key: item,
                      name: t(`finances.tabs.${item}`),
                    };
                  })}
                  field="operation"
                  activeTab={values.operation}
                  onChange={onChange}
                >
                  <TabsContentItem
                    className={`finance-tab ${values.operation === 'EXTRACT' ? '' : 'active'}`}
                  >
                    <FinanceItemGroup>
                      <FinanceItemText className={`-${themeType}`}>
                        {t('finances.page.texts.text6')}
                      </FinanceItemText>
                      <div className="finance__range">
                        <span>Min: 50 FLEX</span>
                        <span>Max: 500 000 FLEX</span>
                      </div>
                      {/* <FinanceItemFormGroup> */}
                      {/* <DropDown*/}
                      {/*  className={`finance-dropdown -${themeType}`}*/}
                      {/*  name="currency"*/}
                      {/*  value={values.currency}*/}
                      {/*  list={CURRENCIES_LIST}*/}
                      {/*  disableUnderline*/}
                      {/*  onChange={onChange}*/}
                      {/* /> */}
                      <Input
                        className={`finance-input sm -${themeType} ${
                          errors.fill_sum ? '-error' : ''
                        }`}
                        type="number"
                        name="fill_sum"
                        value={values.fill_sum}
                        error={errors.fill_sum}
                        placeholder={t('finances.page.form.number.placeholders.fill')}
                        onChange={(field: string, value: string) =>
                          onChange(
                            field,
                            Number(value) > getRatesBtc(500000) ? getRatesBtc(500000) : value
                          )}
                      />
                      {/* </FinanceItemFormGroup> */}
                    </FinanceItemGroup>
                    <div className="finance__currency-items">
                      <InfoItem
                        infoText="USDT"
                        infoValue={getRatesSumm(values.fill_sum, CURRENCIES.USDT)}
                      />
                      <InfoItem
                        infoText="BTC"
                        infoValue={getRatesSumm(values.fill_sum, CURRENCIES.BTC)}
                      />
                    </div>
                    <Button
                      className={`finance__btn lg -${themeType}`}
                      onClick={() => onSubmit()}
                      disabled={!values.fill_sum || values.fill_sum === '0' || loading}
                    >
                      {t('finances.btns.button_fill')}
                      {loading ? <Loader /> : null}
                    </Button>
                  </TabsContentItem>
                  <TabsContentItem
                    className={`finance-tab ${values.operation === 'EXTRACT' ? 'active' : ''}`}
                  >
                    <FinanceItemGroup className="finance__item-output">
                      <FinanceItemText className={`-${themeType}`}>
                        {t('finances.page.texts.text7')}
                      </FinanceItemText>
                      <div className="finance__range">
                        <span>Min: 10 FLEX</span>
                        <span>Max: 500 000 FLEX</span>
                      </div>
                      <Input
                        className={`finance-input sm -${themeType} ${
                          errors.extract_sum ? '-error' : ''
                        }`}
                        type="number"
                        name="extract_sum"
                        value={values.extract_sum}
                        error={errors.extract_sum}
                        placeholder={t('finances.page.form.number.placeholders.extract')}
                        onChange={(field: string, value: string) =>
                          onChange(
                            field,
                            Number(value) > Number(user.userData?.balance_usd)
                              ? getMaxExternal(value)
                              : value
                          )}
                      />
                    </FinanceItemGroup>
                    <div className="finance__currency-items">
                      <InfoItem
                        infoText="USDT"
                        infoValue={getRatesSumm(
                          values.extract_sum,
                          CURRENCIES.USDT,
                          CURRENCIES.USDT
                        )}
                      />
                      <InfoItem
                        infoText="BTC"
                        infoValue={getRatesSumm(
                          values.extract_sum,
                          CURRENCIES.BTC,
                          CURRENCIES.USDT
                        )}
                      />
                    </div>
                    <Button
                      className={`finance__btn lg -${themeType}`}
                      onClick={() => onSubmit()}
                      disabled={!values.extract_sum || values.extract_sum === '0' || loading}
                    >
                      {t('finances.btns.button_extract')}
                      {loading ? <Loader /> : null}
                    </Button>
                  </TabsContentItem>
                </Tabs>
              </div>
            </FinanceItem>
          </FinanceContent>

          {!operationsList.length ? (
            <Placeholder className={`-${themeType}`}>
              <p>{t('finances.table.placeholder.title')}</p>
              <span>{t('finances.table.placeholder.text')}</span>
            </Placeholder>
          ) : (
            <FinanceTable className={`finance-table -${themeType}`}>
              <FinanceTableWrapper className="finance-table__wrapper">
                <FinanceTableHeader className="finance-table__header">
                  <FinanceTableRow className="finance-table__row">
                    <FinanceTableHeaderCol className="finance-table__header-col">
                      {t('finances.table.headers.col1')}
                    </FinanceTableHeaderCol>
                    <FinanceTableHeaderCol className="finance-table__header-col">
                      {t('finances.table.headers.col2')}
                    </FinanceTableHeaderCol>
                    <FinanceTableHeaderCol className="finance-table__header-col">
                      {t('finances.table.headers.col3')}
                    </FinanceTableHeaderCol>
                    <FinanceTableHeaderCol className="finance-table__header-col">
                      {t('finances.table.headers.col4')}
                    </FinanceTableHeaderCol>
                  </FinanceTableRow>
                </FinanceTableHeader>
                <FinanceTableBodyWrapper className="finance-table__body-wrapper">
                  <ScrollBar
                    className={`finance-scrollbar -${themeType}`}
                    style={{ maxHeight: '440px' }}
                  >
                    <FinanceTableBody className="finance-table__body">
                      {operationsList.length
                        ? operationsList.map((operation: Operation, index: number) => (
                          <FinanceTableRow
                            key={`finances-operation-${index + 1}`}
                            className="finance-table__row"
                          >
                            <FinanceTableCol className="finance-table__col">
                              {`${operation.amount_usd} FLEX`}
                            </FinanceTableCol>
                            <FinanceTableCol className="finance-table__col reject">
                              {t(`statuses.${operation.status_name}`)}
                            </FinanceTableCol>
                            <FinanceTableCol className="finance-table__col">
                              {selfMoment(operation.created).format('DD MMMM YYYY, HH:mm:ss')}
                            </FinanceTableCol>
                            <FinanceTableCol className="finance-table__col">
                              {getOperationsDesc(operation)}
                            </FinanceTableCol>
                          </FinanceTableRow>
                        ))
                        : null}

                      {!operations.end && !!operationsList.length ? (
                        <div className="finance-table__btn">
                          <SecondaryButton
                            className={`sm -${themeType}`}
                            onClick={() => loadMore()}
                            disabled={loadingOperations}
                          >
                            {t('home.notifications.load_more')}
                            {loadingOperations ? <Loader /> : null}
                          </SecondaryButton>
                        </div>
                      ) : null}
                    </FinanceTableBody>
                  </ScrollBar>
                </FinanceTableBodyWrapper>
              </FinanceTableWrapper>
            </FinanceTable>
          )}
        </FinanceWrapper>
      </Container>
    </MainContent>
  );
};

const mapState = (state: AppStateType) => {
  const { user, operations, wallet, app } = state;
  return {
    user,
    operations,
    wallet,
    themeType: app.theme,
    loading: selectLoadingByKey(state, types.CREATE_OPERATION_REQUEST),
    loadingOperations: selectLoadingByKey(state, types.GET_MESSAGES_REQUEST),
  };
};

export default connect(mapState, {
  getOperations,
  getOperationsWithdrawals,
  getWalletStat,
  createOperation,
})(Finances);
