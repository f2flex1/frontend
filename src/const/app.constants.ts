export enum CURRENCIES {
  BTC = 'BTC',
  USDT = 'USDT',
}

export const CURRENCIES_LIST = ['BTC', 'USDT'];

export enum FINANCES_TABS {
  FILL = 'FILL',
  EXTRACT = 'EXTRACT',
}

export const FINANCES_TABS_LIST = ['FILL', 'EXTRACT'];

export enum SETTINGS_TABS {
  PROFILE = 'PROFILE',
  SECURITY = 'SECURITY',
  PASSWORD = 'PASSWORD',
}

export const SETTINGS_TABS_LIST = ['PROFILE', 'SECURITY', 'PASSWORD'];

export enum REGISTRATION_TABS {
  EMAIL = 'EMAIL',
  MOBILE = 'MOBILE',
}

export const REGISTRATION_TABS_LIST = ['EMAIL', 'MOBILE'];

export enum ADDITIONALLY_TABS {
  MATERIALS = 'MATERIALS',
  KYC = 'KYC',
}

export const ADDITIONALLY_TABS_LIST = ['MATERIALS', 'KYC'];

export enum FAQ_TABS {
  GENERAL = 'GENERAL',
  REGISTRATION = 'REGISTRATION',
  PAYMENT = 'PAYMENT',
  GUARANTEES = 'GUARANTEES',
  REFERRAL_PROGRAM = 'REFERRAL_PROGRAM',
  OTHER = 'OTHER',
}

export const FAQ_TABS_LIST = [
  'GENERAL',
  'REGISTRATION',
  'PAYMENT',
  'GUARANTEES',
  'REFERRAL_PROGRAM',
  'OTHER',
];

export const CONTRACT_HISTORY_TABS: any = {
  ALL: 'ALL_PERIOD',
  '01': 'JANUARY',
  '02': 'FEBRUARY',
  '03': 'MARCH',
  '04': 'APRIL',
  '05': 'MAY',
  '06': 'JUNE',
  '07': 'JULY',
  '08': 'AUGUST',
  '09': 'SEPTEMBER',
  '10': 'OKTOBER',
  '11': 'NOVEMBER',
  '12': 'DECEMBER',
};

export const CONTRACT_HISTORY_TABS_LIST = [
  'ALL_PERIOD',
  'JANUARY',
  'FEBRUARY',
  'MARCH',
  'APRIL',
  'MAY',
  'JUNE',
  'JULY',
  'AUGUST',
  'SEPTEMBER',
  'OKTOBER',
  'NOVEMBER',
  'DECEMBER',
];

export const REGISTRATION_INPUT_LIST = ['email', 'password', 'password2', 'phone'];
export const LOGIN_INPUT_LIST = ['email', 'password', 'phone'];
export const VERIFICATION_INPUT_LIST = ['code'];
export const CHANGE_PASSWORD_INPUT_LIST = ['code', 'password'];
export const CHANGE_USER_PASSWORD_INPUT_LIST = ['old_password', 'new_password'];
export const PRIVATE_SIGNUP_LIST = ['id', 'token', 'non_field_errors'];

export enum VERIFICATION_TYPE {
  EMAIL = 'EMAIL',
  MOBILE = 'MOBILE',
}

export enum ALERT_TYPES {
  DANGER = 'DANGER',
  SUCCESS = 'SUCCESS',
}

export enum ALERT_CLASSES {
  DANGER = 'alert-danger',
  SUCCESS = 'alert-success',
}

export enum MESSAGES_TYPES {
  INVITE_USER = 'INVITE_USER',
  PERCENT_BY_PACKETS = 'PERCENT_BY_PACKETS',
  LEVEL_UP = 'LEVEL_UP',
}

export enum LEVEL_STYLES_TYPES {
  BEGINNER = 'beginner',
  AMETIS_JUNIOR = 'ametis-junior',
  AMETIS_MIDDLE = 'ametis-middle',
  AMETIS_SENIOR = 'ametis-senior',
  AMETIS_LEAD = 'ametis-lead',
  LAZURIT_JUNIOR = 'lazurit-junior',
  LAZURIT_MIDDLE = 'lazurit-middle',
  LAZURIT_SENIOR = 'lazurit-senior',
  LAZURIT_LEAD = 'lazurit-lead',
  NEFRIT_JUNIOR = 'nefrit-junior',
  NEFRIT_MIDDLE = 'nefrit-middle',
  NEFRIT_SENIOR = 'nefrit-senior',
  NEFRIT_LEAD = 'nefrit-lead',
  EMERALD_JUNIOR = 'emerald-junior',
  EMERALD_MIDDLE = 'emerald-middle',
  EMERALD_SENIOR = 'emerald-senior',
  EMERALD_LEAD = 'emerald-lead',
  RUBY_JUNIOR = 'ruby-junior',
  RUBY_MIDDLE = 'ruby-middle',
  RUBY_SENIOR = 'ruby-senior',
  RUBY_LEAD = 'ruby-lead',
  SAPPHIRE_JUNIOR = 'saphire-junior',
  SAPPHIRE_MIDDLE = 'saphire-middle',
  SAPPHIRE_SENIOR = 'saphire-senior',
  SAPPHIRE_LEAD = 'saphire-lead',
  DIAMOND_JUNIOR = 'diamond-junior',
  DIAMOND_MIDDLE = 'diamond-middle',
  DIAMOND_SENIOR = 'diamond-senior',
  DIAMOND_LEAD = 'diamond-lead',
}

export enum LEVEL_IMAGES_TYPES {
  BEGINNER = '/img/status/beginner.svg',
  AMETIS_JUNIOR = '/img/status/ametis.svg',
  AMETIS_MIDDLE = '/img/status/ametis-middle.svg',
  AMETIS_SENIOR = '/img/status/ametis-senior.svg',
  AMETIS_LEAD = '/img/status/ametis-lead.svg',
  LAZURIT_JUNIOR = '/img/status/lazurit.svg',
  LAZURIT_MIDDLE = '/img/status/lazurit-middle.svg',
  LAZURIT_SENIOR = '/img/status/lazurit-senior.svg',
  LAZURIT_LEAD = '/img/status/lazurit-lead.svg',
  NEFRIT_JUNIOR = '/img/status/nefrit.svg',
  NEFRIT_MIDDLE = '/img/status/nefrit-middle.svg',
  NEFRIT_SENIOR = '/img/status/nefrit-senior.svg',
  NEFRIT_LEAD = '/img/status/nefrit-lead.svg',
  EMERALD_JUNIOR = '/img/status/emerald.svg',
  EMERALD_MIDDLE = '/img/status/emerald-middle.svg',
  EMERALD_SENIOR = '/img/status/emerald-senior.svg',
  EMERALD_LEAD = '/img/status/emerald-lead.svg',
  RUBY_JUNIOR = '/img/status/ruby.svg',
  RUBY_MIDDLE = '/img/status/ruby-middle.svg',
  RUBY_SENIOR = '/img/status/ruby-senior.svg',
  RUBY_LEAD = '/img/status/ruby-lead.svg',
  SAPPHIRE_JUNIOR = '/img/status/saphire.svg',
  SAPPHIRE_MIDDLE = '/img/status/saphire-middle.svg',
  SAPPHIRE_SENIOR = '/img/status/saphire-senior.svg',
  SAPPHIRE_LEAD = '/img/status/saphire-lead.svg',
  DIAMOND_JUNIOR = '/img/status/diamond.svg',
  DIAMOND_MIDDLE = '/img/status/diamond-middle.svg',
  DIAMOND_SENIOR = '/img/status/diamond-senior.svg',
  DIAMOND_LEAD = '/img/status/diamond-lead.svg',
}

export enum NOTIFICATIONS_ICONS {
  DEFAULT = '/img/bell/credit.svg',
  PERCENT_BY_PACKETS = '/img/bell/reward.svg',
  PERCENT_BY_PERCENT = '/img/bell/income.svg',
  BALANCE_CREDIT = '/img/bell/credit.svg',
  BALANCE_DEBIT = '/img/bell/credit.svg',
  OPEN_PACKET = '/img/bell/contract.svg',
  DAILY_DIVIDEND = '/img/bell/credit.svg',
  OPEN_BONUS = '/img/bell/contract.svg',
  INVITE_USER = '/img/bell/user.svg',
  PASSWORD_CHANGE = '/img/bell/credit.svg',
}

export const DISABLED_COUNTRIES_CODES_LIST = ['us', 'ae', 'cn', 'pt', 'es', 'it', 'kr'];

export enum LIMITS {
  MESSAGES = 100,
  OPERATIONS = 100,
}

export const BEFORE_LOUNCH_DATE = '2021-12-03T12:00:00+03:00';

export enum SOCIAL_NETWORKS {
  FACEBOOK = 'FACEBOOK',
  INSTAGRAM = 'INSTAGRAM',
  TELEGRAM = 'TELEGRAM',
  TWITTER = 'TWITTER',
  VK = 'VK',
  YOUTUBE = 'YOUTUBE',
  OK = 'OK',
}

export const SOCIAL_NETWORKS_LANGUAGE = {
  en: {
    list: [
      SOCIAL_NETWORKS.TELEGRAM,
      SOCIAL_NETWORKS.FACEBOOK,
      SOCIAL_NETWORKS.INSTAGRAM,
      SOCIAL_NETWORKS.TWITTER,
      SOCIAL_NETWORKS.YOUTUBE,
    ],
    links: {
      facebook: 'https://www.facebook.com/Fin2Flex',
      instagram: 'https://instagram.com/fin2flex_en',
      youtube: 'https://www.youtube.com/channel/UCalGmZovXEA-yK4OyD_JLQw',
      twitter: 'https://twitter.com/Fin2Flex',
      telegram: {
        bot: 'https://t.me/fin2flexen',
        chat: 'https://t.me/FIN2FLEX_ENG',
      },
    },
  },
  ru: {
    list: [
      SOCIAL_NETWORKS.TELEGRAM,
      SOCIAL_NETWORKS.VK,
      SOCIAL_NETWORKS.INSTAGRAM,
      SOCIAL_NETWORKS.YOUTUBE,
      SOCIAL_NETWORKS.OK,
    ],
    links: {
      vk: 'https://vk.com/fin2_flex',
      instagram: 'https://instagram.com/fin2flex',
      youtube: 'https://www.youtube.com/c/FIN2FLEX',
      ok: 'https://ok.ru/fin2flex',
      telegram: {
        bot: 'https://t.me/fin2flex',
        chat: 'https://t.me/fin2flex_chat',
      },
    },
  },
  th: {
    list: [SOCIAL_NETWORKS.TELEGRAM, SOCIAL_NETWORKS.YOUTUBE],
    links: {
      youtube: 'https://www.youtube.com/channel/UCalGmZovXEA-yK4OyD_JLQw',
      telegram: {
        bot: 'https://t.me/fin2flexen',
        chat: 'https://t.me/Fin2Flex_TH',
      },
    },
  },
  es: {
    list: [
      SOCIAL_NETWORKS.TELEGRAM,
      SOCIAL_NETWORKS.FACEBOOK,
      SOCIAL_NETWORKS.INSTAGRAM,
      SOCIAL_NETWORKS.TWITTER,
      SOCIAL_NETWORKS.YOUTUBE,
    ],
    links: {
      facebook: 'https://www.facebook.com/Fin2Flex',
      instagram: 'https://instagram.com/fin2flex_en',
      youtube: 'https://www.youtube.com/channel/UCalGmZovXEA-yK4OyD_JLQw',
      twitter: 'https://twitter.com/Fin2Flex',
      telegram: {
        bot: 'https://t.me/fin2flexen',
        chat: 'https://t.me/fin2flex_es',
      },
    },
  },
  kk: {
    list: [
      SOCIAL_NETWORKS.TELEGRAM,
      SOCIAL_NETWORKS.VK,
      SOCIAL_NETWORKS.INSTAGRAM,
      SOCIAL_NETWORKS.YOUTUBE,
      SOCIAL_NETWORKS.OK,
    ],
    links: {
      vk: 'https://vk.com/fin2_flex',
      instagram: 'https://instagram.com/fin2flex',
      youtube: 'https://www.youtube.com/c/FIN2FLEX',
      ok: 'https://ok.ru/fin2flex',
      telegram: {
        bot: 'https://t.me/fin2flex',
        chat: 'https://t.me/fin2flex_kz',
      },
    },
  },
};

export const ADDITIONALLY_MATERIALS_LINKS = {
  presentations: [
    {
      lang: 'en',
      title: 'settings.collapses.titles.title1',
      prefix: 'EN',
    },
    {
      lang: 'ru',
      title: 'settings.collapses.titles.title1',
      prefix: 'RU',
    },
    {
      lang: 'es',
      title: 'settings.collapses.titles.title1',
      prefix: 'ES',
    },
    {
      lang: 'it',
      title: 'settings.collapses.titles.title1',
      prefix: 'IT',
    },
    {
      lang: 'kk',
      title: 'settings.collapses.titles.title1',
      prefix: 'KZ',
    },
    {
      lang: 'ko',
      title: 'settings.collapses.titles.title1',
      prefix: 'KO',
    },
    {
      lang: 'pt',
      title: 'settings.collapses.titles.title1',
      prefix: 'PT',
    },
    {
      lang: 'th',
      title: 'settings.collapses.titles.title1',
      prefix: 'TH',
    },
    {
      lang: 'tr',
      title: 'settings.collapses.titles.title1',
      prefix: 'TR',
    },
    {
      lang: 'vi',
      title: 'settings.collapses.titles.title1',
      prefix: 'VIE',
    },
    {
      lang: 'zh',
      title: 'settings.collapses.titles.title1',
      prefix: 'CH',
    },
    {
      lang: 'fr',
      title: 'settings.collapses.titles.title1',
      prefix: 'FR',
    },
  ],
  marketings: [
    {
      lang: 'en',
      title: 'settings.collapses.titles.title2',
      prefix: 'EN',
    },
    {
      lang: 'ru',
      title: 'settings.collapses.titles.title2',
      prefix: 'RU',
    },
    {
      lang: 'es',
      title: 'settings.collapses.titles.title2',
      prefix: 'ES',
    },
    {
      lang: 'it',
      title: 'settings.collapses.titles.title2',
      prefix: 'IT',
    },
    {
      lang: 'kk',
      title: 'settings.collapses.titles.title2',
      prefix: 'KZ',
    },
    {
      lang: 'ko',
      title: 'settings.collapses.titles.title2',
      prefix: 'KO',
    },
    {
      lang: 'pt',
      title: 'settings.collapses.titles.title2',
      prefix: 'PT',
    },
    {
      lang: 'th',
      title: 'settings.collapses.titles.title2',
      prefix: 'TH',
    },
    {
      lang: 'tr',
      title: 'settings.collapses.titles.title2',
      prefix: 'TR',
    },
    {
      lang: 'vi',
      title: 'settings.collapses.titles.title2',
      prefix: 'VIE',
    },
    {
      lang: 'zh',
      title: 'settings.collapses.titles.title2',
      prefix: 'CH',
    },
    {
      lang: 'fr',
      title: 'settings.collapses.titles.title2',
      prefix: 'FR',
    },
  ],
  brandbooks: [
    {
      lang: 'en',
      title: 'settings.collapses.texts.text3',
      prefix: 'EN',
    },
    {
      lang: 'ru',
      title: 'settings.collapses.texts.text3',
      prefix: 'RU',
    },
    {
      lang: 'es',
      title: 'settings.collapses.texts.text3',
      prefix: 'ES',
    },
    {
      lang: 'it',
      title: 'settings.collapses.texts.text3',
      prefix: 'IT',
    },
    {
      lang: 'kk',
      title: 'settings.collapses.texts.text3',
      prefix: 'KZ',
    },
    {
      lang: 'ko',
      title: 'settings.collapses.texts.text3',
      prefix: 'KO',
    },
    {
      lang: 'pt',
      title: 'settings.collapses.texts.text3',
      prefix: 'PT',
    },
    {
      lang: 'th',
      title: 'settings.collapses.texts.text3',
      prefix: 'TH',
    },
    {
      lang: 'tr',
      title: 'settings.collapses.texts.text3',
      prefix: 'TR',
    },
    {
      lang: 'vi',
      title: 'settings.collapses.texts.text3',
      prefix: 'VIE',
    },
    {
      lang: 'zh',
      title: 'settings.collapses.texts.text3',
      prefix: 'CH',
    },
    {
      lang: 'fr',
      title: 'settings.collapses.texts.text3',
      prefix: 'FR',
    },
  ],
  completeBrandbooks: [
    {
      lang: 'en',
      title: 'settings.collapses.texts.text4',
      prefix: 'EN',
    },
    {
      lang: 'ru',
      title: 'settings.collapses.texts.text4',
      prefix: 'RU',
    },
    {
      lang: 'es',
      title: 'settings.collapses.texts.text4',
      prefix: 'ES',
    },
    {
      lang: 'it',
      title: 'settings.collapses.texts.text4',
      prefix: 'IT',
    },
    {
      lang: 'kk',
      title: 'settings.collapses.texts.text4',
      prefix: 'KZ',
    },
    {
      lang: 'ko',
      title: 'settings.collapses.texts.text4',
      prefix: 'KO',
    },
    {
      lang: 'pt',
      title: 'settings.collapses.texts.text4',
      prefix: 'PT',
    },
    {
      lang: 'th',
      title: 'settings.collapses.texts.text4',
      prefix: 'TH',
    },
    {
      lang: 'tr',
      title: 'settings.collapses.texts.text4',
      prefix: 'TR',
    },
    {
      lang: 'vi',
      title: 'settings.collapses.texts.text4',
      prefix: 'VIE',
    },
    {
      lang: 'zh',
      title: 'settings.collapses.texts.text4',
      prefix: 'CH',
    },
    {
      lang: 'fr',
      title: 'settings.collapses.texts.text4',
      prefix: 'FR',
    },
  ],
};

export const ADDITIONALLY_MATERIALS = {
  en: {
    presentation: 'https://f2f.ac/materials/en/PresentationEN.pdf',
    marketing: 'https://f2f.ac/materials/en/MarketingEN.pdf',
    brandbook: 'https://f2f.ac/materials/en/BrandbookEN.pdf',
    completeBrandbook: 'https://drive.google.com/drive/folders/1_ywUBKJx3uEuzy-qe8oJgZ2oEJpTwGQg',
  },
  ru: {
    presentation: 'https://f2f.ac/materials/ru/PresentationRU.pdf',
    marketing: 'https://f2f.ac/materials/ru/MarketingRU.pdf',
    brandbook: 'https://f2f.ac/materials/ru/BrandbookRU.pdf',
    completeBrandbook: 'https://drive.google.com/drive/folders/1_ywUBKJx3uEuzy-qe8oJgZ2oEJpTwGQg',
  },
  es: {
    presentation: 'https://f2f.ac/materials/es/PresentationES.pdf',
    marketing: 'https://f2f.ac/materials/es/MarketingES.pdf',
    brandbook: 'https://f2f.ac/materials/es/BrandbookES.pdf',
    completeBrandbook: 'https://drive.google.com/drive/folders/1_ywUBKJx3uEuzy-qe8oJgZ2oEJpTwGQg',
  },
  it: {
    presentation: 'https://f2f.ac/materials/it/PresentationIT.pdf',
    marketing: 'https://f2f.ac/materials/it/MarketingIT.pdf',
    brandbook: 'https://f2f.ac/materials/it/BrandbookIT.pdf',
    completeBrandbook: 'https://drive.google.com/drive/folders/1_ywUBKJx3uEuzy-qe8oJgZ2oEJpTwGQg',
  },
  ja: {
    presentation: '',
    marketing: '',
    brandbook: '',
    completeBrandbook: 'https://drive.google.com/drive/folders/1_ywUBKJx3uEuzy-qe8oJgZ2oEJpTwGQg',
  },
  kk: {
    presentation: 'https://f2f.ac/materials/kz/PresentationKZ.pdf',
    marketing: 'https://f2f.ac/materials/kz/MarketingKZ.pdf',
    brandbook: 'https://f2f.ac/materials/kz/BrandbookKZ.pdf',
    completeBrandbook: 'https://drive.google.com/drive/folders/1_ywUBKJx3uEuzy-qe8oJgZ2oEJpTwGQg',
  },
  ko: {
    presentation: 'https://f2f.ac/materials/ko/PresentationKO.pdf',
    marketing: 'https://f2f.ac/materials/ko/MarketingKO.pdf',
    brandbook: 'https://f2f.ac/materials/ko/BrandbookKO.pdf',
    completeBrandbook: 'https://drive.google.com/drive/folders/1_ywUBKJx3uEuzy-qe8oJgZ2oEJpTwGQg',
  },
  pt: {
    presentation: 'https://f2f.ac/materials/pt/PresentationPT.pdf',
    marketing: 'https://f2f.ac/materials/pt/MarketingPT.pdf',
    brandbook: 'https://f2f.ac/materials/pt/BrandbookPT.pdf',
    completeBrandbook: 'https://drive.google.com/drive/folders/1_ywUBKJx3uEuzy-qe8oJgZ2oEJpTwGQg',
  },
  th: {
    presentation: 'https://f2f.ac/materials/th/PresentationTH.pdf',
    marketing: 'https://f2f.ac/materials/th/MarketingTH.pdf',
    brandbook: 'https://f2f.ac/materials/th/BrandbookTH.pdf',
    completeBrandbook: 'https://drive.google.com/drive/folders/1_ywUBKJx3uEuzy-qe8oJgZ2oEJpTwGQg',
  },
  tr: {
    presentation: 'https://f2f.ac/materials/tr/PresentationTR.pdf',
    marketing: 'https://f2f.ac/materials/tr/MarketingTR.pdf',
    brandbook: 'https://f2f.ac/materials/tr/BrandbookTR.pdf',
    completeBrandbook: 'https://drive.google.com/drive/folders/1_ywUBKJx3uEuzy-qe8oJgZ2oEJpTwGQg',
  },
  vi: {
    presentation: 'https://f2f.ac/materials/vie/PresentationVIE.pdf',
    marketing: 'https://f2f.ac/materials/vie/MarketingVIE.pdf',
    brandbook: 'https://f2f.ac/materials/vie/BrandbookVIE.pdf',
    completeBrandbook: 'https://drive.google.com/drive/folders/1_ywUBKJx3uEuzy-qe8oJgZ2oEJpTwGQg',
  },
  zh: {
    presentation: 'https://f2f.ac/materials/ch/PresentationCH.pdf',
    marketing: 'https://f2f.ac/materials/ch/MarketingCH.pdf',
    brandbook: 'https://f2f.ac/materials/ch/BrandbookCH.pdf',
    completeBrandbook: 'https://drive.google.com/drive/folders/1_ywUBKJx3uEuzy-qe8oJgZ2oEJpTwGQg',
  },
  fr: {
    presentation: 'https://f2f.ac/materials/fr/PresentationFR.pdf',
    marketing: 'https://f2f.ac/materials/fr/MarketingFR.pdf',
    brandbook: 'https://f2f.ac/materials/fr/BrandbookFR.pdf',
    completeBrandbook: 'https://drive.google.com/drive/folders/1_ywUBKJx3uEuzy-qe8oJgZ2oEJpTwGQg',
  },
};

export const FAQ_QUESTIONS = [
  {
    question: 'Что такое FLEX?',
    answer:
      'FLEX - внутреняя валюта платформы FIN2FLEX. Валюта FLEX используется для взаиморасчетов между пользователями и платформой внутри вашего личного кабинета. 1 FLEX = 1 USD.',
  },
];

export const INVESTMENTS_RULES = [
  {
    from_date: '2021-11-22T00:00:00+03:00',
    to_date: '2021-12-22T00:00:00+03:00',
    max_value: 3000,
  },
  {
    from_date: '2021-12-22T00:00:00+03:00',
    to_date: '2022-01-01T00:00:00+03:00',
    max_value: 5000,
  },
  {
    from_date: '2022-01-01T00:00:00+03:00',
    to_date: '2022-01-28T00:00:00+03:00',
    max_value: 10000,
  },
];

export enum PROGRAMS_ACTIVE_DATE {
  MARKET = '2021-12-18T00:00:00+03:00',
  AUTO = '2022-01-18T00:00:00+03:00',
  CASHBACK = '2022-02-18T00:00:00+03:00',
  HOME = '2022-04-18T00:00:00+03:00',
}
