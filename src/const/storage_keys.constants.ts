export enum STORAGE_KEYS {
  AUTH = 'auth',
  THEME = 'theme',
  REF_ID = 'ref',
}
