export const { API_URL } = process.env;
export const SITE_URL = `https://${window.location.host}`;
export const STATIC_URL = `https://${window.location.host}`;
