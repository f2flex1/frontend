import React from 'react';

import { ScrollBar } from '@/components/elements';
import { MainContentBlock } from '@/components/elements/MainContent/MainContent.Styles';

interface Props {
  className?: string;
  children: any;
}

export const MainContent: React.FC<Props> = ({ className, children }) => {
  return (
    <MainContentBlock className={className}>
      <ScrollBar>{children}</ScrollBar>
    </MainContentBlock>
  );
};
