import styled from 'styled-components';

export const CheckboxContainer: any = styled.label`
  display: inline-flex;
  width: 100%;
  margin: 0;
  text-align: left;
`;

export const CheckboxInput: any = styled.input`
  position: absolute;
  z-index: -1;
  display: none;
  opacity: 0;
  visibility: hidden;

  &:checked {
    + span::after {
      content: '';
      background-image: url("data:image/svg+xml,%3Csvg width='10' height='7' viewBox='0 0 10 7' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M1 3L4.5 6L9.5 1' stroke='white'/%3E%3C/svg%3E");
      background-position: center;
      background-repeat: no-repeat;
    }

    + span::before {
      background-color: var(--violet);
      border-color: var(--violet);
    }
  }

  &:focus {
    + span::before {
      box-shadow: 0 0 0 1px rgba(#777, 0.5);
    }
  }
`;

export const CheckboxLabel: any = styled.span`
  position: relative;
  padding-left: 26px;
  font-weight: 500;
  font-size: 14px;
  line-height: 17px;
  user-select: none;

  &:after,
  &::before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: 17px;
    height: 17px;
  }

  &::before {
    border: 1px solid var(--white);
    border-radius: 3px;
  }

  a {
    text-decoration: underline;
  }
`;
