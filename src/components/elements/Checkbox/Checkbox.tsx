import React, { useState } from 'react';

import {
  CheckboxContainer,
  CheckboxInput,
  CheckboxLabel,
} from '@/components/elements/Checkbox/Checkbox.Styles';

interface Props {
  checked?: boolean;
  className?: string;
}

export const Checkbox: React.FC<Props> = ({ className, children }) => {
  const [checked, setChecked] = useState<boolean>(false);

  const changeState = () => {
    setChecked(!checked);
  };

  return (
    <CheckboxContainer className={className}>
      <CheckboxInput type="checkbox" checked={checked} onChange={changeState} />
      <CheckboxLabel>{children}</CheckboxLabel>
    </CheckboxContainer>
  );
};
