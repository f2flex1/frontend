import styled from 'styled-components';

export const SubTitleBlock: any = styled.h2`
  margin: 0 0 40px;
  font-weight: 400;
  font-size: 25px;
  line-height: 30px;
  color: var(--light);

  &.-light {
    color: var(--brown);
  }

  @media (max-width: 768px) {
    font-size: 15px;
    line-height: 18px;
  }
`;
