import React from 'react';

import { SubTitleBlock } from '@/components/elements/SubTitle/SubTitle.Styles';

interface Props {
  className?: string;
  as?: any;
  children?: any;
}

export const SubTitle: React.FC<Props> = ({ className, as, children }) => {
  return (
    <SubTitleBlock className={className} as={as}>
      {children}
    </SubTitleBlock>
  );
};
