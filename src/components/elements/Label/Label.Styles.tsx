import styled from 'styled-components';

export const LabelBlock: any = styled.label`
  display: block;
  margin: 0 0 13px;
  font-size: 12px;
  line-height: 15px;
  color: #d1d1d1;

  &.-light {
    color: var(--brown);
  }
`;
