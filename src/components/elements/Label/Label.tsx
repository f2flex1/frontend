import React from 'react';
import { connect } from 'react-redux';

import { LabelBlock } from '@/components/elements/Label/Label.Styles';
import { AppStateType } from '@/store';
import { ThemeType } from '@/store/app/reducers';

interface Props {
  htmlFor?: string;
  className?: string;
  children: any;
  themeType?: ThemeType;
}

export const Label: React.FC<Props> = ({ htmlFor, className, children, themeType }) => {
  return (
    <LabelBlock htmlFor={htmlFor} className={`${className} -${themeType}`}>
      {children}
    </LabelBlock>
  );
};

const mapStateToProps = (state: AppStateType) => {
  const { app } = state;
  return {
    themeType: app.theme,
  };
};

export default connect(mapStateToProps)(Label);
