import React, { createRef } from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';

import { cleanStringWithNumberVal } from '@/common/utils/formatters';
import {
  FormControl,
  FormControlInput,
  FormControlInputError,
} from '@/components/elements/Input/Input.Styles';
import { AppStateType } from '@/store';
import { ThemeType } from '@/store/app/reducers';

type InputType = 'email' | 'password' | 'number' | 'text' | 'date' | 'phone';

interface Props {
  id?: string;
  className?: string;
  type: InputType;
  name: string;
  placeholder?: string;
  disabled?: boolean;
  value?: string;
  error?: string;
  onChange?: (field: string, value: string | number) => void;
  onBlur?: (field: string) => void;
  themeType?: ThemeType;
  maxLength?: number;
}

export const Input: React.FC<Props> = (props: Props) => {
  const {
    id,
    className,
    name,
    type,
    value,
    error,
    placeholder,
    disabled,
    onChange,
    onBlur,
    themeType,
    maxLength,
  } = props;

  const { t } = useTranslation();

  const inputRef = createRef();

  const onInputChange = (value: string) => {
    if (!onChange) return false;
    if (type === 'number') {
      return onChange(name, cleanStringWithNumberVal(value));
    }
    return onChange(name, value);
  };

  const onInputBlur = () => {
    if (!onBlur) return;

    onBlur(name);
  };

  return (
    <FormControl className={`${className} -${themeType} ${error ? '-error' : ''}`}>
      <FormControlInput
        ref={inputRef}
        id={id}
        type={type}
        name={name}
        value={value}
        placeholder={placeholder}
        autoComplete="off"
        disabled={disabled}
        onChange={(e: React.ChangeEvent<any>) => onInputChange(e.target.value)}
        onBlur={onInputBlur}
        maxLength={maxLength || 32}
      />
      <FormControlInputError>{t(`${error}`)}</FormControlInputError>
    </FormControl>
  );
};

const mapStateToProps = (state: AppStateType) => {
  const { app } = state;
  return {
    themeType: app.theme,
  };
};

export default connect(mapStateToProps)(Input);
