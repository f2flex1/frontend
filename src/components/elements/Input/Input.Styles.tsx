import styled from 'styled-components';

export const FormControl: any = styled.div`
  margin: 0 0 23px;

  input:-webkit-autofill,
  input:-webkit-autofill:hover,
  input:-webkit-autofill:focus,
  input:-webkit-autofill:active,
  textarea:-webkit-autofill,
  textarea:-webkit-autofill:hover,
  textarea:-webkit-autofill:focus,
  textarea:-webkit-autofill:active,
  select:-webkit-autofill,
  select:-webkit-autofill:hover,
  select:-webkit-autofill:focus,
  select:-webkit-autofill:active {
    -webkit-text-fill-color: var(--white);
    transition: background-color 5000s ease-in-out 0s;
    filter: none;
  }

  &.lg input {
    padding: 21px 30px;
    font-size: 20px;
    line-height: 25px;
  }

  &.sm input {
    padding: 14px 30px;
    font-size: 16px;
    line-height: 21px;
  }

  &.-error {
    input {
      box-shadow: 0 0 0 1px var(--red);
    }

    p {
      animation: fadeIn 0.8s;
      display: block;
      color: var(--red);
    }
  }

  &.-light {
    input:-webkit-autofill,
    input:-webkit-autofill:hover,
    input:-webkit-autofill:focus,
    input:-webkit-autofill:active,
    textarea:-webkit-autofill,
    textarea:-webkit-autofill:hover,
    textarea:-webkit-autofill:focus,
    textarea:-webkit-autofill:active,
    select:-webkit-autofill,
    select:-webkit-autofill:hover,
    select:-webkit-autofill:focus,
    select:-webkit-autofill:active {
      -webkit-text-fill-color: var(--dark-blue);
      transition: background-color 5000s ease-in-out 0s;
      filter: none;
    }

    input {
      color: var(--dark-blue);
      background: var(--white);
      box-shadow: 0 0 11px rgba(0, 0, 0, 0.04);

      &::placeholder {
        font-weight: 700;
        color: rgba(21, 23, 46, 0.65);
      }

      &:focus {
        box-shadow: 0 0 0 1px var(--light);
      }
    }

    &.-error {
      input {
        box-shadow: 0 0 0 1px var(--red);
      }
    }
  }

  @media (max-width: 1400px) {
    &.lg input {
      padding: 14px 30px;
      font-weight: 700;
      font-size: 16px;
      line-height: 21px;
    }
  }
`;

export const FormControlInput: any = styled.input`
  padding: 14px 30px;
  width: 100%;
  font-weight: 400;
  color: var(--white);
  background: rgba(196, 196, 196, 0.07);
  border-radius: var(--border-radius);
  border: none;
  outline: none;
  transition: box-shadow 0.3s ease-in-out;

  &:focus {
    box-shadow: 0 0 0 1px var(--gray);

    &::placeholder {
      opacity: 0;
    }
  }

  &::placeholder {
    font-weight: 700;
    color: rgba(255, 255, 255, 0.65);
    transition: opacity 0.3s ease-in-out;
  }

  &:disabled {
    color: rgba(255, 255, 255, 0.65);
  }
`;

export const FormControlInputError: any = styled.p`
  margin: 8px 0;
  color: var(--red);
  font-weight: 700;
  font-size: 12px;
  line-height: 14px;
  text-align: left;
  display: none;
  transition: opacity 0.3s ease-in-out, visibility 0.3s ease-in-out;
`;
