import styled from 'styled-components';

export const PlaceholderStyles: any = styled.div`
  width: 100%;
  min-width: 250px;
  min-height: 419px;
  padding: 16px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  color: ${({ theme }) => theme.tablePlaceholder.color};
  background-color: ${({ theme }) => theme.tablePlaceholder.background};
  box-shadow: ${({ theme }) => theme.tablePlaceholder.boxShadow};
  border-radius: var(--border-radius);
  text-align: center;

  p {
    margin: 0 0 22px;
    font-weight: bold;
    font-size: 14px;
    line-height: 17px;
  }

  span {
    font-size: 14px;
    line-height: 17px;
  }

  &.notifications-placeholder {
    min-height: 300px;
    background-color: transparent;
  }

  &.actives-placeholder {
    margin: 0 20px;
  }

  &.partnership-placeholder {
    margin-top: 20px;
  }

  &.-light {
    &.notifications-placeholder {
      background-color: transparent;
      box-shadow: none;
    }
  }

  @media (max-width: 1200px) {
    &.actives-placeholder {
      margin: 0 8px;
    }
  }

  @media (max-width: 992px) {
    min-height: 300px;

    &.actives-placeholder {
      margin: 0 16px 0 0;
    }
  }
`;
