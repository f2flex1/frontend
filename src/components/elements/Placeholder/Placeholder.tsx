import React from 'react';

import { PlaceholderStyles } from '@/components/elements/Placeholder/Placeholder.Styles';

interface Props {
  className?: string;
  children?: any;
}

export const Placeholder: React.FC<Props> = ({ className, children }) => {
  return <PlaceholderStyles className={className}>{children}</PlaceholderStyles>;
};
