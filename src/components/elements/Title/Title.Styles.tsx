import styled from 'styled-components';

export const TitleBlock: any = styled.h1`
  margin: 0 0 45px;
  font-size: 25px;
  line-height: 30px;
  color: ${({ theme }) => theme.typography.title};
  font-weight: 700;

  @media (max-width: 768px) {
    margin: 0 0 25px;
    font-size: 14px;
    line-height: 17px;
  }
`;
