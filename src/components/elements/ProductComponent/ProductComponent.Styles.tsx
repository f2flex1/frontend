import styled from 'styled-components';

export const ProductsCardWrapper: any = styled.article`
  position: relative;
  width: 25%;
  max-width: 456px;
  border-radius: var(--border-radius);
  overflow: hidden;
  transition: transform 0.3s ease-in-out;
  //transform: translateZ(0.1px);
  will-change: transform;

  &:last-child {
    margin-right: 45px;
  }

  &:hover {
    transform: translateY(-7px);
  }

  .blur {
    display: none;
  }

  .overlay {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    width: 100%;
    height: 100%;
    display: none;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    z-index: 1;
    background: rgba(196, 196, 196, 0.01);

    h3 {
      margin: 0 0 5px;
    }

    p {
      margin: 0;
      max-width: 210px;
      font-family: var(--poppins);
      font-weight: 300;
      font-size: 14px;
      line-height: 21px;
      text-align: center;
    }
  }

  &.closed {
    .products-item {
      filter: progid:DXImageTransform.Microsoft.Blur(PixelRadius='5');
      -webkit-filter: url(#blur);
      filter: url(#blur);
      -webkit-filter: blur(5px);
      filter: blur(5px);
    }

    .overlay {
      display: flex;
    }
  }

  @media (max-width: 992px) {
    margin-right: 18px;

    &:last-child {
      margin-right: 0;
    }
  }

  @media (max-width: 576px) {
    margin-right: 0;
    margin-bottom: 21px;
    width: 100%;
    max-width: 100%;
  }
`;

export const ProductsCard: any = styled.div`
  position: relative;
  filter: drop-shadow(0px 0px 10px rgba(0, 0, 0, 0.12));
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: flex-start;
  min-height: 350px;
  padding: 48px 38px 34px 38px;
  background: linear-gradient(0deg, #383e49, #383e49);

  @media (max-width: 768px) {
    padding: 28px 15px 22px;
    min-height: 235px;

    .products-item__btn {
      padding: 3px 26px;
      font-size: 12px;
      line-height: 15px;
    }
  }

  @media (max-width: 576px) {
    padding: 28px 15px 22px;
  }
`;

export const ProductsCardImg: any = styled.div`
  position: absolute;
  right: 0;
  bottom: 0;
  z-index: -1;
  transition: transform 0.3s ease-in-out;
  transform: translate(30%, 0);
  -webkit-transform: translate(30%, 0);

  &.loaded {
    -webkit-transform: translate(0, 0);
    transform: translate(0, 0);
  }

  img {
    display: block;
    width: 100%;
    height: auto;
  }

  @media (max-width: 768px) {
    max-width: 50%;
  }
`;

export const ProductsCardBody: any = styled.div``;

export const ProductsCardTitle: any = styled.h3`
  margin: 0 0 18px;
  font-family: var(--poppins);
  font-size: 40px;
  line-height: 33px;
  color: var(--light);
  text-transform: uppercase;
  font-weight: 300;

  span {
    font-weight: 700;
  }

  @media (max-width: 768px) {
    margin: 0 0 11px;
    font-size: 24px;
    line-height: 20px;
  }
`;

export const ProductsCardSubtitle: any = styled.p`
  margin: 0 0 30px;
  font-family: var(--poppins);
  font-weight: 300;
  font-size: 14px;
  line-height: 21px;
  color: var(--white);

  @media (max-width: 768px) {
    margin: 0 0 12px;
    font-size: 12px;
    line-height: 18px;
  }
`;

export const ProductsCardContent: any = styled.div`
  margin-bottom: 16px;

  p {
    margin: 0 0 4px;
    font-family: var(--poppins);
    font-weight: 600;
    font-size: 14px;
    line-height: 21px;
    color: var(--light-gray);
  }

  span {
    font-family: var(--poppins);
    font-weight: 300;
    font-size: 18px;
    line-height: 27px;
    text-transform: uppercase;
    color: #d1d1d1;
  }

  @media (max-width: 768px) {
    margin-bottom: 5px;
  }
`;

export const ProductsCardProperty: any = styled.span`
  font-weight: 500;
  font-size: 10px;
  line-height: 15px;
  color: var(--light-gray);
`;
