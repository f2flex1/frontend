import Skeleton from '@material-ui/lab/Skeleton';
import moment from 'moment-timezone';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import { Button } from '@/components/elements';
import { Image } from '@/components/elements/Image/Image';
import { ProfitabilityCommercial } from '@/pages/Home/Home';
import { ThemeType } from '@/store/app/reducers';

import {
  ProductsCard,
  ProductsCardBody,
  ProductsCardContent,
  ProductsCardImg,
  ProductsCardProperty,
  ProductsCardSubtitle,
  ProductsCardTitle,
  ProductsCardWrapper,
} from './ProductComponent.Styles';

interface Props {
  item: ProfitabilityCommercial;
  className?: string;
  onClick?: () => void;
  themeType?: ThemeType;
}

export const ProductComponent: React.FC<Props> = ({ item, themeType }) => {
  const [imageLoaded, setImageLoaded] = useState(false);
  const { t } = useTranslation();

  useEffect(() => {
    const timer = setTimeout(() => {
      setImageLoaded(true);
    }, 5000);
    return () => clearTimeout(timer);
  }, [setImageLoaded]);

  const onLoad = () => {
    setTimeout(() => {
      setImageLoaded(true);
    }, 200);
  };

  const nowDate = moment(new Date()).unix();
  const startDate = moment(item.date, 'DD.MM.YYYY').unix();

  return (
    <ProductsCardWrapper className={`${nowDate < startDate ? 'closed' : ''}`}>
      <div className="overlay">
        <ProductsCardTitle className="products-item__title">
          <React.Fragment>
            <span>Flex</span>
            {`${item.title}`}
          </React.Fragment>
        </ProductsCardTitle>
        <p>
          {t('home.products.closed')} 
          {' '}
          {`${item.date}`}
        </p>
      </div>
      <svg xmlns="http://www.w3.org/2000/svg" className="blur">
        <defs>
          <filter id="blur">
            <feGaussianBlur stdDeviation="5" />
          </filter>
        </defs>
      </svg>
      <ProductsCard
        className="products__item products-item"
        classes={{ root: '' }}
        style={!imageLoaded ? { pointerEvents: 'none' } : {}}
      >
        <ProductsCardImg className={`${item.imageClassName ?? ''} ${imageLoaded ? 'loaded' : ''}`}>
          <Image
            src={item.image}
            alt={item.title ?? ''}
            animation="wave"
            width={item.imageParams.width}
            height={item.imageParams.height}
            onLoad={onLoad}
          />
        </ProductsCardImg>
        <ProductsCardBody>
          <ProductsCardTitle className="products-item__title">
            {!imageLoaded ? (
              <Skeleton variant="text" animation="wave" width={220} />
            ) : (
              <React.Fragment>
                <span>Flex</span>
                {`${item.title}`}
              </React.Fragment>
            )}
          </ProductsCardTitle>
          <ProductsCardSubtitle className="products-item__subtitle">
            {t(item.subTitle)}
          </ProductsCardSubtitle>

          <ProductsCardContent className="products-item__content">
            <p>{t(item.valueText)}</p>
            <span>{`${item.value} ${item.currency}`}</span>
          </ProductsCardContent>

          <ProductsCardProperty className="products-item__property">
            {t(item.description)}
          </ProductsCardProperty>
        </ProductsCardBody>
        <Button
          themeType={themeType}
          as={Link}
          className="products-item__btn md"
          to={item.redirectUrl}
        >
          {t('home.products.button')}
        </Button>
      </ProductsCard>
    </ProductsCardWrapper>
  );
};
