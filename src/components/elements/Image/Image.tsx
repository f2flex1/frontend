import Skeleton from '@material-ui/lab/Skeleton';
import React, { ImgHTMLAttributes, useCallback, useState } from 'react';

interface Props extends ImgHTMLAttributes<HTMLImageElement> {
  src?: string;
}

interface SkeletonProps {
  height?: number;
  width?: number;
  style?: React.CSSProperties;
  variant?: 'text' | 'rect' | 'circle';
  animation?: 'pulse' | 'wave' | false;
}

const Wrapper: React.FC<SkeletonProps & { loaded: boolean }> = ({
  loaded,
  children,
  variant,
  animation,
  width,
  height,
  style,
}) =>
  loaded ? (
    <React.Fragment>{children}</React.Fragment>
  ) : (
    <Skeleton variant={variant} animation={animation} width={width} height={height} style={style}>
      {children}
    </Skeleton>
  );

export const Image: React.FC<Props & SkeletonProps> = ({
  src,
  height,
  width,
  style,
  variant,
  animation,
  ...rest
}) => {
  const [loading, setLoading] = useState(true);
  const { onLoad } = rest;
  const onLoadCallback: React.ReactEventHandler<HTMLImageElement> = useCallback(
    e => {
      if (onLoad) {
        onLoad(e);
      }
      setLoading(false);
    },
    [setLoading, onLoad]
  );

  return (
    <Wrapper
      variant={variant}
      animation={animation}
      width={width}
      height={height}
      style={style}
      loaded={!!(!loading && src)}
    >
      <img {...rest} alt={rest.alt} src={src} style={style} onLoad={onLoadCallback} />
    </Wrapper>
  );
};
