import React, { ImgHTMLAttributes } from 'react';

type State = {
  loaded: boolean;
  src?: null | string;
  placeholder: null | string;
};

class PreloadedImage extends React.Component<
ImgHTMLAttributes<HTMLImageElement> & { lazy?: boolean },
State
> {
  private preloader: HTMLImageElement;

  private observer: IntersectionObserver;

  private el: HTMLImageElement | null;

  constructor(props: any) {
    super(props);
    const { placeholder } = this.props;
    this.state = {
      loaded: false,
      src: null,
      placeholder: placeholder || null,
    };
  }

  componentDidMount() {
    const { lazy } = this.props;
    if (lazy && 'IntersectionObserver' in window) {
      this.setObserver();
    } else {
      this.setPreloader();
    }
  }

  componentWillUnmount() {
    if (this.observer) this.observer.disconnect();
    if (this.preloader) this.preloader.onload = null;
  }

  setPreloader() {
    this.preloader = new Image();
    const { placeholder } = this.state;
    const { src, onLoad } = this.props;
    this.preloader.onload = (e: any) => {
      if (onLoad) {
        onLoad(e);
      }
      this.setState({
        loaded: true,
        src,
      });
    };
    this.preloader.onerror = () => {
      this.setState({
        loaded: true,
        src: placeholder,
      });
    };

    this.preloader.src = src ?? '';
  }

  setObserver() {
    this.observer = new IntersectionObserver(entries => {
      entries.forEach(entry => {
        if (entry.isIntersecting) {
          this.setPreloader();
          this.observer.disconnect();
        }
      });
    });
    if (this.el) {
      this.observer.observe(this.el);
    }
  }

  render() {
    const { className, style, ...rest } = this.props;
    const { src, placeholder, loaded } = this.state;
    return !loaded ? null : (
      <img
        {...rest}
        src={src || placeholder || undefined}
        ref={el => (this.el = el)}
        onError={e => {
          e.currentTarget.src = placeholder ?? '';
        }}
        className={className}
        style={style ?? {}}
        alt=""
      />
    );
  }
}

export default PreloadedImage;
