import styled from 'styled-components';

export const UserInfoBlock: any = styled.div`
  display: flex;
  align-items: center;

  &.beginner {
    .user-info__img {
      padding: 0;
      border-color: transparent;
      box-shadow: none;
    }
  }

  &.ametis-junior,
  &.ametis-middle,
  &.ametis-senior,
  &.ametis-lead {
    .user-info__img {
      border-color: var(--ametis);
      box-shadow: 0 0 4px var(--ametis);
    }
  }

  &.lazurit-junior,
  &.lazurit-middle,
  &.lazurit-senior,
  &.lazurit-lead {
    .user-info__img {
      border-color: var(--lazurit);
      box-shadow: 0 0 4px var(--lazurit);
    }
  }

  &.nefrit-junior,
  &.nefrit-middle,
  &.nefrit-senior,
  &.nefrit-lead {
    .user-info__img {
      border-color: var(--nefrit);
      box-shadow: 0 0 4px var(--nefrit);
    }
  }

  &.emerald-junior,
  &.emerald-middle,
  &.emerald-senior,
  &.emerald-lead {
    .user-info__img {
      border-color: var(--emerald);
      box-shadow: 0 0 4px var(--emerald);
    }
  }

  &.ruby-junior,
  &.ruby-middle,
  &.ruby-senior,
  &.ruby-lead {
    .user-info__img {
      border-color: var(--ruby);
      box-shadow: 0 0 4px var(--ruby);
    }
  }

  &.saphire-junior,
  &.saphire-middle,
  &.saphire-senior,
  &.saphire-lead {
    .user-info__img {
      border-color: var(--saphire);
      box-shadow: 0 0 4px var(--saphire);
    }
  }

  &.diamond-junior,
  &.diamond-middle,
  &.diamond-senior,
  &.diamond-lead {
    .user-info__img {
      border-color: var(--diamond);
      box-shadow: 0 0 4px var(--diamond);
    }
  }

  &.partners-box__user {
    margin-bottom: 30px;
  }

  &.-light {
    &.partners-box__user {
      .user-info__name {
        color: var(--brown);
      }

      .user-info__id {
        color: #8a8c98;
      }
    }
  }
`;

export const UserInfoBox: any = styled.div`
  flex: 1 1 auto;
  display: flex;
  flex-direction: column;
  font-family: var(--poppins);
`;

export const UserInfoImg: any = styled.div`
  flex: 0 0 auto;
  width: 60px;
  height: 60px;
  padding: 6px;
  margin-right: 28px;
  border-radius: 50%;
  overflow: hidden;
  border: 1px solid;

  img {
    width: 100%;
    height: 100%;
    border-radius: 50%;
    object-fit: cover;
  }

  @media (max-width: 1200px) {
    margin-right: 15px;
  }
`;

export const UserInfoName: any = styled.div`
  margin: 0 0 7px;
  font-weight: 500;
  font-size: 14px;
  line-height: 21px;

  span {
    display: block;
  }

  @media (max-width: 992px) {
    span {
      display: inline-block;
    }
  }
`;

export const UserInfoId: any = styled.span`
  display: inline-block;
  font-weight: 400;
  font-size: 11px;
  line-height: 16px;
  letter-spacing: 0.01em;
  text-transform: uppercase;
  color: #8a8c98;
`;
