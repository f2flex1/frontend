import React from 'react';

import {
  UserInfoBlock,
  UserInfoBox,
  UserInfoId,
  UserInfoImg,
  UserInfoName,
} from '@/components/elements/UserInfo/UserInfo.Styles';

interface Props {
  className?: string;
  src?: string;
  alt?: string;
  userName?: string;
  userLastname?: string;
  userId?: string;
}

export const UserInfo: React.FC<Props> = ({
  className,
  src,
  alt,
  userName,
  userLastname,
  userId,
}) => {
  return (
    <UserInfoBlock className={`user-info ${className}`}>
      <UserInfoImg className="user-info__img">
        <img src={src} alt={alt} />
      </UserInfoImg>
      <UserInfoBox>
        <UserInfoName className="user-info__name">
          <span>{userName}</span> 
          {' '}
          <span>{userLastname}</span>
        </UserInfoName>
        <UserInfoId className="user-info__id">{userId}</UserInfoId>
      </UserInfoBox>
    </UserInfoBlock>
  );
};
