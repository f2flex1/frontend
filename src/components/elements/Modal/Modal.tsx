import React from 'react';

import { Icon } from '@/components/elements';
import { ModalStyles } from '@/components/elements/Modal/Modal.Styles';

interface Props {
  opened?: boolean;
  closeModal?: () => void;
  className?: string;
  children?: any;
}

export const Modal: React.FC<Props> = (props: Props) => {
  const { opened, closeModal, className, children } = props;

  const closeClick = () => {
    if (!closeModal) return false;
    return closeModal();
  };

  if (!opened) {
    return null;
  }

  return (
    <ModalStyles className={className}>
      <div className={`modal__dialog ${opened ? 'active' : ''}`}>
        <button className="modal__close toggle-button" onClick={closeClick}>
          <Icon name="close" size="20" color="var(--white)" />
        </button>
        {children}
      </div>
    </ModalStyles>
  );
};
