import React from 'react';

import { ModalContainer, ModalDialog } from './PageModal.Styles';

interface Props {
  className?: string;
  children: any;
}

export const PageModal: React.FC<Props> = (props: Props) => {
  const { children, className } = props;

  return (
    <ModalContainer className={`modal ${className}`}>
      <ModalDialog className="modal__dialog">{children}</ModalDialog>
    </ModalContainer>
  );
};
