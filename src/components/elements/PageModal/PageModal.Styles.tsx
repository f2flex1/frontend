import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const ModalContainer: any = styled.div`
  margin: 0 auto;

  .modal__input {
    width: 100%;
  }

  .modal__button {
    padding: 10px 65px;
  }

  .modal__price-descr {
    display: block;
    margin: 0 0 30px;
    font-weight: 400;
    font-size: 12px;
    line-height: 15px;
    color: #d1d1d1;
  }

  .left {
    align-self: flex-start;
  }

  &.auth-modal {
    width: 100%;

    .modal__dialog {
      padding: 45px;
    }

    .modal__description {
      margin-bottom: 40px;
    }
  }

  &.finance-modal {
    width: 100%;

    .modal__dialog {
      max-width: 560px;
    }
  }

  &.-light {
    .modal__dialog {
      background-color: var(--white);
      box-shadow: 0 0 11px rgba(0, 0, 0, 0.04);
    }

    .modal__price-descr {
      color: var(--brown);
    }

    .modal__title {
      color: var(--brown);
    }

    .modal__subtitle {
      color: rgba(67, 67, 67, 0.79);
    }

    .modal__price {
      background-image: linear-gradient(180deg, #8569f9 0%, #b7a7fc 100%);
      -webkit-background-clip: text;
      -webkit-text-fill-color: transparent;
      color: #774bff;
    }

    .modal__icon {
      svg {
        fill: #6278af;
      }
    }

    .modal__description {
      color: var(--brown);
    }

    .modal__text {
      color: var(--brown);
    }

    .modal__time {
      color: var(--brown);
    }

    .modal__label {
      color: var(--brown);
    }

    .modal__link {
      color: var(--brown);
    }
  }
`;

export const ModalDialog: any = styled.div`
  width: 100%;
  max-width: 445px;
  margin: 0 auto;
  padding: 41px 57px 34px;
  background: #343943;
  border-radius: var(--border-radius);
  display: flex;
  flex-direction: column;
  align-items: center;

  @media (max-width: 768px) {
    padding: 30px 20px;
  }
`;

export const ModalIcon: any = styled.div`
  margin: 0 0 28px;

  svg {
    fill: #d8e1f7;
  }
`;

export const ModalTitle: any = styled.h4`
  max-width: 290px;
  margin: 0 auto 45px;
  font-weight: 700;
  font-size: 18px;
  line-height: 22px;
  color: var(--light);
  text-align: center;
`;

export const ModalText: any = styled.p`
  margin: 0 0 19px;
  font-size: 18px;
  line-height: 22px;
  color: var(--light);
  text-align: center;
`;

export const ModalPrice: any = styled.div`
  margin: 0 0 45px;
  font-weight: 300;
  font-size: 30px;
  line-height: 37px;
  color: var(--white);
  text-align: center;
  background-image: linear-gradient(180deg, rgba(226, 174, 255, 0.66) 0%, #fc54ff 100%);
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;

  span {
    font-weight: 700;
  }

  &.mb-none {
    margin-bottom: 0;
  }
`;

export const ModalInfo: any = styled.div`
  width: 100%;
  margin: 0 0 24px;

  .info-item {
    margin-bottom: 15px;

    &__text {
      font-weight: 700;
      font-size: 12px;
      line-height: 18px;
      color: #989898;
    }

    &__value {
      font-weight: 700;
      font-size: 14px;
      line-height: 17px;
      color: var(--white);
    }
  }

  &.-light {
    .info-item {
      &__text {
        color: var(--brown);
      }

      &__value {
        color: var(--brown);
      }
    }
  }
`;

export const ModalDescription: any = styled.p`
  margin: 0 0 24px;
  font-size: 14px;
  line-height: 17px;
  color: var(--light);
  text-align: center;
`;

export const ModalTime: any = styled.div`
  margin: 0 0 10px;
  font-size: 30px;
  line-height: 37px;
  color: var(--light);
  text-align: center;
`;

export const ModalLink: any = styled(Link)`
  margin: 0 0 36px;
  font-weight: 500;
  font-size: 14px;
  line-height: 17px;
  text-decoration: underline;
  color: #eaeaea;
`;
