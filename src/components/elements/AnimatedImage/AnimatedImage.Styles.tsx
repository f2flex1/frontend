import styled from 'styled-components';

import { Direction } from '@/components/elements/AnimatedImage/AnimatedImage';

type Props = { direction?: Direction };

const getTranslate = (dir: Direction = 'left'): string => {
  if (dir === 'top') return 'translate(0, 20%)';
  if (dir === 'right') return 'translate(20%, 0)';
  if (dir === 'left') return 'translate(-20%, 0)';
  return 'translate(0, -20%)';
};

export const AnimatedImg = styled.div`
  transition: transform 0.3s ease-in-out;
  transform: ${(props: Props) => getTranslate(props.direction)};
  -webkit-transform: ${(props: Props) => getTranslate(props.direction)};
  &.loaded {
    -webkit-transform: translate(0, 0);
    transform: translate(0, 0);
  }
  img {
    display: block;
    max-width: 100%;
  }

  @media (max-width: 768px) {
    max-width: 50%;
  }
`;
