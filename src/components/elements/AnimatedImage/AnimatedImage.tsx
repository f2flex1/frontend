import React, { useEffect, useState } from 'react';

import { Image } from '@/components/elements/Image/Image';

import { AnimatedImg } from './AnimatedImage.Styles';

export type Direction = 'top' | 'right' | 'left' | 'bottom';

interface Props {
  direction?: Direction;
  src: string;
  width: number;
  height: number;
  alt?: string;
  className?: string;
  imageClassName?: string;
  style?: React.CSSProperties;
}

export const AnimatedImage: React.FC<Props> = ({
  style,
  src,
  direction,
  imageClassName,
  alt,
  width,
  height,
}) => {
  const [imageLoaded, setImageLoaded] = useState(false);

  useEffect(() => {
    const timer = setTimeout(() => {
      setImageLoaded(true);
    }, 5000);
    return () => clearTimeout(timer);
  }, [setImageLoaded]);

  const onLoad = () => {
    setTimeout(() => {
      setImageLoaded(true);
    }, 200);
  };

  return (
    <AnimatedImg
      direction={direction}
      className={`${imageClassName ?? ''} ${imageLoaded ? 'loaded' : ''}`}
    >
      <Image
        style={style}
        src={src}
        alt={alt ?? ''}
        animation="wave"
        width={width}
        height={height}
        onLoad={onLoad}
      />
    </AnimatedImg>
  );
};
