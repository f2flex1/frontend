import styled from 'styled-components';

export const TabsContent: any = styled.div``;

export const TabsControl: any = styled.div`
  width: 100%;
  display: flex;
  margin-bottom: 40px;

  &.contract-history__tabs {
    position: relative;
    margin-bottom: 15px;
  }

  &.setting-header {
    flex-wrap: wrap;
  }

  &.finance-tab {
    margin-bottom: 0;

    + div {
      width: 100%;
    }
  }

  @media (max-width: 768px) {
    &.contract-history__tabs {
      padding: 30px 0 5px;
      margin-bottom: 38px;
      flex-wrap: wrap;

      &:before,
      &:after {
        content: '';
        position: absolute;
        left: 50%;
        transform: translateX(-50%);
        width: 210px;
        height: 1px;
        background-image: linear-gradient(
          90deg,
          rgba(255, 255, 255, 0) 0%,
          rgba(255, 255, 255, 0.19) 55.98%,
          rgba(255, 255, 255, 0) 99.52%
        );
      }

      &:before {
        top: 0;
      }

      &:after {
        bottom: 0;
      }
    }
  }
`;

export const TabsControlItem: any = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 9px 30px;
  font-weight: 500;
  font-size: 12px;
  line-height: 15px;
  color: var(--white);
  text-align: center;
  border-radius: var(--border-radius);
  cursor: pointer;
  background-color: #3c3e40;
  border: 1px solid #5b45a1;
  transition: background-color 0.3s, border-color 0.3s, color 0.3s;
  outline: none;

  &:not(:last-child) {
    margin-right: 16px;
  }

  &:focus,
  &:hover {
    color: var(--white);
    background-color: rgba(202, 190, 239, 0.21);
  }

  &.active {
    border: none;
    background: linear-gradient(
      90deg,
      rgba(120, 128, 141, 0.51) 0%,
      rgba(202, 190, 239, 0.51) 99.03%
    );
  }

  &.primary-tab__item {
    width: 100%;
    border: 1px solid #777777;
    border-radius: var(--border-radius);
    background-color: #343943;
    background-image: none;

    &.active {
      border-color: var(--violet);
      border-radius: var(--border-radius);
      background-color: var(--violet);
    }
  }

  &.contract-history__tabs-item {
    padding: 0;
    margin-bottom: 25px;
    background: none;
    border: none;
    font-size: 12px;
    line-height: 15px;
    text-decoration: underline;
    color: rgba(255, 255, 255, 0.63);
    cursor: pointer;

    &:not(:last-child) {
      margin-right: 23px;
    }

    &:focus {
      color: var(--light-gray);
      box-shadow: none;
    }

    &.active {
      color: var(--white);
    }
  }

  &.setting-header__tab {
    padding: 9px 20px;
  }

  &.-light {
    color: var(--dark-blue);
    background-color: transparent;
    border: 1px solid #9076fa;

    &.active {
      border: none;
      font-weight: 700;
      background-image: linear-gradient(180deg, #abc1f3 0%, #dde7ff 100%);
    }
  }

  @media (max-width: 768px) {
    &.contract-history__tabs-item {
      font-size: 11px;
      line-height: 13px;
    }

    &.setting-header__tab {
      font-size: 11px;
      margin-bottom: 10px;
    }
  }

  @media (max-width: 576px) {
    &.finance-header_tab {
      width: 100%;
    }
  }
`;

export const TabsContentItem: any = styled.div`
  display: none;
  animation: fadeIn 0.8s;

  &.active {
    display: block;
  }

  @keyframes fadeIn {
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }
`;
