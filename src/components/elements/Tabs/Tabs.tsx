import React from 'react';
import { connect } from 'react-redux';

import { AppStateType } from '@/store';
import { ThemeType } from '@/store/app/reducers';

import { TabsContent, TabsControl, TabsControlItem } from './Tabs.Styles';

export interface Props {
  className: string;
  tabHeaderClassName: string;
  tabsList: any[];
  field: string;
  activeTab: string;
  onChange: (field: string, value: string | number) => void;
  children: any;
  width?: string;
  marginRight?: string;
  backgroundColor?: string;
  backgroundImage?: string;
  borderColor?: string;
  themeType?: ThemeType;
}

export const Tabs: React.FC<Props> = (props: Props) => {
  const {
    className,
    tabHeaderClassName,
    tabsList,
    field,
    activeTab,
    onChange,
    children,
    themeType,
  } = props;

  return (
    <React.Fragment>
      <TabsControl className={`${className} -${themeType}`}>
        {tabsList.map((item: any) => (
          <TabsControlItem
            key={`tab-header-item-${item.key}`}
            onClick={() => onChange(field, item.key)}
            className={`${tabHeaderClassName} -${themeType} ${
              activeTab === item.key ? 'active' : ''
            }`}
          >
            {item.name}
          </TabsControlItem>
        ))}
      </TabsControl>
      <TabsContent>{children}</TabsContent>
    </React.Fragment>
  );
};

const mapStateToProps = (state: AppStateType) => {
  const { app } = state;
  return {
    themeType: app.theme,
  };
};

export default connect(mapStateToProps)(Tabs);
