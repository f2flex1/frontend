import Skeleton from '@material-ui/lab/Skeleton';
import React from 'react';
import { useTranslation } from 'react-i18next';

import {
  ProfitCard,
  ProfitCardText,
  ProfitCardTitle,
} from '@/components/elements/ProfitComponent/ProfitComponent.Styles';
import { Profitability } from '@/pages/Home/Home';

interface Props {
  selected?: boolean;
  item: Profitability;
  onClick?: () => void;
}

export const ProfitComponent: React.FC<Props> = (props: Props) => {
  const { selected, item, onClick } = props;
  const { t } = useTranslation();

  return (
    <ProfitCard
      onClick={onClick}
      classes={{
        root: `${selected ? 'profit-card active' : 'profit-card'}`,
      }}
      data-title={item?.tooltip ? t(item?.tooltip) : null}
    >
      <ProfitCardTitle className="profit-card__title">{t(item.title)}</ProfitCardTitle>
      <ProfitCardText className="profit-card__text">
        {!item.value ? (
          <React.Fragment>
            <Skeleton
              variant="text"
              animation="wave"
              width={110}
              height={45}
              style={{ display: 'inline-block', marginRight: 10 }}
            />
            <span className="profit-card__currency">{` ${item.currency}`}</span>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <span className="profit-card__value">{`${item.value || 0}`}</span>
            {' '}
            <span className="profit-card__currency">{`${item.currency}`}</span>
          </React.Fragment>
        )}
      </ProfitCardText>
    </ProfitCard>
  );
};
