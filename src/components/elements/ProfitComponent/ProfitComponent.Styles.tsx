import { Card } from '@material-ui/core';
import styled from 'styled-components';

export const ProfitCard: any = styled(Card)`
  width: calc(100% / 3 - 45px);
  min-width: 280px;
  margin-right: 45px;
  margin-bottom: 30px;
  padding: 55px 35px;
  background: var(--dark);
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.12);
  border-radius: var(--border-radius);
  cursor: pointer;
  position: relative;

  &[data-title] {
    &:before {
      content: attr(data-title);
      position: absolute;
      top: 10px;
      right: 10px;
      max-width: 250px;
      padding: 10px;
      font-size: 10px;
      line-height: 15px;
      color: var(--white);
      background: #292f39;
      box-shadow: 0 0 10px rgba(0, 0, 0, 0.25);
      border-radius: var(--border-radius);
      opacity: 0;
      visibility: hidden;
      transition: opacity 0.5s ease-in-out, visibility 0.5s ease-in-out;
      z-index: 1;
    }

    &:hover {
      &:before {
        opacity: 1;
        visibility: visible;
        content: attr(data-title);
      }
    }
  }

  &.active {
    .profit-card__text {
      background-image: linear-gradient(180deg, rgba(226, 174, 255, 0.66) 0%, #fc54ff 100%);
      -webkit-background-clip: text;
      -webkit-text-fill-color: transparent;
      color: var(--white);

      .profit-card__value {
        font-weight: 500;
      }
    }
  }

  @media (max-width: 992px) {
    width: calc(100% / 3 - 20px);
    min-width: 200px;
    margin-right: 20px;
    margin-bottom: 20px;
    padding: 30px 15px;

    &[data-title] {
      &:before {
        display: none;
      }
    }
  }
`;

export const ProfitCardTitle: any = styled.h3`
  margin: 0;
  font-weight: 400;
  font-size: 14px;
  line-height: 17px;
  color: #d1d1d1;

  @media (max-width: 768px) {
    font-size: 12px;
    line-height: 15px;
  }
`;

export const ProfitCardText: any = styled.div`
  display: inline-block;
  font-family: 'Poppins', sans-serif;
  font-weight: 400;
  font-size: 30px;
  line-height: 45px;
  background-image: linear-gradient(180deg, #e9b9e9 0%, #ffffff 100%);
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
  color: var(--white);

  span {
    vertical-align: middle;
  }

  .profit-card__value {
    font-weight: 400;
  }

  .profit-card__currency {
    font-weight: 300;
  }

  @media (max-width: 992px) {
    font-size: 20px;
    line-height: 30px;
  }
`;
