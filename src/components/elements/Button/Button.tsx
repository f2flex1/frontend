import React from 'react';
import { connect } from 'react-redux';

import { ButtonContainer } from '@/components/elements/Button/Button.Styles';
import { AppStateType } from '@/store';
import { ThemeType } from '@/store/app/reducers';

interface Props {
  type?: string;
  disabled?: boolean;
  className?: string;
  children?: any;
  onClick?: () => void;
  primary?: boolean;
  as?: any;
  to?: any;
  themeType?: ThemeType;
}

export const Button: React.FC<Props> = ({
  as,
  to,
  type,
  disabled,
  className,
  children,
  onClick,
  themeType,
}) => {
  const handleClick = () => {
    if (!onClick) return false;
    return onClick();
  };
  return (
    <ButtonContainer
      as={as}
      to={to}
      className={`${className} loading-btn -${themeType}`}
      type={type}
      disabled={disabled}
      onClick={handleClick}
    >
      {children}
    </ButtonContainer>
  );
};

const mapStateToProps = (state: AppStateType) => {
  const { app } = state;
  return {
    themeType: app.theme,
  };
};

export default connect(mapStateToProps)(Button);
