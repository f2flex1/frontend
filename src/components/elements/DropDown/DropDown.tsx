import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import React from 'react';

import {
  DropDownContainer,
  DropDownListItem,
} from '@/components/elements/DropDown/DropDown.Styles';

interface Props {
  className?: string;
  name: string;
  placeholder?: string;
  value?: string;
  disableUnderline?: boolean;
  fullWidth?: boolean;
  list: string[] | React.ReactNode[];
  onChange: (name: string, value: string | number) => void;
  IconComponent?: any;
  disableRotate?: boolean;
}

export const DropDown: React.FC<Props> = (props: Props) => {
  const {
    className,
    name,
    placeholder,
    value,
    disableUnderline,
    fullWidth,
    list,
    onChange,
    IconComponent,
    disableRotate,
  } = props;

  const menuProps = {
    disablePortal: true,
    anchorOrigin: {
      vertical: 'bottom',
      horizontal: 'left',
    },
    transformOrigin: {
      vertical: 'top',
      horizontal: 'left',
    },
    getContentAnchorEl: null,
  };

  return (
    <DropDownContainer
      MenuProps={menuProps}
      id={name}
      className={`${className}, ${disableRotate ? '-disable-rotate' : ''}`}
      name={name}
      placeholder={placeholder}
      value={value}
      disableUnderline={disableUnderline}
      fullWidth={fullWidth}
      aria-describedby={`${name}-text`}
      IconComponent={IconComponent || ExpandMoreIcon}
      onChange={(e: React.ChangeEvent<any>) => onChange(name, e.target.value)}
    >
      {list.map((item: string) => (
        <DropDownListItem key={`drop-down-item-${item}`} value={item}>
          {item}
        </DropDownListItem>
      ))}
    </DropDownContainer>
  );
};
