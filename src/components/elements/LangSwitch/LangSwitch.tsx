import React from 'react';

import { LangSwitchStyles } from '@/components/elements/LangSwitch/LangSwitch.Styles';

interface Props {
  className?: string;
  children?: any;
}

export const LangSwitch: React.FC<Props> = ({ className, children }) => {
  return <LangSwitchStyles className={className}>{children}</LangSwitchStyles>;
};
