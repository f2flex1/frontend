import styled from 'styled-components';

export const LangSwitchStyles: any = styled.div`
  &.header__lang {
    margin-right: 48px;
  }

  &.sidebar__lang {
    display: none;
  }

  &.registration__lang {
    position: absolute;
    top: 60px;
    right: 80px;
  }

  @media (max-width: 1200px) {
    &.header__lang {
      margin-right: 18px;
    }
  }

  @media (max-width: 992px) {
    &.header__lang {
      display: none;
    }

    &.sidebar__lang {
      display: block;
      padding-left: 10px;
      margin-top: 20px;
    }

    &.registration__lang {
      top: 30px;
      right: 30px;
    }
  }
`;
