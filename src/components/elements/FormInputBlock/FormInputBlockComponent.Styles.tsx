import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import styled from 'styled-components';

export const FormInputBlockComponent: any = styled(FormControl)``;

export const FormInputComponent: any = styled(Input)`
  &.-error {
    border: 1px solid rgba(255, 77, 95, 0.59);
  }
`;

export const FormInputErrorComponent: any = styled(FormHelperText)``;
