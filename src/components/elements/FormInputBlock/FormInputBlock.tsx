import React, { createRef } from 'react';
import { useTranslation } from 'react-i18next';

import { cleanStringWithNumberVal } from '@/common/utils/formatters';
import {
  FormInputBlockComponent,
  FormInputComponent,
  FormInputErrorComponent,
} from '@/components/elements/FormInputBlock/FormInputBlockComponent.Styles';

export type InputType = 'email' | 'password' | 'number' | 'text' | 'date' | 'phone';

interface Props {
  name: string;
  type: InputType;
  disabled?: boolean;
  value: string;
  placeholder?: string;
  error?: string;
  onChange: (field: string, value: string | number) => void;
  onBlur?: (field: string) => void;
}

export const FormInputBlock: React.FC<Props> = (props: Props) => {
  const { name, type, value, placeholder, disabled, error } = props;

  const { t } = useTranslation();

  const inputRef = createRef();

  const onChange = (field: string, value: string) => {
    if (props.type === 'number') {
      props.onChange(field, cleanStringWithNumberVal(value));
      return;
    }
    props.onChange(field, value);
  };

  const onBlur = () => {
    if (!props.onBlur) return;

    props.onBlur(name);
  };

  return (
    <FormInputBlockComponent fullWidth>
      <FormInputComponent
        className={`${error ? '-error' : ''}`}
        ref={inputRef}
        id={name}
        disableUnderline
        aria-describedby={`${name}-text`}
        type={type}
        placeholder={placeholder}
        autoComplete="off"
        name={name}
        value={value}
        disabled={disabled}
        onChange={(e: React.ChangeEvent<any>) => onChange(name, e.target.value)}
        onBlur={onBlur}
      />
      <FormInputErrorComponent>{t(`${error}`)}</FormInputErrorComponent>
    </FormInputBlockComponent>
  );
};
