import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

// import TelegramIcon from '@/assets/img/muiIcons/telegram';
import {
  Icon,
  // DropDown,
} from '@/components/elements';
import { SocialNetworksStyles } from '@/components/elements/SocialNetworks/SocialNetworks.Styles';
import { SOCIAL_NETWORKS, SOCIAL_NETWORKS_LANGUAGE } from '@/const/app.constants';

interface Props {
  className?: string;
  // themeType?: string;
}

export const SocialNetworks: React.FC<Props> = (props: Props) => {
  // const { className, themeType } = props;
  // const { t, i18n } = useTranslation();
  const { className } = props;
  const { i18n } = useTranslation();
  const { language } = i18n;
  const [currentLang, setCurrentLang] = useState<string>(language);

  useEffect(() => {
    if (language !== currentLang) {
      setCurrentLang(language);
    }
  }, [language, currentLang]);

  const renderSocialNetworks = (social: string, links: any, key: number) => {
    switch (social) {
      case SOCIAL_NETWORKS.FACEBOOK:
        return (
          <a className="link" href={links.facebook} target="_blank" rel="noreferrer" key={key}>
            <Icon name="facebook" />
          </a>
        );
      case SOCIAL_NETWORKS.INSTAGRAM:
        return (
          <a className="link" href={links.instagram} target="_blank" rel="noreferrer" key={key}>
            <Icon name="instagram" />
          </a>
        );
      case SOCIAL_NETWORKS.TELEGRAM:
        // const telegram_links = [
        //   <a href={links.telegram.bot} target="_blank" rel="noreferrer">
        //     {t('common.channel')}
        //   </a>,
        //   <a href={links.telegram.chat} target="_blank" rel="noreferrer">
        //     {t('common.chat')}
        //   </a>,
        // ];
        //
        // return (
        //   <DropDown
        //     className={`dropdown-menu social-dropdown link -${themeType}`}
        //     disableUnderline
        //     name="social"
        //     onChange={() => null}
        //     value=""
        //     list={telegram_links}
        //     IconComponent={TelegramIcon}
        //     disableRotate
        //   />
        // );
        return (
          <a className="link" href={links.telegram.bot} target="_blank" rel="noreferrer" key={key}>
            <Icon name="telegram" />
          </a>
        );
      case SOCIAL_NETWORKS.TWITTER:
        return (
          <a className="link" href={links.twitter} target="_blank" rel="noreferrer" key={key}>
            <Icon name="twitter" />
          </a>
        );
      case SOCIAL_NETWORKS.VK:
        return (
          <a className="link" href={links.vk} target="_blank" rel="noreferrer" key={key}>
            <Icon name="vk" />
          </a>
        );
      case SOCIAL_NETWORKS.YOUTUBE:
        return (
          <a className="link" href={links.youtube} target="_blank" rel="noreferrer" key={key}>
            <Icon name="youtube" />
          </a>
        );
      case SOCIAL_NETWORKS.OK:
        return (
          <a className="link" href={links.ok} target="_blank" rel="noreferrer" key={key}>
            <Icon name="classmates" />
          </a>
        );
      default:
        return null;
    }
  };

  return (
    <SocialNetworksStyles className={className}>
      {Object.keys(SOCIAL_NETWORKS_LANGUAGE).includes(currentLang)
        ? SOCIAL_NETWORKS_LANGUAGE[currentLang].list.map((social: string, index: number) =>
          renderSocialNetworks(social, SOCIAL_NETWORKS_LANGUAGE[currentLang].links, index + 1)
        )
        : SOCIAL_NETWORKS_LANGUAGE.en.list.map((social: string, index: number) =>
          renderSocialNetworks(social, SOCIAL_NETWORKS_LANGUAGE.en.links, index + 1)
        )}
    </SocialNetworksStyles>
  );
};
