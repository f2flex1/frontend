import React from 'react';

import { SwitchStyles } from '@/components/elements/Switch/Switch.Styles';

interface Props {
  className?: string;
  checked?: any;
  onChange?: any;
  name?: string;
  color?: string;
}

export const Switch: React.FC<Props> = ({
  className,
  checked,
  onChange,
  name,
  color,
  children,
}) => {
  return (
    <SwitchStyles
      className={className}
      checked={checked}
      onChange={onChange}
      name={name}
      color={color}
    >
      {children}
    </SwitchStyles>
  );
};
