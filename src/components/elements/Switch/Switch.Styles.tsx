import { Switch } from '@material-ui/core';
import styled from 'styled-components';

export const SwitchStyles: any = styled(Switch)`
  align-items: center;
  padding: 0;
  margin-right: 10px;
  width: 45px;
  height: 24px;
  border-radius: 37px;
  overflow: hidden;

  .MuiIconButton-root {
    padding: 0;
  }

  .MuiSwitch-switchBase.Mui-checked {
    transform: translateX(20px);
  }

  .MuiSwitch-switchBase {
    top: 2px;
    left: 2px;
  }

  .MuiSwitch-thumb {
    width: 20px;
    height: 20px;
    background-color: #78808d;
    box-shadow: 0px -5px 7px rgba(42, 47, 72, 0.47), 0px 5px 7px #0a0b1b;
  }

  .MuiSwitch-track {
    background-color: #666a7e;
    border-radius: 37px;
    opacity: 1;
  }

  .Mui-checked + .MuiSwitch-track {
    background-color: #d1d1d1;
    opacity: 1;
  }

  .Mui-checked {
    color: #666a7e;
  }

  &.-light {
    .MuiSwitch-thumb {
      background-color: #774bff;
    }

    .MuiSwitch-track {
      background-color: #d1d1d1;
    }
  }
`;
