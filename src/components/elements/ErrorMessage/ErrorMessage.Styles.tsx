import styled from 'styled-components';

export const ErrorMessageBlock: any = styled.div`
  font-size: 14px;
  line-height: 17px;
  text-align: center;
  color: #ff4d5f;

  &.modal__error {
    margin-bottom: 17px;
  }
`;
