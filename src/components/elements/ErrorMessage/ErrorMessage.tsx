import React from 'react';

import { ErrorMessageBlock } from '@/components/elements/ErrorMessage/ErrorMessage.Styles';

interface Props {
  className?: string;
  children?: any;
}

export const ErrorMessage: React.FC<Props> = ({ className, children }) => {
  return <ErrorMessageBlock className={className}>{children}</ErrorMessageBlock>;
};
