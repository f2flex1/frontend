import styled from 'styled-components';

import { FormControl, FormControlInput } from '@/components/elements/Input/Input.Styles';

export const ReferralInputBlock: any = styled(FormControl)`
  position: relative;
  max-width: 359px;
  margin: 0 0 15px;

  &.finance-ref {
    margin: 35px 0 0;
    max-width: 100%;
  }

  &.-light {
    .ref-input__btn {
      svg {
        stroke: var(--brown);
      }

      &[data-title] {
        &:before {
          content: attr(data-title);
          position: absolute;
          top: 100%;
          padding: 10px;
          font-size: 12px;
          line-height: 15px;
          color: var(--brown);
          background: #fafafa;
          box-shadow: 0 0 11px rgba(0, 0, 0, 0.04);
        }
      }
    }
  }
`;

export const ReferralField: any = styled(FormControlInput)`
  padding: 14px 54px 14px 30px;
  font-weight: 400;
  font-size: 16px;
  line-height: 21px;
  color: rgba(255, 255, 255, 0.65);
  user-select: none;
  overflow-x: scroll;

  &::selection {
    color: inherit;
    background-color: transparent;
  }
`;

export const ReferralCopy: any = styled.button`
  position: absolute;
  top: 50%;
  right: 15px;
  transform: translateY(-50%);
  padding: 0.25rem;
  border: 0;
  background-color: transparent;
  outline: none;
  cursor: pointer;

  &[data-title] {
    &:before {
      content: attr(data-title);
      position: absolute;
      top: 100%;
      padding: 10px;
      font-size: 12px;
      line-height: 15px;
      color: var(--white);
      background: #292f39;
      box-shadow: 0 0 10px rgba(0, 0, 0, 0.25);
      border-radius: var(--border-radius);
      opacity: 0;
      visibility: hidden;
      transition: opacity 0.5s ease-in-out, visibility 0.5s ease-in-out;
    }

    &:hover {
      &:before {
        opacity: 1;
        visibility: visible;
        content: attr(data-title);
      }
    }
  }

  svg {
    stroke: #f0f0f0;
    transition: stroke 0.3s;
  }

  &:hover svg,
  &:focus svg {
    stroke: var(--white);
  }

  @media (max-width: 992px) {
    &[data-title] {
      &:before {
        display: none;
      }
    }
  }
`;
