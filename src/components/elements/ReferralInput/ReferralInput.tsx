import React from 'react';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';

import { Icon } from '@/components/elements';
import {
  ReferralCopy,
  ReferralField,
  ReferralInputBlock,
} from '@/components/elements/ReferralInput/ReferralInput.Styles';
import { AppStateType } from '@/store';
import { ThemeType } from '@/store/app/reducers';

interface Props {
  id?: string;
  className?: string;
  name: string;
  type: string;
  placeholder?: string;
  value?: string;
  ref_id?: string;
  themeType?: ThemeType;
  setUnderstand?: (message: string | null) => void;
}

export const ReferralInput: React.FC<Props> = (props: Props) => {
  const { id, className, name, type, placeholder, value, ref_id, themeType, setUnderstand } = props;
  const { t } = useTranslation();

  const onCopyLink = () => {
    if (!ref_id) {
      if (setUnderstand) {
        setUnderstand(t('common.ref_understand'));
      }
    }
  };

  return (
    <ReferralInputBlock className={`ref-input ${className} -${themeType}`}>
      <ReferralField
        id={id}
        name={name}
        type={type}
        placeholder={placeholder}
        disabled
        defaultValue={value}
      />
      <CopyToClipboard text={`${value}`} onCopy={() => onCopyLink()}>
        <ReferralCopy className="ref-input__btn" type="button" data-title={`${t('common.copy')}`}>
          <Icon name="referral" size="22" />
        </ReferralCopy>
      </CopyToClipboard>
    </ReferralInputBlock>
  );
};
const mapStateToProps = (state: AppStateType) => {
  const { app } = state;
  return {
    themeType: app.theme,
  };
};

export default connect(mapStateToProps)(ReferralInput);
