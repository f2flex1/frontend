import Skeleton from '@material-ui/lab/Skeleton';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { Icon } from '@/components/elements';
import {
  Level,
  LevelBox,
  LevelImg,
  LevelLeft,
  LevelRight,
  LevelText,
} from '@/components/elements/GainsLevel/GainsLevel.Styles';
import { Image } from '@/components/elements/Image/Image';
import ProgressBar from '@/components/elements/ProgressBar/ProgressBar';

interface Props {
  value: number;
  maxValue: number;
  title?: string | null;
  tooltip?: string | null;
  tooltipParams?: any;
  text?: string | null;
  currency?: string;
  img?: string | null;
  isMain?: boolean;
  isBolean?: boolean;
  className?: string;
}

export const GainsLevel: React.FC<Props> = (props: Props) => {
  const {
    title,
    tooltip,
    tooltipParams,
    value,
    currency,
    maxValue,
    img,
    text,
    isMain,
    isBolean,
    className,
  } = props;

  const { t } = useTranslation();

  const isCompleted = value / maxValue >= 1 && !isBolean;
  const progress = isCompleted ? 100 : (value / maxValue) * 100;

  return (
    <Level
      className={`level ${isCompleted ? 'completed' : ''} ${className}`}
      data-title={tooltip ? t(tooltip, tooltipParams) : null}
    >
      <LevelBox className="level__box">
        <LevelLeft className="level__left">
          <LevelImg className="level__img">
            <Image
              src={img ?? undefined}
              alt="level"
              variant="circle"
              animation="wave"
              width={40}
              height={40}
            />
          </LevelImg>
          <LevelText className={`level__text ${isMain ? '' : 'more'}`}>
            <p>{!title ? <Skeleton variant="text" animation="wave" width={110} /> : t(title)}</p>
            <span>{!text ? <Skeleton variant="text" animation="wave" width={110} /> : text}</span>
          </LevelText>
        </LevelLeft>
        {isCompleted ? (
          <LevelRight className="level__right">
            <Icon name="ok" size="28" />
          </LevelRight>
        ) : (
          <LevelRight className="level__right">
            {!value && value !== 0 ? (
              <p>
                <Skeleton variant="text" animation="wave" width={110} />
                FLEX
              </p>
            ) : (
              !isBolean && (
                <p>
                  {`${value.toFixed(2).replace(/\.?0+$/, '')}`}
                  {currency === 'FLEX' ? <span>{currency || ''}</span> : currency || ''}
                </p>
              )
            )}
          </LevelRight>
        )}
      </LevelBox>
      <ProgressBar max={100} value={progress || 0} />
    </Level>
  );
};
