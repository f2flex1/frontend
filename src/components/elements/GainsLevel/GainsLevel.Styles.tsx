import styled from 'styled-components';

export const Level: any = styled.div`
  position: relative;
  width: 100%;
  margin-bottom: 35px;
  border-radius: var(--border-radius);

  &:last-child {
    margin-bottom: 0;
  }

  &[data-title] {
    &:before {
      content: attr(data-title);
      position: absolute;
      top: 100%;
      left: 40px;
      padding: 16px;
      font-size: 12px;
      line-height: 15px;
      color: var(--white);
      background: #292f39;
      box-shadow: 0 0 10px rgba(0, 0, 0, 0.25);
      border-radius: var(--border-radius);
      opacity: 0;
      visibility: hidden;
      transform: translateY(-10px);
      transition: opacity 0.5s ease-in-out, visibility 0.5s ease-in-out, transform 0.5s ease-in-out;
      z-index: 3;
    }

    &:hover {
      &:before {
        opacity: 1;
        visibility: visible;
        transform: translateY(0);
        content: attr(data-title);
      }
    }
  }

  // Green
  &.completed {
    box-shadow: 0 0 12px rgba(0, 255, 240, 0.6);

    .level__right {
      svg {
        fill: #95d9ca;
      }
    }

    &[data-title] {
      &:before {
        color: #95d9ca;
      }
    }
  }

  // Blue
  //&.completed {
  //  box-shadow: 0 0 12px #a1d3ff;
  //
  //  .level__right {
  //    svg {
  //      fill: #a3d4ff;
  //    }
  //  }
  //
  //  &[data-title] {
  //    &:before {
  //      color: #a3d4ff;
  //    }
  //  }
  //}

  // Red
  //&.completed {
  //  box-shadow: 0 0 12px #ffe0f3;
  //
  //  .level__right {
  //    svg {
  //      fill: #fff3fb;
  //    }
  //  }
  //
  //  &[data-title] {
  //    &:before {
  //      color: #fff3fb;
  //    }
  //  }
  //}

  &.-light {
    &[data-title] {
      &:before {
        color: var(--brown);
        background: #fafafa;
        box-shadow: 0 0 11px rgba(0, 0, 0, 0.04);
      }
    }

    .level__text p {
      color: #1b1b1b;
    }

    .level__text span {
      color: #78808d;
    }

    .level__right {
      color: #1b1b1b;
    }

    .progress-bar {
      background: linear-gradient(180deg, #dde7ff 0%, #f3f7ff 100%);
    }

    .progress-bg {
      background-color: var(--light-blue);
    }

    // Green
    &.completed {
      box-shadow: 0 0 12px #85ebd1;

      .level__right {
        svg {
          fill: #85ebd1;
        }
      }

      &[data-title] {
        &:before {
          color: #85ebd1;
        }
      }
    }

    // Blue
    //&.completed {
    //  box-shadow: 0 0 12px #a8d7ff;
    //
    //  .level__right {
    //    svg {
    //      fill: #a8d7ff;
    //    }
    //  }
    //
    //  &[data-title] {
    //    &:before {
    //      color: #a8d7ff;
    //    }
    //  }
    //}

    // Red
    //&.completed {
    //  box-shadow: 0 0 12px #e39fc9;
    //
    //  .level__right {
    //    svg {
    //      fill: #e39fc9;
    //    }
    //  }
    //
    //  &[data-title] {
    //    &:before {
    //      color: #e39fc9;
    //    }
    //  }
    //}
  }

  @media (max-width: 992px) {
    margin-bottom: 20px;

    &[data-title] {
      &:before {
        display: none;
      }
    }
  }
`;
export const LevelBox: any = styled.div`
  position: relative;
  display: flex;
  justify-content: space-between;
  border-radius: var(--border-radius);
  z-index: 2;
  padding: 15px;

  &:hover {
    cursor: pointer;

    ~ .tooltip {
      opacity: 1;
      visibility: visible;
      transform: translateY(0);
    }
  }
`;
export const LevelLeft: any = styled.div`
  flex: 1 1 auto;
  position: relative;
  display: flex;
  align-items: center;
  margin-right: 5px;
`;
export const LevelRight: any = styled.div`
  flex: 0 0 auto;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  text-align: right;
  font-size: 12px;
  line-height: 15px;
  color: var(--white);

  p {
    margin: 0;
    font-weight: 700;
  }

  span {
    display: block;
    text-align: right;
    font-weight: 400;
    color: #a6a6a6;
  }
`;
export const LevelImg: any = styled.div`
  margin-right: 10px;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 40px;
  height: 40px;
  border-radius: 50%;
  overflow: hidden;

  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
`;
export const LevelText: any = styled.div`
  flex: 1;

  p {
    margin: 0;
    font-weight: 700;
    font-size: 10px;
    line-height: 12px;
    color: var(--white);
  }

  span {
    font-weight: 400;
    font-size: 10px;
    line-height: 12px;
    color: #a6a6a6;
  }
`;
