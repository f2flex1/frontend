import styled from 'styled-components';

export const LoaderStyles: any = styled.div`
  display: flex;
  justify-content: center;

  svg {
    width: 100px;
    height: 100px;
    stroke: #2abd7f;
  }
`;
