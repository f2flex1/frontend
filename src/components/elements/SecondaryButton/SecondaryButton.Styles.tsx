import styled from 'styled-components';

import { ButtonContainer } from '@/components/elements/Button/Button.Styles';

export const SecondaryBtn: any = styled(ButtonContainer)`
  background-color: var(--dark);
  border: 1px solid #777777;
  box-sizing: border-box;

  &:hover {
    background-color: var(--violet);
    border-color: var(--violet);
  }

  &.back {
    position: absolute;
    top: 0;
    left: 0;

    &:before {
      content: '<\\a0';
    }
  }

  &.target-button {
    position: relative;
    padding: 7px 50px 7px 20px;
    font-family: var(--poppins);
    font-weight: 300;
    font-size: 12px;
    line-height: 18px;
    background-color: #343943;
    border: 1px solid #5643a3;

    &:before {
      content: '';
      position: absolute;
      top: 14px;
      right: 19px;
      display: block;
      width: 13px;
      height: 6px;
      background: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 15 8' fill='none'%3E%3Cpath d='M1 1L7.5 7L14 1' stroke='%23EAEAEA' stroke-linecap='round' stroke-linejoin='round'/%3E%3C/svg%3E")
        center no-repeat;
    }

    &:hover {
      border-color: #343943;
    }

    &.show {
      &:before {
        transform: rotate(180deg);
      }
    }
  }

  &.-light {
    border-color: var(--dark-blue);
    color: var(--dark-blue);
    background-color: transparent;
    font-weight: bold;

    &:hover {
      background-color: var(--light-blue);
      border-color: var(--light-blue);
    }

    &.target-button {
      &:before {
        background: url("data:image/svg+xml,%3Csvg width='11' height='7' viewBox='0 0 11 7' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M10 1.5L5.5 5.5L1 1.5' stroke='%23434343' stroke-width='1' stroke-linecap='round' stroke-linejoin='round'/%3E%3C/svg%3E")
          center no-repeat;
      }
    }
  }

  @media (max-width: 1200px) {
    &.back {
      display: none;
    }
  }
`;
