import React from 'react';

import { SecondaryBtn } from '@/components/elements/SecondaryButton/SecondaryButton.Styles';

interface Props {
  secondary?: boolean;
  href?: string;
  type?: string;
  disabled?: boolean;
  className?: string;
  children?: any;
  as?: any;
  to?: any;
  onClick?: (e?: any) => void;
}

export const SecondaryButton: React.FC<Props> = (props: Props) => {
  const { secondary, href, to, as, type, disabled, className, children, onClick } = props;
  const handleClick = (e: any) => {
    if (!onClick) return false;
    return onClick(e);
  };

  return (
    <SecondaryBtn
      secondary={secondary}
      className={className}
      href={href}
      to={to}
      as={as}
      type={type}
      disabled={disabled}
      onClick={(e: React.ChangeEvent<any>) => handleClick(e)}
    >
      {children}
    </SecondaryBtn>
  );
};
