import React from 'react';

import { StatusStyles } from '@/components/elements/Status/Status.Styles';

export interface StatusProps {
  className: string;
}

export const Status: React.FC<StatusProps> = ({ className }) => {
  return (
    <StatusStyles className={`status ${className}`}>
      <div className="status__img" />
    </StatusStyles>
  );
};

export default Status;
