import styled from 'styled-components';

export const StatusStyles: any = styled.div`
  position: relative;
  display: inline-flex;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
  overflow: hidden;
  width: 80px;
  height: 80px;

  .status__img {
    width: 100%;
    height: 100%;
    border-radius: 50%;
  }

  &.beginner {
  }

  // Ametis

  &.ametis-junior,
  &.ametis-middle,
  &.ametis-senior,
  &.ametis-lead {
    .status__img {
      background: url('img/status/ametis.svg') center / cover no-repeat;
    }
  }

  &.ametis-junior {
    border: none;
  }

  &.ametis-middle {
    padding: 10px;
    border: 1px solid var(--ametis);
  }

  &.ametis-senior {
    padding: 10px;
    border: 3px solid var(--ametis);
    box-shadow: 0 0 3px var(--ametis);

    &::after {
      content: '';
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      display: block;
      width: 95%;
      height: 95%;
      border-radius: 50%;
      border: 1px solid var(--ametis);
    }
  }

  &.ametis-lead {
    padding: 10px;
    border: 6px solid var(--ametis);
    box-shadow: 0 0 6px var(--ametis);
  }

  // Lazurit

  &.lazurit-junior,
  &.lazurit-middle,
  &.lazurit-senior,
  &.lazurit-lead {
    .status__img {
      background: url('../img/status/lazurit.svg') center / cover no-repeat;
    }
  }

  &.lazurit-junior {
    border: none;
  }

  &.lazurit-middle {
    padding: 10px;
    border: 1px solid var(--lazurit);
  }

  &.lazurit-senior {
    padding: 10px;
    border: 3px solid var(--lazurit);
    box-shadow: 0 0 3px var(--lazurit);

    &::after {
      content: '';
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      display: block;
      width: 95%;
      height: 95%;
      border-radius: 50%;
      border: 1px solid var(--lazurit);
    }
  }

  &.lazurit-lead {
    padding: 10px;
    border: 6px solid var(--lazurit);
    box-shadow: 0 0 6px var(--lazurit);
  }

  // Nefrit

  &.nefrit-junior,
  &.nefrit-middle,
  &.nefrit-senior,
  &.nefrit-lead {
    .status__img {
      background: url('../img/status/nefrit.svg') center / cover no-repeat;
    }
  }

  &.nefrit-junior {
    border: none;
  }

  &.nefrit-middle {
    padding: 10px;
    border: 1px solid var(--nefrit);
  }

  &.nefrit-senior {
    padding: 10px;
    border: 3px solid var(--nefrit);
    box-shadow: 0 0 3px var(--nefrit);

    &::after {
      content: '';
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      display: block;
      width: 95%;
      height: 95%;
      border-radius: 50%;
      border: 1px solid var(--nefrit);
    }
  }

  &.nefrit-lead {
    padding: 10px;
    border: 6px solid var(--nefrit);
    box-shadow: 0 0 6px var(--nefrit);
  }

  // Emerald

  &.emerald-junior,
  &.emerald-middle,
  &.emerald-senior,
  &.emerald-lead {
    .status__img {
      background: url('../img/status/emerald.svg') center / cover no-repeat;
    }
  }

  &.emerald-junior {
    border: none;
  }

  &.emerald-middle {
    padding: 10px;
    border: 1px solid var(--emerald);
  }

  &.emerald-senior {
    padding: 10px;
    border: 3px solid var(--emerald);
    box-shadow: 0 0 3px var(--emerald);

    &::after {
      content: '';
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      display: block;
      width: 95%;
      height: 95%;
      border-radius: 50%;
      border: 1px solid var(--emerald);
    }
  }

  &.emerald-lead {
    padding: 10px;
    border: 6px solid var(--emerald);
    box-shadow: 0 0 6px var(--emerald);
  }

  // Ruby

  &.ruby-junior,
  &.ruby-middle,
  &.ruby-senior,
  &.ruby-lead {
    .status__img {
      background: url('../img/status/ruby.svg') center / cover no-repeat;
    }
  }

  &.ruby-junior {
    border: none;
  }

  &.ruby-middle {
    padding: 10px;
    border: 1px solid var(--ruby);
  }

  &.ruby-senior {
    padding: 10px;
    border: 3px solid var(--ruby);
    box-shadow: 0 0 3px var(--ruby);

    &::after {
      content: '';
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      display: block;
      width: 95%;
      height: 95%;
      border-radius: 50%;
      border: 1px solid var(--ruby);
    }
  }

  &.ruby-lead {
    padding: 10px;
    border: 6px solid var(--ruby);
    box-shadow: 0 0 6px var(--ruby);
  }

  // Saphire

  &.saphire-junior {
    border: none;
  }

  &.saphire-middle {
    padding: 10px;
    border: 1px solid var(--saphire);
  }

  &.saphire-senior {
    padding: 10px;
    border: 3px solid var(--saphire);
    box-shadow: 0 0 3px var(--saphire);

    &::after {
      content: '';
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      display: block;
      width: 95%;
      height: 95%;
      border-radius: 50%;
      border: 1px solid var(--saphire);
    }
  }

  &.saphire-lead {
    padding: 10px;
    border: 6px solid var(--saphire);
    box-shadow: 0 0 6px var(--saphire);
  }

  // Diamond

  &.diamond-junior {
    border: none;
  }

  &.diamond-middle {
    padding: 10px;
    border: 1px solid var(--diamond);
  }

  &.diamond-senior {
    padding: 10px;
    border: 3px solid var(--diamond);
    box-shadow: 0 0 3px var(--diamond);

    &::after {
      content: '';
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      display: block;
      width: 95%;
      height: 95%;
      border-radius: 50%;
      border: 1px solid var(--diamond);
    }
  }

  &.diamond-lead {
    padding: 10px;
    border: 6px solid var(--diamond);
    box-shadow: 0 0 6px var(--diamond);
  }
`;
