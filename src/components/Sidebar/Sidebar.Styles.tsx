import { NavLink } from 'react-router-dom';
import styled from 'styled-components';

export const SidebarWrapper: any = styled.aside`
  flex: 0 0 285px;
  position: relative;
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  padding: 45px 35px 50px 40px;

  &::after {
    content: '';
    position: absolute;
    right: 35px;
    top: 0;
    width: 1px;
    height: 100%;
    background: var(--vertical-border);
  }

  .logo {
    position: relative;
  }

  &.-light {
    background: #f7f8fa;

    .menu__link {
      &:before {
        background-color: var(--white);
      }
    }

    &:after {
      background: linear-gradient(
        rgba(255, 255, 255, 0) 0%,
        #ffffff 50.52%,
        rgba(255, 255, 255, 0) 100%
      );
    }
  }

  @media (max-width: 992px) {
    position: fixed;
    z-index: 10;
    right: 0;
    top: 0;
    margin-top: 104px;
    padding: 34px;
    background-color: var(--dark-gray);
    width: 100%;
    max-width: 50%;
    height: calc(100% - 104px);
    transition: 0.4s ease-in-out;
    transform: scaleX(0.7);
    opacity: 0;
    visibility: hidden;
    transform-origin: right;
    overflow-y: auto;
    scrollbar-width: none;

    ::-webkit-scrollbar {
      display: none;
    }

    &.active {
      transform: scaleX(1);
      opacity: 1;
      visibility: visible;
    }

    &:after {
      display: none;
    }
  }

  @media (max-width: 768px) {
    padding: 34px 16px;
  }

  @media (max-width: 576px) {
    border-radius: 0;
    max-width: 100%;
    box-shadow: none;
  }
`;

export const SidebarHeader: any = styled.div`
  display: none;

  @media (max-width: 992px) {
    display: block;
  }
`;

export const SidebarFooter: any = styled.div`
  margin: 20px auto 0;
  display: flex;
  justify-content: space-between;
  align-items: center;

  .switch {
    align-items: center;
    padding: 7px;
    font-size: 15px;
    line-height: 18px;
    color: #78808d;

    .MuiFormControlLabel-label {
      font-size: 15px;
    }
  }

  @media (max-width: 992px) {
    margin: 30px 0 0;
  }
`;

export const Menu: any = styled.nav`
  position: relative;
  height: 100%;
  display: flex;
  flex-direction: column;

  @media (max-width: 992px) {
    padding: 0;
    margin: 0;
  }
`;

export const MenuList: any = styled.ul`
  height: 100%;
  display: flex;
  flex-direction: column;
`;

export const MenuListItem: any = styled.li`
  border-radius: var(--border-radius);
  overflow: hidden;

  &:first-child {
    margin-top: auto;
  }

  &:last-child {
    margin-top: auto;

    .menu__link:after {
      display: none;
    }
  }

  @media (max-width: 992px) {
    &:last-child {
      margin: 0 0 auto;
    }
  }
`;

export const MenuListLink: any = styled(NavLink)`
  position: relative;
  display: flex;
  align-items: center;
  min-height: 60px;
  padding: 0 20px;
  font-weight: 400;
  font-size: 16px;
  line-height: 22px;
  color: var(--light-gray);
  transition: background 0.3s ease-in-out, color 0.3s ease-in-out;
  border-radius: var(--border-radius);
  border-left: 3px solid transparent;

  span {
    display: block;
  }

  svg {
    width: 28px;
    min-width: 28px;
    fill: var(--light-gray);
    margin-right: 20px;
    transition: fill 0.3s ease-in-out, stroke 0.3s ease-in-out;
  }

  .icon-faq,
  .icon-additionally {
    fill: none;
    stroke: var(--light-gray);
  }

  &::before {
    content: '';
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 1px;
    background-image: var(--horizontal-border);
  }

  &::after {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 1px;
    background-image: var(--horizontal-border);
  }

  &:hover,
  &:focus {
    background: linear-gradient(90deg, #935aed -10%, rgba(41, 47, 57, 0) 23%);
    border-left: 3px solid #7d4392;
    color: #d1d1d1;
    font-weight: 400;

    svg {
      fill: #d1d1d1;
    }

    .icon-faq,
    .icon-additionally {
      fill: none;
      stroke: #d1d1d1;
    }
  }

  &.active {
    background-image: linear-gradient(90deg, #935aed -10%, rgba(41, 47, 57, 0) 23%);
    border-left: 2px solid #7d4392;
    color: var(--white);
    font-weight: 700;
    background-repeat: no-repeat;

    svg {
      fill: var(--white);
    }

    .icon-faq,
    .icon-additionally {
      fill: none;
      stroke: var(--white);
    }
  }

  &.-light {
    color: #929292;

    svg {
      fill: #929292;
    }

    .icon-faq,
    .icon-additionally {
      fill: none;
      stroke: #929292;
    }

    &:hover,
    &:focus {
      color: #4b4b4b;

      svg {
        fill: #4b4b4b;
      }

      .icon-faq,
      .icon-additionally {
        fill: none;
        stroke: #4b4b4b;
      }
    }

    &.active {
      color: #575757;
      border-left: 2px solid #dd59f8;

      svg {
        fill: #575757;
      }

      .icon-faq,
      .icon-additionally {
        fill: none;
        stroke: #575757;
      }
    }
  }

  @media (max-width: 992px) {
    padding: 0 15px;
    min-height: 50px;

    span {
      max-width: 100%;
    }

    &::before,
    &::after {
      display: none;
    }
  }
`;
