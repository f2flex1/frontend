import { connect } from 'react-redux';

import Sidebar from '@/components/Sidebar/Sidebar';
import { AppStateType } from '@/store';

const mapStateToProps = (state: AppStateType) => ({
  userData: state.user.userData,
});
export default connect(mapStateToProps, {})(Sidebar);
