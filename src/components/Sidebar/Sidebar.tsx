import { FormControlLabel } from '@material-ui/core';
import React, { useCallback, useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { useLocation, Link } from 'react-router-dom';

import LanguageSelector from '@/common/components/LanguageSelector';
import { UserBar } from '@/components/Header/Header.styles';
import LogOutButton from '@/components/Header/LogOutButton/LogOutButton';
import {
  Menu,
  MenuList,
  MenuListItem,
  MenuListLink,
  SidebarFooter,
  SidebarHeader,
  SidebarWrapper,
} from '@/components/Sidebar/Sidebar.Styles';
import { Icon, Logo, Switch, LangSwitch, UserInfo, SocialNetworks } from '@/components/elements';
import { LEVEL_STYLES_TYPES } from '@/const/app.constants';
import { STATIC_URL } from '@/const/general.constants';
import { PATHS } from '@/const/paths.constants';
import { AppStateType } from '@/store';
import actions, { setSidebarOpen } from '@/store/app/actions';
import { ThemeType } from '@/store/app/reducers';
import { UserReducerState } from '@/store/user/reducers';

type SidebarProps = {
  user: UserReducerState;
  isSidebarOpen: boolean;
  themeType: ThemeType;
  setTheme: (theme: ThemeType) => void;
  setSidebarOpen: (value: boolean) => void;
};

const Sidebar: React.FC<SidebarProps> = (props: SidebarProps) => {
  const { isSidebarOpen, user, themeType, setTheme, setSidebarOpen } = props;
  const { userData } = user;
  const { pathname } = useLocation();
  const { t } = useTranslation();
  const [urlPath, setUrlPath] = useState<string>('');

  const toggleTheme = useCallback(() => {
    setTheme(themeType === 'light' ? 'dark' : 'light');
  }, [themeType, setTheme]);

  const ref = useRef<HTMLDivElement | null>(null);
  const outsideHandler = () => {
    setSidebarOpen(false);
  };

  useEffect(() => {
    if (urlPath !== pathname) {
      outsideHandler();
    }

    if (!urlPath) {
      setUrlPath(pathname);
    }
  }, [pathname, outsideHandler, urlPath]);

  // useEffect(() => {
  //   function handleClickOutside(event: any) {
  //     event.stopPropagation();
  //     if (ref.current && !ref.current.contains(event.target)) {
  //       outsideHandler();
  //     }
  //   }
  //
  //   document.addEventListener('mousedown', handleClickOutside);
  //   return () => {
  //     document.removeEventListener('mousedown', handleClickOutside);
  //   };
  // }, [ref, outsideHandler]);

  return (
    <SidebarWrapper className={`${isSidebarOpen ? 'active' : ''} -${themeType}`} ref={ref}>
      <Logo className="logo" to="/">
        {themeType === 'dark' ? (
          <img src="/img/main/logo.svg" alt="fin2flex" />
        ) : (
          <img src="/img/main/logo-light.svg" alt="fin2flex" />
        )}
      </Logo>

      <SidebarHeader className="sidebar__header">
        <UserBar className="sidebar__user">
          <Link to="/settings">
            <UserInfo
              className={`${
                LEVEL_STYLES_TYPES[user.userData?.level_info?.level || 'BEGINNER']
              } -${themeType}`}
              src={
                user.userData?.photo
                  ? `${STATIC_URL}${user.userData?.photo}`
                  : `${
                    themeType === 'dark'
                      ? 'img/user-placeholder.svg'
                      : 'img/user-placeholder-light.svg'
                  }`
              }
              alt={`${userData?.first_name ?? ''} ${userData?.last_name ?? ''}`}
              userName={userData?.first_name ?? ''}
              userLastname={userData?.last_name ?? ''}
            />
          </Link>
          <LogOutButton className={`-${themeType}`} />
        </UserBar>
      </SidebarHeader>

      <Menu className="menu">
        <MenuList className="menu__list">
          <MenuListItem className={pathname === '/cabinet' ? 'menu__item active' : 'menu__item'}>
            <MenuListLink to="/cabinet" className={`menu__link -${themeType}`}>
              <Icon name="cabinet" size="28" />
              <span className="menu__text">{t('navBar.cabinet')}</span>
            </MenuListLink>
          </MenuListItem>
          <MenuListItem className={pathname === '/finances' ? 'menu__item active' : 'menu__item'}>
            <MenuListLink to="/finances" className={`menu__link -${themeType}`}>
              <Icon name="finances" size="24" />
              <span className="menu__text">{t('navBar.finances')}</span>
            </MenuListLink>
          </MenuListItem>
          <MenuListItem
            className={pathname === '/investments' ? 'menu__item active' : 'menu__item'}
          >
            <MenuListLink to="/investments" className={`menu__link -${themeType}`}>
              <Icon name="investments" size="24" />
              <span className="menu__text">{t('navBar.investments')}</span>
            </MenuListLink>
          </MenuListItem>
          <MenuListItem className={pathname === '/actives' ? 'menu__item active' : 'menu__item'}>
            <MenuListLink to="/actives" className={`menu__link -${themeType}`}>
              <Icon name="actives" size="28" />
              <span className="menu__text">{t('navBar.actives')}</span>
            </MenuListLink>
          </MenuListItem>
          <MenuListItem
            className={pathname === '/partnership' ? 'menu__item active' : 'menu__item'}
          >
            <MenuListLink to="/partnership" className={`menu__link -${themeType}`}>
              <Icon name="partnership" size="24" />
              <span className="menu__text">{t('navBar.partnership')}</span>
            </MenuListLink>
          </MenuListItem>
          <MenuListItem
            className={pathname === '/additionally' ? 'menu__item active' : 'menu__item'}
          >
            <MenuListLink to={PATHS.ADDITIONALLY} className={`menu__link -${themeType}`}>
              <Icon name="additionally" size="24" />
              <span className="menu__text">{t('navBar.additionally')}</span>
            </MenuListLink>
          </MenuListItem>
          <MenuListItem className={pathname === '/faq' ? 'menu__item active' : 'menu__item'}>
            <MenuListLink to={PATHS.FAQ} className={`menu__link -${themeType}`}>
              <Icon name="faq" size="24" />
              <span className="menu__text">{t('navBar.faq')}</span>
            </MenuListLink>
          </MenuListItem>
          <MenuListItem
            className={
              pathname === '/settings'
                ? 'menu__item menu__item_settings active'
                : 'menu__item menu__item_settings'
            }
          >
            <MenuListLink to="/settings" className={`menu__link -${themeType}`}>
              <Icon name="settings" size="18" />
              <span className="menu__text">{t('navBar.settings')}</span>
            </MenuListLink>
          </MenuListItem>
        </MenuList>
      </Menu>

      <LangSwitch className="sidebar__lang">
        <LanguageSelector className={`-${themeType}`} />
      </LangSwitch>

      <SocialNetworks className="sidebar__social" />

      <SidebarFooter className="sidebar__footer">
        <FormControlLabel
          className="switch"
          control={
            <Switch
              className={`-${themeType}`}
              checked={themeType === 'light'}
              onChange={toggleTheme}
              name="checkedB"
              color="primary"
            />
          }
          label={t(`navBar.theme.${themeType}`)}
        />

        <Logo className="sidebar__logo" to="/">
          {themeType === 'dark' ? (
            <img src="/img/main/logo.svg" alt="fin2flex" />
          ) : (
            <img src="/img/main/logo-light.svg" alt="fin2flex" />
          )}
        </Logo>
      </SidebarFooter>
    </SidebarWrapper>
  );
};

const mapStateToProps = (state: AppStateType) => {
  const { user, app } = state;
  return {
    user,
    themeType: app.theme,
    isSidebarOpen: app.isSidebarOpen,
  };
};

export default connect(mapStateToProps, {
  setTheme: actions.setTheme,
  setSidebarOpen,
})(Sidebar);
