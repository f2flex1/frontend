import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';

import { selfMoment } from '@/common/utils/i18n';
import { papulate } from '@/common/utils/papulate';
import { SiteWrapper } from '@/components/Layout/Layout.Styles';
import { Alert, Modal, Button, ScrollBar } from '@/components/elements';
import { ModalDescription } from '@/components/elements/PageModal/PageModal.Styles';
import { BEFORE_LOUNCH_DATE } from '@/const/app.constants';
import { STORAGE_KEYS } from '@/const/storage_keys.constants';
import { AppStateType } from '@/store';
import { ThemeType } from '@/store/app/reducers';
import { setAlert, setUnderstand } from '@/store/loadingsErrors/actions';

interface Props {
  alert?: any;
  understand?: any;
  children?: any;
  setAlert: (message: string | null, messageType: string | null) => void;
  setUnderstand: (message: string | null) => void;
  themeType: ThemeType;
}

const AuthLayout: React.FC<Props> = (props: Props) => {
  const { alert, understand, children, setAlert, setUnderstand, themeType } = props;

  const pathname = children?.props?.location?.pathname;
  const query = new URLSearchParams(children?.props?.location?.search);
  const queryRef = query.get(STORAGE_KEYS.REF_ID) || '';
  const localRef = localStorage.getItem(STORAGE_KEYS.REF_ID);
  const ref = queryRef || localRef;

  if (!!queryRef && queryRef !== localRef) {
    localStorage.setItem(STORAGE_KEYS.REF_ID, queryRef);
  }

  const isRegistrationWithoutRef = !!(pathname === '/registration' && !ref);

  const { t } = useTranslation();
  const [timer, setTimer] = useState<number>(
    selfMoment(BEFORE_LOUNCH_DATE).unix() - selfMoment().unix()
  );
  const [showBeforeLaunchModal, setShowBeforeLaunchModal] =
    useState<boolean>(isRegistrationWithoutRef);

  useEffect(() => {
    if (!showBeforeLaunchModal) {
      return () => {};
    }

    const s = timer - 1;

    if (s < 1) {
      setTimer(0);
      setShowBeforeLaunchModal(false);
      return () => {};
    }

    const intervalId: ReturnType<typeof setTimeout> = setTimeout(() => {
      setTimer(s);
    }, 1000);

    return () => {
      clearTimeout(intervalId);
    };
  }, [timer, showBeforeLaunchModal]);

  return (
    <ScrollBar className={`-${themeType}`}>
      <SiteWrapper className={`-${themeType}`}>
        <div className={showBeforeLaunchModal ? 'blurred' : ''}>{children}</div>
        {showBeforeLaunchModal ? (
          <Modal opened className={`start-modal -${themeType}`}>
            <div className="modal__logo">
              <img src="/img/main/logo.svg" alt="fin2flex" />
            </div>
            <p className="modal__title">
              {t('modals.titles.respectable_partners')}
              !
            </p>
            <p className="modal__text">
              {t('modals.texts.before_launch')}
              <span className="modal__text_big">FIN2FLEX</span>
              <span className="modal__text_small">{t('modals.texts.left')}</span>
            </p>
            <div className="modal__timer">
              <div className="modal__timer-box">
                <span className="modal__timer-total">{selfMoment(timer * 1000).format('DD')}</span>
                <span className="modal__timer-descr">
                  {t(`common.days.${papulate(Number(selfMoment(timer * 1000).format('DD')))}`)}
                </span>
              </div>
              <div className="modal__timer-box">
                <span className="modal__timer-total">{selfMoment(timer * 1000).format('HH')}</span>
                <span className="modal__timer-descr">
                  {t(`common.hours.${papulate(Number(selfMoment(timer * 1000).format('HH')))}`)}
                </span>
              </div>
              <div className="modal__timer-box">
                <span className="modal__timer-total">{selfMoment(timer * 1000).format('mm')}</span>
                <span className="modal__timer-descr">
                  {t(`common.minutes.${papulate(Number(selfMoment(timer * 1000).format('mm')))}`)}
                </span>
              </div>
              <div className="modal__timer-box">
                <span className="modal__timer-total">{selfMoment(timer * 1000).format('ss')}</span>
                <span className="modal__timer-descr">
                  {t(`common.seconds.${papulate(Number(selfMoment(timer * 1000).format('ss')))}`)}
                </span>
              </div>
            </div>
          </Modal>
        ) : null}

        <Modal
          className={`understanding-modal auth-modal -${themeType}`}
          closeModal={() => setUnderstand('')}
          opened={!!understand.message}
        >
          <ModalDescription className="modal__description">{understand.message}</ModalDescription>
          <Button className={`xl width -${themeType}`} onClick={() => setUnderstand('')}>
            {t('modals.btns.understand_btn')}
          </Button>
        </Modal>
        <Alert alert={alert} pathname={children?.props?.location?.pathname} setAlert={setAlert} />
      </SiteWrapper>
    </ScrollBar>
  );
};

const mapState = (state: AppStateType) => {
  const { loadings, app } = state;
  return {
    alert: loadings.alert,
    understand: loadings.understand,
    themeType: app.theme,
  };
};

export default connect(mapState, { setAlert, setUnderstand })(AuthLayout);
