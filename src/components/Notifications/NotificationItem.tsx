import React from 'react';
import { Link } from 'react-router-dom';
// import { useTranslation } from 'react-i18next';

import { Message } from '@/api';
import i18n, { selfMoment } from '@/common/utils/i18n';
import { NotificationsListItem } from '@/components/Notifications/Notifications.Styles';
import { LEVEL_IMAGES_TYPES, NOTIFICATIONS_ICONS } from '@/const/app.constants';
import { PATHS } from '@/const/paths.constants';

type NotificationProps = {
  message: Message;
};

const NotificationItem: React.FC<NotificationProps> = (props: NotificationProps) => {
  const { message } = props;

  // const { t } = useTranslation();

  const getNotificationDesc = (type_name: string, type_data: any) => {
    switch (type_name) {
      case 'LEVEL_UP':
        return i18n.t(`notifications.${type_name}`, { level: i18n.t(`levels.${type_data.level}`) });
      case 'PERCENT_BY_PACKETS':
        return i18n.t(`notifications.${type_name}`, type_data);
      case 'PERCENT_BY_PERCENT':
        return i18n.t(`notifications.${type_name}`, type_data);
      case 'BALANCE_CREDIT':
        return i18n.t(`notifications.${type_name}`, type_data);
      case 'BALANCE_DEBIT':
        return i18n.t(`notifications.${type_name}`, type_data);
      case 'OPEN_PACKET':
        return i18n.t(`notifications.${type_name}`, type_data);
      case 'DAILY_DIVIDEND':
        return i18n.t(`notifications.${type_name}`, type_data);
      case 'OPEN_BONUS':
        return i18n.t(`notifications.${type_name}`, { level: i18n.t(`levels.${type_data.level}`) });
      case 'INVITE_USER':
        return i18n.t(`notifications.${type_name}`, type_data);
      case 'PASSWORD_CHANGE':
        return i18n.t(`notifications.${type_name}`, type_data);
      default:
        return type_name;
    }
  };

  const getNotificationIcon = (type_name: string, type_data: any) => {
    switch (type_name) {
      case 'LEVEL_UP':
        return LEVEL_IMAGES_TYPES[type_data.level];
      case 'PERCENT_BY_PACKETS':
        return NOTIFICATIONS_ICONS[type_name];
      case 'PERCENT_BY_PERCENT':
        return NOTIFICATIONS_ICONS[type_name];
      case 'BALANCE_CREDIT':
        return NOTIFICATIONS_ICONS[type_name];
      case 'BALANCE_DEBIT':
        return NOTIFICATIONS_ICONS[type_name];
      case 'OPEN_PACKET':
        return NOTIFICATIONS_ICONS[type_name];
      case 'DAILY_DIVIDEND':
        return NOTIFICATIONS_ICONS[type_name];
      case 'OPEN_BONUS':
        return NOTIFICATIONS_ICONS[type_name];
      case 'INVITE_USER':
        return NOTIFICATIONS_ICONS[type_name];
      case 'PASSWORD_CHANGE':
        return NOTIFICATIONS_ICONS[type_name];
      default:
        return NOTIFICATIONS_ICONS.DEFAULT;
    }
  };

  const getNotificationLink = (type_name: string) => {
    switch (type_name) {
      case 'LEVEL_UP':
        return PATHS.FINANCES;
      case 'PERCENT_BY_PACKETS':
        return PATHS.FINANCES;
      case 'PERCENT_BY_PERCENT':
        return PATHS.FINANCES;
      case 'BALANCE_CREDIT':
        return PATHS.FINANCES;
      case 'BALANCE_DEBIT':
        return PATHS.FINANCES;
      case 'OPEN_PACKET':
        return PATHS.FINANCES;
      case 'DAILY_DIVIDEND':
        return PATHS.FINANCES;
      case 'OPEN_BONUS':
        return PATHS.FINANCES;
      case 'INVITE_USER':
        return PATHS.PARTNERSHIP;
      case 'PASSWORD_CHANGE':
        return PATHS.SETTINGS;
      default:
        return PATHS.FINANCES;
    }
  };

  return (
    <NotificationsListItem className="notifications__item">
      <Link to={getNotificationLink(message.key)} className="notifications__box">
        <div className="notifications__img">
          <img src={getNotificationIcon(message.key, message.data)} alt="notification" />
        </div>
        <div className="notifications__content">
          <p className="notifications__text">{getNotificationDesc(message.key, message.data)}</p>
          <span className="notifications__date">{selfMoment(message.created).fromNow()}</span>
        </div>
      </Link>
      {/* <div className="notifications__flex">*/}
      {/*  <span>{t(`statuses.${message.status}`)}</span>*/}
      {/* </div>*/}
    </NotificationsListItem>
  );
};

export default NotificationItem;
