import styled from 'styled-components';

export const NotificationsWrapper = styled.div`
  position: absolute;
  right: 220px;
  top: 123px;
  padding: 26px 10px 26px 22px;
  background-color: #292f39;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.25);
  border-radius: var(--border-radius);
  z-index: 10;
  transform: translateY(-10%);
  opacity: 0;
  visibility: hidden;
  transition: 0.4s;

  &.active {
    transform: translateY(0);
    opacity: 1;
    visibility: visible;
  }

  &.-light {
    background-color: #fafafa;
    box-shadow: 0 0 11px rgba(0, 0, 0, 0.04);

    .notifications__item {
      &:after {
        background-image: linear-gradient(
          90deg,
          rgba(255, 198, 198, 0) 0%,
          rgba(21, 23, 46, 0.17) 51.84%,
          rgba(255, 255, 255, 0) 99.52%
        );
      }
    }

    .notifications__text {
      color: var(--brown);
    }

    .notifications__date {
      color: var(--brown);
    }

    .notifications__flex {
      color: var(--brown);
    }
  }

  @media (max-width: 992px) {
    position: fixed;
    top: 50%;
    right: auto;
    left: 50%;
    transform: translate(-50%, -100%);

    &.active {
      transform: translate(-50%, -50%);
    }
  }
`;

export const NotificationsList = styled.ul`
  padding-right: 20px;

  .notifications__list_btn {
    text-align: center;
  }
`;

export const NotificationsListItem = styled.li`
  position: relative;
  display: flex;
  align-items: center;
  padding: 17px 0 7px;

  &::after {
    content: '';
    position: absolute;
    left: 50%;
    bottom: 0;
    transform: translateX(-50%);
    width: 210px;
    height: 1px;
    background-image: linear-gradient(
      90deg,
      rgba(255, 255, 255, 0) 0%,
      rgba(255, 255, 255, 0.34) 52.35%,
      rgba(255, 255, 255, 0) 99.52%
    );
  }

  .notifications__box {
    display: flex;
  }

  .notifications__img {
    width: 28px;
    height: 28px;
    margin-right: 19px;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 50%;
    overflow: hidden;
    flex: 0 0 auto;

    img {
      width: 100%;
      height: 100%;
      object-fit: cover;
    }
  }

  .notifications__text {
    margin: 0 0 5px;
    font-weight: 500;
    font-size: 11px;
    line-height: 13px;
  }

  .notifications__date {
    font-weight: 500;
    font-size: 9px;
    line-height: 11px;
    color: var(--white);
  }

  .notifications__flex {
    text-align: right;
    font-weight: 700;
    font-size: 14px;
    line-height: 17px;
    color: var(--white);
  }
`;
