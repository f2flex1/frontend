import { ClickAwayListener, Portal } from '@material-ui/core';
import React, { MouseEventHandler, useCallback, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';

import { Message } from '@/api';
import NotificationItem from '@/components/Notifications/NotificationItem';
import {
  NotificationsList,
  NotificationsWrapper,
} from '@/components/Notifications/Notifications.Styles';
import { Loader, Placeholder, ScrollBar, SecondaryButton } from '@/components/elements';
import { AppStateType } from '@/store';
import types from '@/store/actionTypes';
import { ThemeType } from '@/store/app/reducers';
import { selectLoadingByKey } from '@/store/loadingsErrors/selectors';
import { getMessages, setMessagesRead } from '@/store/messages/actions';
import { MessagesState } from '@/store/messages/reducer';

type NotificationProps = {
  messages: MessagesState;
  themeType: ThemeType;
  children: any;
  setMessagesRead: () => void;
  getMessages: (page: number) => void;
  loading: boolean;
};

const NotificationsContainer: React.FC<NotificationProps> = (props: NotificationProps) => {
  const { messages, themeType, children, setMessagesRead, getMessages, loading } = props;
  const ref = useRef<HTMLDivElement | null>(null);
  const [isOpen, setIsOpen] = useState(false);
  const { t } = useTranslation();

  const outsideHandler = useCallback(() => setIsOpen(false), [setIsOpen]);

  const onButtonHandler: MouseEventHandler<HTMLDivElement> = e => {
    e.stopPropagation();
    e.preventDefault();
    setIsOpen(val => !val);

    const unreadedCount = messages.messages.list.filter((message: Message) => !message.read).length;
    if (unreadedCount > 0) {
      setMessagesRead();
    }
  };

  const loadMore = () => {
    getMessages(messages.messages.page + 1);
  };

  return (
    <ClickAwayListener onClickAway={outsideHandler}>
      <div>
        <Portal>
          <NotificationsWrapper
            className={`notifications ${isOpen ? 'active' : ''} -${themeType}`}
            ref={ref}
          >
            {!messages.messages.list.length ? (
              <Placeholder className={`notifications-placeholder -${themeType}`}>
                <p>{t('home.notifications.placeholder.title')}</p>
                <span>{t('home.notifications.placeholder.text')}</span>
              </Placeholder>
            ) : (
              <ScrollBar
                className={`notifications-scrollbar -${themeType}`}
                style={{ maxHeight: '360px', width: '300px' }}
              >
                <NotificationsList className="notifications__list">
                  {messages.messages.list.map((message: Message, index: number) => (
                    <NotificationItem key={`notification-message-${index + 1}`} message={message} />
                  ))}
                  {!messages.messages.end && !!messages.messages.list.length ? (
                    <div className="notifications__list_btn">
                      <SecondaryButton
                        className={`sm -${themeType}`}
                        onClick={() => loadMore()}
                        disabled={loading}
                      >
                        {t('home.notifications.load_more')}
                        {loading ? <Loader /> : null}
                      </SecondaryButton>
                    </div>
                  ) : null}
                </NotificationsList>
              </ScrollBar>
            )}
          </NotificationsWrapper>
        </Portal>
        <div onClick={onButtonHandler} ref={ref}>
          {children}
        </div>
      </div>
    </ClickAwayListener>
  );
};

const mapStateToProps = (state: AppStateType) => {
  const { app } = state;
  return {
    themeType: app.theme,
    loading: selectLoadingByKey(state, types.GET_MESSAGES_REQUEST),
  };
};

export default connect(mapStateToProps, { setMessagesRead, getMessages })(NotificationsContainer);
