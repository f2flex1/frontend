import styled from 'styled-components';

export const SiteWrapper: any = styled.div`
  height: 100%;
  min-height: 100vh;

  & .blurred {
    filter: progid:DXImageTransform.Microsoft.Blur(PixelRadius='5');
    -webkit-filter: url(#blur);
    filter: url(#blur);
    -webkit-filter: blur(5px);
    filter: blur(5px);
  }
`;

export const MainContainer: any = styled.div`
  width: 100%;
  height: 100%;
  display: flex;

  &.blurred {
    filter: progid:DXImageTransform.Microsoft.Blur(PixelRadius='5');
    -webkit-filter: url(#blur);
    filter: url(#blur);
    -webkit-filter: blur(5px);
    filter: blur(5px);
  }
`;

export const Content: any = styled.div`
  flex: 1 1 auto;
  display: flex;
  flex-direction: column;
  //overflow-y: auto;
`;
