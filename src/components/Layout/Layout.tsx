import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';

import { Message } from '@/api';
import Header from '@/components/Header';
import { Content, MainContainer, SiteWrapper } from '@/components/Layout/Layout.Styles';
import SidebarContainer from '@/components/Sidebar/SidebarContainer';
import { Alert, Modal, Status, Button } from '@/components/elements';
import { ModalDescription } from '@/components/elements/PageModal/PageModal.Styles';
import { LEVEL_STYLES_TYPES } from '@/const/app.constants';
import { AppStateType } from '@/store';
import { ThemeType } from '@/store/app/reducers';
import { setAlert, setUnderstand } from '@/store/loadingsErrors/actions';
import { setMessageRead } from '@/store/messages/actions';

interface Props {
  alert?: any;
  understand?: any;
  levelUp: Message | null;
  children?: any;
  setAlert: (message: string | null, messageType: string | null) => void;
  setMessageRead: (messageId: string | undefined) => void;
  setUnderstand: (message: string | null) => void;
  themeType: ThemeType;
}

const Layout: React.FC<Props> = (props: Props) => {
  const {
    alert,
    understand,
    levelUp,
    children,
    setAlert,
    setMessageRead,
    setUnderstand,
    themeType,
  } = props;

  const { t } = useTranslation();
  const [levelUpOpened, setLevelUpOpened] = useState<boolean>(false);
  const [levelUpTimer, setLevelUpTimer] = useState<number>(0);

  const closeLevelUpModal = () => {
    setLevelUpOpened(false);
    setMessageRead(levelUp?.id);
  };

  useEffect(() => {
    const s = levelUpTimer - 1;

    if (s < 1) {
      setLevelUpTimer(0);
      if (levelUpOpened) {
        closeLevelUpModal();
      }
      return () => {};
    }

    const intervalId: ReturnType<typeof setTimeout> = setTimeout(() => {
      setLevelUpTimer(s);
    }, 1000);

    return () => {
      clearTimeout(intervalId);
    };
  }, [levelUpTimer, levelUpOpened]);

  useEffect(() => {
    if (levelUp) {
      setLevelUpOpened(true);
      setLevelUpTimer(5);
    }
  }, [levelUp]);

  return (
    <SiteWrapper className={`-${themeType}`}>
      {/* TODO: Loading modal*/}
      {/* <Modal*/}
      {/*  closeModal={}*/}
      {/*  opened={}*/}
      {/*  className={`loading-modal -${themeType}`}*/}
      {/* >*/}
      {/*  <svg viewBox="0 0 183 183" fill="none" xmlns="http://www.w3.org/2000/svg">*/}
      {/*    <g filter="url(#filter0_ddddi_1997)">*/}
      {/*      <path d="M158 91.5C158 128.227 128.227 158 91.5 158C54.7731 158 25 128.227 25 91.5C25 54.7731 54.7731 25 91.5 25" stroke="white" strokeWidth="3" />*/}
      {/*    </g>*/}
      {/*    <defs>*/}
      {/*      <filter id="filter0_ddddi_1997" x="0.5" y="0.5" width="182" height="182" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">*/}
      {/*        <feFlood floodOpacity="0" result="BackgroundImageFix" />*/}
      {/*        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha" />*/}
      {/*        <feOffset />*/}
      {/*        <feGaussianBlur stdDeviation="11.5" />*/}
      {/*        <feComposite in2="hardAlpha" operator="out" />*/}
      {/*        <feColorMatrix type="matrix" values="0 0 0 0 0.98 0 0 0 0 0 0 0 0 0 1 0 0 0 1 0" />*/}
      {/*        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_1997" />*/}
      {/*        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha" />*/}
      {/*        <feOffset />*/}
      {/*        <feGaussianBlur stdDeviation="11.5" />*/}
      {/*        <feComposite in2="hardAlpha" operator="out" />*/}
      {/*        <feColorMatrix type="matrix" values="0 0 0 0 0.98 0 0 0 0 0 0 0 0 0 1 0 0 0 1 0" />*/}
      {/*        <feBlend mode="normal" in2="effect1_dropShadow_1997" result="effect2_dropShadow_1997" />*/}
      {/*        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha" />*/}
      {/*        <feOffset />*/}
      {/*        <feGaussianBlur stdDeviation="11.5" />*/}
      {/*        <feComposite in2="hardAlpha" operator="out" />*/}
      {/*        <feColorMatrix type="matrix" values="0 0 0 0 0.98 0 0 0 0 0 0 0 0 0 1 0 0 0 1 0" />*/}
      {/*        <feBlend mode="normal" in2="effect2_dropShadow_1997" result="effect3_dropShadow_1997" />*/}
      {/*        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha" />*/}
      {/*        <feOffset/>*/}
      {/*        <feGaussianBlur stdDeviation="11.5" />*/}
      {/*        <feComposite in2="hardAlpha" operator="out" />*/}
      {/*        <feColorMatrix type="matrix" values="0 0 0 0 0.98 0 0 0 0 0 0 0 0 0 1 0 0 0 1 0" />*/}
      {/*        <feBlend mode="normal" in2="effect3_dropShadow_1997" result="effect4_dropShadow_1997" />*/}
      {/*        <feBlend mode="normal" in="SourceGraphic" in2="effect4_dropShadow_1997" result="shape" />*/}
      {/*        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha" />*/}
      {/*        <feOffset />*/}
      {/*        <feGaussianBlur stdDeviation="0.5" />*/}
      {/*        <feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1" />*/}
      {/*        <feColorMatrix type="matrix" values="0 0 0 0 0.98 0 0 0 0 0 0 0 0 0 1 0 0 0 1 0" />*/}
      {/*        <feBlend mode="normal" in2="shape" result="effect5_innerShadow_1997" />*/}
      {/*      </filter>*/}
      {/*    </defs>*/}
      {/*  </svg>*/}
      {/* </Modal>*/}

      <Modal
        closeModal={closeLevelUpModal}
        opened={levelUpOpened}
        className={`level-modal -${themeType}`}
      >
        <p className="modal__title">
          <span>
            {t('modals.titles.congratulations')}
            !
          </span>
          {t('modals.titles.level')}
          {t(`levels.${levelUp?.data?.level}`)}
        </p>
        <Status className={LEVEL_STYLES_TYPES[levelUp?.data?.level]} />
      </Modal>

      <Modal
        className={`understanding-modal auth-modal -${themeType}`}
        closeModal={() => setUnderstand('')}
        opened={!!understand.message}
      >
        <ModalDescription
          className="modal__description"
          dangerouslySetInnerHTML={{ __html: understand.message }}
        />
        <Button className={`xl width -${themeType}`} onClick={() => setUnderstand('')}>
          {t('modals.btns.understand_btn')}
        </Button>
      </Modal>

      <MainContainer className={`main-container ${levelUpOpened ? 'blurred' : ''}`}>
        <SidebarContainer />
        <Content className="content">
          <Header />
          {children}
        </Content>
      </MainContainer>
      <Alert alert={alert} pathname={children?.props?.location?.pathname} setAlert={setAlert} />
    </SiteWrapper>
  );
};

const mapState = (state: AppStateType) => {
  const { loadings, messages, app } = state;
  return {
    alert: loadings.alert,
    understand: loadings.understand,
    levelUp: messages.levelUp,
    themeType: app.theme,
  };
};

export default connect(mapState, { setAlert, setMessageRead, setUnderstand })(Layout);
