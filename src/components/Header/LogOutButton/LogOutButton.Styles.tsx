import styled from 'styled-components';

export const LogOutStyledButton: any = styled.button`
  transition: all 0.3s;
  background-color: transparent;
  border: 0;
  outline: none;
  cursor: pointer;

  svg {
    fill: #78808d;
    overflow: visible;
    transition: fill 0.3s;

    .arrow {
      transition: transform 0.3s ease-in-out;
      transform: translate(0, 0);
      -webkit-transform: translate(0, 0);
    }

    .border {
      transition: transform 0.3s ease-in-out;
      transform: translate(0, 0);
      -webkit-transform: translate(0, 0);
    }
  }

  &.-light {
    svg {
      fill: var(--dark-blue);
    }
  }

  &:hover,
  &:focus {
    filter: blur(1px);

    svg {
      fill: #d95cf9;
      .arrow {
        -webkit-transform: translate(10, 0);
        transform: translate(10%, 0);
      }
      .border {
        -webkit-transform: translate(-10%, 0);
        transform: translate(-10%, 0);
      }
    }
  }
`;
