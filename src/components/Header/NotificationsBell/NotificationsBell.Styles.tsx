import { Badge } from '@material-ui/core';
import styled from 'styled-components';

export const BellStyles: any = styled.button`
  padding: 0;
  margin-right: 27px;
  background-color: transparent;
  border: 0;
  cursor: pointer;

  &:focus {
    box-shadow: none;
    outline: none;

    svg {
      stroke: #d1d1d1;
    }
  }

  &.-light {
    svg {
      stroke: #1b1b1b;
    }
  }
`;

export const BellBadge: any = styled(Badge)`
  span {
    background-color: #db55f7;
  }
`;

export const BellSVG: any = styled.svg`
  stroke: var(--white);
  transition: all 0.3s ease-in-out;
  transform: translateX(0) rotate(0);
  -webkit-transform: translate(0, 0);

  &.left {
    -webkit-transform: translateX(0) rotate(-10deg);
    transform: translateX(5%) rotate(-10deg);
  }

  &.right {
    -webkit-transform: rotate(15deg);
    transform: translateX(-5%) rotate(10deg);
  }

  @media (max-width: 768px) {
  }
`;
