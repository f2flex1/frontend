import React, { useEffect, useState } from 'react';

import {
  BellBadge,
  BellStyles,
  BellSVG,
} from '@/components/Header/NotificationsBell/NotificationsBell.Styles';

type Props = {
  count?: null | string | number;
  className?: string;
};

const selectNextAnimate = (prev: string): string => {
  if (prev === 'left') return 'right';
  return prev === 'right' ? 'none' : 'left';
};

export const NotificationsBell: React.FC<Props> = ({ count, className }) => {
  const [rotate, setRotate] = useState('none');
  const [animate, setAnimate] = useState(false);

  useEffect(() => {
    let timeout: NodeJS.Timeout | undefined;
    const nextStep = selectNextAnimate(rotate);
    const move = (next: string) => {
      timeout = setTimeout(() => {
        setRotate(next);
      }, 300);
    };

    if (animate) {
      if (nextStep !== 'none') {
        move(nextStep);
      } else {
        timeout = setTimeout(() => {
          setAnimate(false);
          setRotate(nextStep);
        }, 300);
      }
    }

    return () => {
      if (timeout) clearInterval(timeout);
    };
  }, [animate, setAnimate, setRotate, rotate]);

  useEffect(() => {
    let timeout: NodeJS.Timeout | undefined;
    const runAnimation = () => {
      timeout = setTimeout(() => {
        setAnimate(true);
      }, 5000);
    };
    if (!animate && count) {
      runAnimation();
    }

    return () => {
      if (timeout) clearInterval(timeout);
    };
  }, [count, animate, setAnimate]);

  return (
    <BellStyles className={className}>
      <BellBadge
        color="secondary"
        badgeContent={count ?? '0'}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
      >
        <BellSVG
          className={rotate}
          width="22"
          height="24"
          viewBox="0 0 22 24"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M17.6667 7.8C17.6667 4.04446 14.6819 1 11 1C7.3181 1 4.33333 4.04446 4.33333 7.8C4.33333 15.7333 1 18 1 18H21C21 18 17.6667 15.7333 17.6667 7.8Z"
            strokeWidth="2"
            strokeLinecap="round"
            strokeLinejoin="round"
          />
          <path
            d="M13 22.0018C12.5864 22.6186 11.8244 22.9982 11 22.9982C10.1756 22.9982 9.41364 22.6186 9 22.0018"
            strokeWidth="2"
            strokeLinecap="round"
            strokeLinejoin="round"
          />
        </BellSVG>
        {/* <NotificationsNoneIcon fontSize="large" color="disabled" /> TODO: improve with MUI icon*/}
      </BellBadge>
    </BellStyles>
  );
};

export default NotificationsBell;
