import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const HeaderWrapper: any = styled.header`
  position: relative;
  flex: 0 0 auto;
  width: 100%;
  height: 104px;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  padding: 35px 45px 5px 45px;
  background: var(--dark-gray);

  &.-light {
    background: #f7f8fa;
  }

  &::after {
    content: '';
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 1px;
    background: linear-gradient(
      90deg,
      rgba(255, 255, 255, 0) 0%,
      rgba(255, 255, 255, 0.06) 71.01%,
      rgba(255, 255, 255, 0) 99.52%
    );
  }

  .user-info {
    margin-right: 20px;
  }

  @media (max-width: 1200px) {
    padding: 35px 15px 5px;
  }

  @media (max-width: 992px) {
    padding: 48px 16px 25px 16px;
    justify-content: space-between;

    &:after {
      display: none;
    }
  }
`;

export const UserBar: any = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  //min-width: 283px;

  @media (max-width: 992px) {
    display: none;

    &.sidebar__user {
      display: flex;
      max-width: 100%;
      margin-bottom: 44px;

      .user-info__img {
        width: 50px;
        height: 50px;
        margin-right: 15px;
      }

      .user-info__name {
        span {
          display: block;
        }
      }
    }
  }

  @media (max-width: 374px) {
    &.sidebar__user {
      .user-info__img {
        width: 40px;
        height: 40px;
        margin-right: 10px;
      }

      .user-info__name {
        margin-right: 10px;
        font-size: 12px;
        line-height: 15px;
      }
    }
  }
`;

export const User: any = styled(Link)`
  display: flex;
  align-items: center;
`;

export const Burger: any = styled.button`
  display: none;
  margin: 0;
  padding: 0;
  border: none;
  background-color: transparent;
  z-index: 99;
  cursor: pointer;

  svg {
    width: 40px;
  }

  .line {
    fill: none;
    stroke: var(--white);
    stroke-width: 4;
    transition: stroke-dasharray 600ms cubic-bezier(0.4, 0, 0.2, 1),
      stroke-dashoffset 600ms cubic-bezier(0.4, 0, 0.2, 1);
  }
  .line1 {
    stroke-dasharray: 60 207;
    stroke-width: 6;
  }
  .line2 {
    stroke-dasharray: 60 60;
    stroke-width: 6;
  }
  .line3 {
    stroke-dasharray: 60 207;
    stroke-width: 6;
  }

  &.active {
    .line1 {
      stroke-dasharray: 90 207;
      stroke-dashoffset: -134;
      stroke-width: 6;
    }
    .line2 {
      stroke-dasharray: 1 60;
      stroke-dashoffset: -40;
      stroke-width: 6;
    }
    .line3 {
      stroke-dasharray: 90 207;
      stroke-dashoffset: -134;
      stroke-width: 6;
    }
  }

  &.-light {
    .line {
      stroke: var(--dark-blue);
    }
  }

  @media (max-width: 992px) {
    display: block;
    order: 1;
  }
`;
