import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { Message } from '@/api';
import LanguageSelector from '@/common/components/LanguageSelector';
import LogOutButton from '@/components/Header/LogOutButton/LogOutButton';
import { NotificationsBell } from '@/components/Header/NotificationsBell/NotificationsBell';
import NotificationsContainer from '@/components/Notifications';
import { LangSwitch, SocialNetworks, UserInfo } from '@/components/elements';
import { LEVEL_STYLES_TYPES } from '@/const/app.constants';
import { STATIC_URL } from '@/const/general.constants';
import { AppStateType } from '@/store';
import { setSidebarOpen } from '@/store/app/actions';
import { ThemeType } from '@/store/app/reducers';
import { MessagesState } from '@/store/messages/reducer';
import { UserReducerState } from '@/store/user/reducers';

import { Burger, HeaderWrapper, UserBar } from './Header.styles';

export interface HeaderProps {
  user: UserReducerState;
  themeType: ThemeType;
  messages: MessagesState;
  isSidebarOpen: boolean;
  setSidebarOpen: (payload: boolean) => void;
}

const Header: React.FC<HeaderProps> = (props: HeaderProps) => {
  const { user, themeType, messages, isSidebarOpen, setSidebarOpen } = props;
  const { userData } = user;

  const toogleSidebar = (event: any) => {
    event.stopPropagation();
    setSidebarOpen(!isSidebarOpen);
  };

  return (
    <HeaderWrapper className={`header -${themeType}`}>
      <SocialNetworks
        className={`header__social -${themeType}`}
        // themeType={themeType}
      />
      <LangSwitch className="header__lang">
        <LanguageSelector className={`-${themeType}`} />
      </LangSwitch>
      <NotificationsContainer messages={messages}>
        <NotificationsBell
          count={messages.messages.list.filter((message: Message) => !message.read).length}
          className={`-${themeType}`}
        />
      </NotificationsContainer>
      <UserBar>
        <Link to="/settings">
          <UserInfo
            className={`${
              LEVEL_STYLES_TYPES[user.userData?.level_info?.level || 'BEGINNER']
            } -${themeType}`}
            src={
              user.userData?.photo
                ? `${STATIC_URL}${user.userData?.photo}`
                : `${
                  themeType === 'dark'
                    ? '/img/user-placeholder.svg'
                    : '/img/user-placeholder-light.svg'
                }`
            }
            alt={`${userData?.first_name ?? ''} ${userData?.last_name ?? ''}`}
            userName={`${userData?.first_name || userData?.phone || userData?.email}`}
            userLastname={userData?.last_name ?? ''}
          />
        </Link>
        <LogOutButton className={`-${themeType}`} />
      </UserBar>
      <Burger
        className={`burger -${themeType} ${isSidebarOpen ? 'active' : ''}`}
        onClick={toogleSidebar}
      >
        <svg viewBox="0 0 100 100">
          <path
            className="line line1"
            d="M 20,29.000046 H 80.000231 C 80.000231,29.000046 94.498839,28.817352 94.532987,66.711331 94.543142,77.980673 90.966081,81.670246 85.259173,81.668997 79.552261,81.667751 75.000211,74.999942 75.000211,74.999942 L 25.000021,25.000058"
          />
          <path className="line line2" d="M 40,50 H 80" />
          <path
            className="line line3"
            d="M 20,70.999954 H 80.000231 C 80.000231,70.999954 94.498839,71.182648 94.532987,33.288669 94.543142,22.019327 90.966081,18.329754 85.259173,18.331003 79.552261,18.332249 75.000211,25.000058 75.000211,25.000058 L 25.000021,74.999942"
          />
        </svg>
      </Burger>
    </HeaderWrapper>
  );
};

const mapState = (state: AppStateType) => {
  const { user, messages, app } = state;
  return {
    user,
    themeType: app.theme,
    messages,
    isSidebarOpen: app.isSidebarOpen,
  };
};

export default connect(mapState, { setSidebarOpen })(Header);
