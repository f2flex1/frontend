import styled from 'styled-components';

export const BreadCrumbList: any = styled.ul`
  display: flex;
  flex-wrap: wrap;
  margin-bottom: 20px;
`;

export const BreadCrumbListItem: any = styled.li`
  display: flex;
  align-items: center;
  margin-bottom: 20px;
  font-weight: 700;
  font-size: 25px;
  line-height: 30px;
  color: ${({ theme }) => theme.breadcrumbs.link};

  &:not(:last-child)::after {
    content: '';
    display: inline-block;
    width: 10px;
    height: 16px;
    margin: 0 30px;
    background: ${({ theme }) => theme.breadcrumbs.separator};
  }

  a {
    font-weight: 400;
    transition: none;
  }

  @media (max-width: 992px) {
    font-size: 14px;
    line-height: 17px;

    &:not(:last-child)::after {
      width: 5px;
      height: 12px;
      margin: 0 17px;
    }
  }
`;
