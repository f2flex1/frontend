import React from 'react';
import { useTranslation } from 'react-i18next';
import { NavLink, useLocation } from 'react-router-dom';

import { BreadCrumbList, BreadCrumbListItem } from '@/components/BreadCrumb/BreadCrumb.Styles';

const BreadCrumb: React.FC<any> = () => {
  const { pathname } = useLocation();
  const { t } = useTranslation();

  const array = pathname
    .split('/')
    .filter(val => !!val)
    .map((route, i, arr) => {
      return {
        url: arr.filter((item, index) => index <= i).join('/'),
        route,
      };
    });
  // if (array[0].route !== 'cabinet') {
  //   array.unshift({ route: 'cabinet', url: '' });
  // }

  return (
    <BreadCrumbList>
      {array.map((route, i) =>
        i === array.length - 1 ? (
          <BreadCrumbListItem key={`breadcrumb-item-${i + 1}`}>
            {t(`navBar.${route.route}`)}
          </BreadCrumbListItem>
        ) : (
          <BreadCrumbListItem key={`breadcrumb-item-${i + 1}`}>
            <NavLink to={`/${route.url}`}>{t(`navBar.${route.route}`)}</NavLink>
          </BreadCrumbListItem>
        )
      )}
    </BreadCrumbList>
  );
};

export default BreadCrumb;
