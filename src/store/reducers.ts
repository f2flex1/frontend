import { combineReducers } from 'redux';

import messagesReducer from '@/store/messages/reducer';

import appReducer from './app/reducers';
import loadingErrorsReducer from './loadingsErrors/reducer';
import modalsReducers from './modals/reducers';
import operationsReducers from './operations/reducers';
import packetsReducers from './packets/reducers';
import partnersReducers from './partners/reducers';
import programsReducers from './programs/reducers';
import usersReducers from './user/reducers';
import walletReducers from './wallet/reducers';

const reducer = combineReducers({
  app: appReducer,
  operations: operationsReducers,
  user: usersReducers,
  modals: modalsReducers,
  wallet: walletReducers,
  loadings: loadingErrorsReducer,
  messages: messagesReducer,
  packets: packetsReducers,
  partners: partnersReducers,
  programs: programsReducers,
});

export default reducer;
