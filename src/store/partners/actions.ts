import { UserData } from '@/api';

import types from '../actionTypes';

const getPartners = (payload: { user: UserData }) => ({
  type: types.GET_PARTNERS_REQUEST,
  payload,
});

export { getPartners };
