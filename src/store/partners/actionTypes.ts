const GET_PARTNERS_REQUEST = 'GET_PARTNERS_REQUEST';
const GET_PARTNERS_SUCCESS = 'GET_PARTNERS_SUCCESS';
const GET_PARTNERS_FAILURE = 'GET_PARTNERS_FAILURE';

type GetPartnersActionType = {
  type: typeof GET_PARTNERS_SUCCESS | typeof GET_PARTNERS_FAILURE;
  payload?: any;
};

export type GetPartnersRequest = {
  type: typeof GET_PARTNERS_REQUEST;
  payload?: any;
};

export type GetPartnersTypes = GetPartnersActionType | GetPartnersRequest;

export type PartnersActionTypes = GetPartnersTypes;

export default {
  GET_PARTNERS_REQUEST,
  GET_PARTNERS_SUCCESS,
  GET_PARTNERS_FAILURE,
} as const;
