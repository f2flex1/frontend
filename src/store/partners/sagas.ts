import { all, call, put, takeLatest } from 'redux-saga/effects';

import { Partner } from '@/api';
import API from '@/api/executor';
import { ALERT_TYPES } from '@/const/app.constants';
import types from '@/store/actionTypes';
import { setAlert, setLoading } from '@/store/loadingsErrors/actions';

// calls
const fetchPartnersCall = (payload?: any) => API.call('fetchPartners', payload);

function* fetchPartners() {
  yield put(setLoading(types.GET_PARTNERS_REQUEST, true));
  try {
    const res: Partner[] = yield call(() => fetchPartnersCall());
    yield all([
      put({ type: types.GET_PARTNERS_SUCCESS, payload: res }),
      put(setLoading(types.GET_PARTNERS_REQUEST, false)),
    ]);
  } catch (e) {
    const error = e?.errObj?.error?.length ? e?.errObj?.error[0] : null;
    yield all([
      put(setLoading(types.GET_PARTNERS_REQUEST, false)),
      put(setAlert(error ? `error.${error}` : 'error.network.404', ALERT_TYPES.DANGER)),
      put({ type: types.GET_PARTNERS_FAILURE }),
    ]);
  }
}

export default function* partnersSagas() {
  yield takeLatest(types.GET_PARTNERS_REQUEST, fetchPartners);
}
