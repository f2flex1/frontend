import cloneDeep from 'lodash/cloneDeep';
import filter from 'lodash/filter';

import { Partner, UserData } from '@/api';
import types, { AppActionTypes } from '@/store/actionTypes';

export type PartnersReducerState = {
  partners: {
    list: Partner[];
    levels: any;
    page: number;
    loaded: boolean;
    user: UserData | null;
  };
};

const INITIAL_STATE: PartnersReducerState = {
  partners: {
    list: [],
    levels: null,
    page: 1,
    loaded: false,
    user: null,
  },
};

const getPartnersLevels = (
  list: Partner[],
  partner: Partner,
  level: number,
  index: number,
  beforeIndexes: number[]
): any => {
  return {
    uid: partner.ref_id,
    index: Number(`${level}${index + 1}`),
    level,
    user: partner,
    opened: false,
    beforeIndexes: [...beforeIndexes, Number(`${level}${index + 1}`)],
    partners: filter(list, { parent: partner.ref_id })
      .filter((parent: Partner) => parent.ref_id)
      .map((parent: Partner, partnerIndex: number) => {
        return getPartnersLevels(list, parent, level + 1, partnerIndex, [
          ...beforeIndexes,
          Number(`${level}${index + 1}`),
        ]);
      }),
  };
};

const createPartnersLevels = (list: Partner[], user: UserData | null): any => {
  return filter(list, { parent: user?.ref_id })
    .filter((parent: Partner) => parent.ref_id)
    .map((parent: Partner, index: number) => {
      return getPartnersLevels(list, parent, 1, index, []);
    });
};

const partnersReducers = (state = INITIAL_STATE, action: AppActionTypes): PartnersReducerState => {
  switch (action.type) {
    case types.GET_PARTNERS_REQUEST:
      const newPartnersRequest = cloneDeep(INITIAL_STATE.partners);
      newPartnersRequest.loaded = true;
      newPartnersRequest.user = action.payload.user;
      return {
        ...state,
        partners: newPartnersRequest,
      };
    case types.GET_PARTNERS_SUCCESS:
      const newPartnersSuccess = cloneDeep(state.partners);
      newPartnersSuccess.list = action.payload || [];
      newPartnersSuccess.levels = createPartnersLevels(action.payload, state.partners.user);
      return {
        ...state,
        partners: newPartnersSuccess,
      };

    case types.GET_PARTNERS_FAILURE:
      return {
        ...state,
      };
    case types.LOGOUT_SUCCESS:
      const logoutData = cloneDeep(INITIAL_STATE);
      return logoutData;
    default:
      return state;
  }
};

export default partnersReducers;
