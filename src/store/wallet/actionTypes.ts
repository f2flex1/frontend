import { ExchangeRates } from '@/api/codecs.wallet';

const GET_WALLET_STAT_REQUEST = 'GET_WALLET_STAT_REQUEST';
const GET_WALLET_STAT_SUCCESS = 'GET_WALLET_STAT_SUCCESS';
const GET_WALLET_STAT_FAILURE = 'GET_WALLET_STAT_FAILURE';
const GET_EXCHANGE_RATES_REQUEST = 'GET_EXCHANGE_RATES_REQUEST';
const GET_EXCHANGE_RATES_SUCCESS = 'GET_EXCHANGE_RATES_SUCCESS';
const GET_EXCHANGE_RATES_FAILURE = 'GET_EXCHANGE_RATES_FAILURE';

type GetWalletStatActionType = {
  type: typeof GET_WALLET_STAT_SUCCESS | typeof GET_WALLET_STAT_FAILURE;
  payload?: any;
};

export type GetWalletStatRequest = {
  type: typeof GET_WALLET_STAT_REQUEST;
};

type GetExchangeRatesActionType = {
  type: typeof GET_EXCHANGE_RATES_SUCCESS | typeof GET_EXCHANGE_RATES_FAILURE;
  payload?: ExchangeRates;
};

export type GetExchangeRatesRequest = {
  type: typeof GET_EXCHANGE_RATES_REQUEST;
};

export type GetWalletStatTypes = GetWalletStatActionType | GetWalletStatRequest;
export type GetExchangeRatesTypes = GetExchangeRatesActionType | GetExchangeRatesRequest;

export type WalletActionTypes = GetWalletStatTypes | GetExchangeRatesTypes;

export default {
  GET_WALLET_STAT_REQUEST,
  GET_WALLET_STAT_SUCCESS,
  GET_WALLET_STAT_FAILURE,
  GET_EXCHANGE_RATES_REQUEST,
  GET_EXCHANGE_RATES_SUCCESS,
  GET_EXCHANGE_RATES_FAILURE,
} as const;
