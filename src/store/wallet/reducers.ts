import cloneDeep from 'lodash/cloneDeep';

import { WalletStat } from '@/api';
import { ExchangeRates } from '@/api/codecs.wallet';
import types, { AppActionTypes } from '@/store/actionTypes';

export type WalletReducerState = {
  stat: WalletStat | null;
  exchangeRates: ExchangeRates;
  loaded: boolean;
};

const INITIAL_STATE: WalletReducerState = {
  stat: null,
  exchangeRates: {
    BTCUSDT: 46660,
    USDTRUB: 74.41,
    BTCRUB: 3471970.6,
  },
  loaded: false,
};

const walletReducers = (state = INITIAL_STATE, action: AppActionTypes): WalletReducerState => {
  switch (action.type) {
    case types.GET_WALLET_STAT_REQUEST:
      return {
        ...state,
        loaded: true,
      };
    case types.GET_WALLET_STAT_SUCCESS:
      return {
        ...state,
        stat: action.payload || null,
      };

    case types.GET_WALLET_STAT_FAILURE:
      return {
        ...state,
        stat: null,
      };
    case types.GET_EXCHANGE_RATES_REQUEST:
      return {
        ...state,
        // exchangeRates:  {},
      };
    case types.GET_EXCHANGE_RATES_SUCCESS:
      return {
        ...state,
        exchangeRates: action.payload || {},
      };

    case types.GET_EXCHANGE_RATES_FAILURE:
      return {
        ...state,
        // exchangeRates:  {},
      };
    case types.LOGOUT_SUCCESS:
      const logoutData = cloneDeep(INITIAL_STATE);
      return logoutData;
    default:
      return state;
  }
};

export default walletReducers;
