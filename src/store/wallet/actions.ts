import types from '../actionTypes';

const getWalletStat = () => ({
  type: types.GET_WALLET_STAT_REQUEST,
});

const getCurrenciesRates = () => ({
  type: types.GET_EXCHANGE_RATES_REQUEST,
});

export { getWalletStat, getCurrenciesRates };
