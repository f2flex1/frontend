import { all, call, put, takeLatest } from 'redux-saga/effects';

import { WalletStat } from '@/api';
import API from '@/api/executor';
import { ALERT_TYPES } from '@/const/app.constants';
import types from '@/store/actionTypes';
import { setAlert, setLoading } from '@/store/loadingsErrors/actions';

// calls
const fetchWalletStatCall = (payload?: any) => API.call('fetchWalletStat', payload);
const fetchCurrenciesRatesCall = (payload?: any) => API.call('fetchCurrenciesRates', payload);

function* fetchWalletStat() {
  yield put(setLoading(types.GET_WALLET_STAT_REQUEST, true));
  try {
    const res: WalletStat = yield call(() => fetchWalletStatCall());
    yield all([
      put({ type: types.GET_WALLET_STAT_SUCCESS, payload: res }),
      put(setLoading(types.GET_WALLET_STAT_REQUEST, false)),
    ]);
  } catch (e) {
    const error = e?.errObj?.error?.length ? e?.errObj?.error[0] : null;
    yield all([
      put(setLoading(types.GET_WALLET_STAT_REQUEST, false)),
      put(setAlert(error ? `error.${error}` : 'error.network.404', ALERT_TYPES.DANGER)),
      put({ type: types.GET_WALLET_STAT_FAILURE }),
    ]);
  }
}

function* fetchCurrenciesRates() {
  yield put(setLoading(types.GET_EXCHANGE_RATES_REQUEST, true));
  try {
    const res: WalletStat = yield call(() => fetchCurrenciesRatesCall());
    yield all([
      put({ type: types.GET_EXCHANGE_RATES_SUCCESS, payload: res }),
      put(setLoading(types.GET_EXCHANGE_RATES_REQUEST, false)),
    ]);
  } catch (e) {
    const error = e?.errObj?.error?.length ? e?.errObj?.error[0] : null;
    yield all([
      put(setLoading(types.GET_EXCHANGE_RATES_REQUEST, false)),
      put(setAlert(error ? `error.${error}` : 'error.network.404', ALERT_TYPES.DANGER)),
      put({ type: types.GET_EXCHANGE_RATES_FAILURE }),
    ]);
  }
}

export default function* walletSagas() {
  yield takeLatest(types.GET_WALLET_STAT_REQUEST, fetchWalletStat);
  yield takeLatest(types.GET_EXCHANGE_RATES_REQUEST, fetchCurrenciesRates);
}
