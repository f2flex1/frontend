const GET_PROGRAMS_REQUEST = 'GET_PROGRAMS_REQUEST';
const GET_PROGRAMS_SUCCESS = 'GET_PROGRAMS_SUCCESS';
const GET_PROGRAMS_FAILURE = 'GET_PROGRAMS_FAILURE';
const GET_PROGRAM_REQUEST = 'GET_PROGRAM_REQUEST';
const GET_PROGRAM_SUCCESS = 'GET_PROGRAM_SUCCESS';
const GET_PROGRAM_FAILURE = 'GET_PROGRAM_FAILURE';
const CREATE_PROGRAM_REQUEST = 'CREATE_PROGRAM_REQUEST';
const CREATE_PROGRAM_SUCCESS = 'CREATE_PROGRAM_SUCCESS';
const CREATE_PROGRAM_FAILURE = 'CREATE_PROGRAM_FAILURE';
const GET_PRODUCTS_REQUEST = 'GET_PRODUCTS_REQUEST';
const GET_PRODUCTS_SUCCESS = 'GET_PRODUCTS_SUCCESS';
const GET_PRODUCTS_FAILURE = 'GET_PRODUCTS_FAILURE';
const SET_ACTIVE_PRODUCT = 'SET_ACTIVE_PRODUCT';
const SET_ACTIVE_PROGRAM = 'SET_ACTIVE_PROGRAM';

type GetProgramsActionType = {
  type: typeof GET_PROGRAMS_SUCCESS | typeof GET_PROGRAMS_FAILURE;
  payload?: any;
};

export type GetProgramsRequest = {
  type: typeof GET_PROGRAMS_REQUEST;
};

type GetProgramActionType = {
  type: typeof GET_PROGRAM_SUCCESS | typeof GET_PROGRAM_FAILURE;
  payload?: any;
};

export type GetProgramRequest = {
  type: typeof GET_PROGRAM_REQUEST;
  payload: any;
};

type CreateProgramActionType = {
  type: typeof CREATE_PROGRAM_SUCCESS | typeof CREATE_PROGRAM_FAILURE;
  payload?: any;
};

export type CreateProgramRequest = {
  type: typeof CREATE_PROGRAM_REQUEST;
  payload: any;
};

type GetProductsActionType = {
  type: typeof GET_PRODUCTS_SUCCESS | typeof GET_PRODUCTS_FAILURE;
  payload?: any;
};

export type GetProductsRequest = {
  type: typeof GET_PRODUCTS_REQUEST;
};

type SetProductType = {
  type: typeof SET_ACTIVE_PRODUCT;
  payload: any;
};

type SetProgramType = {
  type: typeof SET_ACTIVE_PROGRAM;
  payload: any;
};

export type GetProgramsTypes = GetProgramsActionType | GetProgramsRequest;
export type GetProgramTypes = GetProgramActionType | GetProgramRequest;
export type CreateProgramTypes = CreateProgramActionType | CreateProgramRequest;
export type GetProductsTypes = GetProductsActionType | GetProductsRequest;

export type ProgramsActionTypes =
  | GetProgramsTypes
  | GetProgramTypes
  | CreateProgramTypes
  | GetProductsTypes
  | SetProductType
  | SetProgramType;

export default {
  GET_PROGRAMS_REQUEST,
  GET_PROGRAMS_SUCCESS,
  GET_PROGRAMS_FAILURE,
  GET_PROGRAM_REQUEST,
  GET_PROGRAM_SUCCESS,
  GET_PROGRAM_FAILURE,
  CREATE_PROGRAM_REQUEST,
  CREATE_PROGRAM_SUCCESS,
  CREATE_PROGRAM_FAILURE,
  GET_PRODUCTS_REQUEST,
  GET_PRODUCTS_SUCCESS,
  GET_PRODUCTS_FAILURE,
  SET_ACTIVE_PRODUCT,
  SET_ACTIVE_PROGRAM,
} as const;
