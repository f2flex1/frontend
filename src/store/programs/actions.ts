import { Product, Program } from '@/api';

import types from '../actionTypes';

const getPrograms = () => ({
  type: types.GET_PROGRAMS_REQUEST,
});

const getProgram = (payload: { id: number }) => ({
  type: types.GET_PROGRAM_REQUEST,
  payload,
});

const createProgram = (payload: { amount_usd: number }) => ({
  type: types.CREATE_PROGRAM_REQUEST,
  payload,
});

const getProducts = () => ({
  type: types.GET_PRODUCTS_REQUEST,
});

const setProduct = (payload: Product) => (dispatch: any) => {
  dispatch({
    type: types.SET_ACTIVE_PRODUCT,
    payload,
  });
  return Promise.resolve();
};

const setProgram = (payload: Program) => (dispatch: any) => {
  dispatch({
    type: types.SET_ACTIVE_PROGRAM,
    payload,
  });
  return Promise.resolve();
};

export { getPrograms, getProgram, createProgram, getProducts, setProduct, setProgram };
