import { all, call, put, takeLatest } from 'redux-saga/effects';

import { Product, Program } from '@/api';
import API from '@/api/executor';
import { ALERT_TYPES } from '@/const/app.constants';
import types from '@/store/actionTypes';
import { setAlert, setLoading } from '@/store/loadingsErrors/actions';
import { CreateProgramRequest, GetProgramRequest } from '@/store/programs/actionTypes';

// calls
const fetchProgramsCall = (payload?: any) => API.call('fetchPrograms', payload);
const fetchProgramCall = (payload?: any) => API.call('fetchProgram', payload);
const createProgramCall = (payload?: any) => API.call('createProgram', payload);
const fetchProductsCall = (payload?: any) => API.call('fetchProducts', payload);

function* fetchPrograms() {
  yield put(setLoading(types.GET_PROGRAMS_REQUEST, true));
  try {
    const res: Program[] = yield call(() => fetchProgramsCall());
    yield all([
      put({ type: types.GET_PROGRAMS_SUCCESS, payload: res }),
      put(setLoading(types.GET_PROGRAMS_REQUEST, false)),
    ]);
  } catch (e) {
    const error = e?.errObj?.error?.length ? e?.errObj?.error[0] : null;
    yield all([
      put(setLoading(types.GET_PROGRAMS_REQUEST, false)),
      put(setAlert(error ? `error.${error}` : 'error.network.404', ALERT_TYPES.DANGER)),
      put({ type: types.GET_PROGRAMS_FAILURE }),
    ]);
  }
}

function* fetchProgram({ payload }: GetProgramRequest) {
  yield put(setLoading(types.GET_PROGRAM_REQUEST, true));
  try {
    const res: Program = yield call(() => fetchProgramCall(payload));
    yield all([
      put({ type: types.GET_PROGRAM_SUCCESS, payload: res }),
      put(setLoading(types.GET_PROGRAM_REQUEST, false)),
    ]);
  } catch (e) {
    const error = e?.errObj?.error?.length ? e?.errObj?.error[0] : null;
    yield all([
      put(setLoading(types.GET_PROGRAM_REQUEST, false)),
      put(setAlert(error ? `error.${error}` : 'error.network.404', ALERT_TYPES.DANGER)),
      put({ type: types.GET_PROGRAM_FAILURE }),
    ]);
  }
}

function* createProgram({ payload }: CreateProgramRequest) {
  yield put(setLoading(types.CREATE_PROGRAM_REQUEST, true));
  try {
    const res: Program = yield call(() => createProgramCall(payload));
    yield all([
      put({ type: types.CREATE_PROGRAM_SUCCESS, payload: res }),
      put(setLoading(types.CREATE_PROGRAM_REQUEST, false)),
    ]);
  } catch (e) {
    const error = e?.errObj?.error?.length ? e?.errObj?.error[0] : null;
    yield all([
      put(setLoading(types.CREATE_PROGRAM_REQUEST, false)),
      put(setAlert(error ? `error.${error}` : 'error.network.404', ALERT_TYPES.DANGER)),
      put({ type: types.CREATE_PROGRAM_FAILURE }),
    ]);
  }
}

function* fetchProducts() {
  yield put(setLoading(types.GET_PRODUCTS_REQUEST, true));
  try {
    const res: Product[] = yield call(() => fetchProductsCall());
    yield all([
      put({ type: types.GET_PRODUCTS_SUCCESS, payload: res }),
      put(setLoading(types.GET_PRODUCTS_REQUEST, false)),
    ]);
  } catch (e) {
    const error = e?.errObj?.error?.length ? e?.errObj?.error[0] : null;
    yield all([
      put(setLoading(types.GET_PRODUCTS_REQUEST, false)),
      put(setAlert(error ? `error.${error}` : 'error.network.404', ALERT_TYPES.DANGER)),
      put({ type: types.GET_PRODUCTS_FAILURE }),
    ]);
  }
}

export default function* programsSagas() {
  yield takeLatest(types.GET_PROGRAMS_REQUEST, fetchPrograms);
  yield takeLatest(types.GET_PROGRAM_REQUEST, fetchProgram);
  yield takeLatest(types.CREATE_PROGRAM_REQUEST, createProgram);
  yield takeLatest(types.GET_PRODUCTS_REQUEST, fetchProducts);
}
