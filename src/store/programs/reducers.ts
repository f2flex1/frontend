import cloneDeep from 'lodash/cloneDeep';

import { Program, Product } from '@/api';
import types, { AppActionTypes } from '@/store/actionTypes';

export type ProgramsReducerState = {
  programs: {
    list: Program[];
    page: number;
    loaded: boolean;
  };
  products: {
    list: Product[];
    page: number;
    loaded: boolean;
  };
  activeProduct: Product | null;
  activeProgram: Program[];
  buyProductError: string;
};

const INITIAL_STATE: ProgramsReducerState = {
  programs: {
    list: [],
    page: 1,
    loaded: false,
  },
  products: {
    list: [],
    page: 1,
    loaded: false,
  },
  activeProduct: null,
  activeProgram: [],
  buyProductError: '',
};

const programsReducers = (state = INITIAL_STATE, action: AppActionTypes): ProgramsReducerState => {
  switch (action.type) {
    case types.GET_PROGRAMS_REQUEST:
      const newProgramsRequest = cloneDeep(state.programs);
      newProgramsRequest.loaded = true;
      return {
        ...state,
        programs: newProgramsRequest,
      };
    case types.GET_PROGRAMS_SUCCESS:
      const newProgramsSuccess = cloneDeep(state.programs);
      newProgramsSuccess.list = action.payload || newProgramsSuccess.list;
      return {
        ...state,
        programs: newProgramsSuccess,
      };

    case types.GET_PROGRAMS_FAILURE:
      return {
        ...state,
      };
    case types.GET_PROGRAM_REQUEST:
      return {
        ...state,
      };
    case types.GET_PROGRAM_SUCCESS:
      return {
        ...state,
      };
    case types.GET_PROGRAM_FAILURE:
      return {
        ...state,
      };
    case types.CREATE_PROGRAM_REQUEST:
      return {
        ...state,
      };
    case types.CREATE_PROGRAM_SUCCESS:
      const newProgramSuccess = cloneDeep(state.programs);
      newProgramSuccess.list = [...newProgramSuccess.list, action.payload];
      return {
        ...state,
        programs: newProgramSuccess,
        buyProductError: 'SUCCESS',
      };
    case types.CREATE_PROGRAM_FAILURE:
      return {
        ...state,
      };
    case types.GET_PRODUCTS_REQUEST:
      const newProductsRequest = cloneDeep(state.products);
      newProductsRequest.loaded = true;
      return {
        ...state,
        products: newProductsRequest,
      };
    case types.GET_PRODUCTS_SUCCESS:
      const newProductsSuccess = cloneDeep(state.products);
      newProductsSuccess.list = action.payload || newProductsSuccess.list;
      return {
        ...state,
        products: newProductsSuccess,
      };
    case types.GET_PRODUCTS_FAILURE:
      return {
        ...state,
      };
    case types.SET_ACTIVE_PRODUCT:
      return {
        ...state,
        activeProduct: action.payload,
        buyProductError: '',
      };
    case types.SET_ACTIVE_PROGRAM:
      const oldPrograms: Program[] = cloneDeep(state.activeProgram);
      const newActivePrograms: Program[] = [...oldPrograms, action.payload];

      return {
        ...state,
        activeProgram: newActivePrograms,
        buyProductError: '',
      };
    case types.LOGOUT_SUCCESS:
      return cloneDeep(INITIAL_STATE);
    default:
      return state;
  }
};

export default programsReducers;
