import isString from 'lodash/isString';
import { all, call, put, SagaReturnType, takeLatest } from 'redux-saga/effects';

import { AccessTokens, LogInParams, UserData } from '@/api';
import API from '@/api/executor';
import {
  ALERT_TYPES,
  LOGIN_INPUT_LIST,
  PRIVATE_SIGNUP_LIST,
  REGISTRATION_INPUT_LIST,
  VERIFICATION_INPUT_LIST,
  CHANGE_PASSWORD_INPUT_LIST,
  CHANGE_USER_PASSWORD_INPUT_LIST,
} from '@/const/app.constants';
import { Modals } from '@/const/modals.constants';
import { STORAGE_KEYS } from '@/const/storage_keys.constants';
import { setAlert, setLoading } from '@/store/loadingsErrors/actions';
import { openModalWindow } from '@/store/modals/actions';
import {
  LoginRequest,
  RegistrationRequest,
  PrivateSignupRequest,
  UpdateUserDataRequest,
  ForgotPasswordRequest,
  UpdateUserPasswordRequest,
} from '@/store/user/actionTypes';

import types from '../actionTypes';

// calls
const logInCall = (payload?: any) => API.call('logIn', payload);
const registrationCall = (payload?: any) => API.call('registration', payload);
const forgotPasswordCall = (payload?: any) => API.call('forgotPassword', payload);
const changePasswordCall = (payload?: any) => API.call('changePassword', payload);
const fetchUserDataCall = (payload?: any) => API.call('fetchUserData', payload);
const updateUserDataCall = (payload?: any) => API.call('updateUserData', payload);
const updateUserPasswordCall = (payload?: any) => API.call('updateUserPassword', payload);
const verificationCall = (payload?: any) => API.call('verification', payload);
const sendUserOtpCall = (payload?: any) => API.call('sendUserOtp', payload);
const privateSignupCall = (payload?: any) => API.call('privateSignup', payload);

// call types
type Login = SagaReturnType<typeof logInCall>;
type ForgotPassword = SagaReturnType<typeof forgotPasswordCall>;

function* login({ payload }: LoginRequest) {
  yield put(setLoading(types.LOGIN_REQUEST, true));
  try {
    const res: Login = yield call(() => logInCall(payload));

    yield all([
      put({ type: types.LOGIN_SUCCESS, payload: res }),
      put(setLoading(types.LOGIN_REQUEST, false)),
    ]);
  } catch (e) {
    const errors: any = {};
    if (e.errObj) {
      Object.keys(e.errObj)
        .map((key: string) => key)
        .filter((key: string) => LOGIN_INPUT_LIST.includes(key))
        .forEach((key: string) => {
          errors[key] = e.errObj[key].error ? `error.${e.errObj[key].error}` : e.errObj[key][0];
        });
    }

    let error = e?.errObj?.error?.length ? e?.errObj?.error[0] : null;

    if (!error) {
      Object.keys(errors).forEach((key: string) => {
        error = errors[key];
      });
    }

    yield all([
      put(setLoading(types.LOGIN_REQUEST, false)),
      put(setAlert(error ? `error.${error}` : 'error.network.404', ALERT_TYPES.DANGER)),
      put({
        type: types.LOGIN_FAILURE,
        payload: errors,
      }),
    ]);
  }
}

function* registration({ payload }: RegistrationRequest) {
  yield put(setLoading(types.REGISTRATION_REQUEST, true));
  try {
    yield call(() => registrationCall(payload));

    const data: LogInParams = {
      password: payload?.password || '',
    };

    if (payload?.email) {
      data.email = payload?.email;
    }

    if (payload?.phone) {
      data.phone = payload?.phone;
    }

    const resLog: Login = yield call(() => logInCall(data));

    yield all([
      put({ type: types.REGISTRATION_SUCCESS, payload: resLog }),
      put(setLoading(types.REGISTRATION_REQUEST, false)),
    ]);
  } catch (e) {
    const errors: any = {};
    if (e.errObj) {
      Object.keys(e.errObj)
        .map((key: string) => key)
        .filter((key: string) => REGISTRATION_INPUT_LIST.includes(key))
        .forEach((key: string) => {
          errors[key] = e.errObj[key].error ? `error.${e.errObj[key].error}` : e.errObj[key][0];
        });
    }

    let error = e?.errObj?.error?.length ? e?.errObj?.error[0] : null;

    if (!error) {
      Object.keys(errors).forEach((key: string) => {
        error = errors[key];
      });
    }
    yield all([
      put(setLoading(types.REGISTRATION_REQUEST, false)),
      put(setAlert(error ? `error.${error}` : 'error.network.404', ALERT_TYPES.DANGER)),
      put({
        type: types.REGISTRATION_FAILURE,
        payload: errors,
      }),
    ]);
  }
}

function* forgotPassword({ payload }: ForgotPasswordRequest) {
  yield put(setLoading(types.FORGOT_PASSWORD_REQUEST, true));
  try {
    const res: ForgotPassword = yield call(() => forgotPasswordCall(payload));
    yield all([
      put({ type: types.FORGOT_PASSWORD_SUCCESS, payload: res }),
      put(setLoading(types.FORGOT_PASSWORD_REQUEST, false)),
    ]);
  } catch (e) {
    const errors: any = {};
    if (e.errObj) {
      Object.keys(e.errObj)
        .map((key: string) => key)
        .filter((key: string) => LOGIN_INPUT_LIST.includes(key))
        .forEach((key: string) => {
          errors[key] = e.errObj[key].error ? `error.${e.errObj[key].error}` : e.errObj[key][0];
        });
    }

    let error = e?.errObj?.error?.length ? e?.errObj?.error[0] : null;

    if (!error) {
      Object.keys(errors).forEach((key: string) => {
        error = errors[key];
      });
    }
    yield all([
      put(setLoading(types.FORGOT_PASSWORD_REQUEST, false)),
      put(setAlert(error ? `error.${error}` : 'error.network.404', ALERT_TYPES.DANGER)),
      put({
        type: types.FORGOT_PASSWORD_FAILURE,
      }),
    ]);
  }
}

function* changePassword({ payload }: ForgotPasswordRequest) {
  yield put(setLoading(types.CHANGE_PASSWORD_REQUEST, true));
  try {
    // eslint-disable-next-line no-console
    const hasPassword = payload?.password;
    yield call(() => changePasswordCall(payload));
    yield all([
      put({
        type: types.CHANGE_PASSWORD_SUCCESS,
        payload: { code: hasPassword ? 'SUCCESS' : 'CODE_SUCCESS' },
      }),
      put(setLoading(types.CHANGE_PASSWORD_REQUEST, false)),
    ]);
  } catch (e) {
    const errors: any = {};
    if (e.errObj) {
      Object.keys(e.errObj)
        .map((key: string) => key)
        .filter((key: string) => CHANGE_PASSWORD_INPUT_LIST.includes(key))
        .forEach((key: string) => {
          errors[key] = e.errObj[key].error ? `error.${e.errObj[key].error}` : e.errObj[key][0];
        });
    }

    let error = e?.errObj?.error?.length ? e?.errObj?.error[0] : null;

    if (!error) {
      Object.keys(errors).forEach((key: string) => {
        error = errors[key];
      });
    }
    yield all([
      put(setLoading(types.CHANGE_PASSWORD_REQUEST, false)),
      put(setAlert(error ? `error.${error}` : 'error.network.404', ALERT_TYPES.DANGER)),
      put({
        type: types.CHANGE_PASSWORD_FAILURE,
        payload: errors,
      }),
    ]);
  }
}

function* logout() {
  yield put(setLoading(types.LOGOUT_REQUEST, true));
  try {
    localStorage.removeItem(STORAGE_KEYS.AUTH);
    yield all([put({ type: types.LOGOUT_SUCCESS }), put(setLoading(types.LOGOUT_REQUEST, false))]);
  } catch (e) {
    const error = e?.errObj?.error?.length ? e?.errObj?.error[0] : null;

    yield all([
      put(setLoading(types.LOGOUT_REQUEST, false)),
      put(setAlert(error ? `error.${error}` : 'error.network.404', ALERT_TYPES.DANGER)),
      put({
        type: types.LOGOUT_FAILURE,
      }),
    ]);
  }
}

function* fetchUserData() {
  yield put(setLoading(types.GET_USER_DATA_REQUEST, true));
  try {
    const res: UserData = yield call(() => fetchUserDataCall());
    yield all([
      put({ type: types.GET_USER_DATA_SUCCESS, payload: res }),
      put(setLoading(types.GET_USER_DATA_REQUEST, false)),
    ]);
  } catch (e) {
    const error = e?.errObj?.error?.length ? e?.errObj?.error[0] : null;

    yield all([
      put(setLoading(types.GET_USER_DATA_REQUEST, false)),
      put(setAlert(error ? `error.${error}` : 'error.network.404', ALERT_TYPES.DANGER)),
      put({ type: types.GET_USER_DATA_FAILURE }),
    ]);
  }
}

function* updateUserData({ payload }: UpdateUserDataRequest) {
  yield put(setLoading(types.UPDATE_USER_DATA_REQUEST, true));

  try {
    const res: UserData = yield call(() => updateUserDataCall(payload));
    yield all([
      put({ type: types.UPDATE_USER_DATA_SUCCESS, payload: res }),
      put(setLoading(types.UPDATE_USER_DATA_REQUEST, false)),
      put(setAlert('common.data_saved_success', ALERT_TYPES.SUCCESS)),
    ]);
  } catch (e) {
    const errors: any = {};
    if (e.errObj) {
      Object.keys(e.errObj)
        .map((key: string) => key)
        .filter((key: string) => LOGIN_INPUT_LIST.includes(key))
        .forEach((key: string) => {
          errors[key] = e.errObj[key].error ? `error.${e.errObj[key].error}` : e.errObj[key][0];
        });
    }

    let error = e?.errObj?.error?.length ? e?.errObj?.error[0] : null;

    if (!error) {
      Object.keys(errors).forEach((key: string) => {
        error = errors[key];
      });
    }

    yield all([
      put(setLoading(types.UPDATE_USER_DATA_REQUEST, false)),
      put(setAlert(error ? `error.${error}` : 'error.network.404', ALERT_TYPES.DANGER)),
      put({
        type: types.UPDATE_USER_DATA_FAILURE,
        payload: errors,
      }),
    ]);
  }
}

function* updateUserPassword({ payload }: UpdateUserPasswordRequest) {
  yield all([put(setLoading(types.UPDATE_USER_PASSWORD_REQUEST, true))]);
  try {
    const res: UserData = yield call(() => updateUserPasswordCall(payload));
    yield all([
      put({ type: types.UPDATE_USER_PASSWORD_SUCCESS, payload: res }),
      put(setLoading(types.UPDATE_USER_PASSWORD_REQUEST, false)),
      put(setAlert('common.data_saved_success', ALERT_TYPES.SUCCESS)),
    ]);
  } catch (e) {
    const errors: any = {};
    if (e.errObj) {
      Object.keys(e.errObj)
        .map((key: string) => key)
        .filter((key: string) => CHANGE_USER_PASSWORD_INPUT_LIST.includes(key))
        .forEach((key: string) => {
          errors[key] = e.errObj[key].error ? `error.${e.errObj[key].error}` : e.errObj[key][0];
        });
    }

    let error = isString(e?.errObj?.error) ? e?.errObj?.error : e?.errObj?.error[0];

    if (!error) {
      Object.keys(errors).forEach((key: string) => {
        error = errors[key];
      });
    }

    yield all([
      put(setLoading(types.UPDATE_USER_PASSWORD_REQUEST, false)),
      put(setAlert(error ? `error.${error}` : 'error.network.404', ALERT_TYPES.DANGER)),
      put({
        type: types.UPDATE_USER_PASSWORD_FAILURE,
        payload: errors,
      }),
    ]);
  }
}

function* verification({ payload }: any) {
  yield put(setLoading(types.VERIFICATION_REQUEST, true));
  try {
    const res: AccessTokens = yield call(() => verificationCall(payload));
    localStorage.setItem(STORAGE_KEYS.AUTH, JSON.stringify(res));

    yield all([
      put({ type: types.VERIFICATION_SUCCESS, payload: res }),
      put(setLoading(types.VERIFICATION_REQUEST, false)),
      put(openModalWindow({ modalType: Modals.I_UNDERSTAND })),
    ]);
  } catch (e) {
    const errors: any = {};
    if (e.errObj) {
      Object.keys(e.errObj)
        .map((key: string) => key)
        .filter((key: string) => VERIFICATION_INPUT_LIST.includes(key))
        .forEach((key: string) => {
          errors[key] = e.errObj[key].error ? `error.${e.errObj[key].error}` : e.errObj[key][0];
        });
    }

    let error = e?.errObj?.error?.length ? e?.errObj?.error[0] : null;

    if (!error) {
      Object.keys(errors).forEach((key: string) => {
        error = errors[key];
      });
    }
    yield all([
      put(setLoading(types.VERIFICATION_REQUEST, false)),
      put(setAlert(error ? `error.${error}` : 'error.network.404', ALERT_TYPES.DANGER)),
      put({
        type: types.VERIFICATION_FAILURE,
        payload: errors,
      }),
    ]);
  }
}

function* sendUserOtp() {
  yield put(setLoading(types.SEND_USER_OTP_REQUEST, true));
  try {
    yield call(() => sendUserOtpCall());
    yield all([
      put({ type: types.SEND_USER_OTP_SUCCESS }),
      put(setLoading(types.SEND_USER_OTP_REQUEST, false)),
    ]);
  } catch (e) {
    const error = e?.errObj?.error?.length ? e?.errObj?.error[0] : null;
    yield all([
      put(setLoading(types.SEND_USER_OTP_REQUEST, false)),
      put(setAlert(error ? `error.${error}` : 'error.network.404', ALERT_TYPES.DANGER)),
      put({ type: types.SEND_USER_OTP_FAILURE }),
    ]);
  }
}

function* privateSignup({ payload }: PrivateSignupRequest) {
  yield put(setLoading(types.PRIVATE_SIGNUP_REQUEST, true));
  try {
    const res: LogInParams = yield call(() => privateSignupCall(payload));

    const data: LogInParams = {
      password: res?.password || '',
    };

    if (res?.phone) {
      data.phone = res?.phone;
    }

    if (!res?.phone && res?.email) {
      data.email = res?.email;
    }

    yield call(() => logInCall(data));

    yield all([
      put({ type: types.PRIVATE_SIGNUP_SUCCESS, payload: data }),
      put(setLoading(types.PRIVATE_SIGNUP_REQUEST, false)),
    ]);
  } catch (e) {
    const errors: any = {};
    if (e.errObj) {
      Object.keys(e.errObj)
        .map((key: string) => key)
        .filter((key: string) => PRIVATE_SIGNUP_LIST.includes(key))
        .forEach((key: string) => {
          errors[key] = e.errObj[key].error ? `error.${e.errObj[key].error}` : e.errObj[key][0];
        });
    }

    let error = e?.errObj?.error?.length ? e?.errObj?.error[0] : null;

    if (!error) {
      Object.keys(errors).forEach((key: string) => {
        error = errors[key];
      });
    }
    yield all([
      put(setLoading(types.PRIVATE_SIGNUP_REQUEST, false)),
      put(setAlert(error ? `error.${error}` : 'error.network.404', ALERT_TYPES.DANGER)),
      put({ type: types.PRIVATE_SIGNUP_FAILURE }),
    ]);
  }
}

export default function* usersSagas() {
  yield takeLatest(types.LOGIN_REQUEST, login);
  yield takeLatest(types.REGISTRATION_REQUEST, registration);
  yield takeLatest(types.FORGOT_PASSWORD_REQUEST, forgotPassword);
  yield takeLatest(types.CHANGE_PASSWORD_REQUEST, changePassword);
  yield takeLatest(types.LOGOUT_REQUEST, logout);
  yield takeLatest(types.GET_USER_DATA_REQUEST, fetchUserData);
  yield takeLatest(types.UPDATE_USER_DATA_REQUEST, updateUserData);
  yield takeLatest(types.UPDATE_USER_PASSWORD_REQUEST, updateUserPassword);
  yield takeLatest(types.VERIFICATION_REQUEST, verification);
  yield takeLatest(types.SEND_USER_OTP_REQUEST, sendUserOtp);
  yield takeLatest(types.PRIVATE_SIGNUP_REQUEST, privateSignup);
}
