import { LevelInfoData } from '@/api';
import { AppStateType } from '@/store';

export const selectUserName = (state: AppStateType, double?: boolean): string => {
  return state.user.userData
    ? `${state.user.userData.first_name}${double ? ` ${state.user.userData.last_name}` : ''}`
    : '';
};

export const selectUserLastName = (state: AppStateType): string =>
  state.user.userData?.last_name || '';

export const selectUserLevelInfo = (state: AppStateType): LevelInfoData | null =>
  state.user.userData?.level_info || null;
