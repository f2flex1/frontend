import {
  LogInParams,
  RegistrationParams,
  ForgotPasswordParams,
  UserData,
  VerificationParams,
  UserNewPassword,
  PrivateSignupParams,
} from '@/api';

const LOGIN_REQUEST = 'LOGIN_REQUEST';
const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
const LOGIN_FAILURE = 'LOGIN_FAILURE';
const REGISTRATION_REQUEST = 'REGISTRATION_REQUEST';
const REGISTRATION_SUCCESS = 'REGISTRATION_SUCCESS';
const REGISTRATION_FAILURE = 'REGISTRATION_FAILURE';
const FORGOT_PASSWORD_REQUEST = 'FORGOT_PASSWORD_REQUEST';
const FORGOT_PASSWORD_SUCCESS = 'FORGOT_PASSWORD_SUCCESS';
const FORGOT_PASSWORD_FAILURE = 'FORGOT_PASSWORD_FAILURE';
const CHANGE_PASSWORD_REQUEST = 'CHANGE_PASSWORD_REQUEST';
const CHANGE_PASSWORD_SUCCESS = 'CHANGE_PASSWORD_SUCCESS';
const CHANGE_PASSWORD_FAILURE = 'CHANGE_PASSWORD_FAILURE';
const LOGOUT_REQUEST = 'LOGOUT_REQUEST';
const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
const LOGOUT_FAILURE = 'LOGOUT_FAILURE';
const GET_USER_DATA_REQUEST = 'GET_USER_DATA_REQUEST';
const GET_USER_DATA_SUCCESS = 'GET_USER_DATA_SUCCESS';
const GET_USER_DATA_FAILURE = 'GET_USER_DATA_FAILURE';
const UPDATE_USER_DATA_REQUEST = 'UPDATE_USER_DATA_REQUEST';
const UPDATE_USER_DATA_SUCCESS = 'UPDATE_USER_DATA_SUCCESS';
const UPDATE_USER_DATA_FAILURE = 'UPDATE_USER_DATA_FAILURE';
const UPDATE_USER_PASSWORD_REQUEST = 'UPDATE_USER_PASSWORD_REQUEST';
const UPDATE_USER_PASSWORD_SUCCESS = 'UPDATE_USER_PASSWORD_SUCCESS';
const UPDATE_USER_PASSWORD_FAILURE = 'UPDATE_USER_PASSWORD_FAILURE';
const VERIFICATION_REQUEST = 'VERIFICATION_REQUEST';
const VERIFICATION_SUCCESS = 'VERIFICATION_SUCCESS';
const VERIFICATION_FAILURE = 'VERIFICATION_FAILURE';
const SEND_USER_OTP_REQUEST = 'SEND_USER_OTP_REQUEST';
const SEND_USER_OTP_SUCCESS = 'SEND_USER_OTP_SUCCESS';
const SEND_USER_OTP_FAILURE = 'SEND_USER_OTP_FAILURE';
const PRIVATE_SIGNUP_REQUEST = 'PRIVATE_SIGNUP_REQUEST';
const PRIVATE_SIGNUP_SUCCESS = 'PRIVATE_SIGNUP_SUCCESS';
const PRIVATE_SIGNUP_FAILURE = 'PRIVATE_SIGNUP_FAILURE';

type LoginActionType = {
  type: typeof LOGIN_SUCCESS | typeof LOGIN_FAILURE;
  payload?: any;
};

export type LoginRequest = {
  type: typeof LOGIN_REQUEST;
  payload?: LogInParams;
};

type RegistrationActionType = {
  type: typeof REGISTRATION_SUCCESS | typeof REGISTRATION_FAILURE;
  payload?: any;
};

export type RegistrationRequest = {
  type: typeof REGISTRATION_REQUEST;
  payload?: RegistrationParams;
};

type ForgotPasswordActionType = {
  type: typeof FORGOT_PASSWORD_SUCCESS | typeof FORGOT_PASSWORD_FAILURE;
  payload?: ForgotPasswordParams;
};

export type ForgotPasswordRequest = {
  type: typeof FORGOT_PASSWORD_REQUEST;
  payload?: ForgotPasswordParams;
};

type ChangePasswordActionType = {
  type: typeof CHANGE_PASSWORD_SUCCESS | typeof CHANGE_PASSWORD_FAILURE;
  payload?: any;
};

export type ChangePasswordRequest = {
  type: typeof CHANGE_PASSWORD_REQUEST;
  payload?: ForgotPasswordParams;
};

type LogoutActionType = {
  type: typeof LOGOUT_SUCCESS | typeof LOGOUT_FAILURE;
};

export type LogoutRequest = {
  type: typeof LOGOUT_REQUEST;
};

type GetUserDataActionType = {
  type: typeof GET_USER_DATA_SUCCESS | typeof GET_USER_DATA_FAILURE;
  payload?: UserData;
};

export type GetUserDataRequest = {
  type: typeof GET_USER_DATA_REQUEST;
};

type UpdateUserDataActionType = {
  type: typeof UPDATE_USER_DATA_SUCCESS | typeof UPDATE_USER_DATA_FAILURE;
  payload?: UserData;
};

export type UpdateUserDataRequest = {
  type: typeof UPDATE_USER_DATA_REQUEST;
  payload: UserData;
};

type UpdateUserPasswordActionType = {
  type: typeof UPDATE_USER_PASSWORD_SUCCESS | typeof UPDATE_USER_PASSWORD_FAILURE;
  payload?: any;
};

export type UpdateUserPasswordRequest = {
  type: typeof UPDATE_USER_PASSWORD_REQUEST;
  payload: UserNewPassword;
};

type VerificationActionType = {
  type: typeof VERIFICATION_SUCCESS | typeof VERIFICATION_FAILURE;
  payload?: any;
};

export type VerificationRequest = {
  type: typeof VERIFICATION_REQUEST;
  payload?: VerificationParams;
};

type SendUserOtpActionType = {
  type: typeof SEND_USER_OTP_SUCCESS | typeof SEND_USER_OTP_FAILURE;
  payload?: any;
};

export type SendUserOtpRequest = {
  type: typeof SEND_USER_OTP_REQUEST;
};

type PrivateSignupActionType = {
  type: typeof PRIVATE_SIGNUP_SUCCESS | typeof PRIVATE_SIGNUP_FAILURE;
  payload?: any;
};

export type PrivateSignupRequest = {
  type: typeof PRIVATE_SIGNUP_REQUEST;
  payload?: PrivateSignupParams;
};
// refactored for shorter version
export type LoginActionTypes = LoginActionType | LoginRequest;
export type RegistrationActionTypes = RegistrationActionType | RegistrationRequest;
export type ForgotPasswordActionTypes = ForgotPasswordActionType | ForgotPasswordRequest;
export type ChangePasswordActionTypes = ChangePasswordActionType | ChangePasswordRequest;
export type LogoutActionTypes = LogoutActionType | LogoutRequest;
export type GetUserDataActionTypes = GetUserDataActionType | GetUserDataRequest;
export type UpdateUserDataActionTypes = UpdateUserDataActionType | UpdateUserDataRequest;
export type UpdateUserPasswordActionTypes =
  | UpdateUserPasswordActionType
  | UpdateUserPasswordRequest;
export type VerificationActionTypes = VerificationActionType | VerificationRequest;
export type SendUserOtpActionTypes = SendUserOtpActionType | SendUserOtpRequest;
export type PrivateSignupActionTypes = PrivateSignupActionType | PrivateSignupRequest;

export type UserActionTypes =
  | LoginActionTypes
  | RegistrationActionTypes
  | ForgotPasswordActionTypes
  | ChangePasswordActionTypes
  | LogoutActionTypes
  | GetUserDataActionTypes
  | UpdateUserDataActionTypes
  | UpdateUserPasswordActionTypes
  | VerificationActionTypes
  | SendUserOtpActionTypes
  | PrivateSignupActionTypes;

export default {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  REGISTRATION_REQUEST,
  REGISTRATION_SUCCESS,
  REGISTRATION_FAILURE,
  FORGOT_PASSWORD_REQUEST,
  FORGOT_PASSWORD_SUCCESS,
  FORGOT_PASSWORD_FAILURE,
  CHANGE_PASSWORD_REQUEST,
  CHANGE_PASSWORD_SUCCESS,
  CHANGE_PASSWORD_FAILURE,
  LOGOUT_REQUEST,
  LOGOUT_SUCCESS,
  LOGOUT_FAILURE,
  GET_USER_DATA_REQUEST,
  GET_USER_DATA_SUCCESS,
  GET_USER_DATA_FAILURE,
  UPDATE_USER_DATA_REQUEST,
  UPDATE_USER_DATA_SUCCESS,
  UPDATE_USER_DATA_FAILURE,
  UPDATE_USER_PASSWORD_REQUEST,
  UPDATE_USER_PASSWORD_SUCCESS,
  UPDATE_USER_PASSWORD_FAILURE,
  VERIFICATION_REQUEST,
  VERIFICATION_SUCCESS,
  VERIFICATION_FAILURE,
  SEND_USER_OTP_REQUEST,
  SEND_USER_OTP_SUCCESS,
  SEND_USER_OTP_FAILURE,
  PRIVATE_SIGNUP_REQUEST,
  PRIVATE_SIGNUP_SUCCESS,
  PRIVATE_SIGNUP_FAILURE,
} as const;
