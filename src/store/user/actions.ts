import { UserNewPassword, PrivateSignupParams } from '@/api';

import types from '../actionTypes';

const login = (payload: any) => ({
  type: types.LOGIN_REQUEST,
  payload,
});

const registration = (payload: any) => ({
  type: types.REGISTRATION_REQUEST,
  payload,
});

const forgotPassword = (payload: any) => ({
  type: types.FORGOT_PASSWORD_REQUEST,
  payload,
});

const changePassword = (payload: any) => ({
  type: types.CHANGE_PASSWORD_REQUEST,
  payload,
});

const logout = () => ({
  type: types.LOGOUT_REQUEST,
});

const fetchUserData = () => ({
  type: types.GET_USER_DATA_REQUEST,
});

const updateUserData = (payload: any) => ({
  type: types.UPDATE_USER_DATA_REQUEST,
  payload,
});

const updateUserPassword = (payload: UserNewPassword) => ({
  type: types.UPDATE_USER_PASSWORD_REQUEST,
  payload,
});

const verification = (payload: any) => ({
  type: types.VERIFICATION_REQUEST,
  payload,
});

const sendUserOtp = () => ({
  type: types.SEND_USER_OTP_REQUEST,
});

const privateSignup = (payload: PrivateSignupParams) => ({
  type: types.PRIVATE_SIGNUP_REQUEST,
  payload,
});

export {
  login,
  registration,
  forgotPassword,
  changePassword,
  logout,
  fetchUserData,
  updateUserData,
  updateUserPassword,
  verification,
  sendUserOtp,
  privateSignup,
};
