import cloneDeep from 'lodash/cloneDeep';

import { ForgotPasswordParams, LogInParams, UserData, VerificationParams } from '@/api';
import { STORAGE_KEYS } from '@/const/storage_keys.constants';

import types, { AppActionTypes } from '../actionTypes';

export type UserReducerState = {
  loggedIn: boolean;
  userData: UserData | null;
  registrationErrors: UserData;
  loginErrors: LogInParams | {};
  verification: VerificationParams;
  verificationErrors: VerificationParams;
  forgotPasswordErrors: ForgotPasswordParams;
  auth: {
    accessToken: string;
    refreshToken: string;
  };
  loginData: any;
  notifications: Notification[] | [];
  userErrors: UserData | {};
  otpSended: boolean;
  privateSignup: boolean;
  privateSignupError: boolean;
};

const auth = JSON.parse(localStorage.getItem(STORAGE_KEYS.AUTH) || '{}');

const INITIAL_STATE: UserReducerState = {
  loggedIn: !!auth.access_token,
  userData: null,
  loginData: null,
  auth: {
    accessToken: auth.access_token,
    refreshToken: auth.refresh_token,
  },
  notifications: [],
  registrationErrors: {},
  forgotPasswordErrors: {},
  loginErrors: {},
  verification: {
    code: '',
  },
  verificationErrors: {
    code: '',
  },
  userErrors: {},
  otpSended: false,
  privateSignup: false,
  privateSignupError: false,
};

const usersReducers = (state = INITIAL_STATE, action: AppActionTypes): UserReducerState => {
  switch (action.type) {
    case types.LOGIN_REQUEST:
      return {
        ...state,
        loginData: action.payload || null,
      };
    case types.LOGIN_SUCCESS:
      return {
        ...state,
        verificationErrors: {
          code: action.payload?.data?.message,
        },
      };

    case types.LOGIN_FAILURE:
      return {
        ...state,
        loggedIn: false,
        loginErrors: action.payload || {},
      };
    case types.REGISTRATION_REQUEST:
      return {
        ...state,
        loginData: {
          email: action.payload?.email,
          phone: action.payload?.phone,
          password: action.payload?.password || '',
        },
      };
    case types.REGISTRATION_SUCCESS:
      return {
        ...state,
        verificationErrors: {
          code: action.payload?.data?.message,
        },
      };
    case types.REGISTRATION_FAILURE:
      return {
        ...state,
        registrationErrors: action.payload || {},
      };
    case types.FORGOT_PASSWORD_REQUEST:
      return {
        ...state,
        loginData: action.payload || null,
      };
    case types.FORGOT_PASSWORD_SUCCESS:
      return {
        ...state,
        forgotPasswordErrors: {
          code: 'CODE_SENDED',
        },
      };
    case types.FORGOT_PASSWORD_FAILURE:
      return {
        ...state,
        loginData: null,
      };
    case types.CHANGE_PASSWORD_REQUEST:
      const newChangePasswordRequest = cloneDeep(state.loginData);
      if (action.payload?.code) {
        newChangePasswordRequest.code = action.payload?.code;
      }
      return {
        ...state,
        loginData: newChangePasswordRequest,
      };
    case types.CHANGE_PASSWORD_SUCCESS:
      return {
        ...state,
        forgotPasswordErrors: {
          code: action.payload.code,
        },
      };
    case types.CHANGE_PASSWORD_FAILURE:
      return {
        ...state,
        forgotPasswordErrors: action.payload || {},
      };
    case types.VERIFICATION_REQUEST:
      return {
        ...state,
      };
    case types.VERIFICATION_SUCCESS:
      return {
        ...state,
        loggedIn: true,
      };
    case types.VERIFICATION_FAILURE:
      return {
        ...state,
        verificationErrors: action.payload || {},
      };
    case types.SEND_USER_OTP_REQUEST:
      return {
        ...state,
        otpSended: false,
      };
    case types.SEND_USER_OTP_SUCCESS:
      return {
        ...state,
        otpSended: true,
      };
    case types.SEND_USER_OTP_FAILURE:
      return {
        ...state,
        otpSended: false,
      };
    case types.LOGOUT_REQUEST:
      return {
        ...state,
      };
    case types.LOGOUT_SUCCESS:
      const logoutData = cloneDeep(INITIAL_STATE);
      logoutData.loggedIn = false;
      logoutData.auth = {
        accessToken: '',
        refreshToken: '',
      };
      return logoutData;
    case types.LOGOUT_FAILURE:
      return {
        ...state,
      };
    case types.GET_USER_DATA_REQUEST:
      return {
        ...state,
      };
    case types.GET_USER_DATA_SUCCESS:
      return {
        ...state,
        userData: action.payload || null,
      };
    case types.GET_USER_DATA_FAILURE:
      return {
        ...state,
        userData: null,
      };
    case types.UPDATE_USER_DATA_REQUEST:
      return {
        ...state,
      };
    case types.UPDATE_USER_DATA_SUCCESS:
      return {
        ...state,
        userData: action.payload || null,
      };
    case types.UPDATE_USER_DATA_FAILURE:
      return {
        ...state,
        userErrors: action.payload || {},
      };
    case types.UPDATE_USER_PASSWORD_REQUEST:
      return {
        ...state,
        loginErrors: {},
      };
    case types.UPDATE_USER_PASSWORD_SUCCESS:
      return {
        ...state,
      };
    case types.UPDATE_USER_PASSWORD_FAILURE:
      return {
        ...state,
        loginErrors: action.payload || {},
      };
    case types.PRIVATE_SIGNUP_REQUEST:
      return {
        ...state,
      };
    case types.PRIVATE_SIGNUP_SUCCESS:
      return {
        ...state,
        privateSignup: true,
        loginData: action.payload || null,
      };
    case types.PRIVATE_SIGNUP_FAILURE:
      return {
        ...state,
        privateSignupError: true,
      };
    default:
      return state;
  }
};

export default usersReducers;
