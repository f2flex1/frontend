import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import thunk from 'redux-thunk';

import { STORAGE_KEYS } from '@/const/storage_keys.constants';

import { getMessages } from './messages/actions';
import reducers from './reducers';
import rootSaga from './sagas';
import { fetchUserData } from './user/actions';
import { getCurrenciesRates } from './wallet/actions';

const sagaMiddleware = createSagaMiddleware();

const store = createStore(reducers, applyMiddleware(sagaMiddleware, thunk));

sagaMiddleware.run(rootSaga);

const auth = JSON.parse(localStorage.getItem(STORAGE_KEYS.AUTH) || '{}');

if (!!auth.access_token) {
  store.dispatch(fetchUserData());
  store.dispatch(getMessages());
  store.dispatch(getCurrenciesRates());
}

export type AppStoreType = typeof store;
export type AppStateType = ReturnType<typeof store.getState>;

export default store;
