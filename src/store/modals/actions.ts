import appActionTypes from '@/store/actionTypes';
import { ModalsActionTypes, PayloadModalsWindow } from '@/store/modals/actionTypes';

export const openModalWindow = (payload: PayloadModalsWindow): ModalsActionTypes => ({
  type: appActionTypes.OPEN_MODAL_WINDOW,
  payload,
});

export const closeModalWindow = (payload: PayloadModalsWindow): ModalsActionTypes => ({
  type: appActionTypes.CLOSE_MODAL_WINDOW,
  payload,
});

export default {
  openModalWindow,
  closeModalWindow,
};
