import { Modals } from '@/const/modals.constants';

const OPEN_MODAL_WINDOW = 'modals/OPEN_MODAL_WINDOW';
const CLOSE_MODAL_WINDOW = 'modals/CLOSE_MODAL_WINDOW';

type ModalType = Modals.I_UNDERSTAND;

export interface PayloadModalsWindow {
  context?: null;
  modalType: ModalType;
}

type OpenModalType = {
  type: typeof OPEN_MODAL_WINDOW;
  payload: PayloadModalsWindow;
};

type CloseModalType = {
  type: typeof CLOSE_MODAL_WINDOW;
  payload: PayloadModalsWindow;
};

export type ModalsActionTypes = OpenModalType | CloseModalType;

export default {
  OPEN_MODAL_WINDOW,
  CLOSE_MODAL_WINDOW,
} as const;
