import { Modals } from '@/const/modals.constants';
import { ModalsActionTypes } from '@/store/modals/actionTypes';

import types from '../actionTypes';

export type ModalsReducerState = {
  [Modals.I_UNDERSTAND]: {
    isShow: boolean;
    context?: null;
  };
};

const INITIAL_STATE: ModalsReducerState = {
  [Modals.I_UNDERSTAND]: {
    isShow: false,
    context: null,
  },
};

const modalsReducers = (state = INITIAL_STATE, action: ModalsActionTypes): ModalsReducerState => {
  switch (action.type) {
    case types.OPEN_MODAL_WINDOW: {
      const { modalType, context } = action.payload;

      return {
        ...state,
        [modalType]: {
          isShow: true,
          context,
        },
      };
    }
    case types.CLOSE_MODAL_WINDOW: {
      const { modalType } = action.payload;

      return {
        ...state,
        [modalType]: {
          isShow: false,
          context: null,
        },
      };
    }
    default:
      return state;
  }
};

export default modalsReducers;
