import { OperationCreatedReducerState } from '@/store/operations/reducers';

import types from '../actionTypes';

const getOperations = (page?: number) => ({
  type: types.GET_OPERATIONS_REQUEST,
  page,
});

const getOperationsWithdrawals = () => ({
  type: types.GET_OPERATIONS_WITHDRAWALS_REQUEST,
});

const createOperation = (payload: OperationCreatedReducerState) => ({
  type: types.CREATE_OPERATION_REQUEST,
  payload,
});

export { getOperations, getOperationsWithdrawals, createOperation };
