import { all, call, put, takeLatest } from 'redux-saga/effects';

import { Operation } from '@/api';
import API from '@/api/executor';
import { ALERT_TYPES, FINANCES_TABS } from '@/const/app.constants';
import types from '@/store/actionTypes';
import { setAlert, setLoading } from '@/store/loadingsErrors/actions';
import { CreateOperationRequest } from '@/store/operations/actionTypes';

// calls
const fetchOperationsCall = (payload?: any) => API.call('fetchOperations', payload);
const fetchOperationsWithdrawalsCall = (payload?: any) =>
  API.call('fetchOperationsWithdrawals', payload);
const createOperationsCall = (payload?: any) => API.call('createOperation', payload);

function* fetchOperations({ page }: any) {
  yield put(setLoading(types.GET_OPERATIONS_REQUEST, true));
  try {
    const res: Operation[] = yield call(() => fetchOperationsCall(page || 1));

    yield all([
      put({ type: types.GET_OPERATIONS_SUCCESS, payload: res }),
      put(setLoading(types.GET_OPERATIONS_REQUEST, false)),
    ]);
  } catch (e) {
    const error = e?.errObj?.error?.length ? e?.errObj?.error[0] : null;
    yield all([
      put(setLoading(types.GET_OPERATIONS_REQUEST, false)),
      put(setAlert(error ? `error.${error}` : 'error.network.404', ALERT_TYPES.DANGER)),
      put({ type: types.GET_OPERATIONS_FAILURE }),
    ]);
  }
}

function* fetchWithdrawals() {
  yield put(setLoading(types.GET_OPERATIONS_WITHDRAWALS_REQUEST, true));
  try {
    const res: Operation[] = yield call(() => fetchOperationsWithdrawalsCall());

    yield all([
      put({ type: types.GET_OPERATIONS_WITHDRAWALS_SUCCESS, payload: res }),
      put(setLoading(types.GET_OPERATIONS_WITHDRAWALS_REQUEST, false)),
    ]);
  } catch (e) {
    const error = e?.errObj?.error?.length ? e?.errObj?.error[0] : null;
    yield all([
      put(setLoading(types.GET_OPERATIONS_WITHDRAWALS_REQUEST, false)),
      put(setAlert(error ? `error.${error}` : 'error.network.404', ALERT_TYPES.DANGER)),
      put({ type: types.GET_OPERATIONS_WITHDRAWALS_FAILURE }),
    ]);
  }
}

function* createOperations({ payload }: CreateOperationRequest) {
  yield put(setLoading(types.CREATE_OPERATION_REQUEST, true));
  switch (payload?.type) {
    case FINANCES_TABS.FILL:
      yield all([
        put({ type: types.CREATE_OPERATION_SUCCESS, payload }),
        put(setLoading(types.CREATE_OPERATION_REQUEST, false)),
      ]);
      break;
    case FINANCES_TABS.EXTRACT:
      yield all([
        put({ type: types.CREATE_OPERATION_SUCCESS, payload }),
        put(setLoading(types.CREATE_OPERATION_REQUEST, false)),
      ]);
      break;
    case 'EXTRACT_SUBMIT':
      try {
        const res: Operation = yield call(() =>
          createOperationsCall({
            amount_usd: payload.sum,
          })
        );
        yield all([
          put({ type: types.CREATE_OPERATION_SUCCESS, payload: res }),
          put(setLoading(types.CREATE_OPERATION_REQUEST, false)),
        ]);
      } catch (e) {
        const error = e?.errObj?.error?.length ? e?.errObj?.error[0] : null;
        yield all([
          put(setLoading(types.CREATE_OPERATION_REQUEST, false)),
          put(setAlert(error ? `error.${error}` : 'error.network.404', ALERT_TYPES.DANGER)),
          put({ type: types.CREATE_OPERATION_FAILURE }),
        ]);
      }
      break;
    default:
      break;
  }
}

export default function* operationsSagas() {
  yield takeLatest(types.GET_OPERATIONS_REQUEST, fetchOperations);
  yield takeLatest(types.GET_OPERATIONS_WITHDRAWALS_REQUEST, fetchWithdrawals);
  yield takeLatest(types.CREATE_OPERATION_REQUEST, createOperations);
}
