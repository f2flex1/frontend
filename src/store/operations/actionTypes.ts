import { OperationCreatedReducerState } from '@/store/operations/reducers';

const GET_OPERATIONS_REQUEST = 'GET_OPERATIONS_REQUEST';
const GET_OPERATIONS_SUCCESS = 'GET_OPERATIONS_SUCCESS';
const GET_OPERATIONS_FAILURE = 'GET_OPERATIONS_FAILURE';
const GET_OPERATIONS_WITHDRAWALS_REQUEST = 'GET_OPERATIONS_WITHDRAWALS_REQUEST';
const GET_OPERATIONS_WITHDRAWALS_SUCCESS = 'GET_OPERATIONS_WITHDRAWALS_SUCCESS';
const GET_OPERATIONS_WITHDRAWALS_FAILURE = 'GET_OPERATIONS_WITHDRAWALS_FAILURE';
const CREATE_OPERATION_REQUEST = 'CREATE_OPERATION_REQUEST';
const CREATE_OPERATION_SUCCESS = 'CREATE_OPERATION_SUCCESS';
const CREATE_OPERATION_FAILURE = 'CREATE_OPERATION_FAILURE';

type GetOperationsActionType = {
  type: typeof GET_OPERATIONS_SUCCESS | typeof GET_OPERATIONS_FAILURE;
  payload?: any;
};

export type GetOperationsRequest = {
  type: typeof GET_OPERATIONS_REQUEST;
  page?: number;
};

type GetOperationsWithdrawalsActionType = {
  type: typeof GET_OPERATIONS_WITHDRAWALS_SUCCESS | typeof GET_OPERATIONS_WITHDRAWALS_FAILURE;
  payload?: any;
};

export type GetOperationsWithdrawalsRequest = {
  type: typeof GET_OPERATIONS_WITHDRAWALS_REQUEST;
};

type CreateOperationActionType = {
  type: typeof CREATE_OPERATION_SUCCESS | typeof CREATE_OPERATION_FAILURE;
  payload?: any;
};

export type CreateOperationRequest = {
  type: typeof CREATE_OPERATION_REQUEST;
  payload: OperationCreatedReducerState;
};

export type GetOperationsTypes = GetOperationsActionType | GetOperationsRequest;
export type GetOperationsWithdrawalsTypes =
  | GetOperationsWithdrawalsActionType
  | GetOperationsWithdrawalsRequest;
export type CreateOperationTypes = CreateOperationActionType | CreateOperationRequest;

export type OperationsActionTypes =
  | GetOperationsTypes
  | GetOperationsWithdrawalsTypes
  | CreateOperationTypes;

export default {
  GET_OPERATIONS_REQUEST,
  GET_OPERATIONS_SUCCESS,
  GET_OPERATIONS_FAILURE,
  GET_OPERATIONS_WITHDRAWALS_REQUEST,
  GET_OPERATIONS_WITHDRAWALS_SUCCESS,
  GET_OPERATIONS_WITHDRAWALS_FAILURE,
  CREATE_OPERATION_REQUEST,
  CREATE_OPERATION_SUCCESS,
  CREATE_OPERATION_FAILURE,
} as const;
