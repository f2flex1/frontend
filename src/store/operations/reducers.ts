import cloneDeep from 'lodash/cloneDeep';
import orderBy from 'lodash/orderBy';

import { Operation } from '@/api';
import history from '@/common/utils/history';
import { selfMoment } from '@/common/utils/i18n';
import { FINANCES_TABS, LIMITS } from '@/const/app.constants';
import types, { AppActionTypes } from '@/store/actionTypes';

export type OperationCreatedReducerState = {
  type: string;
  sum: string;
  currency: string;
  id?: string;
};

export type OperationsReducerState = {
  operationsList: Operation[];
  operationsWithdrawalsList: Operation[];
  operationCreated: OperationCreatedReducerState | null;
  page: number;
  end: boolean;
  loaded: boolean;
  operationsWithdrawalsLoaded: boolean;
};

const INITIAL_STATE: OperationsReducerState = {
  operationsList: [],
  operationsWithdrawalsList: [],
  operationCreated: null,
  page: 1,
  end: false,
  loaded: false,
  operationsWithdrawalsLoaded: false,
};

const operationsReducers = (
  state = INITIAL_STATE,
  action: AppActionTypes
): OperationsReducerState => {
  switch (action.type) {
    case types.GET_OPERATIONS_REQUEST:
      return {
        ...state,
        loaded: true,
        page: action.page || 1,
      };
    case types.GET_OPERATIONS_SUCCESS:
      const oldOperations = cloneDeep(state.operationsList);
      const newOperations = action.payload ? orderBy(action.payload, 'created', 'desc') : [];

      let newOperationsList =
        state.page === 1 ? newOperations : [...oldOperations, ...newOperations];
      let end = false;

      if (newOperations.length < LIMITS.OPERATIONS) {
        end = true;
      }

      if (!!state.operationsWithdrawalsList.length && !!newOperations.length) {
        const startDate =
          state.page === 1
            ? selfMoment(state.operationsWithdrawalsList[0].created).unix()
            : selfMoment(newOperations[0].created).unix();
        const endDate = end
          ? selfMoment(
            state.operationsWithdrawalsList[state.operationsWithdrawalsList.length - 1].created
          ).unix()
          : selfMoment(newOperations[newOperations.length - 1].created).unix();

        const newWithdrawals = state.operationsWithdrawalsList.filter((withdrawal: Operation) => {
          const withdrawalsStart = selfMoment(withdrawal.created).unix() <= startDate;
          const withdrawalsEnd = end ? true : selfMoment(withdrawal.created).unix() > endDate;
          return withdrawalsStart && withdrawalsEnd;
        });

        if (newWithdrawals.length) {
          newOperationsList = orderBy([...newOperationsList, ...newWithdrawals], 'created', 'desc');
        }
      }

      if (!!state.operationsWithdrawalsList.length && !newOperations.length) {
        newOperationsList = orderBy(
          [...newOperationsList, ...state.operationsWithdrawalsList],
          'created',
          'desc'
        );
      }

      return {
        ...state,
        operationsList: newOperationsList,
        end,
      };

    case types.GET_OPERATIONS_FAILURE:
      return {
        ...state,
        operationsList: [],
      };
    case types.GET_OPERATIONS_WITHDRAWALS_REQUEST:
      return {
        ...state,
        operationsWithdrawalsLoaded: true,
      };
    case types.GET_OPERATIONS_WITHDRAWALS_SUCCESS:
      const newOperationsWithdrawals = action.payload.length
        ? orderBy(
          action.payload.filter((opr: Operation) => opr.status_name === 'PENDING'),
          'created',
          'desc'
        )
        : [];

      return {
        ...state,
        operationsWithdrawalsList: newOperationsWithdrawals,
      };

    case types.GET_OPERATIONS_WITHDRAWALS_FAILURE:
      return {
        ...state,
        operationsWithdrawalsList: [],
      };
    case types.CREATE_OPERATION_REQUEST:
      return {
        ...state,
      };
    case types.CREATE_OPERATION_SUCCESS:
      switch (action.payload?.type) {
        case FINANCES_TABS.FILL:
          history.push('/finances/deposit');
          break;
        case FINANCES_TABS.EXTRACT:
          history.push('/finances/withdraw');
          break;
        default:
          break;
      }
      return {
        ...state,
        operationCreated: action.payload || null,
      };
    case types.CREATE_OPERATION_FAILURE:
      return {
        ...state,
        operationCreated: null,
      };
    case types.LOGOUT_SUCCESS:
      const logoutData = cloneDeep(INITIAL_STATE);
      return logoutData;
    default:
      return state;
  }
};

export default operationsReducers;
