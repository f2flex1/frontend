import { ThemeType } from '@/store/app/reducers';

const LOADING_START = 'app/LOADING_START';
const LOADING_END = 'app/LOADING_END';
const SET_THEME = 'app/SET_THEME';
const SET_SIDEBAR_OPEN = 'app/SET_SIDEBAR_OPEN';

type LoadingStartType = {
  type: typeof LOADING_START;
  payload: null;
};
type LoadingEndType = {
  type: typeof LOADING_END;
  payload: null;
};
type SetThemeType = {
  type: typeof SET_THEME;
  payload: ThemeType;
};
type SetSidebarIsOpenType = {
  type: typeof SET_SIDEBAR_OPEN;
  payload: boolean;
};

export type LoadingActionTypes =
  | LoadingStartType
  | LoadingEndType
  | SetThemeType
  | SetSidebarIsOpenType;

export default {
  LOADING_START,
  LOADING_END,
  SET_THEME,
  SET_SIDEBAR_OPEN,
} as const;
