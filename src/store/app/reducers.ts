import { STORAGE_KEYS } from '@/const/storage_keys.constants';

import types, { AppActionTypes } from '../actionTypes';

export type ThemeType = 'light' | 'dark';

const storageTheme: ThemeType | string = localStorage.getItem(STORAGE_KEYS.THEME) || 'dark';

export type AppReducerState = {
  loading: boolean;
  theme: ThemeType | string;
  isSidebarOpen: boolean;
};

const INITIAL_STATE: AppReducerState = {
  loading: true,
  theme: storageTheme,
  isSidebarOpen: false,
};

const appReducers = (state = INITIAL_STATE, action: AppActionTypes): AppReducerState => {
  switch (action.type) {
    case types.SET_THEME: {
      localStorage.setItem(STORAGE_KEYS.THEME, action.payload);
      return {
        ...state,
        theme: action.payload,
      };
    }
    case types.SET_SIDEBAR_OPEN: {
      return {
        ...state,
        isSidebarOpen: action.payload,
      };
    }
    case types.LOADING_START:
      return {
        ...state,
        loading: true,
      };
    case types.LOADING_END:
      return {
        ...state,
        loading: false,
      };
    default:
      return state;
  }
};

export default appReducers;
