import { LoadingActionTypes } from '@/store/app/actionTypes';
import { ThemeType } from '@/store/app/reducers';

import types from '../actionTypes';

// Type returns type of action
const loading = (start = true): LoadingActionTypes => ({
  type: start ? types.LOADING_START : types.LOADING_END,
  payload: null,
});

const setTheme = (payload: ThemeType): LoadingActionTypes => ({
  type: types.SET_THEME,
  payload,
});

export const setSidebarOpen = (payload: boolean): LoadingActionTypes => ({
  type: types.SET_SIDEBAR_OPEN,
  payload,
});

export default {
  loading,
  setTheme,
  setSidebarOpen,
};
