import appTypes, { LoadingActionTypes } from './app/actionTypes';
import loadingErrorsTypes, { LoadingErrorsTypes } from './loadingsErrors/actionTypes';
import messagesTypes, { MessagesTypes } from './messages/actionTypes';
import modalsType, { ModalsActionTypes } from './modals/actionTypes';
import operationsTypes, { OperationsActionTypes } from './operations/actionTypes';
import packetsTypes, { PacketsActionTypes } from './packets/actionTypes';
import partnersTypes, { PartnersActionTypes } from './partners/actionTypes';
import programsTypes, { ProgramsActionTypes } from './programs/actionTypes';
import usersTypes, { UserActionTypes } from './user/actionTypes';
import walletTypes, { WalletActionTypes } from './wallet/actionTypes';

const appActionTypes = {
  ...appTypes,
  ...modalsType,
  ...usersTypes,
  ...loadingErrorsTypes,
  ...messagesTypes,
  ...operationsTypes,
  ...packetsTypes,
  ...partnersTypes,
  ...walletTypes,
  ...programsTypes,
} as const;

export type AppActionTypes =
  | LoadingActionTypes
  | UserActionTypes
  | LoadingErrorsTypes
  | MessagesTypes
  | OperationsActionTypes
  | PacketsActionTypes
  | PartnersActionTypes
  | ModalsActionTypes
  | WalletActionTypes
  | ProgramsActionTypes;

export default appActionTypes;
