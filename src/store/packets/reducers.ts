import cloneDeep from 'lodash/cloneDeep';

import { Contract, Packet } from '@/api';
import types, { AppActionTypes } from '@/store/actionTypes';

export type PacketsReducerState = {
  packets: {
    list: Packet[];
    page: number;
    loaded: boolean;
  };
  contracts: {
    list: Contract[];
    page: number;
    loaded: boolean;
  };
  activeContract: Contract | null;
  buyContractError: string;
};

const INITIAL_STATE: PacketsReducerState = {
  packets: {
    list: [],
    page: 1,
    loaded: false,
  },
  contracts: {
    list: [],
    page: 1,
    loaded: false,
  },
  activeContract: null,
  buyContractError: '',
};

const packetsReducers = (state = INITIAL_STATE, action: AppActionTypes): PacketsReducerState => {
  switch (action.type) {
    case types.GET_PACKETS_REQUEST:
      const newPacketsRequest = cloneDeep(state.packets);
      newPacketsRequest.loaded = true;
      return {
        ...state,
        packets: newPacketsRequest,
      };
    case types.GET_PACKETS_SUCCESS:
      const newPacketsSuccess = cloneDeep(state.packets);
      newPacketsSuccess.list = action.payload || newPacketsSuccess.list;
      return {
        ...state,
        packets: newPacketsSuccess,
      };

    case types.GET_PACKETS_FAILURE:
      return {
        ...state,
      };
    case types.GET_PACKET_REQUEST:
      return {
        ...state,
      };
    case types.GET_PACKET_SUCCESS:
      return {
        ...state,
      };
    case types.GET_PACKET_FAILURE:
      return {
        ...state,
      };
    case types.CREATE_PACKET_REQUEST:
      return {
        ...state,
      };
    case types.CREATE_PACKET_SUCCESS:
      const newPacketSuccess = cloneDeep(state.packets);
      newPacketSuccess.list = [...newPacketSuccess.list, action.payload];
      return {
        ...state,
        packets: newPacketSuccess,
        buyContractError: 'SUCCESS',
      };
    case types.CREATE_PACKET_FAILURE:
      return {
        ...state,
      };
    case types.GET_CONTRACTS_REQUEST:
      const newContractsRequest = cloneDeep(state.contracts);
      newContractsRequest.loaded = true;
      return {
        ...state,
        contracts: newContractsRequest,
      };
    case types.GET_CONTRACTS_SUCCESS:
      const newContractsSuccess = cloneDeep(state.contracts);
      newContractsSuccess.list = action.payload || newContractsSuccess.list;
      return {
        ...state,
        contracts: newContractsSuccess,
      };
    case types.GET_CONTRACTS_FAILURE:
      return {
        ...state,
      };
    case types.SET_ACTIVE_CONTRACT:
      return {
        ...state,
        activeContract: action.payload,
        buyContractError: '',
      };
    case types.LOGOUT_SUCCESS:
      const logoutData = cloneDeep(INITIAL_STATE);
      return logoutData;
    default:
      return state;
  }
};

export default packetsReducers;
