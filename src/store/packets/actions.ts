import { Contract } from '@/api';

import types from '../actionTypes';

const getPackets = () => ({
  type: types.GET_PACKETS_REQUEST,
});

const getPacket = (payload: { id: number }) => ({
  type: types.GET_PACKET_REQUEST,
  payload,
});

const createPacket = (payload: { amount_usd: number }) => ({
  type: types.CREATE_PACKET_REQUEST,
  payload,
});

const getContracts = () => ({
  type: types.GET_CONTRACTS_REQUEST,
});

const setContract = (payload: Contract) => (dispatch: any) => {
  dispatch({
    type: types.SET_ACTIVE_CONTRACT,
    payload,
  });
  return Promise.resolve();
};

export { getPackets, getPacket, createPacket, getContracts, setContract };
