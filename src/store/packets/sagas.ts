import { all, call, put, takeLatest } from 'redux-saga/effects';

import { Contract, Packet } from '@/api';
import API from '@/api/executor';
import { ALERT_TYPES } from '@/const/app.constants';
import types from '@/store/actionTypes';
import { setAlert, setLoading } from '@/store/loadingsErrors/actions';
import { CreatePacketRequest, GetPacketRequest } from '@/store/packets/actionTypes';

// calls
const fetchPacketsCall = (payload?: any) => API.call('fetchPackets', payload);
const fetchPacketCall = (payload?: any) => API.call('fetchPacket', payload);
const createPacketCall = (payload?: any) => API.call('createPacket', payload);
const fetchContractsCall = (payload?: any) => API.call('fetchContracts', payload);

function* fetchPackets() {
  yield put(setLoading(types.GET_PACKETS_REQUEST, true));
  try {
    const res: Packet[] = yield call(() => fetchPacketsCall());
    yield all([
      put({ type: types.GET_PACKETS_SUCCESS, payload: res }),
      put(setLoading(types.GET_PACKETS_REQUEST, false)),
    ]);
  } catch (e) {
    const error = e?.errObj?.error?.length ? e?.errObj?.error[0] : null;
    yield all([
      put(setLoading(types.GET_PACKETS_REQUEST, false)),
      put(setAlert(error ? `error.${error}` : 'error.network.404', ALERT_TYPES.DANGER)),
      put({ type: types.GET_PACKETS_FAILURE }),
    ]);
  }
}

function* fetchPacket({ payload }: GetPacketRequest) {
  yield put(setLoading(types.GET_PACKET_REQUEST, true));
  try {
    const res: Packet = yield call(() => fetchPacketCall(payload));
    yield all([
      put({ type: types.GET_PACKET_SUCCESS, payload: res }),
      put(setLoading(types.GET_PACKET_REQUEST, false)),
    ]);
  } catch (e) {
    const error = e?.errObj?.error?.length ? e?.errObj?.error[0] : null;
    yield all([
      put(setLoading(types.GET_PACKET_REQUEST, false)),
      put(setAlert(error ? `error.${error}` : 'error.network.404', ALERT_TYPES.DANGER)),
      put({ type: types.GET_PACKET_FAILURE }),
    ]);
  }
}

function* createPacket({ payload }: CreatePacketRequest) {
  yield put(setLoading(types.CREATE_PACKET_REQUEST, true));
  try {
    const res: Packet = yield call(() => createPacketCall(payload));
    yield all([
      put({ type: types.CREATE_PACKET_SUCCESS, payload: res }),
      put(setLoading(types.CREATE_PACKET_REQUEST, false)),
    ]);
  } catch (e) {
    const error = e?.errObj?.error?.length ? e?.errObj?.error[0] : null;
    yield all([
      put(setLoading(types.CREATE_PACKET_REQUEST, false)),
      put(setAlert(error ? `error.${error}` : 'error.network.404', ALERT_TYPES.DANGER)),
      put({ type: types.CREATE_PACKET_FAILURE }),
    ]);
  }
}

function* fetchContracts() {
  yield put(setLoading(types.GET_CONTRACTS_REQUEST, true));
  try {
    const res: Contract[] = yield call(() => fetchContractsCall());
    yield all([
      put({ type: types.GET_CONTRACTS_SUCCESS, payload: res }),
      put(setLoading(types.GET_CONTRACTS_REQUEST, false)),
    ]);
  } catch (e) {
    const error = e?.errObj?.error?.length ? e?.errObj?.error[0] : null;
    yield all([
      put(setLoading(types.GET_CONTRACTS_REQUEST, false)),
      put(setAlert(error ? `error.${error}` : 'error.network.404', ALERT_TYPES.DANGER)),
      put({ type: types.GET_CONTRACTS_FAILURE }),
    ]);
  }
}

export default function* packetsSagas() {
  yield takeLatest(types.GET_PACKETS_REQUEST, fetchPackets);
  yield takeLatest(types.GET_PACKET_REQUEST, fetchPacket);
  yield takeLatest(types.CREATE_PACKET_REQUEST, createPacket);
  yield takeLatest(types.GET_CONTRACTS_REQUEST, fetchContracts);
}
