const GET_PACKETS_REQUEST = 'GET_PACKETS_REQUEST';
const GET_PACKETS_SUCCESS = 'GET_PACKETS_SUCCESS';
const GET_PACKETS_FAILURE = 'GET_PACKETS_FAILURE';
const GET_PACKET_REQUEST = 'GET_PACKET_REQUEST';
const GET_PACKET_SUCCESS = 'GET_PACKET_SUCCESS';
const GET_PACKET_FAILURE = 'GET_PACKET_FAILURE';
const CREATE_PACKET_REQUEST = 'CREATE_PACKET_REQUEST';
const CREATE_PACKET_SUCCESS = 'CREATE_PACKET_SUCCESS';
const CREATE_PACKET_FAILURE = 'CREATE_PACKET_FAILURE';
const GET_CONTRACTS_REQUEST = 'GET_CONTRACTS_REQUEST';
const GET_CONTRACTS_SUCCESS = 'GET_CONTRACTS_SUCCESS';
const GET_CONTRACTS_FAILURE = 'GET_CONTRACTS_FAILURE';
const SET_ACTIVE_CONTRACT = 'SET_ACTIVE_CONTRACT';

type GetPacketsActionType = {
  type: typeof GET_PACKETS_SUCCESS | typeof GET_PACKETS_FAILURE;
  payload?: any;
};

export type GetPacketsRequest = {
  type: typeof GET_PACKETS_REQUEST;
};

type GetPacketActionType = {
  type: typeof GET_PACKET_SUCCESS | typeof GET_PACKET_FAILURE;
  payload?: any;
};

export type GetPacketRequest = {
  type: typeof GET_PACKET_REQUEST;
  payload: any;
};

type CreatePacketActionType = {
  type: typeof CREATE_PACKET_SUCCESS | typeof CREATE_PACKET_FAILURE;
  payload?: any;
};

export type CreatePacketRequest = {
  type: typeof CREATE_PACKET_REQUEST;
  payload: any;
};

type GetContractsActionType = {
  type: typeof GET_CONTRACTS_SUCCESS | typeof GET_CONTRACTS_FAILURE;
  payload?: any;
};

export type GetContractsRequest = {
  type: typeof GET_CONTRACTS_REQUEST;
};

type SetContractType = {
  type: typeof SET_ACTIVE_CONTRACT;
  payload: any;
};

export type GetPacketsTypes = GetPacketsActionType | GetPacketsRequest;
export type GetPacketTypes = GetPacketActionType | GetPacketRequest;
export type CreatePacketTypes = CreatePacketActionType | CreatePacketRequest;
export type GetContractsTypes = GetContractsActionType | GetContractsRequest;

export type PacketsActionTypes =
  | GetPacketsTypes
  | GetPacketTypes
  | CreatePacketTypes
  | GetContractsTypes
  | SetContractType;

export default {
  GET_PACKETS_REQUEST,
  GET_PACKETS_SUCCESS,
  GET_PACKETS_FAILURE,
  GET_PACKET_REQUEST,
  GET_PACKET_SUCCESS,
  GET_PACKET_FAILURE,
  CREATE_PACKET_REQUEST,
  CREATE_PACKET_SUCCESS,
  CREATE_PACKET_FAILURE,
  GET_CONTRACTS_REQUEST,
  GET_CONTRACTS_SUCCESS,
  GET_CONTRACTS_FAILURE,
  SET_ACTIVE_CONTRACT,
} as const;
