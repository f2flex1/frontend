import { all, fork } from 'redux-saga/effects';

import appSagas from './app/sagas';
import messagesSagas from './messages/sagas';
import operationsSagas from './operations/sagas';
import packetsSagas from './packets/sagas';
import partnersSagas from './partners/sagas';
import programsSagas from './programs/sagas';
import usersSagas from './user/sagas';
import walletSagas from './wallet/sagas';

export default function* rootSaga() {
  yield all([
    fork(appSagas),
    fork(operationsSagas),
    fork(packetsSagas),
    fork(partnersSagas),
    fork(usersSagas),
    fork(walletSagas),
    fork(messagesSagas),
    fork(programsSagas),
  ]);
}
