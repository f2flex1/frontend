import cloneDeep from 'lodash/cloneDeep';
import filter from 'lodash/filter';
import orderBy from 'lodash/orderBy';

import { Message } from '@/api';
import { LIMITS, MESSAGES_TYPES } from '@/const/app.constants';
import types, { AppActionTypes } from '@/store/actionTypes';

export type MessagesState = {
  messages: {
    list: Message[];
    page: number;
    end: boolean;
    loaded: boolean;
  };
  levelUp: Message | null;
};

export const initialState: MessagesState = {
  messages: {
    list: [],
    page: 1,
    end: false,
    loaded: false,
  },
  levelUp: null,
};

const messagesReducer = (
  state: MessagesState = initialState,
  action: AppActionTypes
): MessagesState => {
  switch (action.type) {
    case types.GET_MESSAGES_REQUEST:
      const newMessagesRequest = cloneDeep(state.messages);
      newMessagesRequest.loaded = true;
      newMessagesRequest.page = action.page || 1;

      return {
        ...state,
        messages: newMessagesRequest,
      };
    case types.GET_MESSAGES_SUCCESS:
      const newMessagesSuccessList = cloneDeep(state.messages);
      const newMessagesSuccess = action.payload ? orderBy(action.payload, 'created', 'desc') : [];
      newMessagesSuccessList.list =
        state.messages.page === 1
          ? newMessagesSuccess
          : [...newMessagesSuccessList.list, ...newMessagesSuccess];

      if (newMessagesSuccess.length < LIMITS.MESSAGES) {
        newMessagesSuccessList.end = true;
      }

      const levelUp: Message[] = filter(action.payload, {
        key: MESSAGES_TYPES.LEVEL_UP,
        read: false,
      });

      return {
        ...state,
        messages: newMessagesSuccessList,
        levelUp: levelUp.length ? levelUp[levelUp.length - 1] : null,
      };

    case types.GET_MESSAGES_FAILURE:
      return {
        ...state,
      };
    case types.SET_IS_MESSAGE_READ: {
      return {
        ...state,
        levelUp: null,
      };
    }
    case types.SET_IS_MESSAGES_READ: {
      return {
        ...state,
      };
    }
    case types.LOGOUT_SUCCESS:
      const logoutData = cloneDeep(initialState);
      return logoutData;
    default:
      return state;
  }
};

export default messagesReducer;
