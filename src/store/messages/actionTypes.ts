import { Message } from '@/api';

const GET_MESSAGES_REQUEST = 'GET_MESSAGES_REQUEST';
const GET_MESSAGES_SUCCESS = 'GET_MESSAGES_SUCCESS';
const GET_MESSAGES_FAILURE = 'GET_MESSAGES_FAILURE';
const SET_IS_MESSAGE_READ = 'SET_IS_MESSAGE_READ';
const SET_IS_MESSAGES_READ = 'SET_IS_MESSAGES_READ';

// interfaces
type GetMessagesActionType = {
  type: typeof GET_MESSAGES_SUCCESS | typeof GET_MESSAGES_FAILURE;
  payload?: Message[];
};

export type GetMessagesRequest = {
  type: typeof GET_MESSAGES_REQUEST;
  page?: number;
};

export interface SetIsMessageRead {
  type: typeof SET_IS_MESSAGE_READ;
  messageId: string;
}

export interface SetIsMessagesRead {
  type: typeof SET_IS_MESSAGES_READ;
}

export type GetMessagesTypes = GetMessagesActionType | GetMessagesRequest;
export type MessagesTypes = GetMessagesTypes | SetIsMessageRead | SetIsMessagesRead;

export default {
  GET_MESSAGES_REQUEST,
  GET_MESSAGES_SUCCESS,
  GET_MESSAGES_FAILURE,
  SET_IS_MESSAGE_READ,
  SET_IS_MESSAGES_READ,
} as const;
