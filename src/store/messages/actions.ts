import types from '@/store/actionTypes';

import { SetIsMessageRead } from './actionTypes';

// Internal ACTIONS CREATORS
export const getMessages = (page?: number) => ({
  type: types.GET_MESSAGES_REQUEST,
  page,
});

export const setMessageRead = (messageId: string): SetIsMessageRead => ({
  type: types.SET_IS_MESSAGE_READ,
  messageId,
});

export const setMessagesRead = () => ({
  type: types.SET_IS_MESSAGES_READ,
});
