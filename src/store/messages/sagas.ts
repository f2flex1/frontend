import { all, call, put, takeLatest } from 'redux-saga/effects';

import { Message } from '@/api';
import API from '@/api/executor';
import { ALERT_TYPES } from '@/const/app.constants';
import types from '@/store/actionTypes';
import { setAlert, setLoading } from '@/store/loadingsErrors/actions';

// calls
const fetchMessagesCall = (payload?: any) => API.call('fetchMessages', payload);
const setMessageReadCall = (payload?: any) => API.call('setMessageRead', payload);
const setMessagesReadCall = (payload?: any) => API.call('setMessagesRead', payload);

function* fetchMessages({ page }: any) {
  yield put(setLoading(types.GET_MESSAGES_REQUEST, true));
  try {
    const res: Message[] = yield call(() => fetchMessagesCall(page || 1));
    yield all([
      put({ type: types.GET_MESSAGES_SUCCESS, payload: res }),
      put(setLoading(types.GET_MESSAGES_REQUEST, false)),
    ]);
  } catch (e) {
    const error = e?.errObj?.error?.length ? e?.errObj?.error[0] : null;
    yield all([
      put(setLoading(types.GET_MESSAGES_REQUEST, false)),
      put(setAlert(error ? `error.${error}` : 'error.network.404', ALERT_TYPES.DANGER)),
      put({ type: types.GET_MESSAGES_FAILURE }),
    ]);
  }
}

function* setMessageRead({ messageId }: any) {
  yield put(setLoading(types.SET_IS_MESSAGE_READ, true));
  try {
    yield call(() => setMessageReadCall(messageId));
    yield all([put(setLoading(types.SET_IS_MESSAGE_READ, false))]);
  } catch (e) {
    const error = e?.errObj?.error?.length ? e?.errObj?.error[0] : null;
    yield all([
      put(setLoading(types.SET_IS_MESSAGE_READ, false)),
      put(setAlert(error ? `error.${error}` : 'error.network.404', ALERT_TYPES.DANGER)),
    ]);
  }
}

function* setMessagesRead() {
  yield put(setLoading(types.SET_IS_MESSAGES_READ, true));
  try {
    yield call(() => setMessagesReadCall());
    const res: Message[] = yield call(() => fetchMessagesCall(1));
    yield all([
      put(setLoading(types.SET_IS_MESSAGES_READ, false)),
      put({ type: types.GET_MESSAGES_SUCCESS, payload: res }),
    ]);
  } catch (e) {
    const error = e?.errObj?.error?.length ? e?.errObj?.error[0] : null;
    yield all([
      put(setLoading(types.SET_IS_MESSAGES_READ, false)),
      put(setAlert(error ? `error.${error}` : 'error.network.404', ALERT_TYPES.DANGER)),
    ]);
  }
}

export default function* operationsSagas() {
  yield takeLatest(types.GET_MESSAGES_REQUEST, fetchMessages);
  yield takeLatest(types.SET_IS_MESSAGE_READ, setMessageRead);
  yield takeLatest(types.SET_IS_MESSAGES_READ, setMessagesRead);
}
