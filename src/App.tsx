import { CssBaseline, MuiThemeProvider, responsiveFontSizes } from '@material-ui/core';
import { StylesProvider } from '@material-ui/core/styles';
import React, { useMemo } from 'react';
import { useSelector } from 'react-redux';
import { Redirect, Route, Router } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';

import { GlobalStyle } from '@/App.Styles';
import darkTheme, { dark } from '@/common/utils/darkTheme';
import history from '@/common/utils/history';
import lightTheme, { light } from '@/common/utils/lightTheme';
import { PATHS } from '@/const/paths.constants';
import Actives from '@/pages/Actives';
import ContractHistory from '@/pages/Actives/contractHistory';
import PurchaseHistory from '@/pages/Actives/purchaseHistory';
import Additionally from '@/pages/Additionally';
import ForgotPassword from '@/pages/Auth/ForgotPassword';
import ForgotPasswordConfirm from '@/pages/Auth/ForgotPassword/ForgotPasswordConfirm';
import ForgotPasswordReset from '@/pages/Auth/ForgotPassword/ForgotPasswordReset';
import ForgotPasswordSuccess from '@/pages/Auth/ForgotPassword/ForgotPasswordSuccess';
import Login from '@/pages/Auth/Login';
import LoginByEmail from '@/pages/Auth/Login/LoginByEmail';
import LoginByNumber from '@/pages/Auth/Login/LoginByNumber';
import PrivateEntry from '@/pages/Auth/PrivateEntry';
import Registration from '@/pages/Auth/Registration';
import Faq from '@/pages/Faq';
import Finances from '@/pages/Finances';
import FinancesCreate from '@/pages/Finances/Create';
import FinancesExtract from '@/pages/Finances/Extract';
import FinancesFill from '@/pages/Finances/Fill';
import Home from '@/pages/Home';
import Investments from '@/pages/Investments';
import BuyContract from '@/pages/Investments/buyContract';
import BuyContractSuccess from '@/pages/Investments/buyContractSuccess';
import CreateContract from '@/pages/Investments/createContract';
import Partnership from '@/pages/Partnership';
import CashbackProgram from '@/pages/Programs/Cashback/Cashback';
import CashbackProgramOrder from '@/pages/Programs/Cashback/Order/CashbackOrder';
import MarketProgram from '@/pages/Programs/Market';
import MarketOrder from '@/pages/Programs/Market/Order/MarketOrder';
import MarketBuyProduct from '@/pages/Programs/Market/buyProduct/MarketBuyProduct';
import MarketBuyProductSuccess from '@/pages/Programs/Market/buyProductSuccess/MarketBuyProductSuccess';
import Settings from '@/pages/Settings';
import GuestRoutes from '@/routes/guestRoutes';
import PrivateRoutes from '@/routes/privateRoute';
import { AppStateType } from '@/store';

const App: React.FC = () => {
  const themeType = useSelector((state: AppStateType) => state.app.theme);
  const theme = useMemo(
    () => responsiveFontSizes(themeType === 'dark' ? darkTheme : lightTheme),
    [themeType]
  );

  const customTheme = useMemo(() => (themeType === 'dark' ? dark : light), [themeType]);

  return (
    <StylesProvider injectFirst>
      <ThemeProvider theme={customTheme}>
        <MuiThemeProvider theme={theme}>
          <GlobalStyle />
          <CssBaseline />
          <Router history={history}>
            <Route exact path="/" render={() => <Redirect to={PATHS.CABINET} />} />
            <PrivateRoutes exact path={PATHS.CABINET} component={Home} title="navBar.cabinet" />
            <PrivateRoutes
              exact
              path={PATHS.INVESTMENTS}
              component={Investments}
              title="navBar.investments"
            />
            <PrivateRoutes
              exact
              path={PATHS.INVESTMENTS_CREATE_CONTRACT}
              component={CreateContract}
              title="navBar.investments"
            />
            <PrivateRoutes
              exact
              path={PATHS.INVESTMENTS_CREATE_CONTRACT_BUY_CONTRACT}
              component={BuyContract}
              title="navBar.investments"
            />
            <PrivateRoutes
              exact
              path={PATHS.INVESTMENTS_CREATE_CONTRACT_BUY_CONTRACT_SUCCESS}
              component={BuyContractSuccess}
              title="navBar.investments"
            />
            <PrivateRoutes
              exact
              path={PATHS.FINANCES}
              component={Finances}
              title="navBar.finances"
            />
            <PrivateRoutes
              exact
              path={PATHS.FINANCES_DEPOSIT}
              component={FinancesFill}
              title="navBar.finances"
            />
            <PrivateRoutes
              exact
              path={PATHS.FINANCES_WITHDRAW}
              component={FinancesExtract}
              title="navBar.finances"
            />
            <PrivateRoutes
              exact
              path={PATHS.FINANCES_CREATE}
              component={FinancesCreate}
              title="navBar.finances"
            />
            <PrivateRoutes exact path={PATHS.ACTIVES} component={Actives} title="navBar.actives" />
            <PrivateRoutes
              exact
              path={PATHS.ACTIVES_CONTRACTS_HISTORY}
              component={ContractHistory}
              title="navBar.actives"
            />
            <PrivateRoutes
              exact
              path={PATHS.ACTIVES_PURCHASE_HISTORY}
              component={PurchaseHistory}
              title="navBar.actives"
            />
            <PrivateRoutes
              exact
              path={PATHS.PARTNERSHIP}
              component={Partnership}
              title="navBar.partnership"
            />
            <PrivateRoutes
              exact
              path={PATHS.ADDITIONALLY}
              component={Additionally}
              title="navBar.additionally"
            />
            <PrivateRoutes exact path={PATHS.FAQ} component={Faq} title="navBar.faq" />
            <PrivateRoutes
              exact
              path={PATHS.SETTINGS}
              component={Settings}
              title="navBar.settings"
            />
            <PrivateRoutes
              exact
              path={PATHS.PROGRAMS_MARKET}
              component={MarketProgram}
              title="navBar.market"
            />
            <PrivateRoutes
              exact
              path={PATHS.PROGRAMS_MARKET_ORDER}
              component={MarketOrder}
              title="navBar.market"
            />
            <PrivateRoutes
              exact
              path={PATHS.PROGRAMS_MARKET_BUY_PRODUCT}
              component={MarketBuyProduct}
              title="navBar.market"
            />
            <PrivateRoutes
              exact
              path={PATHS.PROGRAMS_MARKET_BUY_PRODUCT_SUCCESS}
              component={MarketBuyProductSuccess}
              title="navBar.market"
            />
            <PrivateRoutes
              exact
              path={PATHS.PROGRAMS_CASHBACK}
              component={CashbackProgram}
              title="navBar.market"
            />
            <PrivateRoutes
              exact
              path={PATHS.PROGRAMS_CASHBACK_ORDER}
              component={CashbackProgramOrder}
              title="navBar.market"
            />
            <GuestRoutes
              exact
              path={PATHS.LOGIN}
              component={Login}
              title="login.page.titles.title"
            />
            <GuestRoutes
              exact
              path={PATHS.LOGIN_BY_EMAIL}
              component={LoginByEmail}
              title="verification.page.titles.login_by_email"
            />
            <GuestRoutes
              exact
              path={PATHS.LOGIN_BY_NUMBER}
              component={LoginByNumber}
              title="verification.page.titles.login_by_number"
            />
            <GuestRoutes
              exact
              path={PATHS.REGISTRATION}
              component={Registration}
              title="registration.page.title"
            />
            <GuestRoutes
              exact
              path={PATHS.FORGOT_PASSWORD}
              component={ForgotPassword}
              title="forgotPassword.page.title"
            />
            <GuestRoutes
              exact
              path={PATHS.FORGOT_PASSWORD_CONFIRM}
              component={ForgotPasswordConfirm}
              title="forgotPassword.page.title"
            />
            <GuestRoutes
              exact
              path={PATHS.FORGOT_PASSWORD_RESET}
              component={ForgotPasswordReset}
              title="forgotPassword.page.title"
            />
            <GuestRoutes
              exact
              path={PATHS.FORGOT_PASSWORD_SUCCESS}
              component={ForgotPasswordSuccess}
              title="forgotPassword.page.title2"
            />
            <GuestRoutes
              exact
              path={PATHS.PRIVATE_ENTRY}
              component={PrivateEntry}
              title="registration.page.title"
            />
            <GuestRoutes
              path="/landing"
              component={() => {
                window.location.href = PATHS.LANDING;
                return null;
              }}
            />
            {/* <PrivateRoutes path="*" component={() => <div>Nothing found 404</div>} />*/}
          </Router>
        </MuiThemeProvider>
      </ThemeProvider>
    </StylesProvider>
  );
};

export default App;
