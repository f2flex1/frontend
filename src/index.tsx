import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import { I18nextProvider } from 'react-i18next';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';

import ErrorBoundary from '@/common/components/ErrorBoundary';
import i18n from '@/common/utils/i18n';
import Loading from '@/components/Loading';

import App from './App';
import store from './store';

import './assets/styles/main.scss';

ReactDOM.render(
  <React.StrictMode>
    <ErrorBoundary>
      <Provider store={store}>
        <I18nextProvider i18n={i18n}>
          <Router>
            <Suspense fallback={<Loading />}>
              <App />
            </Suspense>
          </Router>
        </I18nextProvider>
      </Provider>
    </ErrorBoundary>
  </React.StrictMode>,
  document.getElementById('app')
);
