import { makeHandler } from '@/api/makeHandler';

export const fetchOperations = makeHandler(
  'fetchOperations',
  (page: number) => ({
    method: 'GET',
    path: `/user/operations/?page=${page}`,
  }),
  (res: { data: Operation[] }) => res.data
);

export const fetchOperationsWithdrawals = makeHandler(
  'fetchOperationsWithdrawals',
  () => ({
    method: 'GET',
    path: '/withdrawals/',
  }),
  (res: { data: Operation[] }) => res.data
);

export const createOperation = makeHandler(
  'createOperation',
  (data: { amount_usd: string }) => ({
    method: 'POST',
    path: '/withdrawals/',
    data,
  }),
  (res: { data: Operation[] }) => res.data
);

export type Operation = {
  /** ID */
  id?: string;
  /** Сумма операции USD */
  amount_usd: string;
  /** Тип операции */
  type_name: string;
  /** Дата создания */
  created: string;
  /** Статус операции */
  status_name: string;
  /** Уровень */
  level?: string;
  /** Название пакета */
  packet_name?: string;
};
