import { makeHandler } from '@/api/makeHandler';

export const fetchPrograms = makeHandler(
  'fetchPrograms',
  () => ({
    method: 'GET',
    path: '/packets/',
  }),
  (res: { data: Program[] }) => res.data
);

export const fetchProgram = makeHandler(
  'fetchProgram',
  (data: { id: number }) => ({
    method: 'GET',
    path: `/packets/${data.id}`,
  }),
  (res: { data: Program }) => res.data
);

export const createProgram = makeHandler(
  'createProgram',
  (data: { amount_usd: string }) => ({
    method: 'POST',
    path: '/packets/buy/',
    data,
  }),
  (res: { data: Program }) => res.data
);

export const fetchProducts = makeHandler(
  'fetchProducts',
  () => ({
    method: 'GET',
    path: '/common/packet_settings/',
  }),
  (res: { data: Product[] }) => res.data
);

export type Program = {
  /** Id */
  id?: number;
  /** Название */
  name: string;
  /** Сумма операции USD */
  amount_usd: string | number;
  /** Дата создания */
  created?: string;
  /** Кол-во дней */
  days?: number;
  /** Открыт ли пакет */
  is_open?: boolean;
  /** Бонус */
  is_bonus?: boolean;
  /** Дней открыт */
  days_open?: number;
  /** Дней осталось */
  days_left?: number;
  /** Процент */
  percent?: number;
};

export type Product = {
  /** Название контракта */
  name: string;
  /** Сумма операции USD */
  amount_usd: number;
  /** Ежедневный Процент */
  daily_percents: number;
  /** Кол-во дней */
  days: number;
};
