import { makeHandler } from '@/api/makeHandler';

export const fetchMessages = makeHandler(
  'fetchMessages',
  (page: number) => ({
    method: 'GET',
    path: `/messages/old/?page=${page}`,
  }),
  (res: { data: Message[] }) => res.data
);

export const setMessageRead = makeHandler(
  'setMessageRead',
  (messageId: string) => ({
    method: 'GET',
    path: `/messages/${messageId}/`,
  }),
  () => true
);

export const setMessagesRead = makeHandler(
  'setMessagesRead',
  () => ({
    method: 'POST',
    path: '/messages/read/',
  }),
  () => true
);

export type Messages = {
  /** Кол-во сообщений */
  count: number;
  /** Сдедующая страница */
  next: string;
  /** Предыдущая страница */
  previous: string;
  /** Сообщения */
  results: Message[];
};

export type Message = {
  /** ID */
  id: string;
  /** Ключ сообщения */
  key: string;
  /** Текст */
  text: string;
  /** Переменные для сообщения */
  data: any;
  /** Статус */
  status: string;
  /** Тип */
  type: string;
  /** Дата создания */
  created: string;
  /** Параметр прочтения */
  read: boolean;
};

export type MessageData = {
  /** Сумма */
  amount?: number;
  /** Email */
  email?: string;
  /** Линия */
  line?: number;
  /** Имя */
  first_name?: any;
  /** Фамилия */
  last_name?: string;
  /** Процент */
  percent?: string;
  /** Телефон */
  phone?: string;
  /** Id пользователя */
  ref_id?: number;
  /** Новый уровень */
  level?: string;
  /** Номер уровня */
  level_num?: number;
};
