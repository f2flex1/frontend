import { makeHandler } from '@/api/makeHandler';

export const fetchWalletStat = makeHandler(
  'fetchWalletStat',
  () => ({
    method: 'GET',
    path: '/user/finances/',
  }),
  (res: { data: WalletStat[] }) => res.data
);

export const fetchCurrenciesRates = makeHandler(
  'fetchCurrenciesRates',
  () => ({
    method: 'GET',
    path: '/common/ticker/',
  }),
  (res: { data: ExchangeRates }) => res.data
);

export type WalletStat = {
  /** Инвестировано */
  invested: number;
  /** Ожидается */
  waiting: number;
  /** Получен поход */
  income: number;
  /** Чистая прибыль */
  profit: number;
  /** Баланс */
  balance: number;
};

export type ExchangeRates = {
  /** Курс BTC к USDT */
  BTCUSDT?: number;
  /** Курс USDT к RUB */
  USDTRUB?: number;
  /** Курс BTC к RUB */
  BTCRUB?: number;
};
