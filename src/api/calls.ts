import {
  logIn,
  registration,
  forgotPassword,
  verification,
  changePassword,
  privateSignup,
} from '@/api/codecs.auth';
import { fetchMessages, setMessageRead, setMessagesRead } from '@/api/codecs.messages';
import {
  fetchOperations,
  createOperation,
  fetchOperationsWithdrawals,
} from '@/api/codecs.operations';
import { fetchPackets, fetchPacket, createPacket, fetchContracts } from '@/api/codecs.packets';
import { fetchPartners } from '@/api/codecs.partners';
import { createProgram, fetchProducts, fetchProgram, fetchPrograms } from '@/api/codecs.programs';
import { fetchUserData, updateUserData, updateUserPassword, sendUserOtp } from '@/api/codecs.users';
import { fetchWalletStat, fetchCurrenciesRates } from '@/api/codecs.wallet';

const handlers = [
  logIn,
  registration,
  forgotPassword,
  verification,
  privateSignup,
  changePassword,
  fetchUserData,
  updateUserData,
  updateUserPassword,
  sendUserOtp,
  fetchOperations,
  fetchOperationsWithdrawals,
  createOperation,
  fetchPackets,
  fetchPacket,
  createPacket,
  fetchContracts,
  fetchPartners,
  fetchWalletStat,
  fetchCurrenciesRates,
  fetchMessages,
  setMessageRead,
  setMessagesRead,
  fetchPrograms,
  fetchProgram,
  createProgram,
  fetchProducts,
] as const;

const handlersMap: Map<ApiHandlerTypes, ApiHandlers> = new Map();
for (const h of handlers) {
  handlersMap.set(h.type, h);
}

type DiscriminateUnion<T, K extends keyof T, V extends T[K]> = T extends Record<K, V> ? T : never;

type ApiHandlers = typeof handlers[number];
export type ApiHandlerTypes = ApiHandlers['type'];
export type ApiHandler<Type extends ApiHandlerTypes = ApiHandlerTypes> = DiscriminateUnion<
ApiHandlers,
'type',
Type
>;

export type ApiHandlerParams<Type extends ApiHandlerTypes> = Parameters<
ApiHandler<Type>['prepare']
>[0] extends {}
  ? Parameters<ApiHandler<Type>['prepare']>[0]
  : undefined;
export type ApiHandlerResponse<Type extends ApiHandlerTypes> = ReturnType<
ApiHandler<Type>['decode']
>;

export function getHandler<Type extends ApiHandlerTypes>(type: Type): ApiHandler<Type> {
  return handlersMap.get(type) as ApiHandler<Type>;
}
