import { makeHandler } from '@/api/makeHandler';

export const fetchPackets = makeHandler(
  'fetchPackets',
  () => ({
    method: 'GET',
    path: '/packets/',
  }),
  (res: { data: Packet[] }) => res.data
);

export const fetchPacket = makeHandler(
  'fetchPacket',
  (data: { id: number }) => ({
    method: 'GET',
    path: `/packets/${data.id}`,
  }),
  (res: { data: Packet }) => res.data
);

export const createPacket = makeHandler(
  'createPacket',
  (data: { amount_usd: string }) => ({
    method: 'POST',
    path: '/packets/buy/',
    data,
  }),
  (res: { data: Packet }) => res.data
);

export const fetchContracts = makeHandler(
  'fetchContracts',
  () => ({
    method: 'GET',
    path: '/common/packet_settings/',
  }),
  (res: { data: Contract[] }) => res.data
);

export type Packet = {
  /** Id */
  id: number;
  /** Название */
  name: string;
  /** Сумма операции USD */
  amount_usd: number;
  /** Дата создания */
  created: string;
  /** Кол-во дней */
  days: number;
  /** Открыт ли пакет */
  is_open: boolean;
  /** Бонус */
  is_bonus: boolean;
  /** Дней открыт */
  days_open: number;
  /** Дней осталось */
  days_left: number;
  /** Процент */
  percent: number;
};

export type Contract = {
  /** Название контракта */
  name: string;
  /** Сумма операции USD */
  amount_usd: number;
  /** Ежедневный Процент */
  daily_percents: number;
  /** Кол-во дней */
  days: number;
};
