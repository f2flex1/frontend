import { makeHandler } from './makeHandler';

export const logIn = makeHandler(
  'logIn',
  (data: LogInParams) => ({
    method: 'POST',
    path: '/auth/login_send_code/',
    data,
  }),
  (data: LogInParams) => data
);

export const registration = makeHandler(
  'registration',
  (data: RegistrationParams) => ({
    method: 'POST',
    path: '/auth/signup/',
    data,
  }),
  (data: LogInParams) => data
);

export const forgotPassword = makeHandler(
  'forgotPassword',
  (data: ForgotPasswordParams) => ({
    method: 'POST',
    path: '/auth/recovery_password_send_code/',
    data,
  }),
  () => true
);

export const changePassword = makeHandler(
  'changePassword',
  (data: ForgotPasswordParams) => ({
    method: 'PUT',
    path: '/auth/recovery_password/',
    data,
  }),
  () => true
);

export const verification = makeHandler(
  'verification',
  (data: VerificationParams) => ({
    method: 'POST',
    path: '/auth/login/',
    data,
  }),
  (res: { data: LogInResponse }) => {
    return {
      access_token: res.data.token,
      refresh_token: '',
    };
  }
);

export const privateSignup = makeHandler(
  'privateSignup',
  (data: PrivateSignupParams) => ({
    method: 'POST',
    path: '/auth/private_signup/',
    data,
  }),
  (res: { data: LogInParams }) => res.data
);

export type LogInParams = {
  /** Номер мобильного телефона */
  phone?: string;
  /** e-mail */
  email?: string;
  /** Пароль */
  password: string;
  /** Пароль для телефона */
  phonePassword?: string;
};

export type RegistrationParams = {
  /** Номер мобильного телефона */
  phone?: string;
  /** e-mail */
  email?: string;
  /** Id пригласителя */
  inviter?: string;
  /** Пароль */
  password: string;
  /** Повтор Пароля */
  password2: string;
  /** Условия пользовательского соглашения */
  terms?: string;
  /** Актуальный язык при регистрации пользователя */
  language?: string;
};

export type ForgotPasswordParams = {
  /** e-mail */
  email?: string;
  /** Номер мобильного телефона */
  phone?: string;
  /** Проверочный код */
  code?: string;
  /** Пароль */
  password?: string;
  /** Повтор Пароля */
  password2?: string;
};

export type VerificationParams = {
  /** Номер мобильного телефона */
  phone?: string;
  /** e-mail */
  email?: string;
  /** Пароль */
  password?: string;
  /** code */
  code: string;
};

export type PrivateSignupParams = {
  /** id */
  id: string;
  /** token */
  token: string;
};

export type LogInResponse = {
  token: string;
};

export type AccessTokens = {
  access_token: string;
  refresh_token: string;
};
