import { makeHandler } from '@/api/makeHandler';

export const fetchPartners = makeHandler(
  'fetchPartners',
  () => ({
    method: 'GET',
    path: '/partners/',
  }),
  (res: { data: Partner[] }) => res.data
);

export type Partner = {
  /** Id */
  ref_id: number;
  /** Id пригласителя */
  parent: number;
  /** Кол-во приглашенных */
  partners: number;
  /** Имя */
  first_name: string;
  /** Фамилия */
  last_name: string;
  /** Телефон */
  phone: string;
  /** Email */
  email: string;
  /** Товарооборот */
  turnover: number;
  /** Аватар */
  photo: number;
  /** Уровень */
  level: number;
};
