import { makeHandler } from '@/api/makeHandler';

export const fetchUserData = makeHandler(
  'fetchUserData',
  () => ({
    method: 'GET',
    path: '/user/info/',
  }),
  (res: { data: UserData }) => res.data
);

export const updateUserData = makeHandler(
  'updateUserData',
  (data: any) => {
    const changeData: any = {
      method: 'PATCH',
      path: '/user/change/',
    };

    if (data.type === 'form') {
      changeData.form = data.data;
    } else {
      changeData.data = data.data;
    }

    return changeData;
  },
  (res: { data: UserData }) => res.data
);

export const updateUserPassword = makeHandler(
  'updateUserPassword',
  (data: UserNewPassword) => ({
    method: 'PUT',
    path: '/auth/password_change/',
    data,
  }),
  () => true
);

export const sendUserOtp = makeHandler(
  'sendUserOtp',
  () => ({
    method: 'POST',
    path: '/user/send_otp/',
  }),
  () => true
);

export type UserData = {
  /** Id реферала */
  ref_id?: string;
  /** Имя */
  first_name?: string;
  /** Фамилия */
  last_name?: string;
  /** Обьект пригласителя */
  parent?: ParentData;
  /** E-mail */
  email?: string;
  /** Номер мобильного телефона */
  phone?: string;
  /** Адрес кошелька */
  debit_address?: string;
  /** Адрес кошелька для пополнения BTC */
  credit_btc_address?: string;
  /** Адрес кошелька для пополнения USDT */
  credit_usdt_address?: string;
  /** Аватарка */
  photo?: string;
  /** Включен ли двухфакторная авторизация */
  is_twofactor_auth?: boolean;
  /** Информация о уровене */
  level_info?: LevelInfoData;
  /** Код для двухфакторной авторизации */
  code?: string;
  /** Новый Адрес кошелька */
  new_debit_address?: string;
  /** Товарооборот */
  turnover?: number;
  /** Баланс */
  balance_usd?: number;
  /** nickname */
  nickname?: string;
  /** language */
  language?: string;
};

export type ParentData = {
  /** Id */
  id: string;
  /** Имя */
  name: string;
  /** Аватарка */
  photo: string;
  /** Уровень */
  level: string;
  /** Уровень */
  nickname: string;
};

export type LevelInfoData = {
  /** Инвестировано */
  investments: string;
  /** Уровень */
  level: string;
  /** Следующий бонус */
  next_bonus_balance: number;
  /** Процент до следующего уровня */
  next_percent: number;
  /** Следующий Уровень */
  next_level: string;

  next_bonus_packets: any;
  next_percent_by_percent: any;
  percent: number;
  percent_by_percent: any;
  required_investments: number;
  required_partner_lines: number[];
  partner_turnover: number;
  required_partner_turnover: number;
};

export type UserNewPassword = {
  /** Старый пароль */
  old_password?: string;
  /** Новый пароль */
  new_password?: string;
};
