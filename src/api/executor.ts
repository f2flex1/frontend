import { API_URL } from '@/const/general.constants';
import { STORAGE_KEYS } from '@/const/storage_keys.constants';

import { ApiHandlerParams, ApiHandlerTypes, getHandler, ApiHandlerResponse } from './calls';
import { ApiError, isApiError } from './error';
import { Request, FormDataEntry } from './request';

type ErrorHandler = (e: ApiError) => void;

type ProtoError = {
  errors: Record<string, string[]>;
};

const extraGoodHttpStatuses = new Set<number>([204]);
const unauthorizedHttpStatuses = new Set<number>([401]);

function processFormData(form: FormDataEntry[]) {
  if (!Array.isArray(form)) return null;
  const res = new FormData();
  for (const entry of form) {
    res.append(entry.name, entry.value);
  }
  return res;
}

class Api {
  private timeout: number;

  private errorHandler?: ErrorHandler;

  constructor() {
    this.timeout = 60 * 1000;
  }

  call<Type extends ApiHandlerTypes>(
    type: Type,
    params: ApiHandlerParams<Type>
  ): Promise<ApiHandlerResponse<Type>>;
  call(type: any, params: any): any {
    const h = getHandler(type);
    const req = h.prepare(params);
    const a = this.makeRequest(req).then((data: any) => h.decode(data));
    if (this.errorHandler) a.catch(this.errorHandler);
    return a;
  }

  private makeRequest(params: Request): Promise<any> {
    const url = `${API_URL}${params.path}`;
    const auth = JSON.parse(localStorage.getItem(STORAGE_KEYS.AUTH) || '{}');

    const headers: Record<string, string> = {
      ...params.headers,
    };

    if (!!auth.access_token) {
      headers['Authorization'] = `Token ${auth.access_token}`;
    }

    const jsonData = params.data ? JSON.stringify(params.data) : processFormData(params.form);

    if (params.data) {
      headers['Content-Type'] = 'application/json';
    }

    const init: RequestInit = {
      method: params.method,
      headers,
      body: jsonData,
    };

    const abortController = new AbortController();
    init.signal = abortController.signal;

    let timeoutHandle: ReturnType<typeof setTimeout> | undefined = setTimeout(
      () => abortController.abort(),
      this.timeout
    );
    const cleanupTimeout = () => {
      if (!timeoutHandle) return;
      clearTimeout(timeoutHandle);
      timeoutHandle = undefined;
    };

    // TODO: appProvider.application.logger.info('lib request', url, init);

    return fetch(url, init)
      .then(res => {
        if (res.ok || extraGoodHttpStatuses.has(res.status)) {
          const contentType = res.headers.get('Content-Type');
          const contentLength = res.headers.get('content-length');

          if (contentType === 'text/plain' || contentLength === '0') {
            return Promise.resolve({});
          }
          return res.json().then(r => {
            const responseData: any = r.data || { data: r } || {};
            return responseData;
          });
        }

        if (unauthorizedHttpStatuses.has(res.status)) {
          localStorage.removeItem(STORAGE_KEYS.AUTH);
          document.location.reload();
          return Promise.resolve({});
        }

        const err = new ApiError('NetworkError');
        err.httpStatus = res.status;
        err.httpStatusText = res.statusText;
        err.message = '';

        return res.text().then((text: string) => {
          try {
            const data = JSON.parse(text) as ProtoError;
            const errors = data?.errors || data;
            // const errorDetail: Record<string, string> = {};
            // for (const i in errors) {
            //   if (Object.prototype.hasOwnProperty.call(errors, i)) {
            //     const e = errors[i];
            //     if (e instanceof Array) {
            //       err.message = `${i}: ${e.join(';')}`;
            //       errorDetail[i] = e.join(';');
            //     } else {
            //       errorDetail[i] = e;
            //     }
            //   }
            // }
            // if (Object.prototype.hasOwnProperty.call(errorDetail, 'detail')) {
            //   err.message = errorDetail.detail;
            // }
            // err.details = errorDetail;
            err.errObj = errors;
          } catch (e) {
            err.message = text;
          }
          throw err;
        });
      })
      .catch((e: Error) => {
        if (isApiError(e)) throw e;
        throw new ApiError('NetworkError', e.toString());
      })
      .finally(() => {
        cleanupTimeout();
      });
  }
}

export default new Api();
