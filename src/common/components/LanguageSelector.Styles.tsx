import { Select } from '@material-ui/core';
import styled from 'styled-components';

export const LanguageSelectorStyles: any = styled(Select)`
  background-color: transparent;
  border-radius: 5px;
  width: auto;

  .MuiSelect-select {
    //padding: 0.25rem;
    font-weight: 300;
    font-size: 14px;
    line-height: 17px;
    color: var(--white);
    background-color: transparent;
  }

  .MuiSelect-selectMenu {
    height: auto;
    border-radius: 0;
  }

  .MuiMenu-paper {
    border-radius: var(--border-radius);
    box-shadow: 0 0 11px rgba(0, 0, 0, 0.04);
  }

  .MuiList-root {
    background: linear-gradient(to top, rgba(147, 90, 237, 0.21) 0.14%, rgba(41, 47, 57, 0) 99.86%);
    border-radius: 13px;
  }

  .MuiList-padding {
    padding: 11px 8px;
  }

  .MuiListItem-button {
    font-weight: 300;
    font-size: 14px;
    line-height: 17px;
    color: var(--white);
    border-radius: 8px;

    &::after {
      display: none;
    }

    &:focus,
    &:hover {
      background-color: transparent;
      text-decoration: underline;
    }

    &.Mui-selected {
      background-color: transparent;
      text-decoration: underline;
    }
  }

  .MuiMenuItem-root {
    min-height: auto;
  }

  &.header__lang {
    .MuiSelect-select {
      padding: 0.25rem;
    }

    > .MuiSelect-icon {
      display: none;
    }
  }

  &.-light {
    .MuiSelect-select {
      color: var(--dark-blue);
    }

    .MuiMenu-paper {
      background-color: var(--white);
    }

    .MuiListItem-button {
      color: #2a323f;
    }
  }
`;
