import React from 'react';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface Props {}

interface State {
  hasError: boolean;
}

export default class ErrorBoundary extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      hasError: false,
    };
  }

  static getDerivedStateFromError() {
    return { hasError: true };
  }

  // componentDidCatch(error: Error | null, errorInfo: object) {
  //   // TODO: connect any logger for front errors
  //   console.log(error);
  //   console.log(errorInfo);
  // }

  render() {
    const { hasError } = this.state;
    const { children } = this.props;

    if (hasError) {
      return <div>Error</div>;
    }

    return children;
  }
}
