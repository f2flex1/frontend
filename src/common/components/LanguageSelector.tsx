import { MenuItem, Input } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import React, { useCallback, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';

import { LanguageSelectorStyles } from '@/common/components/LanguageSelector.Styles';
import { AppStateType } from '@/store';
import { updateUserData } from '@/store/user/actions';
import { UserReducerState } from '@/store/user/reducers';

import { LANGUAGES } from '../utils/i18n';

const LANGUAGE_LIST = Array.from(LANGUAGES.entries());
const LANGUAGE_CODES_LIST = LANGUAGE_LIST.map(l => l[0]);

interface Props {
  user: UserReducerState;
  className?: string;
  updateUserData: (payload: any) => void;
}

const LanguageSelector: React.FC<Props> = (props: Props) => {
  const { user, className, updateUserData } = props;
  const { i18n } = useTranslation();
  const [loading, setLoading] = useState(false);

  const changeApiLanguage = (languageCode: string) => {
    if (user.loggedIn) {
      updateUserData({
        data: {
          language: languageCode,
        },
        type: 'json',
      });
    }
    setLoading(false);
  };

  const changeLanguage = useCallback(
    (event: React.ChangeEvent<{ name?: string; value: unknown }>) => {
      const languageCode = typeof event.target.value === 'string' && event.target.value;
      if (!languageCode || LANGUAGE_CODES_LIST.indexOf(languageCode) < 0) return;
      setLoading(true);
      i18n.changeLanguage(languageCode, () => changeApiLanguage(languageCode));
    },
    [i18n]
  );

  const menuProps = {
    disablePortal: true,
    anchorOrigin: {
      vertical: 'bottom',
      horizontal: 'left',
    },
    transformOrigin: {
      vertical: 'top',
      horizontal: 'left',
    },
    getContentAnchorEl: null,
  };

  return (
    <LanguageSelectorStyles
      className={className}
      name="age"
      IconComponent={ExpandMoreIcon}
      MenuProps={menuProps}
      value={loading ? '...' : i18n.language}
      renderValue={(language: string) => language.toUpperCase()}
      onChange={changeLanguage}
      input={<Input disableUnderline />}
    >
      {LANGUAGE_LIST.map(l => (
        <MenuItem value={l[0]} key={l[0]}>
          {l[1]}
        </MenuItem>
      ))}
    </LanguageSelectorStyles>
  );
};

const mapState = (state: AppStateType) => {
  const { user } = state;
  return {
    user,
  };
};

export default connect(mapState, { updateUserData })(LanguageSelector);
