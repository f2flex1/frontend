import React, { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import ReactPhoneInput from 'react-phone-input-2';

import { PhoneInputStyles } from '@/common/components/PhoneInput.Styles';
import { FormControlInputError } from '@/components/elements/Input/Input.Styles';
import { DISABLED_COUNTRIES_CODES_LIST } from '@/const/app.constants';

type PhoneInputProps = {
  className?: string;
  error?: string;
  placeholder?: string;
  name: string;
  value: string;
  onChange: (val: string, isValid: boolean) => void;
};

function PhoneInput(props: PhoneInputProps) {
  const { className, value, onChange, error, name, ...rest } = props;
  const { i18n, t } = useTranslation();
  const { language } = i18n;
  const [languageCountry, setLanguageCountry] = useState<string>(language);
  const [reInit, setReInit] = useState<boolean>(false);

  const handleChangePhone = useCallback(
    (val: string, data: any) => {
      if (val) {
        const validNumberCount: number = (data.format.match(/\./g) || []).length;
        let isValid: boolean = val.length === validNumberCount;

        if (DISABLED_COUNTRIES_CODES_LIST.includes(data.countryCode)) {
          isValid = false;
        }

        onChange(val, isValid);
      }
    },
    [onChange, name]
  );

  useEffect(() => {
    if (languageCountry !== language) {
      onChange('', false);
      setReInit(true);
    }
  }, [language]);

  useEffect(() => {
    if (!!reInit) {
      setLanguageCountry(language);
      setReInit(false);
    }
  }, [reInit]);

  return (
    <PhoneInputStyles className={`${className} ${error ? '-error' : ''}`}>
      {!reInit ? (
        <ReactPhoneInput
          specialLabel=""
          country={languageCountry}
          value={value}
          onChange={handleChangePhone}
          excludeCountries={DISABLED_COUNTRIES_CODES_LIST}
        />
      ) : null}
      <input type="hidden" {...rest} />
      <FormControlInputError>{t(`${error}`)}</FormControlInputError>
    </PhoneInputStyles>
  );
}

export default PhoneInput;
