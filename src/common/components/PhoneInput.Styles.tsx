import styled from 'styled-components';

import { FormControl } from '@/components/elements/Input/Input.Styles';

export const PhoneInputStyles = styled(FormControl)`
  input {
    padding: 14px 30px;
    width: 100%;
    color: var(--white);
    background: rgba(196, 196, 196, 0.07);
    border-radius: var(--border-radius);
    border: none;
    outline: none;

    &:focus {
      box-shadow: 0 0 0 1px var(--gray);

      &::placeholder {
        opacity: 0;
      }
    }

    &::placeholder {
      color: rgba(255, 255, 255, 0.65);
      transition: opacity 0.3s;
    }

    &:disabled {
      color: rgba(255, 255, 255, 0.65);
    }
  }

  &.lg input {
    padding: 21px 30px;
    font-size: 20px;
    line-height: 25px;
  }

  &.-error input {
    box-shadow: 0 0 0 1px var(--red);
  }

  &.-light {
    input {
      font-weight: 400;
      color: var(--dark-blue);
      background: var(--white);
      box-shadow: 0 0 11px rgba(0, 0, 0, 0.04);

      &::placeholder {
        font-weight: 700;
        color: rgba(21, 23, 46, 0.65);
      }
    }
  }

  @media (max-width: 992px) {
    &.lg input {
      padding: 14px 30px;
      font-weight: 700;
      font-size: 16px;
      line-height: 21px;
    }
  }
`;
