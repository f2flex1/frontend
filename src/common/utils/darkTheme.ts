import { createTheme } from '@material-ui/core';

import themeOverrides from '@/common/utils/themeOverrides';

const darkTheme = createTheme({
  palette: {
    type: 'dark',
    background: {
      default: 'var(--dark-gray)',
      paper: 'var(--dark)',
    },
  },
  overrides: {
    ...themeOverrides,
  },
  typography: {
    fontFamily: ['Montserrat', 'Poppins'].join(','),
  },
});

export const dark = {
  typography: {
    title: '#eaeaea',
  },
  breadcrumbs: {
    link: '#eaeaea',
    separator: 'url(\'/img/svg/icon_arrow.svg\') center / contain no-repeat',
  },
  dropdown: {
    selectMenu: {
      background: 'rgba(196, 196, 196, 0.07)',
      color: 'rgba(255, 255, 255, 0.65)',
      boxShadow: 'none',
    },
    paper: {
      background: '#343943',
      boxShadow: 'none',
    },
    icon: 'rgba(255, 255, 255, 0.85)',
  },
  table: {
    container: {
      background: '#414753',
      boxShadow: 'none',
    },
    header: {
      color: '#868686',
    },
    body: {
      color: '#ffffff',
    },
    bodyRow: {
      after: 'linear-gradient(3.98deg, #373a43 191.71%, rgba(0, 0, 0, 0.83) 282.29%)',
    },
  },
  tablePlaceholder: {
    background: '#353b44',
    color: '#ffffff',
    boxShadow: 'none',
  },
  scrollbar: {
    track: 'rgba(56, 62, 73, 0.56)',
    thumb: '#292f39',
  },
  accordion: {
    btn: '#ffffff',
    content: '#EFEFEF',
    arrow:
      'url("data:image/svg+xml;charset=UTF-8,%3csvg width=\'15\' height=\'8\' viewBox=\'0 0 15 8\' fill=\'none\' xmlns=\'http://www.w3.org/2000/svg\'%3e%3cpath d=\'M1 0.679199L7.5 6.6792L14 0.679199\' stroke=\'%23EAEAEA\' stroke-linecap=\'round\' stroke-linejoin=\'round\'/%3e%3c/svg%3e ") center / cover no-repeat',
  },
  card: {
    background: '#373D47',
    title: {
      background: 'linear-gradient(180deg, #e9b9e9 0%, #ffffff 100%)',
    },
    list: {
      text: '#d1d1d1',
      value: '#ffffff',
    },
  },
  create: {
    optionsText: {
      color: '#ffffff',
    },
  },
  total: {
    text: 'rgba(255, 255, 255, 0.53)',
    price: 'linear-gradient(180deg, #ff89cb 0%, #ff89cb 100%)',
    btn: {
      color: '#ffffff',
      background: '#8000FF',
      border: '#8000FF',
    },
  },
  market: {
    flexLabel: '#eaeaea',
    content: {
      text: '#ffffff',
    },
  },
};

export default darkTheme;
