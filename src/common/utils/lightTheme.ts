import { createTheme } from '@material-ui/core';

import themeOverrides from '@/common/utils/themeOverrides';

const lightTheme = createTheme({
  palette: {
    type: 'light',
    background: {
      default: '#f7f8fa',
      paper: '#ffffff',
    },
  },
  overrides: {
    ...themeOverrides,
  },
  typography: {
    fontFamily: ['Montserrat', 'Poppins'].join(','),
  },
});

export const light = {
  typography: {
    title: '#3e3e3e',
  },
  breadcrumbs: {
    link: '#434343',
    separator: 'url(\'/img/svg/icon_arrow-dark.svg\') center / contain no-repeat',
  },
  dropdown: {
    selectMenu: {
      background: '#ffffff',
      color: '#15172E',
      boxShadow: '0 0 11px rgba(0, 0, 0, 0.04)',
    },
    paper: {
      background: '#ffffff',
      boxShadow: '0 0 11px rgba(0, 0, 0, 0.04)',
    },
    icon: 'rgba(21, 23, 46, 0.85)',
  },
  table: {
    container: {
      background: '#ffffff',
      boxShadow: '0 0 11px rgba(0, 0, 0, 0.04)',
    },
    header: {
      color: '#636363',
    },
    body: {
      color: '#999999',
    },
    bodyRow: {
      after: 'linear-gradient(3.98deg, #212121 191.71%, rgba(0, 0, 0, 0.83) 282.29%)',
    },
  },
  tablePlaceholder: {
    background: '#fafafa',
    color: '#434343',
    boxShadow: '0 0 11px rgba(0, 0, 0, 0.04)',
  },
  scrollbar: {
    track: 'rgba(56, 62, 73, 0.07)',
    thumb: 'rgba(192, 192, 192, 0.46)',
  },
  accordion: {
    btn: '#434343',
    content: '#434343',
    arrow:
      'url("data:image/svg+xml,%3Csvg width=\'15\' height=\'8\' viewBox=\'0 0 15 8\' fill=\'none\' xmlns=\'http://www.w3.org/2000/svg\'%3E%3Cpath d=\'M1 1L7.5 7L14 1\' stroke=\'%23434343\' stroke-linecap=\'round\' stroke-linejoin=\'round\'/%3E%3C/svg%3E%0A") center / cover no-repeat',
  },
  card: {
    background: '#fafafa',
    title: {
      background: 'linear-gradient(180deg, #8569F9 0%, #B7A7FC 100%)',
    },
    list: {
      text: '#636363',
      value: '#434343',
    },
  },
  create: {
    optionsText: {
      color: '#434343',
    },
  },
  total: {
    text: 'rgba(0, 0, 0, 0.53)',
    price: 'linear-gradient(180deg, #8569F9 0%, #B7A7FC 100%)',
    btn: {
      color: '#15172E',
      background: '#DDE7FF',
      border: '#DDE7FF',
    },
  },
  market: {
    flexLabel: '#434343',
    content: {
      text: '#434343',
    },
  },
};

export default lightTheme;
