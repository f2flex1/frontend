import i18n from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import XHR from 'i18next-xhr-backend';
import moment from 'moment-timezone';
import { initReactI18next } from 'react-i18next';

export const LANGUAGES = new Map([
  ['en', 'English'],
  ['ru', 'Русский'],
  ['es', 'Español'],
  ['it', 'Italiano'],
  ['ja', '日本'],
  ['kk', 'Қазақ'],
  ['ko', '한국인'],
  ['pt', 'Português'],
  ['th', 'ไทย'],
  ['tr', 'Türk'],
  ['vi', 'Tiếng Việt'],
  ['zh', '中文'],
  ['fr', 'Français'],
]);

i18n
  .use(XHR)
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    debug: false, // process.env.NODE_ENV !== 'production',
    fallbackLng: false,
    supportedLngs: Array.from(LANGUAGES.keys()),
    ns: 'translation',
    defaultNS: 'translation',
    // XHR Backend
    backend: {
      loadPath: '/locales/{{lng}}/{{ns}}.json',
    },

    // Interpolation
    interpolation: {
      // not needed for react
      escapeValue: false,
    },
    detection: {
      order: ['querystring', 'cookie', 'localStorage', 'sessionStorage', 'navigator', 'htmlTag'],
    },
  });

const selfMoment = (date?: string | number) => {
  // const timezoneOffset = new Date().getTimezoneOffset();

  i18n.on('languageChanged', (lng: string) => {
    return moment(date).lang(lng);
  });

  return moment(date).lang(i18n.language);
};

export { selfMoment };

export default i18n;
