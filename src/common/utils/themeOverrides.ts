import { Overrides } from '@material-ui/core/styles/overrides';

const themeOverrides: Overrides = {
  // Name of class
  MuiCard: {
    // Name of the rule
    root: {
      // Some CSS
    },
  },
};

export default themeOverrides;
