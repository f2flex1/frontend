import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { Route, Redirect, RouteProps } from 'react-router-dom';

import Layout from '@/components/Layout';
import { AppStateType } from '@/store';
import { getMessages } from '@/store/messages/actions';
import { MessagesState } from '@/store/messages/reducer';
import { fetchUserData } from '@/store/user/actions';
import { UserReducerState } from '@/store/user/reducers';
import { getCurrenciesRates } from '@/store/wallet/actions';

interface PrivateRoutesProps extends RouteProps {
  user: UserReducerState;
  component: any;
  loggedIn: boolean;
  messages: MessagesState;
  getMessages: () => void;
  fetchUserData: () => void;
  getCurrenciesRates: () => void;
  title?: string;
  rest?: Object;
}

const PrivateRoutes: React.FC<PrivateRoutesProps> = (props: PrivateRoutesProps) => {
  const {
    user,
    component: Component,
    loggedIn,
    messages,
    getMessages,
    fetchUserData,
    getCurrenciesRates,
    title,
    ...rest
  } = props;

  const { userData } = user;
  const { t, i18n } = useTranslation();
  const [changeLanguageInProgress, setShangeLanguageInProgress] = useState(false);

  useEffect(() => {
    if (loggedIn) {
      if (!messages.messages.loaded) {
        getMessages();
        fetchUserData();
        getCurrenciesRates();
      }
    }
  }, [loggedIn, messages]);

  useEffect(() => {
    if (userData?.language) {
      if (i18n.language !== userData?.language) {
        if (!changeLanguageInProgress) {
          setShangeLanguageInProgress(true);
          i18n.changeLanguage(userData?.language, () => setShangeLanguageInProgress(false));
        }
      }
    }
  }, [userData]);

  return (
    <Route
      {...rest}
      render={props => {
        document.title = `${t(title || '')} - FIN2FLEX`;
        return loggedIn ? (
          <Layout>
            <Component {...props} />
          </Layout>
        ) : (
          <Redirect to="/login" />
        );
      }}
    />
  );
};

const mapStateToProps = (state: AppStateType) => {
  const { user, messages } = state;
  return {
    messages,
    user,
    loggedIn: user.loggedIn,
  };
};

export default connect(mapStateToProps, {
  getMessages,
  fetchUserData,
  getCurrenciesRates,
})(PrivateRoutes);
